const path = require('path');
const gulp = require('gulp');
const terser = require('gulp-terser');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');

const dirpath = path.resolve(__dirname) + "/";

let jsfiles = [
  dirpath + 'src/js/3d/polyfill/polyfill.js',
  dirpath + 'src/js/3d/polyfill/typedarray.js',
  dirpath + 'src/js/3d/polyfill/Blob.js',
  dirpath + 'src/js/3d/polyfill/es6-promise.auto.min.js',
  dirpath + 'src/js/3d/polyfill/DragDropTouch.js',

  dirpath + 'src/js/3d/three/three.js',

  dirpath + 'src/js/3d/three/loaders/GLTFLoader.js',
  dirpath + 'src/js/3d/three/controls/OrbitControls.js',
  dirpath + 'src/js/3d/three/geometries/ConvexGeometry.js',
  dirpath + 'src/js/3d/three/util/QuickHull.js',

  dirpath + 'src/js/3d/utils/Tween.js',
  dirpath + 'src/js/3d/utils/utils.js',
  dirpath + 'src/js/3d/item.js',
  dirpath + 'src/js/3d/layout.js',
  dirpath + 'src/js/3d/ModelLoader.js',
  dirpath + 'src/js/3d/DataProvider.js',
  dirpath + 'src/js/3d/DragControls.js',
  dirpath + 'src/js/3d/ViewerControls.js',
  dirpath + 'src/js/3d/CameraPlayer.js',
  dirpath + 'src/js/3d/viewer.js',
  dirpath + 'src/js/3d/FloorPlan.js',
];

const outdir = dirpath + 'dist/js/';
const outfilename = "3d.js";


function devBuild() {
  return gulp.src(jsfiles)
    .pipe(sourcemaps.init())
    .pipe(concat(outfilename))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(outdir));
}


// public tasks
function build() {
  return gulp.src(jsfiles)
    .pipe(sourcemaps.init())
    .pipe(concat(outfilename))
    .pipe(terser())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(outdir));
}

function watch() {
  return gulp.watch(jsfiles, devBuild);
}


exports.build = build;
exports.watch = watch;
exports.default = build;

React App for SPZ
=================


### Check Npm version
```
npm -v
6.10.2
```

### Update Npm version
```
sudo npm i npm 
```

## Deployment
### Clone & Install
```
git clone https://it_spazious@bitbucket.org/it_spazious/react_app.git

cd react_app

npm install
```

### Run
```
npm start
```

### Visit:
http://localhost:8080/#/property/2/capacity-chart

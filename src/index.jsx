import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import HotelPage from './js/components/HotelPage/HotelPage';
import EventList from './js/components/EventList/EventList';
import CreateEvent from './js/components/EventList/CreateEvent';
import HotelList from './js/components/HotelAdmin/HotelList';
import Notifications from './js/components/Notifications/Notifications';
import HotelBrandList from './js/components/HotelBrandPage/HotelBrandList';
import CreateHotelBrand from './js/components/HotelBrandPage/CreateHotelBrand';
import HotelBrandProfile from './js/components/HotelBrandPage/HotelBrandProfile';
import UsersList from './js/components/Users/UsersList';
import UserProfile from './js/components/Users/UserProfile';
import CreateHotelProfile from './js/components/HotelAdmin/CreateHotelProfile';
import RFPManagement from './js/components/EventList/RFPManagement';
import EventHistoryPopup from './js/components/EventList/EventHistoryPopup';
import LoginPage from './js/components/UserManagement/LoginPage';
import ResetPassword from './js/components/UserManagement/ResetPassword';
import ResetPasswordSent from './js/components/UserManagement/ResetPasswordSent';
import LoginWithNewPassword from './js/components/UserManagement/LoginWithNewPassword';
import customHistory from './js/components/customHistory';

      // <Route exact path="/" render={() => <LoginPage />} />
      // <Route exact path="/property/:property(login)?" render={() => <LoginPage />} />
const Root = () => (
  <Router history={customHistory}>
    <Switch>
      <Route exact path="/property/:property(event)?/list" render={() => <EventList />} />
      <Route exact path="/property/:property(event)?/create" render={() => <CreateEvent />} />
      <Route exact path="/property/:property(notifications)?" render={() => <Notifications />} />
      <Route exact path="/property/:property(brand)?/list" render={() => <HotelBrandList />} />
      <Route exact path="/property/:property(brand)?/create" render={() => <CreateHotelBrand />} />
      <Route path="/property/brand:property(profile)?" render={() => <HotelBrandProfile />} />
      <Route path="/property/event:property(view)?" render={() => <RFPManagement />} />
      <Route exact path="/property/:property(users)?/list" render={() => <UsersList />} />
      <Route path="/property/users:property(profile)?" render={() => <UserProfile />} />
      <Route exact path="/property/:property(hotel)?/list" render={() => <HotelList />} />
      <Route exact path="/property/:property(hotel)?/create" render={() => <CreateHotelProfile />} />
      <Route path="/property/hotel:property(view)?" render={() => <CreateHotelProfile />} />
      <Route exact path="/property/:property(popup)?" render={() => <EventHistoryPopup />} />
      <Route exact path="/property/:property(reset_password)?" render={() => <ResetPassword />} />
      <Route exact path="/property/:property(reset_password_sent)?" render={() => <ResetPasswordSent />} />
      <Route exact path="/:property(password/reset)?" render={() => <LoginWithNewPassword />} />
      <Route path="/property/:property([0-9]{1,10})?" render={() => <HotelPage />} />
    </Switch>
  </Router>
);
ReactDOM.render(<Root />, document.getElementById('root'));

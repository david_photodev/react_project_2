import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthStore from '../stores/AuthStore';
export default {

	getNotificationInfos (userId, callback, nextUrl) {
		const url = nextUrl || `${AuthStore.getApiHost()}/api/users/${userId}/notifications?status=1`
		AppDispatcher.handleServerAction({
			actionType: 'CHECK_GET_NOTIFICATION_API_CALLED',
			isApiCalled: true,
		});	
		AuthStore.getHeaders(null, (headers) => {
      		request.get(url)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const notificationList = JSON.parse(res.text);
	        AppDispatcher.handleServerAction({
				actionType: 'GET_NOTIFICATION_LIST',
				notificationList,
			});
			if (notificationList.next) {
              this.getNotificationInfos(userId, callback, notificationList.next);
            }
            if (!notificationList.next) {
              if (callback) {
                callback(notificationList);
              }
            }
	      } else {
	        console.log('err', err);
	      }
	    });
	   });
	},

    clearStoreInfo() {
        AppDispatcher.handleViewAction({
            actionType: 'CLEAR_STORE_NOTIFICATIONS_INFO',
        });
    },

	searchBy(name) {
		AppDispatcher.handleViewAction({
			actionType: 'NOTIFICATION_SEARCH',
			name,
		});
	},

	notificationApiSwitch (isApiCalled) {
		AppDispatcher.handleServerAction({
			actionType: 'CHECK_GET_NOTIFICATION_API_CALLED',
			isApiCalled: isApiCalled,
		});
	},

	updateNotificationStatus (userId, notificationId, callback) {
		let updateInfo = {};
		updateInfo['status'] = 2;
		AuthStore.getHeaders(null, (headers) => {
				headers['Content-Type'] = 'application/json';
				request.patch(`${AuthStore.getApiHost()}/api/users/${userId}/notifications/${notificationId}`)
				.send(JSON.stringify(updateInfo))
				.set(headers)
				.end((err, res) => {
					if (!err) {
						const updatedResult = JSON.parse(res.text);
						callback(updatedResult);
					} else {
						console.log('err', err);
					}
				});
			});
	}
};
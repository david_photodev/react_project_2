import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import AuthStore from '../stores/AuthStore';

export default {
  // getData() {
  //   setTimeout(() => {
  //     AppDispatcher.handleViewAction({
  //       actionType: 'GET_DATA',
  //     });
  //   }, 1);
  // },
  getFurnitures(propertyId) {
    console.log('propertyId', propertyId);
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.get(`${AuthStore.getApiHost()}/api/hotel/${propertyId}/furnitures`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'GET_FURNITURE',
              furniture: response.results,
            });
          } else {
            console.log('err', err);
          }
        });
    });
  },
  getFurnitureCategories(propertyId) {
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.get(`${AuthStore.getApiHost()}/api/hotel/${propertyId}/furnitures/categories`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'GET_FURNITURE_CATEGORIES',
              categories: response.results,
            });
          } else {
            console.log('err', err);
          }
        });
    });
  },
  filterData(id, name) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.FILTER,
      id,
      name,
    });
  },
  // search(filterBy, name) {
  //   AppDispatcher.handleViewAction({
  //     actionType: AppConstants.SEARCH,
  //     filterBy,
  //     name,
  //   });
  // },
};

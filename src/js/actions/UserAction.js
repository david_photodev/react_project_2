import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthStore from '../stores/AuthStore';

const allUsersList = [];
export default {
  getUserList(callback, nextAPI) {
    AuthStore.getHeaders(null, (headers) => {
      const apiRequest = nextAPI || `${AuthStore.getApiHost()}/api/users/`;
      request.get(apiRequest)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const userList = JSON.parse(res.text);
            const activeUsers = userList.results.filter(user => user.profile.status.id !== 3);
            const userHotelListRes = [];
            activeUsers.map((user) => {
              window[`user${user.id}`] = new Promise((resolve, reject) => {
                request.get(user.hotels)
                  .set(headers)
                  .end((error, response) => {
                    if (!error) {
                      const userHotel = JSON.parse(response.text);
                      userHotel['user'] = user.id;
                      resolve(userHotel);
                    } else {
                      console.log('err', err);
                      reject(response);
                    }
                  });
              });
              userHotelListRes.push(eval(`user${user.id}`));
            });
            Promise.all(userHotelListRes).then((response) => {
              const updatedUserList = activeUsers.map((eachUser) => {
                const updatedUserHotel = response.filter(hotelRes => hotelRes.user === eachUser.id)[0];
                eachUser.hotel = updatedUserHotel.results;
                return eachUser;
              });
              AppDispatcher.handleServerAction({
                actionType: 'SINGLE_USER_LIST',
                userList,
              });
              if (userList.next) {
                this.getUserList(callback, userList.next);
              }
              if (!userList.next) {
                if (callback) {
                  callback(true);
                }
              }
            });
          } else {
            console.log('err', err);
          }
        });
    });
  },

  sortBy(key, asc) {
    AppDispatcher.handleViewAction({
      actionType: 'USER_LIST_SORT',
      key,
      asc,
    });
  },

  searchBy(name) {
    AppDispatcher.handleViewAction({
      actionType: 'USER_LIST_SEARCH',
      name,
    });
  },

  deleteUser(user) {
    let params = {
      profile: {
        status: 3,
      },
    };
    params = JSON.stringify(params);
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/users/${user.id}/`)
        .send(params)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const statusRes = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'DELETE_USER',
              user,
            });
          } else {
            console.log('err', err);
          }
        });
    });
  },
  selectedUser(user, callback) {
    const chainId = user.profile.hotel_chain.url;
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${chainId}/hotels`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            user.availableHotels = JSON.parse(res.text).results;
            AppDispatcher.handleViewAction({
              actionType: 'SELECTED_USER',
              user,
            });
            if (callback) {
              callback(user);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },

  updateHotelProfileUser(hotelId, availableUsers, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.patch(`${AuthStore.getApiHost()}/api/hotel/${hotelId}`)
        .send({ users: availableUsers })
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const statusRes = JSON.parse(res.text);
            if (callback) {
              callback(true);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },

  // update the new hotel user
  updateNewHotelUser(hotelId, user, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/hotel/${hotelId}`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const selectedHotelInfo = JSON.parse(res.text);
            const isAvailableUser = selectedHotelInfo.hasOwnProperty('users') && selectedHotelInfo.users.length >= 1;
            if (isAvailableUser) {
              const availableUsers = selectedHotelInfo.users.map(hotelUser => hotelUser.id);
              if (!availableUsers.includes(user.id)) {
                availableUsers.push(user.id);
                this.updateHotelProfileUser(hotelId, availableUsers, callback);
              }
            } else {
              const selectedUser = [];
              selectedUser.push(user.id);
              this.updateHotelProfileUser(hotelId, selectedUser, callback);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },

  selectedHotel(hotelId, user, callback) {
    if (user.hotel && user.hotel.length >= 1) {
      AuthStore.getHeaders(null, (headers) => {
        request.get(`${AuthStore.getApiHost()}/api/hotel/${user.hotel[0].id}`)
          .set(headers)
          .end((err, res) => {
            if (!err) {
              const alreadySelectedHotel = JSON.parse(res.text);
              const isAvailableUser = alreadySelectedHotel.hasOwnProperty('users') && alreadySelectedHotel.users.length >= 1;
              if (isAvailableUser) {
                const availableUsers = alreadySelectedHotel.users.map(hotelUser => hotelUser.id);
                const index = availableUsers.indexOf(user.id);
                availableUsers.splice(index, 1);
                this.updateHotelProfileUser(user.hotel[0].id, availableUsers, (res) => {
                  this.updateNewHotelUser(hotelId, user, (response) => {
                    if (response) {
                      this.readHotel(hotelId, (updatedHotel) => {
                        user.hotel = [updatedHotel];
                        AppDispatcher.handleViewAction({
                          actionType: 'UPDATED_HOTEL_USER',
                          user,
                        });
                        callback(true);
                      });
                    }
                  });
                });
              } else {
                const selectedUser = [];
                selectedUser.push(user.id);
                this.updateHotelProfileUser(hotelId, selectedUser, (response) => {
                  if (response) {
                    this.readHotel(hotelId, (updatedHotel) => {
                      user.hotel = [updatedHotel];
                      AppDispatcher.handleViewAction({
                        actionType: 'UPDATED_HOTEL_USER',
                        user,
                      });
                      callback(true);
                    });
                  }
                });
              }
            } else {
              console.log('err', err);
            }
          });
      });
    } else {
      this.updateNewHotelUser(hotelId, user, (res) => {
        if (res) {
          this.readHotel(hotelId, (updatedHotel) => {
            user.hotel = [updatedHotel];
            AppDispatcher.handleViewAction({
              actionType: 'UPDATED_HOTEL_USER',
              user,
            });
            callback(true);
          });
        }
      });
    }
  },

  readHotel(hotelId, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/hotel/${hotelId}`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const selectedHotel = JSON.parse(res.text);
            console.log('selectedHotel', selectedHotel);
            if (callback) {
              callback(selectedHotel);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },

  updateUserProfile(userId, updateInfo, callback) {
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/users/${userId}/`)
        .send(JSON.stringify(updateInfo))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const updatedResult = JSON.parse(res.text);
            callback(updatedResult);
          } else {
            callback(false);
            console.log('err', err);
          }
        });
    });
  },

  readUser(userId, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/users/${userId}/`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const selectedUser = JSON.parse(res.text);
            request.get(selectedUser.hotels)
              .set(headers)
              .end((error, response) => {
                if (!error) {
                  const userHotel = JSON.parse(response.text);
                  selectedUser['hotel'] = userHotel.results;
                  this.selectedUser(selectedUser, (userRes) => {
                    if (userRes) {
                      callback(userRes);
                    }
                  });
                } else {
                  console.log('err', err);
                }
              });
          }
        });
    });
  },
};

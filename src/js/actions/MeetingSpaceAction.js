import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import MeetingSpaceConstants from '../constants/MeetingSpaceConstants';
import RFPManagementAction from './RFPManagementAction';
import AuthStore from '../stores/AuthStore';

let supportedFileTypes = {};
let names = [];
export default {
  getMeetingRoomData(meetingRoomDatas, capacity) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_MEETING_ROOM_INFO',
      meetingRoomDatas,
      capacity,
    });
  },
  updateRoomSetup(id, setupId, data) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_ROOM_CONFIG',
      id,
      setupId,
      data,
    });
  },
  updateLayout(room, setup, type = 0) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_LAYOUT',
      room,
      setup,
      type,
    });
  },
  removeLayout(room, setup) {
    AppDispatcher.handleViewAction({
      actionType: 'REMOVE_LAYOUT',
      room,
      setup,
    });
  },
  setViewer(viewer) {
    AppDispatcher.handleViewAction({
      actionType: 'SET_VIEWER',
      viewer,
    });
  },
  duplicateLayout(room, setup) {
    AppDispatcher.handleViewAction({
      actionType: 'DUPLICATE_LAYOUT',
      room,
      setup,
    });
  },
  updateAddons(room, setup, type) {
    AppDispatcher.handleViewAction({
      actionType: 'ADDON_UPDATE',
      room,
      setup,
      type,
    });
  },
  updateFoodAndServices(params) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_FOOD_SERVICES',
      params,
    });
  },
  handleNewPeriod() {
    AppDispatcher.handleViewAction({
      actionType: 'ADD_NEW_PERIOD',
    });
  },
  deletePeriod(id) {
    AppDispatcher.handleViewAction({
      actionType: 'DELETE_PERIOD',
      id,
    });
  },
  editPeriod(id, item, type) {
    AppDispatcher.handleViewAction({
      actionType: 'EDIT_PERIOD',
      id,
      item,
      type,
    });
  },
  savePeriod(guestInfo) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_PERIOD',
      guestInfo,
    });
  },
  editContact(item, type) {
    AppDispatcher.handleViewAction({
      actionType: 'EDIT_CONTACT',
      item,
      type,
    });
  },
  notificationMsg(msg, val, type) {
    AppDispatcher.handleViewAction({
      actionType: 'ADD_NOTIFICATION',
      msg,
      val,
      type,
    });
  },
  updateLayoutInputs(params) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_LAYOUT_INPUTS',
      params,
    });
  },
  updateActivityName(params) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_ACTIVITY_NAME',
      params,
    });
  },
  submitLayout(layout, roomId) {
    AppDispatcher.handleViewAction({
      actionType: 'SUBMIT_LAYOUT',
      layout,
      roomId,
    });
    RFPManagementAction.active3DEdit(true);
  },
  showSetupView(item) {
    AppDispatcher.handleViewAction({
      actionType: 'SHOW_SETUP_VIEW',
      item,
    });
  },
  createOwnSetup() {
    AppDispatcher.handleViewAction({
      actionType: 'CREATE_OWN_SETUP',
    });
  },
  updateEventDate(date) {
    AppDispatcher.handleViewAction({
      actionType: 'ADD_EVENTDATE',
      date,
    });
  },
  saveContact(contactDetails) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_CONTACT',
      contactDetails,
    });
  },
  saveActivityInfo(ActivityInfos) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_ACTIVITY',
      ActivityInfos,
    });
  },
  saveEventDetails(eventInfos) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_EVENT',
      eventInfos,
    });
  },
  handleRoomSetupList(room, setup_id, list) {
    AppDispatcher.handleViewAction({
      actionType: 'ROOM_SETUP',
      room,
      setup_id,
      list,
    });
  },
  filterSelectedRoom(typeOfSetup, typeOfCapacity) {
    AppDispatcher.handleViewAction({
      actionType: 'FILTER_SETUP',
      typeOfSetup,
      typeOfCapacity,
    });
  },
  selectedLayout(params) {
    AppDispatcher.handleViewAction({
      actionType: 'SELECTED_LAYOUT',
      params,
    });
  },
  savedFurnitureData(data) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVED_FURNITURE_DATA',
      data,
    });
  },
  saveFloorPlan(fp) {
    setTimeout(() => {
      AppDispatcher.handleViewAction({
        actionType: 'SAVE_FLOOR_PLAN',
        fp,
      });
    }, 1);
  },
  sortBy(key, order, setup) {
    AppDispatcher.handleViewAction({
      actionType: 'SORT_MEETING_ROOM',
      key,
      order,
      setup,
    });
  },
  saveDroppedItems(droppedItems) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_DROPPED_ITEMS',
      droppedItems,
    });
  },
  createEvent(params, callback) {
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.post(AuthStore.getApiHost() + '/api/events')
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: MeetingSpaceConstants.CREATE_EVENT,
              data: response,
            });
            callback(response);
          } else {
            console.log('err', err);
            callback(false);
          }
        });
    });

  },
  getHotelData(property, setupImages, callback){
    const data = {};
    const available_setup = [];
    const api_host = AuthStore.getApiHost();
    AuthStore.getHeaders(property, (headers) => {
      request.get(api_host+`/api/hotel/${property}`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const responses = JSON.parse(res.text);
            data.hotelId = responses.id;
            data.hotel_name = responses.name;
            data.address = responses.address;
            data.capacity = responses.capacity;
            data.virtual_tour_url = responses.virtual_tour_url;
            data.ga_tracking = responses.ga_tracking;
            data.description = responses.description;
            data.email_address = responses.email_address;
            data.phone = responses.phone;
            data.location = responses.location;
            data.city = responses.city;
            data.country = responses.country;
            data.hotel_chain_name = responses.hotel_chain.name;
            data.hotel_manager_email = [responses.email_address];
            let setup_types_url = '';
            AuthStore.getHeaders(property, (headers) => {
              request.get(responses.hotel_chain.url)
                .set(headers)
                .end((err, res) => {
                  if (!err) {
                    const hotel_chain_response = JSON.parse(res.text);
                    data.main_typography = hotel_chain_response.main_typography;
                    data.primary_color = hotel_chain_response.primary_color;
                    data.secondary_color = hotel_chain_response.secondary_color;
                    data.hotel_logo = hotel_chain_response.logo_positive;
                    data.logo_negative = hotel_chain_response.logo_negative;
                    data.privacy_policy = hotel_chain_response.privacy_policy;
                    data.terms_and_condtions = hotel_chain_response.privacy_policy;
                    data.sender_email = hotel_chain_response.sender_email;
                    setup_types_url = hotel_chain_response.setup_types;
                    AppDispatcher.handleServerAction({
                      actionType: 'SAVE_HOTEL_DATA',
                      data,
                    });
                    AuthStore.getHeaders(property, (headers) => {
                      request.get(setup_types_url)
                        .set(headers)
                        .end((err, res) => {
                          if (!err) {
                            const hotel_setup_types = JSON.parse(res.text);
                            if (hotel_setup_types.results.length >= 1) {
                              hotel_setup_types.results.map((result)=>{
                                let setupNameUpdated = result.name.toLowerCase();
                                setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
                                available_setup.push({
                                  'id':result.id,
                                  'shape':result.name,
                                  'type':setupNameUpdated,
                                  'img':setupNameUpdated in setupImages ? setupImages[setupNameUpdated] : setupImages['theatre'],
                                });
                              });
                              // To place the custom setup in the last
                              let customIndex;
                              const customSetup = available_setup.filter((setup, i) => {
                                if (setup.type.toLowerCase() === 'custom') {
                                  customIndex = i;
                                  return true;
                                }
                              });
                              available_setup.splice(customIndex, 1);
                              available_setup.splice(available_setup.length, 1, customSetup[0]);
                            }
                            AppDispatcher.handleServerAction({
                              actionType: 'SAVE_AVAILABLE_SETUP',
                              available_setup,
                            });
                            if(callback) {
                              callback(data);
                            }
                          }
                          else {
                            console.log('err-hotel_setup_types', err);
                          }
                        })
                    });
                  }
                  else {
                    console.log('err-hotel_chain', err);
                  }
                })
            });
          }
          else {
            console.log('err-hotel', err);
          }
        })
    });
  },

  getFloorPlanDetails(property, callback) {
    const data = {};
    const api_host = AuthStore.getApiHost();
    AuthStore.getHeaders(property, (headers) => {
      request.get(api_host+`/api/hotel/${property}/floorplan`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            data.hotelId = response.id;

            let floorplan = null;
            if (response.assets) {
              for (let i=0, l=response.assets.length; i<l; i++) {
                let asset = response.assets[i];
                if (asset.type.id === 1) {
                  floorplan = {
                    id: "fp-"+asset.id,
                    path: asset.file
                  };
                  break;
                }
              } 
            }
            data.floorplan = floorplan;
            data.rooms = [];

            if (response.rooms) {
              for (let i=0, l=response.rooms.length; i<l; i++) {
                let room = response.rooms[i];
                for (let j=0; j<room.assets.length; j++) {
                  let asset = room.assets[j];
                  if (asset.type.id === 1) {
                    data.rooms.push({
                      id: room.meeting_room.id,
                      model: {
                        id: "roomfp-"+asset.id,
                        path: asset.file
                      }
                    });
                    // floor plan found for the room, no need to check other assets
                    break;
                  }
                }
              }
            }

            if(callback) {
              callback(null, data);
            }
          }
          else {
            console.error('Could not fetch floorplan details', err);
            if(callback) {
              callback(err);
            }
          }
        })
    });
  },

  getMeetingRoomDetails(setupImages, property, capacity, callback, nextUrl) {
    const MeetingRoomDetails = new Promise((resolve, reject) => {
      AuthStore.getHeaders(property, (headers) => {
      const url = nextUrl || `${AuthStore.getApiHost()}/api/hotel/${property}/meeting_rooms?status=1`;
      request.get(url)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            resolve(response);
          } else {
            console.log('err', err);
            reject(res);
          }
        });
      });
    });
    Promise.all([MeetingRoomDetails]).then((response) => {
      response[0].results.map((room) => {
        const meeting_room = room;
        const _360_image = [];
        const gltf_3D_model = [];
        const gltf_floorplan_model = [];
        const setup = [];
        const scenes = [];
        const room_services = [];
        const model3d = {};
        const model2d = {};
        let virtual_tour_url = null;
        const image = room.assets.filter(eachAsset => eachAsset.type.id === 3).sort(function(a, b){            
          if(a.order < b.order) return -1;
          if(a.order > b.order) return 1;
          return 0;
        });
        room.assets.map((result) => {
          // if (result.file) {
          //   result.file = result.file;
          // }
          if (result.type.id === 3 && result.order === 1) {
            meeting_room.room_image = result.file;
          }
          if (result.type.id === 13) {
            image.push(result);
          }
          if (result.type.id === 4) {
            if (result.order === 1) {
              virtual_tour_url = result.url;
            }
          }
          if (result.type.id === 5) {
            _360_image.push(result);
          }
          if (result.type.id === 7) {
            gltf_3D_model.push(result);
            model3d.id = result.id;
            model3d.fileType = 'gltf';
            model3d.path = result.file;
          }
          if (result.type.id === 11) {
            gltf_floorplan_model.push(result);
            model2d.id = result.id;
            model2d.fileType = 'gltf';
            model2d.path = result.file;
          }
        });
        if (Object.keys(model2d).length >= 1  && Object.keys(model3d).length >= 1) {
          scenes.push({
            model3d,
            model2d,
          });
        }
        meeting_room.image = image;
        meeting_room.scenes = scenes;
        meeting_room._360_image = _360_image;
        meeting_room.gltf_3D_model = gltf_3D_model;
        meeting_room.gltf_floorplan_model = gltf_floorplan_model;
        meeting_room.room_name = meeting_room.name;
        meeting_room.room_space = meeting_room.surface_meters+'m²';
        meeting_room.room_capacity = 'Max.'+Math.max.apply(Math, meeting_room.room_setups.map(function(o) { return o.capacity; }))+' pax.';
        meeting_room.available_model = meeting_room.customizable;
        meeting_room.virtUrl = virtual_tour_url;
        let customSetup;
        meeting_room.room_setups.map((room_setup)=>{
          // if (room_setup.type.name.toLowerCase() !== 'custom'){
          const updateFurniture = () => {
            const furnitures = {};
            if (room_setup.furnitures.length >= 1) {
              room_setup.furnitures.map((furniture => {
                if (furniture.item_type === 'ChairItem') {
                  furnitures.chair = {
                    id: furniture.id,
                    type: 'gltf',
                    model_path: furniture.model_3d,
                  };
                }
                if (furniture.item_type === 'TableItem') {
                  furnitures.table = {
                    id: furniture.id,
                    type: 'gltf',
                    model_path: furniture.model_3d,
                  };
                }
              }));
              return furnitures;
            }
            return furnitures;
          };

          let setupNameUpdated = room_setup.type.name.toLowerCase();
          setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
          const img = setupImages.hasOwnProperty(setupNameUpdated) ? setupImages[setupNameUpdated] : setupImages['theatre'];
          setup.push({
            id: room_setup.type.id,
            shape: room_setup.type.name,
            type: setupNameUpdated,
            count: room_setup.capacity,
            img,
            furnitures: updateFurniture(),
            config: room_setup.config,
          });
          // }
          // if (room_setup.type.name.toLowerCase() === 'custom'){
          //   customSetup = room_setup;
          // }
        });
        // setup.push({
        //   id: customSetup.type.id,
        //   shape: customSetup.type.name.charAt(0).toUpperCase() + customSetup.type.name.slice(1),
        //   type: customSetup.type.name,
        //   count: customSetup.capacity,
        // });
        meeting_room.services.map((service)=>{
          room_services.push(service.type)
        });
        meeting_room.setup = setup;
        meeting_room.room_services = room_services;
      });
      this.getMeetingRoomData(response[0].results, capacity);
      if (response[0].next) {
        this.getMeetingRoomDetails(setupImages, property, capacity, callback, response[0].next)
      }
      if (!response[0].next) {
        if (callback) {
          callback(true);
        }
      }
    });
  },

  ActivityInfo(results) {
    const tempSelectedRooms = {} ;
    if (results) {
      results.map((result, i) => {
        const room_id = result.meeting_room.id;
        if ( ! tempSelectedRooms[room_id]) {
          tempSelectedRooms[room_id] = {
            'setup': [],
            'allLayouts': [],
            'id'  : room_id,
            'available_model':result.model_3d.available_model || false,
            'room_name':result.meeting_room.name,
            'scenes': [],
          }
        }

        result.activity_name = result.name;
        result.chairs_count = result.attendees;
        result.count = result.attendees;
        result.attendees_count = result.attendees;
        const startDate = moment(result.start, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY HH:mm').split(' ');
        result.event_date_starts = startDate[0];
        result.event_time_starts = startDate[1];
        const endDate = moment(result.end, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY HH:mm').split(' ');
        result.event_date_ends = endDate[0];
        result.event_time_ends = endDate[1];
        result.img = result.model_3d.img;
        result.model_3d = result.model_3d;
        result.type = result.model_3d.layout;
        result.droppedItems = result.model_3d.droppedItems || [];
        result.shape = result.type.charAt(0).toUpperCase() + result.type.slice(1);
        result.setup_id = i+1;
        result.services_url = result.services;
        result.services = [];
        result.services_ids = [];
        result.apId = result.id;
        const updateFurniture = (item) => {
          const furnitures = {};
          if (item.length >= 1) {
            item.map((furniture => {
              if (furniture.item_type === 'ChairItem') {
                furnitures.chair = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
              if (furniture.item_type === 'TableItem') {
                furnitures.table = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
            }));
            return furnitures;
          }
          return furnitures;
        };
        AuthStore.getHeaders(null, (headers) => {
          request.get(result.meeting_room.url)
            .set(headers)
            .end((err, res) => {
              if (!err) {
                const meetingRoomInfo = JSON.parse(res.text);
                meetingRoomInfo['setUpId'] = result.id;
                if (meetingRoomInfo.room_setups.length) {
                  const filteredSetup = meetingRoomInfo.room_setups.filter(roomSetup =>
                    roomSetup.type.name.toLowerCase() === result.model_3d.layout.toLowerCase());
                  result.config = filteredSetup[0].config;
                  result.config = result.model_3d.config || filteredSetup[0].config;
                  result.furnitures = updateFurniture(filteredSetup[0].furnitures);
                }
                AppDispatcher.handleViewAction({
                  actionType: 'SAVE_MEETING_ROOM_INFO',
                  meetingRoomInfo,
                });
              }  else {
                console.log('err-saveMeetingRoomInfo', err);
              }
            });
        });

        AuthStore.getHeaders(null, (headers) => {
          request.get(result.services_url)
            .set(headers)
            .end((err, res) => {
              if (!err) {
                const response = JSON.parse(res.text);
                response.results.map((temp)=>{
                  result.services.push(temp.type);
                  result.services_ids.push(temp.type.id);
                });
              }  else {
                console.log('err-getServices', err);
              }
            });
        });
        const sceneValue = {
          model3d: result.model_3d.model3d,
          model2d: result.model_3d.model2d,
        };
        tempSelectedRooms[room_id]['scenes'].push(sceneValue);
        tempSelectedRooms[room_id]['setup'].push(result);
        tempSelectedRooms[room_id]['allLayouts'].push(result);
      });
    } else {
      console.log('No Info Available');
    }

    const selectedRooms = [];
    for (const k in tempSelectedRooms) {
      if (tempSelectedRooms.hasOwnProperty(k)) {
        selectedRooms.push(tempSelectedRooms[k]);
      }
    }
    this.saveActivityInfo(selectedRooms);
  },

  getActivityInformations(eventId) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(AuthStore.getApiHost() + '/api/events/' + eventId + '/activity_programs')
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            RFPManagementAction.getActivityPrograms(response.results);
            this.ActivityInfo(response.results);
          } else {
            console.log('err-getActivityInformations', err);
          }
        });
    });

  },

  getAttachmentFile(eventId) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(AuthStore.getApiHost() + '/api/events/' + eventId + '/attachments')
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const response = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: MeetingSpaceConstants.ATTACHED_FILE,
              data: response,
            });
          } else {
            console.log('err-getAttachmentFile', err);
          }
        });
    });
  },

  getEventID(uuId, callback) {
     AuthStore.getHeaders(null, (headers) => {
       request.get(AuthStore.getApiHost() + '/api/events/' + uuId + '')
           .set(headers)
           .end((err, res) => {
             if (!err) {
               const response = JSON.parse(res.text);
               AppDispatcher.handleServerAction({
                 actionType: MeetingSpaceConstants.CREATE_EVENT,
                 data: response,
               });
               this.saveEventDetails(response);
               callback(response.id);
             } else {
               console.log('err-getEventID', err);
             }
           });
     });
  },

  getContactInformations(eventId) {
    const guestResponse = new Promise((resolve, reject) => {
      AuthStore.getHeaders(null, (headers) => {
        request.get(AuthStore.getApiHost()+`/api/guestroom_request?event_id=${eventId}`)
          .set(headers)
          .end((err, res) => {
            if (!err) {
              const response = JSON.parse(res.text);
              resolve(response);
            } else {
              console.log('err-contactDetailsResponse', err);
              reject(res);
            }
          });
      });

    });
    const contactDetailsResponse = new Promise((resolve, reject) => {
      AuthStore.getHeaders(null, (headers) => {
        request.get(AuthStore.getApiHost()+'/api/events/' + eventId + '/contact_informations')
          .set(headers)
          .end((err, res) => {
            if (!err) {
              const response = JSON.parse(res.text);
              resolve(response);
            } else {
              console.log('err-contactDetailsResponse', err);
              reject(res);
            }
          });
      });

    });

    Promise.all([guestResponse, contactDetailsResponse]).then((response) => {
      response[1].results.map((result) => {
        result.name_of_event = result.event.name;
        result.address_line_2 = result.address_2;
        result.email = result.mail;
        delete result.mail;
        delete result.address_2;
        this.saveContact(result);
      });
      this.savePeriod(response[0].results);
    });
  },

  activityProgram(events, hotelInfo, callback) {
    const eventId = events.id;
    const {
      selectedRooms, scenes, viewer, contactDetails, guestInfo,
    } = hotelInfo;
    const roomNames = [];
    selectedRooms.map((room) => {
      room.setup.map((setup) => {
        const requestData = {};
        const model3d = {};
        model3d['data'] = {};
        const storageName = `SceneData ${room.id}${setup.setup_id} ${setup.type}`;
        const data = DataProvider.getData(storageName);
        if (data) {
          model3d['data'] = data;
        }
        const startDate = moment(`${setup.event_date_starts} ${setup.event_time_starts}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
        const endDate = moment(`${setup.event_date_ends} ${setup.event_time_ends}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
        model3d['layout'] = setup.type;
        model3d['config'] = setup.config;
        model3d['model3d'] = {};
        model3d['model2d'] = {};
        model3d['droppedItems'] = setup.droppedItems || [];
        if (room.scenes.length >= 1) {
          model3d['model3d'] = room.scenes[0].model3d;
          model3d['model2d'] = room.scenes[0].model2d;
        }
        model3d['available_model'] = room.available_model || false;
        model3d['img'] = setup.img || false;
        requestData['name'] = setup.activity_name || 'Activity name';
        requestData['start'] = startDate;
        requestData['end'] = endDate;
        requestData['description'] = `This is a test description for ${setup.shape}`;
        requestData['attendees'] = setup.chairs_count;
        requestData['model_3d'] = model3d;
        requestData['event'] = eventId;
        requestData['meeting_room'] = room.id;
        requestData['services'] = setup.service || [];

        window[`roomsetup${room.id}${setup.id}${setup.setup_id}`] = new Promise((resolve, reject) => {
          AuthStore.getHeaders(null, (headers) => {
            headers['Content-Type'] = 'application/json';
            request.post(AuthStore.getApiHost()+'/api/activity_programs')
              .send(JSON.stringify(requestData))
              .set(headers)
              .end((err, res) => {
                if (!err) {
                  const response = JSON.parse(res.text);
                  resolve(response);
                } else {
                  console.log('err', err);
                  reject(res);
                  callback(false);
                }
              });
          });

        });
        roomNames.push(eval(`roomsetup${room.id}${setup.id}${setup.setup_id}`));
      });
    });
    const contactInfo = JSON.parse(JSON.stringify(contactDetails));
    contactInfo.mail = contactInfo.email;
    contactInfo.address_2 = contactInfo.address_line_2;
    contactInfo.event = eventId;

    delete contactInfo.email;
    delete contactInfo.address_line_2;
    Object.keys(contactInfo).map((key)=>{
      if(!contactInfo[key]){
        delete contactInfo[key];
      }
    });
    const contactDetailsParams = new Promise((resolve, reject) => {
      AuthStore.getHeaders(null, (headers) => {
        headers['Content-Type'] = 'application/json';
        request.post(AuthStore.getApiHost()+'/api/contact_informations')
          .send(JSON.stringify(contactInfo))
          .set(headers)
          .end((err, res) => {
            if (!err) {
              const response = JSON.parse(res.text);
              resolve(response);
            } else {
              console.log('err', err);
              reject(res);
              callback(false);
            }
          });
      });

    });
    roomNames.push(contactDetailsParams);
    guestInfo.map((guestRoomInfo) => {
      guestRoomInfo.start = moment(guestRoomInfo.start).format('YYYY-MM-DD HH:mm');
      guestRoomInfo.end = moment(guestRoomInfo.end).format('YYYY-MM-DD HH:mm');
      guestRoomInfo.event = eventId;
      window[`guest${guestRoomInfo.id}`] = new Promise((resolve, reject) => {
        AuthStore.getHeaders(null, (headers) => {
          headers['Content-Type'] = 'application/json';
          request.post(AuthStore.getApiHost()+'/api/guestroom_request')
            .send(JSON.stringify(guestRoomInfo))
            .set(headers)
            .end((err, res) => {
              if (!err) {
                const response = JSON.parse(res.text);
                resolve(response);
              } else {
                console.log('err', err);
                reject(res);
                callback(false);
              }
            });
        });

      });
      roomNames.push(eval(`guest${guestRoomInfo.id}`));
    });
    Promise.all(roomNames).then((response) => {
      const statusChange = {};
      statusChange['id'] = eventId;
      statusChange['status'] = 1;
      AuthStore.getHeaders(null, (headers) => {
        headers['Content-Type'] = 'application/json';
        request.patch(AuthStore.getApiHost()+`/api/events/${eventId}`)
          .send(JSON.stringify(statusChange))
          .set(headers)
          .end((err, res) => {
            if (!err) {
              const statusRes = JSON.parse(res.text);
              callback(true);
            } else {
              console.log('err', err);
              callback(false);
            }
          });
      });

    });
  },
  // getAttachementTypes(callback) {
  //   AuthStore.getHeaders(null, (headers) => {
  //     request.get(AuthStore.getApiHost()+'/api/events/attachments/types')
  //       .set(headers)
  //       .end((err, res) => {
  //         if (!err) {
  //           const response = JSON.parse(res.text);
  //           const supportedFileTypes = {};
  //           const types = response.results;
  //           const names = [];
  //           for (let i = 0; i < types.length; i++) {
  //             supportedFileTypes[types[i].mime_type] = types[i].id;
  //             names.push(types[i].name);
  //           }
  //           supportedFileTypes['names'] = names;
  //           console.log('supportedFileTypes', supportedFileTypes);
  //           callback(supportedFileTypes);
  //           AppDispatcher.handleServerAction({
  //             actionType: MeetingSpaceConstants.ATTACHMENT_TYPES,
  //             data: response,
  //           });
  //         } else {
  //           console.log('err', err);
  //           callback(false);
  //         }
  //       });
  //   });
  // },
  getAttachementTypes(callback, nextAPI) {
    AuthStore.getHeaders(null, (headers) => {
      const apiRequest = nextAPI || `${AuthStore.getApiHost()}/api/events/attachments/types`;
      request.get(apiRequest)
      .set(headers)
      .end((err, res) => {
        if (!err) {
          const response = JSON.parse(res.text);
          // const supportedFileTypes = {};

          const types = response.results;
          for (let i = 0; i < types.length; i++) {
            supportedFileTypes[types[i].mime_type] = types[i].id;
            if (!(names.includes(types[i].name))) {
              names.push(types[i].name);
            }
          }
          supportedFileTypes['names'] = names;
          if (response.next) {
            this.getAttachementTypes(callback, response.next);
          }
          if (!response.next) {
            if (callback) {
              callback(supportedFileTypes);
              AppDispatcher.handleServerAction({
                actionType: MeetingSpaceConstants.ATTACHMENT_TYPES,
                data: supportedFileTypes,
              });
              supportedFileTypes = {};
              names = [];
              // callback(true);
            }
          }
        } else {
          console.log('err', err);
        }
      });
      });
    },
  replaceImage(bool) {
    AppDispatcher.handleViewAction({
      actionType: 'REPLACE_IMAGE',
      bool,
    });
  },
  storeSceneData(scenes) {
    AppDispatcher.handleViewAction({
      actionType: 'STORE_SCENE',
      scenes,
    });
  },
  addedFileCount(length) {
    AppDispatcher.handleViewAction({
      actionType: 'DOC_LENGTH',
      length,
    });
  },
  uploadedFile(file) {
    AppDispatcher.handleViewAction({
      actionType: 'UPLOADED_FILE',
      file,
    });
  },
  storeEventId(eventId, uuId) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_EVENT_ID',
      eventId,
      uuId,
    });
  },
  showFloorRoom(room) {
    AppDispatcher.handleViewAction({
      actionType: 'SHOW_SELECTED_FLOOR_ROOM',
      room,
    });
  },
  hideFloorRoom() {
    AppDispatcher.handleViewAction({
      actionType: 'HIDE_SELECTED_FLOOR_ROOM',
    });
  },
  restoreData() {
    AppDispatcher.handleViewAction({
      actionType: 'RESTORE_DATA',
    });
  },
  storeCreatedEventInfo(eventInfo) {
    AppDispatcher.handleViewAction({
      actionType: 'CREATE_EVENT_INFO',
      eventInfo,
    });
  },
};

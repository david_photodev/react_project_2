import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthStore from '../stores/AuthStore';

export default {
  getHotelBrandList(callback, nextAPI) {
    AuthStore.getHeaders(null, (headers) => {
      const apiRequest = nextAPI || `${AuthStore.getApiHost()}/api/hotel_chain`;
      request.get(apiRequest)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const brandList = JSON.parse(res.text);
            AppDispatcher.handleViewAction({
              actionType: 'SINGLE_CHAIN_LIST',
              brandList,
            });
            if (brandList.next) {
              this.getHotelBrandList(callback, brandList.next);
            }
            if (!brandList.next) {
              if (callback) {
                callback(true);
              }
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },
  readChain(chainId, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/hotel_chain/${chainId}`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const brand = JSON.parse(res.text);
            this.selectedProfile(brand);
            if (callback) {
              callback(true);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },

  getSortedBrandList(sortBy) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_SORTED_HOTEL_BRAND_LIST',
      sortBy,
    });
  },

  searchBy(name) {
    AppDispatcher.handleViewAction({
      actionType: 'HOTEL_BRAND_SEARCH',
      name,
    });
  },

  selectedProfile(brand) {
    AppDispatcher.handleViewAction({
      actionType: 'SHOW_PROFILE',
      brand,
    });
  },

  getTypography() {
    request.get(`https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyA6UHrZfQRw9o_NRYSC_mWiwnc47sN8ZiU`)
      .end((err, res) => {
        if (!err) {
          const typography = JSON.parse(res.text);
          AppDispatcher.handleViewAction({
            actionType: 'GET_TYPOGRAPHY',
            typography,
          });
        } else {
          console.log('err', err);
        }
      });
  },
  deleteHotelBrand(hotelBrandId) {
    // AuthStore.getHeaders(null, (headers) => {
    //      request.delete(AuthStore.getApiHost()+`/api/hotel_chain/${hotelBrandId}`)
    //     .set(headers)
    //     .end((err, res) => {
    //       if (!err) {
    //         this.getHotelBrandList();	      
    //       } else {
    //         console.log('err', err);
    //       }
    //     });
    //    });
    AppDispatcher.handleViewAction({
      actionType: 'GET_HOTEL_BRAND_LIST',
      brandList: hotelBrandId,
    });
  },
  saveProfile(profileInfo) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_PROFILE_INFO',
      profileInfo,
    });
  },
  getGaTracking(url, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(url)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const hotelResponse = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'GA_TRACKING',
              hotelResponse,
            });
            callback(hotelResponse);
          } else {
            console.log('err', err);
          }
        });
    });
  },

  getNotificationMsg(msg,type) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_NOTIFICATION_MSG',
      msg,
      type,
    });
  },

  getGeometryLocation(address, callback) {
    request.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${AuthStore.getGoogleGeoLocationKey()}`)
      .end((err, res) => {
        if (!err) {
          const GeometryLocation = JSON.parse(res.text);
          callback(GeometryLocation);
        } else {
          console.log('err', err);
        }
      });
  },

  updateBrand(brandId, brandInfo, callback) {
		const url = AuthStore.getApiHost()+"/api/hotel_chain/"+brandId+"";
		var formData  = new FormData();
		for(var name in brandInfo) {
      		formData.append(name, brandInfo[name]);
    	}
    	AuthStore.getHeaders(null, (headers) => {
	      fetch(url, {
        	mode: 'cors',
        	method: 'PATCH',
        	headers: headers,
        	body: formData
      		}).then(function (response) {
		        if (response.ok) {
					Promise.all([response.clone().json()]).then((response) => {
		        		callback(response[0]);
		        	});
        		}
      		});
	    });
  	},

  	statusChange(brandList, isActive) {
		const params = {
			status: isActive ? 1 : 2,
		};
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/hotel_chain/${brandList.id}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	    	AppDispatcher.handleServerAction({
				actionType: 'BRAND_STATUS_CHANGE',
				brandList,
				isActive,
			});
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

  	createHotelBrand(profileInfo, callback) {
  		const url = AuthStore.getApiHost()+"/api/hotel_chain";
  		var formData  = new FormData();
    	for(var name in profileInfo) {
      		formData.append(name, profileInfo[name]);
    	}

	  	AuthStore.getHeaders(null, (headers) => {
	      fetch(url, {
        	mode: 'cors',
        	method: 'POST',
        	headers: headers,
        	body: formData
      		}).then(function (response) {
		        if (response.ok) {
					AppDispatcher.handleViewAction({
						actionType: 'SET_PROFILE_INFO_AS_EMPTY',
					});
					callback(response);
        		}
      		});
	    });
  	}

};
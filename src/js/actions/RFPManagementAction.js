import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthStore from '../stores/AuthStore';
export default {
  getUserEventInfo(data, eventType) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_USER_EVENT_INFO',
      data,
      eventType,
    });
  },
  getPastEventInfo(data, activeCount){
  	AppDispatcher.handleViewAction({
      actionType: 'GET_PAST_EVENT_INFO',
      data,
      activeCount,
    });
  },
  getAllHistoryInfo(allEventHistory) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_ALL_EVENT_HISTORY_INFO',
      allEventHistory,
    });
  },
  getUserEventStatus(status) {
    AppDispatcher.handleViewAction({
      actionType: 'GET_USER_EVENT_STATUS',
      status,
    });
  },

  clearStoreInfo() {
  	AppDispatcher.handleViewAction({
      actionType: 'CLEAR_STORE_EVENT_INFO',
    });
  },
	search(searchVal) {
		AppDispatcher.handleViewAction({
      actionType: 'EVENT_SEARCH',
      searchVal,
    });
	},
  getSelectedEventId(eventId, uuId){
  	AppDispatcher.handleViewAction({
	    actionType: 'GET_SELECTED_EVENT_ID',
	    eventId,
	    uuId,
	  })
	},
	getUserSelectedEventHotelInfo(eventHotelInfo){
		AppDispatcher.handleViewAction({
	    actionType: 'GET_SELECTED_EVENT_HOTEL_INFO',
	    eventHotelInfo,
	  });
	},
	getSetupTypes(setupTypes){
		AppDispatcher.handleViewAction({
	    actionType: 'GET_SETUP_TYPES',
	    setupTypes,
	  });
	},
	updateAddons(room, setup, type) {
    AppDispatcher.handleViewAction({
      actionType: 'ADDON_UPDATE',
      room,
      setup,
      type,
    });
  },
  paginationClick(pageClick) {
    AppDispatcher.handleViewAction({
      actionType: 'EVENT_LIST_PAGE',
      page: pageClick,
    });
  },
 	selectedHotel(hotel) {
 		AppDispatcher.handleViewAction({
      actionType: 'CREATE_HOTEL_EVENT',
      hotel,
    });
 	},
 notificationMsg(msg, val,) {
    AppDispatcher.handleViewAction({
      actionType: 'ADD_NOTIFICATION',
      msg,
      val,
    });
  },
  active3DEdit(isActive) {
  	AppDispatcher.handleViewAction({
      actionType: 'EDIT_3D_HANDLE',
      isActive,
    });
  },
  getSelectedEventInfo(eventId){
	  	AuthStore.getHeaders(null, (headers) => {
	      request.get(AuthStore.getApiHost()+`/api/events/${eventId}`)
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
							AppDispatcher.handleViewAction({
					      actionType: 'GET_SELECTED_EVENT_INFO',
					      response,
					    });
					    
					    AuthStore.getHeaders(null, (headers) => {
					      request.get(response.hotel.url)
							    .set(headers)
							    .end((err, res) => {
							      if (!err) {
							        const hotelResponse = JSON.parse(res.text);
							        hotelResponse['data'] = {'location':hotelResponse.location}
									    this.getUserSelectedEventHotelInfo(hotelResponse);
									    if(hotelResponse.hasOwnProperty('hotel_chain')){
									    	AuthStore.getHeaders(null, (headers) => {
					      					request.get(hotelResponse.hotel_chain.url)
												    .set(headers)
												    .end((err, res) => {
												      if (!err) {
											      	 	const selectedHotelChain = JSON.parse(res.text);
											      	 	AppDispatcher.handleViewAction({
														      actionType: 'GET_EVENT_HOTEL_CHAIN',
														      selectedHotelChain,
														    });
												      } else {
												        console.log('err', err);
												      }
			    								});
		    								});
									    	AuthStore.getHeaders(null, (headers) => {
					      					request.get(hotelResponse.hotel_chain.url+'/setup_types')
												    .set(headers)
												    .end((err, res) => {
												      if (!err) {
												        const setupTypes = JSON.parse(res.text);
														    this.getSetupTypes(setupTypes);
												      } else {
												        console.log('err', err);
												      }
			    								});
		    								});
									    }
							      } else {
							        console.log('err', err);
							      }
			    				});
		    			});

		    			// callback(true)
			      } else {
			        console.log('err', err);
			      }
			    });
		    });
  },
	getUserEventlist(event, callback) {
		AppDispatcher.handleViewAction({
			actionType: 'SET_SELECTED_EVENT_INFO_AS_EMPTY'
		})
		const activeUrl = `/api/events?is_active=1&ordering=-last_activity`;
		const pastUrl = `/api/events?end_date=${moment().subtract(1, 'days').format('YYYY-MM-DD')} 23:59:59`;
		const UserEventInfo = new Promise((resolve, reject) => {
			AuthStore.getHeaders(null, (headers) => {
	      	request.get(AuthStore.getApiHost()+activeUrl)
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			        resolve(response);
			        // callback(true);
			      } else {
			        console.log('err', err);
			        reject(res);
			      }
			    });
	    	});
	  	});
		const pastEventInfo = new Promise((resolve, reject) => {
			AuthStore.getHeaders(null, (headers) => {
	      	request.get(AuthStore.getApiHost()+pastUrl)
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			        resolve(response);
			      } else {
			        console.log('err', err);
			        reject(res);
			      }
			    });
	    	});
	  	});
	  	const UserEventStatus = new Promise((resolve, reject) => {
			AuthStore.getHeaders(null, (headers) => {
	      	request.get(AuthStore.getApiHost()+'/api/events/statuses?page=1')
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			        resolve(response);
			      } else {
			        console.log('err', err);
			        reject(res);
			      }
			    });
	    	});
	  	});

	  Promise.all([UserEventInfo, pastEventInfo, UserEventStatus]).then((response) => {
	  	const allHistory = [];
	  	response[0].results.map(result => {
	  		const UserEventStatus = new Promise((resolve, reject) => {
					this.getSingleHistoryInfo(result.id, {}, null, (res)=>{
						resolve(res);
					});
				});
	  		allHistory.push(UserEventStatus);
			});
			Promise.all(allHistory).then((allHistoryResponse) => {
		  	this.getUserEventInfo(response[0], event);
		  	this.getPastEventInfo(response[1], response[0].count);
		  	this.getUserEventStatus(response[2]);
		  	this.getAllHistoryInfo(allHistoryResponse);
		  	callback(true);
			});
	  });
	},
	getSingleHistoryInfo (eventId, histories, nextUrl, callback) {
		const url = nextUrl || AuthStore.getApiHost()+`/api/events/${eventId}/history`
		AuthStore.getHeaders(null, (headers) => {
			request.get(url)
				.set(headers)
				.end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			        response['id'] = eventId;
			        if (!nextUrl) {
			        	histories = response;
			        }
			        if (nextUrl) {
			        	histories['results'] = histories.results.concat(response.results);
			        }
			        if (response.next) {
			        	this.getSingleHistoryInfo (eventId, histories, response.next, callback)
			        }

			        if (!response.next) {
			        	if (callback) {
									callback(histories);
			        	}
			        }
			      } else {
			      	reject(res);
			        console.log('err', err);
			      }
				});
		});
	},
	getHistoryInfo(eventId){
		this.getSingleHistoryInfo(eventId, {}, null, (res)=>{
	    const historyInfo = res;
	    AppDispatcher.handleViewAction({
				actionType: 'GET_HISTORY_INFO',
				historyInfo,
			})
	    const orderedHistoryInfo = historyInfo.results.sort(function(a, b){
	    var keyA = new Date(a.date),
	        keyB = new Date(b.date);
	    // Compare the 2 dates
	    if(keyA < keyB) return -1;
	    if(keyA > keyB) return 1;
	    return 0;
		});
	    if (orderedHistoryInfo.length >=1) {
	    	AuthStore.getHeaders(null, (headers) => {
	  		request.get(AuthStore.getApiHost()+`/api/users/${orderedHistoryInfo[orderedHistoryInfo.length-1].user.id}/`)
		    .set(headers)
		    .end((err, res) => {
		      if (!err) {
		        const userHistoryInfo = JSON.parse(res.text);
		        AppDispatcher.handleViewAction({
			      actionType: 'GET_USER_INFO_OF_LATEST_HISTORY',
			      userHistoryInfo,
			    });
		      } else {
		        console.log('err', err);
		      }
		    });
	    });
	    }
	  });
		      
	},
	getEventsStatus(){
		AuthStore.getHeaders(null, (headers) => {
	      request.get(AuthStore.getApiHost()+'/api/events/statuses?page=1')
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const status = JSON.parse(res.text);
			        AppDispatcher.handleViewAction({
				      actionType: 'GET_USER_EVENT_STATUS',
				      status,
				    });
			      } else {
			        console.log('err', err);
			      }
			    });
	    });
	},

	getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, attendees_min, attendees_max, sortType, sortBy = '', selectedPage = 1, event, callback){
		const attendeesUrl = this.getEventlistBasedOnAttendees(attendees_min, attendees_max);
		let url = '';
		if (event === 'past') {
			url = `/api/events?end_date=${moment().subtract(1, 'days').format('YYYY-MM-DD')} 23:59:59&page=${selectedPage}`;
		} else if (event === 'active'){
			url = `/api/events?is_active=1&page=${selectedPage}`;
		} 
		if (moment(fromDate, "DD/MM/YYYY", true).isValid() && moment(toDate, "DD/MM/YYYY", true).isValid()){
			if (event === 'past') {
				url = `/api/events?start_date=${moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD')}&end_date=${moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD')} 23:59:59&page=${selectedPage}`
			} else {
				url = url+`&start_date=${moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD')}&end_date=${moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD')} 23:59:59&page=${selectedPage}`
			}
			
		}
		if (eventTypeId){
			if (url === ''){
				url = `/api/events?type=${eventTypeId}&page=${selectedPage}`;
			}
			else{
				url = url+`&type=${eventTypeId}`;
			}
		}
		if (attendeesUrl){
			if (url === ''){
				url = `/api/events?${attendeesUrl}&page=${selectedPage}`;
			}
			else{
				url = url+`&${attendeesUrl}`;
			}
		}
		if (url === ''){
			url = `/api/events?page=${selectedPage}`;
		}

		if (sortType){
			// sortBy = sortBy ? '+':'-';
			url = url+`&ordering=${sortBy}${sortType}`
		} else {
			url = url+`&ordering=-last_activity`
		}
		const UserEventInfo =new Promise((resolve, reject) => {
			AuthStore.getHeaders(null, (headers) => {
	      request.get(AuthStore.getApiHost()+url)
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			        resolve(response);
			      } else {
			      	reject(res);
			        console.log('err', err);
			      }
			    });
	    });
	  });

	  Promise.all([UserEventInfo]).then((response) => {
	  	const allHistory = [];
	  	response[0].results.map(result => {
	  		const UserEventStatus = new Promise((resolve, reject) => {
					this.getSingleHistoryInfo(result.id, {}, null, (res)=>{
						resolve(res);
					});
				});
	  		allHistory.push(UserEventStatus);
			});
			Promise.all(allHistory).then((allHistoryResponse) => {
		  	this.getUserEventInfo(response[0], event);
		  	this.getAllHistoryInfo(allHistoryResponse);
		  	if (callback) {
		  		callback(true);
		  	}
			})
	  });
	},
	getSortedEventList(type, sortBy, event, selectedPage, callback){
		// sortBy = sortBy ? '+':'-';
		let url = `/api/events?page=${selectedPage}&ordering=${sortBy}${type}`;
		if (event === 'past') {
			url = url+`&end_date=${moment().subtract(1, 'days').format('YYYY-MM-DD')} 23:59:59`;
		} else if (event === 'active'){
			url = url+`&is_active=1`;
		} 
		const UserEventInfo = new Promise((resolve, reject) => {
			AuthStore.getHeaders(null, (headers) => {
				request.get(AuthStore.getApiHost()+url)
			    .set(headers)
			    .end((err, res) => {
			      if (!err) {
			        const response = JSON.parse(res.text);
			      	resolve(response);
			      } else {
			      	reject(res);
			        console.log('err', err);
			      }
			    });
	    });
	  });
	  Promise.all([UserEventInfo]).then((response) => {
	  	const allHistory = [];
	  	response[0].results.map(result => {
	  		const UserEventStatus = new Promise((resolve, reject) => {
					this.getSingleHistoryInfo(result.id, {}, null, (res)=>{
						resolve(res);
					});
				});
	  		allHistory.push(UserEventStatus);
			});
			Promise.all(allHistory).then((allHistoryResponse) => {
		  	this.getUserEventInfo(response[0], event);
		  	this.getAllHistoryInfo(allHistoryResponse);
		  	callback(true);
			})
	  });
	},
	getEventlistBasedOnAttendees(attendees_min, attendees_max){
		let attendeesUrl;
		if ((!isNaN(parseInt(attendees_min))) || (!isNaN(parseInt(attendees_max)))) { 
			if (attendees_min && attendees_max){
				attendeesUrl = `attendees_min=${attendees_min}&attendees_max=${attendees_max}`;
			} else if (attendees_min) {
				attendeesUrl = `attendees_min=${attendees_min}`;
			} else {
				attendeesUrl = `attendees_max=${attendees_max}`;
			}
			return attendeesUrl;
		} else {
			return null;
		}

		// AuthStore.getHeaders(null, (headers) => {
		// 	request.get(AuthStore.getApiHost()+url)
		//     .set(headers)
		//     .end((err, res) => {
		//       if (!err) {
		//         const data = JSON.parse(res.text);
		//         AppDispatcher.handleViewAction({
	 //    				actionType: 'GET_USER_EVENT_INFO',
	 //    				data,
	 //  				})
		//       } else {
		//         console.log('err', err);
		//       }
		//     });
  //   });
	},
	getUserEventTypes(eventId){
		AuthStore.getHeaders(null, (headers) => {
      request.get(AuthStore.getApiHost()+`/api/events/types?page=1`)
		    .set(headers)
		    .end((err, res) => {
		      if (!err) {
		        const evnentTypes = JSON.parse(res.text);
		        AppDispatcher.handleViewAction({
	    				actionType: 'GET_EVENT_TYPES',
	    				evnentTypes,
	  				})
		      } else {
		        console.log('err', err);
		      }
		    });
    });
	},
	updateEvent(event, callback){
		const statusChange = {};
      statusChange['id'] = event.id;
      statusChange['status'] = event.status.id;
      AuthStore.getHeaders(null, (headers) => {
        headers['Content-Type'] = 'application/json';
        request.patch(AuthStore.getApiHost()+`/api/events/${event.id}`)
          .send(JSON.stringify(statusChange))
          .set(headers)
          .end((err, res) => {
            if (!err) {
              	const statusRes = JSON.parse(res.text);
              	if (!callback) {
              		this.getHistoryInfo(event.id);
              	} else {
	              	const historyInfo = new Promise((resolve, reject) => {
		              	this.getSingleHistoryInfo(event.id, {}, null, (res)=>{
											resolve(res);
										});
		            	});
		            const eventInfo = new Promise((resolve, reject) => {
		              	AuthStore.getHeaders(null, (headers) => {
		      				request.get(AuthStore.getApiHost()+`/api/events/${event.id}`)
				    			.set(headers)
				    			.end((err, res) => {
				      				if (!err) {
				        				const response = JSON.parse(res.text);
				        				resolve(response);
		              				} else {
		              					reject(res);
		              					console.log('err', err);
		            				}
		          				});
		      			});
		            });
		            Promise.all([historyInfo, eventInfo]).then((response) => {
		            	callback(response);
		            });
              	}	
            } else {	
            	console.log('err', err);
            }
          });
      });
	},
	uploadingFile(file) {
		AppDispatcher.handleViewAction({
      actionType: 'UPLOADING_FILE',
      file,
    });
	},
	removeUploadedFile(eventId, attachmentId) {
		AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.delete(AuthStore.getApiHost()+`/api/events/${eventId}/attachments/${attachmentId}`)
        .set(headers)
        .end((err, res) => {
          if (res.ok) {
          	AppDispatcher.handleServerAction({
	    				actionType: 'REMOVE_ATTACH_FILE',
	    				eventId,
	    				attachmentId,
	  				});
          } else {
            console.log('err', err);
          }
        });
    });
	},

	getHotelList(callback, nextAPI) {
		AuthStore.getHeaders(null, (headers) => {
			const apiRequest = nextAPI || `${AuthStore.getApiHost()}/api/hotel?status=1`;
      request.get(apiRequest)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const hotelList = JSON.parse(res.text);
	        AppDispatcher.handleViewAction({
    				actionType: 'SINGLE_HOTEL_LIST',
    				hotelList,
  				});
  				if (hotelList.next) {
            this.getHotelList(callback, hotelList.next);
          }
          if (!hotelList.next) {
            if (callback) {
              callback(true);
            }
          }
	      } else {
	        console.log('err', err);
	      }
	    });
	  });
  },

	hotelList(hotels) {
		AppDispatcher.handleViewAction({
			actionType: 'HOTEL_LIST',
			hotels,
		});
	},

	updateEventInfo(params, callback) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/events/${params.id}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
	},

	getActivityPrograms(res) {
		console.log('response', res);
		AppDispatcher.handleViewAction({
			actionType: 'GET_ACTIVITY_PROGRAMS',
			res,
		});
	},

	updateActivityName(params) {
    AppDispatcher.handleViewAction({
      actionType: 'UPDATE_ACTIVITY_NAME',
      params,
    });
  },

  savePeriod(guestInfo) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_PERIOD',
      guestInfo,
    });
  },

  saveContact(contactDetails) {
    AppDispatcher.handleViewAction({
      actionType: 'SAVE_CONTACT',
      contactDetails,
    });
  },

  duplicateLayout(room, setup) {
    AppDispatcher.handleViewAction({
      actionType: 'DUPLICATE_LAYOUT',
      room,
      setup,
    });
  },

  duplicateGuestInfo(addGuest){
    AppDispatcher.handleViewAction({
    	actionType: 'DUPLICATE_GUEST_INFO',
    	addGuest,
    });
  },

  removeLayout(room, setup) {
    AppDispatcher.handleViewAction({
      actionType: 'REMOVE_LAYOUT',
      room,
      setup,
    });
  },

  removeGuestInfolayout(guest) {
  	AppDispatcher.handleViewAction({
      actionType: 'REMOVE_GUEST_INFO',
      guest,
    });
  },

  updateEachAP(params, ap, callback) {
  	AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/activity_programs/${params.id}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	delete ap.edit;
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
  },

  createAP(params, ap, callback) {
  	AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.post(`${AuthStore.getApiHost()}/api/activity_programs`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	ap.id = updated.id;
	      	ap.apId = updated.id;
	      	ap.edit = true;
	      	delete ap.create;
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
  },

  createGuestInfo(params, guest, callback) {
    AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.post(`${AuthStore.getApiHost()}/api/guestroom_request`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	guest.id = updated.id;
	      	guest.edit = true;
	      	delete guest.create;
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
  },

  updateAPsInfo(updateAPs, callback) {
  	updateAPs.map(apInfo => {
  		apInfo.setup.map(ap => {
  			const updateAPFields = ['id', 'activity_name', 'event_date_starts', 'event_date_ends', 'chairs_count', 'apId',
		    	'event_time_starts', 'event_time_ends', 'meeting_room', 'event', 'services_ids', 'model_3d', 'description',
		    	'edit', 'create', 'meetingRoomId',
		    ];
		    const apInfoSetup = JSON.parse(JSON.stringify(ap));
	    	Object.keys(apInfoSetup).filter(info => {
		      if (!updateAPFields.includes(info)) {
		        delete apInfoSetup[info];
		      }
		    });
		    const model3d = {};
        model3d['data'] = {};
        const storageName = `SceneData ${apInfo.id}${ap.setup_id} ${ap.type}`;
        const data = DataProvider.getData(storageName);
        if (data) {
        	apInfoSetup.edit = true;
          model3d['data'] = data;
        }
        model3d['layout'] = ap.type;
        model3d['model3d'] = {};
        model3d['model2d'] = {};
        if (apInfo.scenes.length >= 1) {
          model3d['model3d'] = apInfo.scenes[0].model3d;
          model3d['model2d'] = apInfo.scenes[0].model2d;
        }
        model3d['available_model'] = apInfo.available_model || false;
        model3d['droppedItems'] = ap.droppedItems;
        model3d['img'] = ap.img || false;
	    	const params = {
	    		id: apInfoSetup.apId,
	    		name: apInfoSetup.activity_name || 'Activity name',
	    		description: apInfoSetup.description,
	    		start: moment(`${apInfoSetup.event_date_starts} ${apInfoSetup.event_time_starts}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'),
	    		end: moment(`${apInfoSetup.event_date_ends} ${apInfoSetup.event_time_ends}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'),
	    		meeting_room: apInfoSetup.meetingRoomId ? apInfoSetup.meetingRoomId : apInfoSetup.meeting_room.id,
	    		event: apInfoSetup.event.id,
	    		services: apInfoSetup.services_ids,
	    		attendees: apInfoSetup.chairs_count,
	    		model_3d: data ? model3d : apInfoSetup.model_3d,
	    	};
  			this.active3DEdit(false);
	    	if (apInfoSetup.create) {
	    		delete params.id;
	    		this.createAP(params, ap, callback);
	    	} else {
	    		this.updateEachAP(params, ap, callback);
	    	}
	    });
  	});
  },

  updateEachGuest(guest, callback) {
    const params = JSON.parse(JSON.stringify(guest));
		params['event'] = params.event.id ? params.event.id : params.event;
  	AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/guestroom_request/${guest.id}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	delete guest.edit;
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
  },

  updateGuestsInfo(guestInfo, callback) {
		guestInfo.map(guest => {
    	const params = JSON.parse(JSON.stringify(guest));
    	if (params.edit && !params.create) {
    		this.updateEachGuest(params, callback);
    	} 
    	if (params.create) {
    		this.createGuestInfo(params, guest, callback);
    	}
    });
	},

	deletePeriod(id) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.delete(`${AuthStore.getApiHost()}/api/guestroom_request/${id}`)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	if (res.ok) {
		      	AppDispatcher.handleViewAction({
				      actionType: 'DELETE_PERIOD',
				      id,
				    });
	      	}
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	updateContactInfo(contact, callback) {
		const params = JSON.parse(JSON.stringify(contact));
		params['mail'] = params.email;
		params['event'] = params.event.id;
		const replaceItems = Object.keys(params).filter(info => params[info] === '');
	  replaceItems.forEach((element) => {
	    delete params[element];
	  });
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/contact_informations/${contact.id}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updated = JSON.parse(res.text);
	      	if (callback) {
	      		callback(true);
	      	}
	      } else {
	        console.log('err', err);
	        callback(false);
	      }
	    });
    });
	},

	deleteAP(room, setup) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.delete(`${AuthStore.getApiHost()}/api/activity_programs/${setup.id}`)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	if (res.ok) {
		      	this.removeLayout(room, setup);
	      	}
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	}
};
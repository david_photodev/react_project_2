import request from 'superagent';
import AppDispatcher from '../dispatcher/AppDispatcher';
import AuthStore from '../stores/AuthStore';

export default {
  getHotelList(selectedPage, isSortByNameActive, sortByNameAsc, callback) {
    let order = '';
    if (isSortByNameActive) {
      if (sortByNameAsc) {
        order = '&ordering=+name';
      } else {
        order = '&ordering=-name';
      }
    } else {
      order = '&ordering=-created_at';
    }
    AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/hotel?page=${selectedPage}${order}`)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const hotelList = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'GET_HOTEL_LIST',
              hotelList,
            });
            callback(true);
          } else {
            console.log('err', err);
          }
        });
    });
  },
  getHotelChainList(callback, nextAPI) {
    AuthStore.getHeaders(null, (headers) => {
      const apiRequest = nextAPI || `${AuthStore.getApiHost()}/api/hotel_chain`;
      request.get(apiRequest)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const chainList = JSON.parse(res.text);
            AppDispatcher.handleViewAction({
              actionType: 'CHAIN_LIST',
              chainList,
            });
            if (chainList.next) {
              this.getHotelChainList(callback, chainList.next);
            }
            if (!chainList.next) {
              if (callback) {
                callback(true);
              }
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },
  getAvailableSetup(setupURL, setUpImages, callback) {
    AuthStore.getHeaders(null, (headers) => {
      request.get(setupURL)
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const availableSetups = JSON.parse(res.text);
            if (callback) {
              const available_setup = availableSetups.results.map((result) => {
                let setupNameUpdated = result.name.toLowerCase();
                setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
                result.img = setupNameUpdated in setUpImages ? setUpImages[setupNameUpdated] : setUpImages['theatre'];
                result.type = setupNameUpdated;
                return result;
              });
              callback(available_setup);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },
  createHotel(params, callback) {
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      console.log(params);
      request.post(`${AuthStore.getApiHost()}/api/hotel`)
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const createdHotel = JSON.parse(res.text);
            AppDispatcher.handleServerAction({
              actionType: 'CREATE_HOTEL',
              createdHotel,
            });
            callback(createdHotel);
          } else {
            console.log('err', err);
          }
        });
    });
  },
  updateFacility(hotelId, params, callback) {
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.post(`${AuthStore.getApiHost()}/api/hotel/${hotelId}/facilities`)
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const facility = JSON.parse(res.text);
            facility.tempId = params.tempId;
            if (callback) {
              callback(facility);
            }
          } else {
            console.log('err', err);
          }
        });
    });
  },
  getAssetsUploadResponse(uploadedInfo, callback) {
    const url = AuthStore.getApiHost()+`/api/meeting_room/${uploadedInfo.meeting_room}/assets`;
    var formData  = new FormData();
    for(var name in uploadedInfo) {
          formData.append(name, uploadedInfo[name]);
      }
      AuthStore.getHeaders(null, (headers) => {
        fetch(url, {
          mode: 'cors',
          method: 'POST',
          headers: headers,
          body: formData
          }).then(function (response) {
            if (response.ok) {
              Promise.all([response.clone().json()]).then((response) => {
                callback(response[0]);
              });
            } else {
              Promise.all([response.clone().json()]).then((response) => {
                callback(response[0]);
              });
            }
          })
        });
	},

	updatePhotoOrder(meeting_room, assetId, order) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/meeting_room/${meeting_room}/assets/${assetId}`)
      .send(JSON.stringify({order:order}))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const updatedPhotoResponse = JSON.parse(res.text);
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	deleteAssets(assetId,RoomId, callback) {
		AuthStore.getHeaders(null, (headers) => {
	      request.delete(`${AuthStore.getApiHost()}/api/meeting_room/${RoomId}/assets/${assetId}`)
		    .set(headers)
		    .end((err, res) => {
		      if (!err) {
		        callback(true);
		      } else {
		        console.log('err', err);
		        callback(false);
		      }
		    });
    	});
	},

	updatelinks(meeting_room, assetId, linkUrl, callback) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/meeting_room/${meeting_room}/assets/${assetId}`)
      .send(JSON.stringify({url:linkUrl}))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const updatedPhotoResponse = JSON.parse(res.text);
	        callback(updatedPhotoResponse);
	      } else {
	        console.log('err', err);
	        const updatedPhotoResponse = JSON.parse(res.text);
	        callback(updatedPhotoResponse);
	      }
	    });
    });
	},
  updateMeetingRoom(hotelId, params, callback) {
    params.assets = params.assets.map((asset) => {
      return asset.id;
    });
    params.services = params.services.map((service) => {
      return service.id;
    });
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.post(`${AuthStore.getApiHost()}/api/meeting_room`)
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const meetingRoom = JSON.parse(res.text);
            meetingRoom['measurement'] = params.measurement;
            const roomSetupRes = [];
            params.room_setups.map((setup) => {
              window[`setup${setup.type}`] = new Promise((resolve, reject) => {
                request.post(`${AuthStore.getApiHost()}/api/meeting_room/${meetingRoom.id}/room_setups`)
                  .send(JSON.stringify(setup))
                  .set(headers)
                  .end((error, response) => {
                    if (!error) {
                      const updatedSetup = JSON.parse(response.text);
                      // userHotel['user'] = user.id;
                      resolve(updatedSetup);
                    } else {
                      console.log('err', err);
                      reject(response);
                    }
                  });
              });
              roomSetupRes.push(eval(`setup${setup.type}`));
            });
            Promise.all(roomSetupRes).then((response) => {
              const updatedRoomSetups = response;
              meetingRoom['room_setups'] = updatedRoomSetups;
              if (callback) {
                console.log('update meetingRoom', meetingRoom);
                callback(meetingRoom);
              }
            });
          } else {
            console.log('err', err);
          }
        });
    });
	},

	reviseFacility(facilityId, params, callback) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/hotel/${params.id}/facilities/${facilityId}`)
      .send(JSON.stringify({ name: params.name }))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const facility = JSON.parse(res.text);
	        facility.tempId = params.tempId;
	        if (callback) {
	        	callback(facility);
	        }
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	createFacility(hotelId, facilityInfo, callback) {
		facilityInfo.map(facility => {
			const params = {
				id: hotelId,
				tempId: facility.tempId,
				name: facility.name,
			};
			if (facility.id && facility.edit) {
				delete facility.edit;
				this.reviseFacility(facility.id, params, callback);
			} else if (facility.id) {
				callback(facility);
			} else {
				if (facility.name) {
					this.updateFacility(hotelId, params, callback);
				}
			}
		});
	},
  reviseMeetingRoom(room, callback) {
    const params = JSON.parse(JSON.stringify(room));
    if (params.assets.length) {
      params.assets = params.assets.map((asset) => {
        return asset.id;
      });
      params.services = params.services.map((service) => {
        return service.id;
      });
    }
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/meeting_room/${room.id}`)
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            const meetingRoom = JSON.parse(res.text);
            meetingRoom['measurement'] = room.measurement;
            const roomSetupRes = [];
            room.room_setups.map((setup) => {
              window[`setup_update${setup.type}`] = new Promise((resolve, reject) => {
                if (setup.id) {
                  request.put(`${AuthStore.getApiHost()}/api/meeting_room/${room.id}/room_setups/${setup.id}`)
                    .send(JSON.stringify(setup))
                    .set(headers)
                    .end((error, response) => {
                      if (!error) {
                        const updatedSetup = JSON.parse(response.text);
                        resolve(updatedSetup);
                      } else {
                        console.log('err', err);
                        reject(response);
                      }
                    });
                } else {
                  request.post(`${AuthStore.getApiHost()}/api/meeting_room/${room.id}/room_setups`)
                    .send(JSON.stringify(setup))
                    .set(headers)
                    .end((error, response) => {
                      if (!error) {
                        const updatedSetup = JSON.parse(response.text);
                        resolve(updatedSetup);
                      } else {
                        console.log('err', err);
                        reject(response);
                      }
                    });
                }
              });
              roomSetupRes.push(eval(`setup_update${setup.type}`));
            });
            Promise.all(roomSetupRes).then((response) => {
              const updatedRoomSetups = response;
              meetingRoom.assets = room.assets;
              meetingRoom['room_setups'] = updatedRoomSetups;
              if (callback) {
                callback(meetingRoom);
              }
            });
          } else {
            console.log('err', err);
          }
        });
    });
  },
  createMeetingRoom(hotelId, meetingRooms, callback) {
    meetingRooms.map((room) => {
      const selectedRoom = room;
      const removeItems = Object.keys(selectedRoom).filter(info => selectedRoom[info] === '');
      removeItems.forEach((element) => {
        delete selectedRoom[element];
      });
      selectedRoom['hotel'] = hotelId;
      if (selectedRoom.dimensions) {
        let dimension = selectedRoom.dimensions.replace(/ /g, '');
        dimension = dimension.toLowerCase().includes('*')
          ? dimension.toLowerCase().split('*') : dimension.toLowerCase().split('x');
        dimension = dimension.map((dim) => {
          dim = `${dim}${selectedRoom.measurement === 'meters' ? 'm' : (selectedRoom.measurement === 'feet' ? 'sq.ft.' : '')}`;
          return dim;
        });
        dimension = dimension.join(' x ');
        selectedRoom.dimensions = dimension;
      }
      delete selectedRoom.tempId;
      // room.room_setups;
      if (room.hasOwnProperty('id') && room.hasOwnProperty('type') && room.type === 'edit') {
        const params = JSON.parse(JSON.stringify(room));
        delete params.type;
        // delete room.tempId;
        this.reviseMeetingRoom(params, (res) => {
          callback(res);
        });
        // addedMeetingRooms.push(room);
      } else {
        this.updateMeetingRoom(hotelId, selectedRoom, (res) => {
          callback(res);
        });
      }
    });
  },
	sortBy(hotelListDetails) {
		AppDispatcher.handleViewAction({
			actionType: 'HOTEL_LIST_SORT',
			hotelListDetails,
		});
	},

	searchBy(name) {
		AppDispatcher.handleViewAction({
			actionType: 'HOTEL_LIST_SEARCH',
			name,
		});
	},

	getNotificationMsg(msg, type) {
		AppDispatcher.handleViewAction({
			actionType: 'GET_NOTIFICATION_MSG',
			msg,
			type,
		});
	},

	sort(meetingRooms, key, sortingOrder, setup, callback) {
		if (key === 'room_space') {
	      meetingRooms = meetingRooms.sort((a, b) => {
	        a = parseFloat(a.surface_meters, 10);
	        b = parseFloat(b.surface_meters, 10);
	        a = isNaN(a) ? 0 : a;
	        b = isNaN(b) ? 0 : b;
	        return a - b;
	      });
	    } else if (key === 'dimensions') {
	    	meetingRooms = meetingRooms.sort((a, b) => {
              a = parseFloat(a.dimensions, 10);
              b = parseFloat(b.dimensions, 10);
              a = isNaN(a) ? 0 : a;
	          b = isNaN(b) ? 0 : b;
              return a - b;
	    	});
	    } else if (key === 'ceiling_height') {
	    	meetingRooms = meetingRooms.sort((a, b) => {
              a = parseFloat(a.ceiling_height, 10);
              b = parseFloat(b.ceiling_height, 10);
              a = isNaN(a) ? 0 : a;
              b = isNaN(b) ? 0 : b;
              return a - b;
	    	});
	    } else if (key === 'setup') {
	    	meetingRooms = meetingRooms.sort((a, b) => {
              a = parseInt(a.room_setups.filter(value => value.name.toLowerCase() === setup.type.toLowerCase())[0].capacity, 10);
              b = parseInt(b.room_setups.filter(value => value.name.toLowerCase() === setup.type.toLowerCase())[0].capacity, 10);
              a = isNaN(a) ? 0 : a;
              b = isNaN(b) ? 0 : b;
              return a - b;
	    	});
	    }

	    if (!sortingOrder) {
	      meetingRooms = meetingRooms.reverse();
	    }

	    if (meetingRooms) {
          if (callback) {
		    callback(meetingRooms);
		  }
	    }
	},

  paginationClick(pageClick) {
    AppDispatcher.handleViewAction({
      actionType: 'HOTEL_LIST_PAGE',
      page: pageClick,
    });
  },
  statusChange(hotel, isActive) {
    const params = {
      status: isActive ? 1 : 2,
    };    
    AuthStore.getHeaders(null, (headers) => {
      headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/hotel/${hotel.id}`)
        .send(JSON.stringify(params))
        .set(headers)
        .end((err, res) => {
          if (!err) {
            console.log('success');
          } else {
            console.log('err', err);
          }
        });
    });
    AppDispatcher.handleServerAction({
      actionType: 'STATUS_CHANGE',
      hotel,
      isActive,
    });
  },

	roomStatusChange(roomId, params, callback) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.patch(`${AuthStore.getApiHost()}/api/meeting_room/${roomId}`)
      .send(JSON.stringify(params))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const updatedRoom = JSON.parse(res.text);
	      	callback(true);
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	deleteMeetingRoom(room, callback) {
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.delete(`${AuthStore.getApiHost()}/api/meeting_room/${room.id}`)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	if (res.ok) {
		      	if (callback) {
		      		callback(room);
		      	}
	      	}
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	readHotel(hotelId, callback) {
		AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}/api/hotel/${hotelId}`)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const hotelInfo = JSON.parse(res.text);
	      	if (callback) {
	      		callback(hotelInfo);
	      	}
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	updateHotel(hotelId, params, callback) {
		const updateFields = ['phone', 'zipcode'];
		const updatedHotelParams = JSON.parse(JSON.stringify(params));
		if(updatedHotelParams.users.length) {
			updatedHotelParams['users'] = updatedHotelParams.users.map(param => {
				return parseInt(param.id, 10);
			});
		}
		const replaceItems = Object.keys(updatedHotelParams).filter(info => !updateFields.includes(info) && updatedHotelParams[info] === '');
	  replaceItems.forEach((element) => {
	    updatedHotelParams[element] = null;
	  });
		AuthStore.getHeaders(null, (headers) => {
			headers['Content-Type'] = 'application/json';
      request.put(`${AuthStore.getApiHost()}/api/hotel/${hotelId}`)
      .send(JSON.stringify(updatedHotelParams))
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	      	const hotelInfo = JSON.parse(res.text);
	      	if (callback) {
	      		callback(hotelInfo);
	      	}
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},

	getHotelListBySearch(findHotel, page=null, callback) { 	
		let url = 	`/api/hotel?name=${findHotel}`
		if (page) {
			url = url+`&page=${page}`
		}
		AuthStore.getHeaders(null, (headers) => {
      request.get(`${AuthStore.getApiHost()}${url}`)
	    .set(headers)
	    .end((err, res) => {
	      if (!err) {
	        const hotelList = JSON.parse(res.text);
	        AppDispatcher.handleServerAction({
				actionType: 'GET_HOTEL_LIST',
				hotelList,
			});
			callback(true);
	      } else {
	        console.log('err', err);
	      }
	    });
    });
	},
};
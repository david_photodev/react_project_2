export default {
  CREATE_EVENT: 'CREATE_EVENT',
  ACTIVITY_PROGRAMS: 'ACTIVITY_PROGRAMS',
  ATTACHMENT_TYPES: 'ATTACHMENT_TYPES',
  ATTACHED_FILE: 'ATTACHED_FILE',
};

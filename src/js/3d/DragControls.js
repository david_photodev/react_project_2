"use strict";

THREE.DragControls = function ( _domElement, _furnitures, _camera, renderer, controls, scene) {

	var _hud = new HUD(this, scene);
	this.hud = _hud;
	var _plane = new THREE.Plane();
	var _raycaster = new THREE.Raycaster();

	var _mouse = new THREE.Vector2();
	var _offset = new THREE.Vector3();
	var _intersection = new THREE.Vector3();
	var _worldPosition = new THREE.Vector3();
	var _inverseMatrix = new THREE.Matrix4();

	var states = {
		UNSELECTED: 0, // no object selected
		SELECTED: 1, // selected but inactive
		DRAGGING: 2, // performing an action while mouse depressed
		ROTATING: 3,  // rotating with mouse down
		ROTATING_FREE: 4, // rotating with mouse up
		PANNING: 5, // not used right now
		DRAG_SELECT: 6 // select multiple items in 2D
	};
	var _state = states.UNSELECTED;

	var _selectedItem = [];
	var _intersectingItem = null;
	var _mouseOverItem = null;
	var _rotateMouseOver = null;

	var _mouseDown = false;
	var _mouseMoved = false;

	var _rotateMouseOver = false;

	var _selected = null, _hovered = null;
	var _highlighted = null;

	var _defaultCursorStyle = 'auto';

	var scope = this;

	var globleIntersection = null;
	var collidingFurnitures = [];
	var roomBoundingBoxes = [];

	this.camera = _camera;
	this.viewer = null;

	this.screen = { left: 0, top: 0, width: 0, height: 0 };

	this.enabled = true;
	this.disposed = false;
	this.dragSelection = false;
	this.mode = 'edit'; // 'select', 'edit',
	this.tool = TOOLS.VIEW;
	this.viewMode = '3D';
	this.touchSelectThresholdTime = 1000; // long press duration in milliseconds

	var _selectionBox = new THREE.Box3();
	var _selectionStart = new THREE.Vector3();


	this.doesIntersectingItemExist = function () {
		if (_intersectingItem) {
			return true;
		} else  {
			return false;
		}
	};

	this.setViewMode = function (mode) {
		scope.viewMode = mode;
		updateDefaultCursor(true);
	};

	this.setCamera = function (camera) {
		scope.camera = camera;
	};

	this.setViewer = function (viewer) {
		scope.viewer = viewer;
	};

	this.setTool = function (tool) {
		this.tool = tool;
		this.enableDragSelection((tool === TOOLS.SELECT));
		updateDefaultCursor(true);
	};

	this.setRoomBoundingBoxes = function (boundingBoxesOfRoom) {
		roomBoundingBoxes = boundingBoxesOfRoom;
	};

	this.enableDragSelection = function (enable) {
		scope.dragSelection = enable;
	};

	this.clearCollidingFurnitures = function () {
		collidingFurnitures = [];
	};

	this.detectCollision = function (firstItem, secondItem) {
		return detectCollision(firstItem, secondItem);
	};

	this.detectCollisionBetweenFurnitures = function () {
		detectCollisionBetweenFurnitures();
	};

	// remove deleted furnitures from colliding list if they are present
	this.removeDeletedFurnituresFromCollidingList = function (furnitures) {
		for (let i = 0; i < furnitures.length; i++) {
			let index = collidingFurnitures.indexOf(furnitures[i]);
			if (index >= 0) {
				collidingFurnitures.splice(index, 1);
			}
		}
		detectCollisionBetweenFurnitures();
	};

	this.updateCollidingFurnitures = function (furnitures) {
		collidingFurnitures = furnitures;
	};

	function init() {

		setGroundPlane();
		activate();

		scope.handleResize();
	}

	function setGroundPlane() {
		// ground plane used to find intersections
		var size = 10000;
		_plane = new THREE.Mesh(
			new THREE.PlaneGeometry(size, size),
			new THREE.MeshBasicMaterial({
				alphaTest: 0, visible: false
			}));
		_plane.rotation.x = -Math.PI / 2;
		// _plane.visible = false;
		scene.add(_plane);
	}

	function activate() {
		attachEventListeners(true);
	}

	function attachEventListeners(attach) {
		var func = attach ? 'addEventListener' : 'removeEventListener';
		_domElement[func]( 'mousemove', onMouseMove, false );
		_domElement[func]( 'mousedown', onMouseDown, false );
		_domElement[func]( 'mouseup', onMouseUp, false );
		_domElement[func]( 'mouseleave', onMouseLeave, false );
		_domElement[func]( 'contextmenu', onContextMenu, false );

		_domElement[func]( 'touchstart', touchstart, false );
		_domElement[func]( 'touchmove', touchmove, false );
		_domElement[func]( 'touchend', touchend, false );

		// window[func]( 'keydown', onKeyDown, false );
		// window[func]( 'keyup', onKeyUp, false );

		// _domElement.addEventListener( 'mouseleave', onDocumentMouseCancel, false );
		// _domElement.addEventListener( 'touchmove', onDocumentTouchMove, false );
		// _domElement.addEventListener( 'touchstart', onDocumentTouchStart, false );
		// _domElement.addEventListener( 'touchend', onDocumentTouchEnd, false );
		// _domElement.addEventListener( 'click', onDocumentMouseClick, false );
	}

	function updateDefaultCursor(set) {
		if (scope.viewMode === '2D' && (scope.tool === TOOLS.RULER || scope.tool === TOOLS.SELECT)) {
			_defaultCursorStyle = 'crosshair';
		} else {
			_defaultCursorStyle = 'auto';
		}
		if (set) {
			setCursorStyle(_defaultCursorStyle);
		}
	}

	function setCursorStyle(style) {
		if ( ! style) {
			style = _defaultCursorStyle;
		}
		_domElement.style.cursor = style;
	}

	function switchState(newState) {
		// console.log("SWTCH_STATE", _state, newState);
		if (newState !== _state) {
			onExit(_state);
			onEntry(newState);
		}
		_state = newState;
		_hud.setRotating(scope.isRotating());
	}

	function onEntry(state) {
		switch (state) {
			case states.UNSELECTED:
				scope.setItemUnSelected(null);
			// controls.setEnabled(true);
			// console.log("Enabled");
			// break;
			case states.SELECTED:
				// controls.setEnabled(true);
				break;
			case states.ROTATING:
			case states.ROTATING_FREE:
				controls.setEnabled(false);
				break;
			case states.DRAGGING:
				setCursorStyle("move");
				clickPressed();
				controls.setEnabled(false);
				break;
			case states.DRAG_SELECT:
				setCursorStyle("crosshair");
				// clickPressed();
				controls.setEnabled(false);
				break;
		}
	}

	function onExit(state) {
		switch (state) {
			case states.UNSELECTED:
			case states.SELECTED:
				break;
			case states.DRAGGING:
				if (_mouseOverItem) {
					setCursorStyle("pointer");
				} else {
					setCursorStyle(_defaultCursorStyle);
				}
				controls.setEnabled(true);
				break;
			case states.ROTATING:
			case states.ROTATING_FREE:
			case states.DRAG_SELECT:
				controls.setEnabled(true);
				break;
		}
	}


	function mouseToVec3(mouse, transform) {
		var vector;
		if (transform) {
			vector = new THREE.Vector3(mouse.x, 0.5, mouse.y);
		} else {
			vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
		}
		vector.unproject(scope.camera);
		return vector;
	}

	this.getIntersectingItem = function (mouse, items) {

		// var vector = mouseToVec3(mouse);

		// var direction = vector.sub(scope.camera.position).normalize();
		// _raycaster.set(scope.camera.position, direction);
		var item = null;

		_raycaster.setFromCamera( mouse, scope.camera );
		_raycaster.linePrecision = 0.1;

		var meshes = items.map(function (obj) {
			return obj.scene;
		});

		var intersections = _raycaster.intersectObjects(meshes, true);
		if (intersections.length) {
			item = Utils.itemForObject3D(intersections[0].object, items);
		}

		// for ( var i = 0, l = objects.length; i < l; i ++ ) {
		//     if(_raycaster.intersectObject( objects[i].scene, true ).length) {
		//         return objects[i];
		//     }
		// }
		return item;
	};

	function updateIntersections() {

		// check the rotate arrow
		var hudObject = _hud.getObject();
		// console.log("hudObject", hudObject);
		if (hudObject != null) {
			var hudIntersects = scope.getIntersections(_mouse, hudObject, true);
			if (hudIntersects.length > 0) {
				if ( ! _rotateMouseOver ) {
					var box = new THREE.Box3();
					box.setFromObject(hudIntersects[0].object);
				}

				_rotateMouseOver = true;
				_hud.setMouseover(true);
				_intersectingItem = null;
				return;
			}
		}
		_rotateMouseOver = false;
		_hud.setMouseover(false);

		_intersectingItem = scope.getIntersectingItem(_mouse, _furnitures);

	}

	this.itemIntersection = function (vec2, items) {
		// @TODO - check if item need to be searched against wall
		// default is to floor (_plane)

		var intersections = []
		if (items[0].snapToWall) {
			intersections = this.getIntersections(vec2, scene.walls, true, true);
		} else {
			intersections = this.getIntersections(vec2, _plane);
		}

		if (intersections.length > 0) {
			return intersections[0];
		} else {
			return null;
		}
	};

	this.getIntersections = function (mouse, objects, recursive, filterByNormals) {

		recursive = recursive || false;
		filterByNormals = filterByNormals || false;

		_raycaster.linePrecision = 20;

		var vector = mouseToVec3(mouse);

		var direction = vector.sub(scope.camera.position).normalize();
		_raycaster.set(scope.camera.position, direction);

		var intersections;
		if (objects instanceof Array) {
			intersections = _raycaster.intersectObjects(objects, recursive);
		} else {
			intersections = _raycaster.intersectObject(objects, recursive);
		}

		// filter by normals, if true
		if (filterByNormals) {
			intersections = Utils.removeIf(intersections, function (intersection) {
				var dot = intersection.face.normal.dot(direction);
				if (dot > 0) {
					console.log("IGNORE WALL", intersection);
				}
				return (dot > 0)
			});
		}

		return intersections;
	};

	// Get first intersecting object
	this.getIntersection = function (mouse, objects) {

	};

	this.setItemSelected = function(item, multi) {
		if (_state === states.UNSELECTED) {
			switchState(states.SELECTED);
		}

		if (item.selected) {
			// already selected
			return;
		}

		if ( ! multi && _selectedItem.length) {
			// un select existing item(s)
			for(var i=0; i<_selectedItem.length; i++) {
				_selectedItem[i].setSelected(false);
			}
			_selectedItem.splice(0);
		}

		_selectedItem.push(item);
		item.setSelected(true);

		// console.log("setItemSelected", item, _selectedItem);

		// if (_state !== states.DRAG_SELECT) {
		//   scope.dispatchEvent( { type: EVENT.ITEM_SELECTED, object: _selectedItem, item: item, multiSelection: multi } );
		// }

	};

	this.setItemUnSelected = function(item, multi) {
		if (item) { // un-select this item
			if ( ! item.selected) {
				return;
			}
			item.setSelected(false);
			var i = _selectedItem.indexOf(item);
			if (i >= 0) {
				_selectedItem.splice(i, 1);
			}
		} else {// un-select all items
			for(var i=0; i<_selectedItem.length; i++) {
				_selectedItem[i].setSelected(false);
			}
			_selectedItem.splice(0);

		}

		// console.log("setItemUnSelected", item, _selectedItem);

		if (_state !== states.DRAG_SELECT) {
			scope.dispatchEvent( { type: EVENT.ITEM_UNSELECTED, object: _selectedItem, item: item } );
		}


	};

	// this.setSelectedItem = function(item, multiSelect) {
	//
	//     if (item !== null && _state === states.UNSELECTED) {
	//         switchState(states.SELECTED);
	//     }
	//
	//     if ( ! multiSelect && _selectedItem.length) {
	//         for(var i=0; i<_selectedItem.length; i++) {
	//             _selectedItem[i].setSelected(false);
	//         }
	//         _selectedItem.splice(0);
	//     }
	//
	//     if (item != null) {
	//         _selectedItem.push(item);
	//         _selectedItem.setSelected(true);
	//         scope.dispatchEvent( { type: EVENT.ITEM_SELECTED, object: _selectedItem } );
	//     } else {
	//         _selectedItem = null;
	//         scope.dispatchEvent( { type: EVENT.ITEM_UNSELECTED, object: _selectedItem } );
	//     }
	// };

	function updateMouseover() {
		if (_intersectingItem !== null) {
			if (_mouseOverItem !== null) {
				if (_mouseOverItem !== _intersectingItem) {
					_mouseOverItem.mouseOff();
					_mouseOverItem = _intersectingItem;
					_mouseOverItem.mouseOver();
				} else {
					// do nothing, mouseover already set
				}
			} else {
				_mouseOverItem = _intersectingItem;
				_mouseOverItem.mouseOver();
				setCursorStyle('pointer');
			}
		} else if (_mouseOverItem != null) {
			_mouseOverItem.mouseOff();
			setCursorStyle(_defaultCursorStyle);
			_mouseOverItem = null;
		}
	}

	function touchstart( event ) {
		if ( ! scope.enabled) return;

		event.preventDefault();
		if (event.touches.length === 1) {
			eventDown(event.touches[ 0 ], true);
			if (_state === states.SELECTED || _state === states.DRAGGING) {
				scope.touchStartTime = new Date().getTime();
				scope.touchStartPoint = event.touches[ 0 ];
				scope.touchTimer = setTimeout(handleTouchTimer, scope.touchSelectThresholdTime);
			}
		} else if (_state !== states.UNSELECTED){
			switchState(states.UNSELECTED);
		}
	}

	function touchmove( event ) {
		if ( ! scope.enabled) return;

		event.preventDefault();
		if (event.touches.length === 1) {
			if (scope.viewMode === '3D') {
				eventMove(event.touches[ 0 ], false);
			} else {
				eventMove(event.touches[ 0 ], true);
			}
			try {
				if (scope.touchTimer) {
					const currentTouch = event.touches[ 0 ];
					const dist = Math.sqrt(
						Math.pow((currentTouch.pageX-scope.touchStartPoint.pageX), 2)
						+ Math.pow((currentTouch.pageY-scope.touchStartPoint.pageY), 2)
					);
					if (dist > 5) {
						clearTimeout(scope.touchTimer);
						scope.touchTimer = null;
					}
				}
			} catch (e) {
				console.error("Error while handling touch move timer", e);
			}

		}
	}

	function touchend( event ) {
		if ( ! scope.enabled) return;

		if (scope.touchTimer) {
			clearTimeout(scope.touchTimer);
			scope.touchTimer = null;
		}

		if (event.touches.length === 0) {
			eventUp(null, true);
		}

	}

	function handleTouchTimer() {
		if (_state === states.SELECTED || _state === states.DRAGGING) {
			if (_selectedItem.length) {
				scope.dispatchEvent({
					type: EVENT.ITEM_SELECTED,
					object: _selectedItem,
					item: _selectedItem[_selectedItem.length - 1],
					multiSelection: (_selectedItem.length > 1)
				});
			}
		}
		scope.touchTimer = null;
	}

	function onMouseDown( event ) {
		if ( ! scope.enabled) return;

		event.preventDefault();
		eventDown(event);

	}

	function onMouseUp( event ) {
		if ( ! scope.enabled) return;

		event.preventDefault();
		// switch (_state) {
		//   case states.DRAGGING:
		//   case states.ROTATING:
		//   case states.ROTATING_FREE:
		//     detectCollisionBetweenFurnitures();
		//     break;
		// }
		onMouseLeave(event);
	}

	function detectCollisionBetweenFurnitures() {
		if (_furnitures === undefined) {
			return;
		}

		var currentCollidingFurnitures = [];

		for (var i = 0; i < collidingFurnitures.length; i++) {
			for (var j = i; j < collidingFurnitures.length; j++) {
				if (i === j) {
					continue;  // same item
				}
				var collided = detectCollision(collidingFurnitures[i], collidingFurnitures[j]);
				if (collided) {
					currentCollidingFurnitures.push(collidingFurnitures[i]);
					currentCollidingFurnitures.push(collidingFurnitures[j]);
				}
			}
		}

		currentCollidingFurnitures = Utils.deDuplicateArray(currentCollidingFurnitures);

		var nonCollidingFurnitures = collidingFurnitures.filter(function(i) {
			return currentCollidingFurnitures.indexOf(i) < 0;
		});

		for (var i = 0; i < nonCollidingFurnitures.length; i++) {
			nonCollidingFurnitures[i].setColliding(false);
		}

		let filteredArray = _furnitures.filter(function(i) {
			return _selectedItem.indexOf(i) < 0;
		});

		for (var i = 0; i < _selectedItem.length; i++) {
			for (var j = 0; j < filteredArray.length; j++) {
				var collided = detectCollision(_selectedItem[i], filteredArray[j]);
				if (collided) {
					currentCollidingFurnitures.push(_selectedItem[i]);
					currentCollidingFurnitures.push(filteredArray[j]);
				}
			}
		}

		currentCollidingFurnitures = Utils.deDuplicateArray(currentCollidingFurnitures); /* we can make a list of previous colliding furnitures and
		 new colliding furnitures and setColliding only newColliding furnitures */

	 for (var i = 0; i < currentCollidingFurnitures.length; i++) {
			currentCollidingFurnitures[i].setColliding(true);
		}

		collidingFurnitures = currentCollidingFurnitures;

	 // also highlight out of bound furnitures
		detectOutOfBoundFurnitures();
	}

	function detectOutOfBoundFurnitures() {
		if ( ! _furnitures) {
			return;
		}

		for (var i=0, l=_furnitures.length; i<l; i++) {
			var furniture = _furnitures[i];
			furniture.setOutOfBound(isItemOutOfBound(furniture));
		}

	}

	/**
	 * We could check for item bounding box against fcarpets,
	 * but checking just the center to improve performance
	 */
	function isItemOutOfBound(item) {
		var ibox = item.getBox3();

		// check if in meeeting room
		if (scope.viewer.fbox) {
			if ( ! scope.viewer.fbox.containsItemBox(ibox)) {
				return true;  // completely out of meeting room
			}
		}

		// check if inside a fcarpet
		var fcarpets = scope.viewer.fcarpets;
		if ( ! fcarpets || ! fcarpets.length) {
			return false;  // no fcarpets set
		}
		for (var i=0; i<fcarpets.length; i++) {
			if (fcarpets[i].containsItemBox(ibox)) {
				return false;
			}
		}

		return true;  // out of all the fcarpets
	}

	var cRay = new THREE.Raycaster();
	var cRayDir = new THREE.Vector3();
	function detectCollision(firstItem, secondItem) {
		if (firstItem === secondItem) {
			return false; // for collision detection two items must be separate
		}

		// var minDistance = (firstItem.getSize().x+secondItem.getSize().x)/2;
		// var dist = firstItem.scene.position.distanceTo(secondItem.scene.position);
		// if (dist < minDistance) {
		//   return true;
		// }
		// return false;

		var start = firstItem.getCenter();
		var end = secondItem.getCenter();
		cRayDir.copy(end).sub(start).normalize();
		cRay.set(start, cRayDir);
		firstItem.convexHull.updateMatrixWorld();
		secondItem.convexHull.updateMatrixWorld();

		var secondItemInter = cRay.intersectObject(secondItem.convexHull);
		if ( ! secondItemInter.length) {
			// no intersection with second item

			var firstItemBoundingBox = firstItem.getBox3();
			var secondItemBoundingBox = secondItem.getBox3();

			if ((firstItem.convexHull.position.equals(secondItem.convexHull.position)) && (firstItemBoundingBox.equals(secondItemBoundingBox))) {
				return true;
			}

			return false;
		}

		if(start.distanceTo(end) < secondItemInter[0].distance) {
			// distance to center of second item is less than its intersection => start is inside second item
			return true;
		}

		var firstItemInter = cRay.intersectObject(firstItem.convexHull);
		if ( ! firstItemInter.length) return false;

		var delta = secondItemInter[0].distance - firstItemInter[firstItemInter.length-1].distance;
		if (delta < 0) {
			return true
		}

		// var intersects = cRay.intersectObjects([firstItem.convexHull, secondItem.convexHull]);
		// if (intersects.length > 1) {
		//   if (intersects[0].object === secondItem.convexHull) {
		//     console.log("VH: COLLITION: intersect 2nd mesh first");
		//     return true;
		//   }
		//   if(intersects[1].distance-intersects[0].distance < 0) {
		//     console.log("VH: COLLITION: No min gap");
		//     return true;
		//   }
		// }
		return false;

		// return firstItem.getBox3().intersectsBox(secondItem.getBox3());
	}

	function onMouseLeave( event ) {
		if ( ! scope.enabled) return;

		eventUp(event);

	}

	function onMouseMove( event ) {
		if ( ! scope.enabled) return;

    // avoid e.preventDefault when mousemove event is triggered by DragDropTouch library
    if ( ! event.isDDT) {
      // console.log("DC:onMouseMove: preventDefault isDDT", event.isDDT);
      event.preventDefault();
    }

		eventMove(event, true);

	}

	function eventDown(event, isTouchEvent) {

		_mouseMoved = false;
		_mouseDown = true;

		setMouse(event);
		updateIntersections();

		switch (_state) {
			case states.SELECTED:
				if (_rotateMouseOver) {
					switchState(states.ROTATING);
				} else if (_intersectingItem != null) {
					scope.setItemSelected(_intersectingItem, (event.ctrlKey || event.metaKey));
					if (_intersectingItem.drag.enabled) {
						switchState(states.DRAGGING);
					}
				} else if (scope.dragSelection && ! (event.ctrlKey || event.metaKey)) {
					startDragSelection(event);
					switchState(states.DRAG_SELECT);
				}
				break;
			case states.UNSELECTED:
				if (_intersectingItem != null) {
					scope.setItemSelected(_intersectingItem, (event.ctrlKey || event.metaKey));
					if (_intersectingItem.drag.enabled) {
						switchState(states.DRAGGING);
					}
				} else if (scope.dragSelection && ! (event.ctrlKey || event.metaKey)) {
					startDragSelection(event);
					switchState(states.DRAG_SELECT);
				}
				break;
			case states.DRAGGING:
			case states.ROTATING:
				break;
			case states.ROTATING_FREE:
				switchState(states.SELECTED);
				break;
			case states.DRAG_SELECT:
				startDragSelection(event);
				break;
		}
	}

	function eventMove(event, dragEnabled) {

		_mouseMoved = true;

		setMouse(event);

		if ( ! _mouseDown) {
			updateIntersections();
		}

		switch (_state) {
			case states.UNSELECTED:
				updateMouseover();
				break;
			case states.SELECTED:
				updateMouseover();
				break;
			case states.DRAGGING:
				// dragging not allowed on touch screens
				if (dragEnabled) {
					clickDragged();
					_hud.update();
				}
				break;
			case states.ROTATING:
			case states.ROTATING_FREE:
				clickDragged();
				_hud.update();
				break;
			case states.DRAG_SELECT:
				if (_mouseDown) {
					updateDragSelection( event );
				}
				break;
		}
	}

	function eventUp(event, isTouchEvent) {
		_mouseDown = false;
		var currentTime = new Date().getTime();

		switch (_state) {
			case states.DRAGGING:
				for (var i=0; i<_selectedItem.length; i++) {
					_selectedItem[i].clickReleased();
				}
				switchState(states.SELECTED);
				if (_selectedItem.length) {
					if ( ! isTouchEvent) {
						scope.dispatchEvent( {
							type: EVENT.ITEM_SELECTED,
							object: _selectedItem,
							item: _selectedItem[_selectedItem.length-1],
							multiSelection: (_selectedItem.length>1)
						} );
					}
				}
				detectCollisionBetweenFurnitures();
				scope.dispatchEvent({type: EVENT.ITEM_DRAG_END, object: _selectedItem});
				break;
			case states.ROTATING:
				if (!_mouseMoved) {
					switchState(states.ROTATING_FREE);
				} else {
					switchState(states.SELECTED);
				}
				detectCollisionBetweenFurnitures();
				break;
			case states.UNSELECTED:
				if (!_mouseMoved) {
					checkWallsAndFloors();
				}
				break;
			case states.SELECTED:
				if (_intersectingItem == null && !_mouseMoved) {
					switchState(states.UNSELECTED);
					checkWallsAndFloors();
				} else if (_selectedItem.length) {
					if ( ! isTouchEvent ) {
						scope.dispatchEvent( {
							type: EVENT.ITEM_SELECTED,
							object: _selectedItem,
							item: _selectedItem[_selectedItem.length-1],
							multiSelection: (_selectedItem.length>1)
						} );
					} else {
						console.log("IGNORING TOUCH_UP", currentTime - scope.touchStartTime);
					}
				}
				break;
			case states.ROTATING_FREE:
				detectCollisionBetweenFurnitures();
				break;
			case states.DRAG_SELECT:
				endDragSelection();
				break;
		}
	}

	function onContextMenu( event ) {
		if ( ! scope.enabled) return;

		event.preventDefault();
	}

	// drag selection
	function createDragSelector() {

		scope.selector = document.createElement('div');
		scope.selector.className = 'drag-selection-3d';
		_domElement.appendChild(scope.selector);

	}

	var _dragStart = new THREE.Vector2();
	var _dragEnd = new THREE.Vector2();
	function startDragSelection( event ) {
		if ( ! scope.selector) {
			createDragSelector();
		}

		_dragStart.copy(getMouseOnScreen(event.pageX, event.pageY, true));
		_dragEnd.copy(_dragStart);

		// unselect all
		scope.setItemUnSelected(null);

		// update existing furniture boxes
		for (var i=0, l=_furnitures.length; i<l; i++) {
			_furnitures[i].updateSize(true);
			// if (_furnitures[i].needsSizeUpdate) {
			//   _furnitures[i].updateSize(true);
			// }
		}

		let intersections = scope.getIntersections(_mouse, _plane);
		if (intersections.length) {
			_selectionStart = intersections[0].point;
		}


		// _selectionStart = mouseToVec3(_mouse);

		// console.log("DRAG_START _mouse", JSON.stringify(_mouse));
		// console.log("DRAG_START _selectionStart", JSON.stringify(_selectionStart));

		_selectionBox.min.copy(_selectionStart);
		_selectionBox.max.set(_selectionStart.x, _camera.position.y, _selectionStart.z);

		updateSelectionBox(true);

	}

	function updateDragSelection( event ) {

		_dragEnd.copy(getMouseOnScreen(event.pageX, event.pageY, true));

		let intersections = scope.getIntersections(_mouse, _plane);
		if (intersections.length) {
			var end = intersections[0].point;

			if (end.x > _selectionStart.x) {
				_selectionBox.max.x = end.x;
			} else {
				_selectionBox.min.x = end.x;
				_selectionBox.max.x = _selectionStart.x;
			}

			if (end.z > _selectionStart.z) {
				_selectionBox.max.z = end.z;
			} else {
				_selectionBox.min.z = end.z;
				_selectionBox.max.z = _selectionStart.z;
			}
		}

		updateSelectionBox(true);

		// var end = mouseToVec3(_mouse);
		// _selectionBox.max.copy(end);

		selectFromBox();
	}

	function endDragSelection() {
		updateSelectionBox(false);
		if (_selectedItem.length) {
			scope.dispatchEvent( { type: EVENT.ITEM_SELECTED, object: _selectedItem, item: null, multiSelection: true } );
		}
		if (_selectedItem.length) {
			switchState(states.SELECTED);
		} else {
			switchState(states.UNSELECTED);
		}
	}

	function selectFromBox() {

		for (var i=0, l=_furnitures.length; i<l; i++) {
			var item = _furnitures[i];

			if (_selectionBox.intersectsBox(item.box3)) {
				// console.log("selectFromBox: Y ", item.box3, _selectionBox);
				scope.setItemSelected(item, true);
			} else {
				// console.log("selectFromBox: N ", item.box3, _selectionBox);
				scope.setItemUnSelected(item, true);
			}
		}

	}

	function unselectFromBox() {
		for (var i=0, l=_furnitures.length; i<l; i++) {
			scope.setItemUnSelected(_furnitures[i], true);
		}
	}

	function updateSelectionBox(visible) {

		if ( ! scope.selector) {
			return;
		}

		if (visible) {

			scope.selector.style.left = Math.min(_dragEnd.x, _dragStart.x) + "px";
			scope.selector.style.top = Math.min(_dragEnd.y, _dragStart.y) + "px";
			scope.selector.style.width = Math.abs(_dragEnd.x-_dragStart.x) + "px";
			scope.selector.style.height = Math.abs(_dragEnd.y-_dragStart.y) + "px";
			scope.selector.style.visibility = "visible";

		} else {
			scope.selector.style.visibility = "hidden";
		}
	}

	function setMouse( event ) {

		// _mouse.x = event.clientX;
		// _mouse.y = event.clientY;

		// var rect = _domElement.getBoundingClientRect();
		//
		// _mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
		// _mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

		// var widthMargin = $(_domElement).offset().left;
		// var heightMargin = $(_domElement).offset().top;
		//
		// _mouse.x = ((event.clientX - widthMargin) / (window.innerWidth - widthMargin)) * 2 - 1;
		// _mouse.y = -((event.clientY - heightMargin) / (window.innerHeight - heightMargin)) * 2 + 1;

		_mouse.copy( getMouseOnScreen( event.pageX, event.pageY ) );

	}

	this.handleResize = function () {

		if ( _domElement === document ) {

			this.screen.left = 0;
			this.screen.top = 0;
			this.screen.width = window.innerWidth;
			this.screen.height = window.innerHeight;

		} else {

			var box = _domElement.getBoundingClientRect();
			// adjustments come from similar code in the jquery offset() function
			var d = _domElement.ownerDocument.documentElement;
			this.screen.left = box.left + window.pageXOffset - d.clientLeft;
			this.screen.top = box.top + window.pageYOffset - d.clientTop;
			this.screen.width = box.width;
			this.screen.height = box.height;

		}

	};

	var getMouseOnScreen = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnScreen( pageX, pageY, inDOM ) {

			if (inDOM) {
				vector.set(
					( pageX - scope.screen.left ),
					( pageY - scope.screen.top )
				);
			} else {
				// mouse co-ordinates normalized to the range -1 to 1
				// Top left corner of the domElement is (-1, 1) and center is (0, 0)
				vector.set(
					(( pageX - scope.screen.left ) / scope.screen.width) * 2 - 1,
					-(( pageY - scope.screen.top ) / scope.screen.height) * 2 + 1
				);
			}


			return vector;

		};

	}() );

	function clickPressed(vec2) {
		vec2 = vec2 || _mouse;
		var intersection = scope.itemIntersection(_mouse, _selectedItem);
		if (intersection) {
			for(var i=0; i<_selectedItem.length; i++) {
				_selectedItem[i].clickPressed(intersection);
			}
		}
	}

	function clickDragged(vec2) { // rotate items if state is ROTATE or ROTATE_FREE or move if state is DRAGGING
		vec2 = vec2 || _mouse;

		var fire = false;

		var intersection = scope.itemIntersection(_mouse, _selectedItem);
		if (intersection) {
			if (scope.isRotating()) {
				for(var i=0; i<_selectedItem.length; i++) {
					_selectedItem[i].rotate(intersection);
				}
				scope.dispatchEvent({type: EVENT.ITEM_ROTATED, object: _selectedItem})
			} else {

				var goingOutOfBound = doItemsGoOutOfBounds(intersection); // if any item intersects with the walls do not move
				if (goingOutOfBound) {
					for (var i = 0; i < _selectedItem.length; i++) {
						_selectedItem[i].clickDragged(globleIntersection);
					}
				} else {
					for(var i=0; i<_selectedItem.length; i++) {
						_selectedItem[i].clickDragged(intersection);
					}
				}
				scope.dispatchEvent({type: EVENT.ITEM_DRAG_MOVE, object: _selectedItem});
			}
		}
	}

	function doItemsGoOutOfBounds(intersection) {
		return false;  // allow moving furniture anywhere. Later we will just highlight any out of bound furnitures

		if (!roomBoundingBoxes.length) {
			return false;
		}

		var goingOutOfBound = false;
		var endPosition = null;

		for (var i = 0; i < _selectedItem.length; i++) {

			var selectedItem = _selectedItem[i];
			var selectedItemSize = selectedItem.getSize();

			var roomBoundingBox = new THREE.Box3();
			var oldRoomBoundingBox = roomBoundingBox;
			for (var j = 0; j < roomBoundingBoxes.length; j++) {
				var boundingBox = roomBoundingBoxes[j];
				if (boundingBox.intersectsBox(selectedItem.getBox3())) {
					roomBoundingBox = boundingBox;
				}
			}

			if (oldRoomBoundingBox === roomBoundingBox) { // if can not find bounding box of selected items then return
				return false;
			}

			var pos = intersection.point.clone().sub(selectedItem.dragOffset.clone()); // future position

			var itemWidthInXDirection = selectedItemSize.x * Math.abs(Math.cos(selectedItem.scene.rotation.y)) + selectedItemSize.z * Math.abs(Math.sin(selectedItem.scene.rotation.y));
			var itemLengthInZDirection = selectedItemSize.z * Math.abs(Math.cos(selectedItem.scene.rotation.y)) + selectedItemSize.x * Math.abs(Math.sin(selectedItem.scene.rotation.y));

			var xAxisIntersected = (pos.x + itemWidthInXDirection / 2) > roomBoundingBox.max.x;
			var negativeXAxisIntersected = (pos.x - itemWidthInXDirection / 2) < roomBoundingBox.min.x;
			var zAxisIntersected = (pos.z + itemWidthInXDirection / 2) > roomBoundingBox.max.z;
			var negativeZAxisIntersected = (pos.z - itemLengthInZDirection / 2) < roomBoundingBox.min.z;

			var isIntersected = xAxisIntersected || negativeXAxisIntersected || zAxisIntersected || negativeZAxisIntersected;

			if (isIntersected) {

				globleIntersection = Object.assign({}, intersection);
				endPosition = Object.assign({}, pos);

				if (xAxisIntersected) {
					if (endPosition.x <= pos.x) {
						endPosition.x = pos.x;
						globleIntersection.point.x = roomBoundingBox.max.x - itemWidthInXDirection / 2 + selectedItem.dragOffset.x;
					}
				}

				if (negativeXAxisIntersected) {
					if (endPosition.x >= pos.x) {
						endPosition.x = pos.x;
						globleIntersection.point.x = roomBoundingBox.min.x + itemWidthInXDirection / 2 + selectedItem.dragOffset.x;
					}
				}

				if (zAxisIntersected) {
					if (endPosition.z <= pos.z) {
						endPosition.z = pos.z;
						globleIntersection.point.z = roomBoundingBox.max.z - itemLengthInZDirection / 2 + selectedItem.dragOffset.z;
					}
				}

				if (negativeZAxisIntersected) {
					if (endPosition.z >= pos.z) {
						endPosition.z = pos.z;
						globleIntersection.point.z = roomBoundingBox.min.z + itemLengthInZDirection / 2 + selectedItem.dragOffset.z;
					}
				}

				goingOutOfBound = true;
			}

		}

		return goingOutOfBound;
	}

	this.isRotating = function () {
		return (_state === states.ROTATING || _state  === states.ROTATING_FREE);
	};

	this.getState = function(){
		return _state;
	};

	function checkWallsAndFloors() {
		// @TODO - check boundries
	}

	this.render = function(renderer_, camera_) {
		if (_hud) {
			// console.log("RENDER_HUD", _hud.getScene(), camera_);
			renderer_.render(_hud.getScene(), scope.camera);
		}
	};

	this.dispose = function() {
		if (this.disposed) {
			return;
		}

		this.disposed = true;
		this.enabled = false;

		attachEventListeners(false);

		this.hud = null;
		if(_plane.parent) {
			_plane.parent.remove(_plane);
		}
		_plane = null;
		_selectedItem.splice(0);
		_intersectingItem = null;
		_mouseOverItem = null;
		this.viewer = null;
		this.camera = null;
		scope = null;
		_domElement = null;
		_furnitures = null;
		_camera = null;
		renderer = null;
		controls = null;
		scene = null;

		_selectedItem = [];
		_intersectingItem = null;
		_mouseOverItem = null;

		globleIntersection = null;
		collidingFurnitures = [];
		roomBoundingBoxes = [];
	};

	init();

	// function deactivate() {
	//
	// 	_domElement.removeEventListener( 'mousemove', onDocumentMouseMove, false );
	// 	_domElement.removeEventListener( 'mousedown', onDocumentMouseDown, false );
	// 	_domElement.removeEventListener( 'mouseup', onDocumentMouseCancel, false );
	// 	_domElement.removeEventListener( 'mouseleave', onDocumentMouseCancel, false );
	// 	_domElement.removeEventListener( 'touchmove', onDocumentTouchMove, false );
	// 	_domElement.removeEventListener( 'touchstart', onDocumentTouchStart, false );
	// 	_domElement.removeEventListener( 'touchend', onDocumentTouchEnd, false );
	//
	// }
	//
	// function dispose() {
	//
	// 	deactivate();
	//
	// }
	//
	// function onDocumentMouseMove( event ) {
	//
	// 	event.preventDefault();
	//
	// 	var rect = _domElement.getBoundingClientRect();
	//
	// 	_mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
	// 	_mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;
	//
	// 	_raycaster.setFromCamera( _mouse, scope.camera );
	//
	// 	if ( _selected && scope.enabled ) {
	//
	// 		if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {
	//
	//             var pos = _intersection.sub( _offset ).applyMatrix4( _inverseMatrix );
	//             if (_selected.drag.x) _selected.scene.position.x = pos.x;
	//             if (_selected.drag.y) _selected.scene.position.y = pos.y;
	//             if (_selected.drag.z) _selected.scene.position.z = pos.z;
	//
	// 			// _selected.scene.position.copy( _intersection.sub( _offset ).applyMatrix4( _inverseMatrix ) );
	//
	// 		}
	//
	// 		scope.dispatchEvent( { type: 'drag', object: _selected } );
	//
	// 		return;
	//
	// 	}
	//
	// 	_raycaster.setFromCamera( _mouse, scope.camera );
	//
	// 	// var intersects = _raycaster.intersectObjects( _furnitures );
	//     var object = getIntersectingObject();
	//
	// 	// if ( intersects.length > 0 ) {
	// 	if ( object ) {
	//
	// 		// var object = intersects[ 0 ].object;
	//
	// 		_plane.setFromNormalAndCoplanarPoint( scope.camera.getWorldDirection( _plane.normal ), _worldPosition.setFromMatrixPosition( object.scene.matrixWorld ) );
	//
	// 		if ( _hovered !== object ) {
	//
	// 			scope.dispatchEvent( { type: 'hoveron', object: object } );
	//
	// 			_domElement.style.cursor = 'pointer';
	// 			_hovered = object;
	//
	// 		}
	//
	// 	} else {
	//
	// 		if ( _hovered !== null ) {
	//
	// 			scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );
	//
	// 			_domElement.style.cursor = 'auto';
	// 			_hovered = null;
	//
	// 		}
	//
	// 	}
	//
	// }
	//
	// function getIntersectingObject() {
	//     for ( var i = 0, l = _furnitures.length; i < l; i ++ ) {
	//         if (_furnitures[i].drag.enabled) {
	//             if(_raycaster.intersectObjects( [_furnitures[i].scene], true ).length > 0) {
	//                 return _furnitures[i];
	//             }
	//         }
	//     }
	//     return null;
	// }
	//
	// function onDocumentMouseDown( event ) {
	//
	// 	event.preventDefault();
	//
	// 	_raycaster.setFromCamera( _mouse, scope.camera );
	//
	// 	// var intersects = _raycaster.intersectObjects( _furnitures );
	//     var object = getIntersectingObject();
	//
	// 	// if ( intersects.length > 0 ) {
	// 	if ( object ) {
	//
	// 		_selected = object;  // intersects[ 0 ].object;
	//
	// 		if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {
	//
	// 			_inverseMatrix.getInverse( _selected.scene.parent.matrixWorld );
	// 			_offset.copy( _intersection ).sub( _worldPosition.setFromMatrixPosition( _selected.scene.matrixWorld ) );
	//
	// 		}
	//
	// 		_domElement.style.cursor = 'move';
	//
	// 		scope.dispatchEvent( { type: 'dragstart', object: _selected } );
	//
	// 	}
	//
	//
	// }
	//
	// function onDocumentMouseCancel( event ) {
	//
	// 	event.preventDefault();
	//
	// 	if ( _selected ) {
	//
	// 		scope.dispatchEvent( { type: 'dragend', object: _selected } );
	//
	// 		_selected = null;
	//
	// 	}
	//
	// 	_domElement.style.cursor = _hovered ? 'pointer' : 'auto';
	//
	// }
	//
	// function onDocumentMouseClick( event ) {
	//     event.preventDefault();
	//
	//     if ( ! _highlighted) {
	//         _raycaster.setFromCamera( _mouse, scope.camera );
	//         var object = getIntersectingObject();
	//         if (object) {
	//             _highlighted = object;
	//
	//             scope.dispatchEvent( { type: 'selected', object: _highlighted } );
	//
	//             console.log("CLICK_SELECTED", _highlighted);
	//         }
	//     } else if (_highlighted) {
	//         scope.dispatchEvent( { type: 'deselected', object: _highlighted } );
	//         console.log("CLICK_DESELECTED", _highlighted);
	//         _highlighted = null;
	//     }
	// }
	//
	// function onDocumentTouchMove( event ) {
	//
	// 	event.preventDefault();
	// 	event = event.changedTouches[ 0 ];
	//
	// 	var rect = _domElement.getBoundingClientRect();
	//
	// 	_mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
	// 	_mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;
	//
	// 	_raycaster.setFromCamera( _mouse, scope.camera );
	//
	// 	if ( _selected && scope.enabled ) {
	//
	// 		if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {
	//
	// 			_selected.scene.position.copy( _intersection.sub( _offset ).applyMatrix4( _inverseMatrix ) );
	//
	// 		}
	//
	// 		scope.dispatchEvent( { type: 'drag', object: _selected } );
	//
	// 		return;
	//
	// 	}
	//
	// }
	//
	// function onDocumentTouchStart( event ) {
	//
	// 	event.preventDefault();
	// 	event = event.changedTouches[ 0 ];
	//
	// 	var rect = _domElement.getBoundingClientRect();
	//
	// 	_mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
	// 	_mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;
	//
	// 	_raycaster.setFromCamera( _mouse, scope.camera );
	//
	// 	var intersects = _raycaster.intersectObjects( _furnitures );
	//
	// 	if ( intersects.length > 0 ) {
	//
	// 		_selected = intersects[ 0 ].object;
	//
	// 		_plane.setFromNormalAndCoplanarPoint( scope.camera.getWorldDirection( _plane.normal ), _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );
	//
	// 		if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {
	//
	// 			_inverseMatrix.getInverse( _selected.parent.matrixWorld );
	// 			_offset.copy( _intersection ).sub( _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );
	//
	// 		}
	//
	// 		_domElement.style.cursor = 'move';
	//
	// 		scope.dispatchEvent( { type: 'dragstart', object: _selected } );
	//
	// 	}
	//
	//
	// }
	//
	// function onDocumentTouchEnd( event ) {
	//
	// 	event.preventDefault();
	//
	// 	if ( _selected ) {
	//
	// 		scope.dispatchEvent( { type: 'dragend', object: _selected } );
	//
	// 		_selected = null;
	//
	// 	}
	//
	// 	_domElement.style.cursor = 'auto';
	//
	// }
	//
	// activate();

	// API

	// this.activate = activate;
	// this.deactivate = deactivate;
	// this.dispose = dispose;

	// Backward compatibility

	// this.setObjects = function () {
	//
	// 	console.error( 'THREE.DragControls: setObjects() has been removed.' );
	//
	// };
	//
	// this.on = function ( type, listener ) {
	//
	// 	console.warn( 'THREE.DragControls: on() has been deprecated. Use addEventListener() instead.' );
	// 	scope.addEventListener( type, listener );
	//
	// };
	//
	// this.off = function ( type, listener ) {
	//
	// 	console.warn( 'THREE.DragControls: off() has been deprecated. Use removeEventListener() instead.' );
	// 	scope.removeEventListener( type, listener );
	//
	// };
	//
	// this.notify = function ( type ) {
	//
	// 	console.error( 'THREE.DragControls: notify() has been deprecated. Use dispatchEvent() instead.' );
	// 	scope.dispatchEvent( { type: type } );
	//
	// };

};


THREE.DragControls.EVENT = {

};

THREE.DragControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.DragControls.prototype.constructor = THREE.DragControls;


var HUD = function (controls, scene) {
	var scope = this;
	var _controls = controls;
	// var scene = scene || new THREE.Scene();
	var scene = new THREE.Scene();
	var selectedItem = null;
	var rotating = false;
	var mouseover = false;
	var tolerance = 10;
	var height = 5;
	var distance = 20;
	var color = "#0a0cc0";
	var hoverColor = "#ff0000"; //"#f1c40f";
	var activeObject = null;

	this.getScene = function () {
		return scene;
	};

	this.getObject = function () {
		return activeObject;
	};

	function init() {
		// _controls.addEventListener( EVENT.ITEM_SELECTED, itemSelected );
		// _controls.addEventListener( EVENT.ITEM_UNSELECTED, itemUnselected );
	}

	function resetSelectedItem() {
		selectedItem = null;
		if (activeObject) {
			scene.remove(activeObject);
			activeObject = null;
		}
	}

	function itemSelected(event) {
		var item = event.object;
		if (selectedItem !== item) {
			resetSelectedItem();
			if (item.allowRotate && !item.fixed) {
				selectedItem = item;
				activeObject = makeObject(selectedItem);
				scene.add(activeObject);
			}
		}
	}

	function itemUnselected(event) {
		resetSelectedItem();
	}

	this.setRotating = function (isRotating) {
		rotating = isRotating;
		setColor();
	};

	this.setMouseover = function (isMousedOver) {
		mouseover = isMousedOver;
		setColor();
	};

	function setColor() {
		if (activeObject) {
			activeObject.children.forEach(function (obj) {
				obj.material.color.set(getColor());
				obj.material.needsUpdate = true;
			});
		}
		// _viewer.needsUpdate();
	}

	function getColor() {
		return (mouseover || rotating) ? hoverColor : color;
	}

	this.update = function () {
		if (activeObject) {
			activeObject.rotation.y = selectedItem.scene.rotation.y;
			activeObject.position.x = selectedItem.scene.position.x;
			activeObject.position.z = selectedItem.scene.position.z;
		}
	};

	function makeLineGeometry(item) {
		var geometry = new THREE.Geometry();
		geometry.vertices.push(new THREE.Vector3(0, 0, 0), rotateVector(item));
		return geometry;
	}

	function rotateVector(item) {
		var halfSize = item.getHalfSize();
		var vec = new THREE.Vector3(0, 0, Math.max(halfSize.x, halfSize.z) + 1.4 + distance*PIXEL_TO_SCENE);
		return vec;
	}

	function makeLineMaterial(rotating) {
		var mat = new THREE.LineBasicMaterial({
			color: getColor(),
			linewidth: 3
		});
		return mat;
	}

	function makeLine(item) {

		var start = new THREE.Vector3();
		var end = rotateVector(item);



		var width = 1*PIXEL_TO_SCENE;
		var length = end.distanceTo(start);

		var geometry = new THREE.BoxGeometry(width, width, length);
		var material = new THREE.MeshBasicMaterial({
			color: getColor()
		});
		var cube = new THREE.Mesh(geometry, material);

		// position the cube
		cube.position.x = width/2; //-4;
		cube.position.y = width/2; //3;
		cube.position.z = length/2;

		return cube;
	}

	function makeCone(item) {
		var coneGeo = new THREE.CylinderGeometry(5*PIXEL_TO_SCENE, 0, 10*PIXEL_TO_SCENE);
		var coneMat = new THREE.MeshBasicMaterial({
			color: getColor()
		});
		var cone = new THREE.Mesh(coneGeo, coneMat);
		cone.position.copy(rotateVector(item));
		cone.rotation.x = -Math.PI / 2.0;
		return cone;
	}

	function makeSphere(item) {
		var geometry = new THREE.SphereGeometry(4*PIXEL_TO_SCENE, 16, 16);
		var material = new THREE.MeshBasicMaterial({
			color: getColor()
		});
		var sphere = new THREE.Mesh(geometry, material);
		return sphere;
	}

	function makeObject(item) {
		var object = new THREE.Object3D();
		// var line = new THREE.Line(makeLineGeometry(item), makeLineMaterial(scope.rotating), THREE.LineSegments);

		var line = makeLine(item);
		var cone = makeCone(item);
		var sphere = makeSphere(item);
		object.add(line);
		object.add(cone);
		object.add(sphere);

		console.log("makeObject", item.scene.rotation.y, item.scene);

		object.rotation.y = item.scene.rotation.y;
		object.position.x = item.scene.position.x;
		object.position.z = item.scene.position.z;
		object.position.y = height*PIXEL_TO_SCENE;
		return object;
	}
	init();
}

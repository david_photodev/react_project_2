"use strict";

var Layout = function (options) {
	this.options = this.options || {};
	Object.assign(this.options, (options || {}));
};
Layout.prototype.setModels = function (models) {
	this.models = models;
};

Layout.prototype.setObjectOnFloor = function (object, pos, itemBox) {
	var box = itemBox || new THREE.Box3().setFromObject(object);
	pos.y -= box.min.y;
	object.position.copy(pos);

	// console.log("OBJECT: Box: ", JSON.stringify(box), " POS:", JSON.stringify(pos));
};

Layout.prototype.addItem = function (noOfItems, itemModel, itemType, itemSize, itemPosition, rotationAngle, containerToAdd, itemID, directionToMove, itemCounter, maxItems) {

	if (itemCounter >= maxItems) {
		console.log("addItem: itemCounter >= maxItems", itemCounter, maxItems);
		return itemCounter;
	}

	for (var i=0; i < noOfItems; i++) {
				var item = Utils.cloneObject3D(itemModel.scene);
				if (itemType === "chair") {
						item.name = "CH-" + itemID;
				} else {
						item.name = "TA-" + itemID;
				}

				item.position.copy(itemPosition);

				item.rotation.y = (rotationAngle);

				if (itemType == "chair") {
					var iItem = new ChairItem(item, itemModel.data);
				} else {
					var iItem = new TableItem(item, itemModel.data);
				}

				containerToAdd.push(iItem);

				if (directionToMove == "forward") { //directions are according to the user's point of view
						itemPosition.z -= itemSize;
				} else if (directionToMove == "backward") {
						itemPosition.z += itemSize;
				} else if (directionToMove == "left") {
						itemPosition.x -= itemSize;
				} else {
						itemPosition.x += itemSize;
				}

				itemID++;

				itemCounter++;
				if (itemCounter === maxItems) {
					console.log("addItem: itemCounter === maxItems", maxItems);
					break;
				}

		}

		return itemCounter;
};

var LayoutFactory = function () {
};

LayoutFactory.load = function (options, peopleCount, tableCount, chairsPerTable, models, fcarpets, getCount) {

	let layout = null;
	switch (options.type.toLowerCase()) {
		case 'theatre':
			layout = new TheatreLayout(options);
			break;

		case 'classroom':
			layout = new ClassroomLayout(options);
			break;

		case 'boardroom':
			layout = new BoardroomLayout(options);
			break;

		case 'ushape':
			layout = new ULayout(options);
			break;

		case 'banquet':
		case 'round':
			layout = new BanquetLayout(options);
			break;

		case 'cabaret':
			layout = new CabaretLayout(options);
			break;

			// case 'meetingroom':
			// layout = new MeetingroomLayout(options);
			// break;
			//
			case 'cocktail':
			layout = new CocktailLayout(options);
			break;
	}

	layout.setModels(models);
	return layout.load(peopleCount, tableCount, chairsPerTable, fcarpets, getCount);
};

var TheatreLayout = function(options) {
		this.options = {
				columns: 2,
				rowGap: 10,
				colGap: 30,
				gapBtnChairs: 1
		};
		this.models = {
				chair: null
		};

		Layout.call(this, options);
};

TheatreLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: TheatreLayout,

	load: function(peopleCount, tableCount, chairsPerTable, fcarpets, getCount) {
		const cModel = this.models['chair'];

		const cBox = new THREE.Box3().setFromObject(cModel.scene);
		const cSize = new THREE.Vector3();
		cBox.getSize(cSize);

		const opt = this.options;
		const columns = opt.columns;
		const colGap = opt.colGap / 100;
		const rowGap = opt.rowGap / 100;
		const gapBtnChairs = opt.gapBtnChairs / 100;

		const itemWidth = cSize.x + gapBtnChairs;
		const itemLength = cSize.z + rowGap;

		const items = [];
		var peopleCounter = 0;
		var maxPeopleCount = 0;

		for (var i = 0; i < fcarpets.length; i++) {
			var carpet = fcarpets[i];
			const carpetSize = new THREE.Vector3();
			carpet.getSize(carpetSize);

			var itemsPerColumnRow = Math.floor((carpetSize.x - colGap * (columns - 1)) / (itemWidth * columns));
			const itemsPerRow = itemsPerColumnRow * columns;
			const rowWidth = itemsPerRow * itemWidth + colGap * (columns - 1); // here

			let pos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z);
			pos.y -= cBox.min.y; // place on the floor
			pos.x += (carpetSize.x / 2 - rowWidth / 2) + itemWidth / 2;
			pos.z += (itemLength / 2);

			const maxRows = Math.floor(carpetSize.z / itemLength);
			let rows = Math.ceil(peopleCount / itemsPerRow);
			if (rows > maxRows) {
				rows = maxRows;
			}

			maxPeopleCount += maxRows * itemsPerRow;

			var debugInfo = {
				"rows": rows,
				"maxRows": maxRows,
				"itemsPerRow": itemsPerRow,
				"columns": columns,
			};
			console.log("TheatreLayout: carpet["+i+"]", JSON.stringify(debugInfo));

			const start = pos.clone();

			for (let r = 0; r < rows; r++) { // rows

				if (peopleCounter === peopleCount) {
					break;
				}

				for (let c = 0; c < columns; c++) { // column groups

					if (peopleCounter === peopleCount) {
						break;
					}

					for (let i = 0; i < itemsPerColumnRow; i++) {
						var mChair;
						if (!getCount) {
							mChair = Utils.cloneObject3D(cModel.scene);
						} else  {
							mChair = cModel.scene
						}

						mChair.name = "CH-" + r + "-" + c + "-" + i;
						mChair.position.copy(pos);

						let iChair = new ChairItem(mChair, cModel.data);

						items.push(iChair);

						pos.x += itemWidth; // move to next chair in the row

						peopleCounter++;
						if (peopleCounter === peopleCount) {
							break;
						}

					}
					pos.x += colGap; // move to next major column
				}
				pos.z += itemLength; // move to next row
				pos.x = start.x; // reset to left most position
			}

		}

		if (peopleCounter < peopleCount) {
			console.warn("TheatreLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount);
		}

		if (getCount) {
			return  {
				"chairs" : peopleCounter,
				"tables" : 0,
				"maxChairs" : maxPeopleCount,
				"maxTables" : 0
			}
		}

		return items;
	}

});

var ClassroomLayout = function(options) {
	this.options = {
		columns: 1,
		rowGap: 10,
		colGap: 30,
		hGapBtnChairs: 2,  // not-used
		hGapBtnTables: 0.1,  // 1mm gap between tables(gap is to avoid collision)
		vGapBtnChairsAndTables: 5,
		noOfTablesPerColumnRow: 2,  // not-used
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

ClassroomLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: ClassroomLayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets, getCount) {
		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];

		const opt = this.options;
		var columns = opt.columns;

		if (columns > 1) {
			var colGap = opt.colGap / 100;
		} else {
			var colGap = 0; // ignore colGap if number of columns is equal to 1
		}

		const rowGap = opt.rowGap / 100;
		const hGapBtnChairs = opt.hGapBtnChairs / 100; //min horizontal gap b/w two chairs
		const vGapBtnChairsAndTables = opt.vGapBtnChairsAndTables / 100; //gap b/w chair and it's table
		const hGapBtnTables = opt.hGapBtnTables / 100;

		var cBox = new THREE.Box3().setFromObject(chairModel.scene);
		var cSize = new THREE.Vector3();
		cBox.getSize(cSize);

		var tBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tSize = new THREE.Vector3();
		tBox.getSize(tSize);

		var cItemWidth = cSize.x + hGapBtnChairs; //initial chair item width
		var tItemWidth = tSize.x + hGapBtnTables; //initial table item width
		const rowLength = cSize.z + tSize.z + vGapBtnChairsAndTables + rowGap; //length of between two combined rows of chairs and tables

		const items = [];
		var peopleCounter = 0;
		var tableCounter = 0;
		var maxPeopleCount = 0;

		for (var k = 0; k < fcarpets.length; k++) {
			var carpet = fcarpets[k];
			const carpetSize = new THREE.Vector3();
			carpet.getSize(carpetSize);

			var maxColumns = Math.floor((carpetSize.x + colGap) / (tItemWidth + colGap));

			if (maxColumns === 0) { // if tItemWidth > carpetSize.x we can't place a single table with this colgap and table width
				console.warn("Can not place given classroom layout for carpet", carpet, "bacause: table width", tItemWidth, "is greater than carpetWidth", carpetSize.x);
				continue;
			}

			if (columns > maxColumns) {
				var oldColumns = columns;
				columns = maxColumns;
				if (columns === 1) { // no need to use colGap when number of columns === 1
					colGap = 0
				}
				console.warn("Cannot show", oldColumns, "columns showing", columns, "colums instead." );
			}

			var noOfTablesPerColumnRow = Math.floor((carpetSize.x - colGap * (columns - 1)) / (tItemWidth * columns));

			var chairsPerColumnRow =  noOfTablesPerColumnRow * chairsPerTable;
			var yAxis = new THREE.Vector3(0, 1, 0);

			cItemWidth = noOfTablesPerColumnRow * tItemWidth/chairsPerColumnRow;
			var itemsPerRow = columns * chairsPerColumnRow;

			var centreAlignmentPadding = (carpetSize.x - noOfTablesPerColumnRow * columns * tItemWidth - colGap * (columns - 1))/2;

			var tPos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z);
			tPos.y -= tBox.min.y;  // place on the floor
			tPos.x += tItemWidth/2 + centreAlignmentPadding;
			tPos.z += rowGap + tSize.z/2;

			var cPos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z);
			cPos.y -= cBox.min.y;  // place on the floor
			cPos.x += cItemWidth/2 + centreAlignmentPadding;
			cPos.z += rowGap + tSize.z + vGapBtnChairsAndTables + cSize.x/2;

			var maxRows = Math.floor(carpetSize.z/rowLength);
			var rows = Math.ceil(peopleCount/itemsPerRow);

			var debugInfo = {
				"rows-required": rows,
				"maxRows": maxRows,
				"itemsPerRow": itemsPerRow,
				"columns": columns,
				"carpetSize": carpetSize,
				"carpet": carpet
			};
			console.log("ClassroomLayout: carpet["+k+"]", JSON.stringify(debugInfo));
			if (rows > maxRows) {
				rows = maxRows;
			}

			maxPeopleCount += maxRows * itemsPerRow;

			var cStart = cPos.clone();
			var tStart = tPos.clone();

			for (var r=0; r<rows; r++) {  // row

				if (peopleCounter === peopleCount) {
					break;
				}

				for (var c=0; c<columns; c++) {  // column groups

					if (peopleCounter === peopleCount) {
						break;
					}

					for (var i = 0; i < noOfTablesPerColumnRow; i++) {

						if (peopleCounter === peopleCount) {
							break;
						}

						var mTable;
						if (!getCount) {
							mTable = Utils.cloneObject3D(tableModel.scene);
						} else  {
							mTable = tableModel.scene;
						}

						mTable.name = "TA-"+r+"-"+c+"-"+i;
						mTable.position.copy(tPos);

						var iTable = new TableItem(mTable, tableModel.data);

						items.push(iTable);

						tPos.x += tItemWidth;  // move to next table in the row
						tableCounter++;

						for (var j=0; j<chairsPerTable; j++) {
							var mChair;
							if (!getCount) {
								mChair = Utils.cloneObject3D(chairModel.scene);
							} else {
								mChair = chairModel.scene;
							}

							mChair.name = "CH-"+r+"-"+c+"-"+i+"-"+j;
							mChair.position.copy(cPos);

							var iChair = new ChairItem(mChair, chairModel.data);
							items.push(iChair);

							cPos.x += cItemWidth;  // move to next chair in the row

							peopleCounter++;
							if (peopleCounter === peopleCount) {
								break;
							}

						}

					}

					cPos.x += colGap;  // move to next major column
					tPos.x += colGap;

				}

				cPos.x = cStart.x;  // reset to left most position
				cPos.z += rowLength;  // move to next row

				tPos.x = tStart.x;
				tPos.z += rowLength;
			}

		}

		if (peopleCounter < peopleCount) {
			console.warn("ClassroomLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount, {
				"chairsPerTable": chairsPerTable,
				"chairs-required": peopleCount,
				"chairs-added": peopleCounter,
				"columns": columns,
				"options": JSON.stringify(this.options)
			});
		}

		if (getCount) {
			return  {
				"chairs" : peopleCounter,
				"tables" : tableCounter,
				"maxChairs": maxPeopleCount,
				"maxTables": Math.ceil(maxPeopleCount / chairsPerTable)
			}
		}

		return items;
	}

});

var BoardroomLayout = function (options) {
	this.options = {
		minHGapBtnChairs: 5,  // not-used
		distanceBtnChairAndTable: 5
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

BoardroomLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: BoardroomLayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets) {
		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];

		if ( ! fcarpets.length) {
			console.error("No fcarpets provided");
			return [];
		}

		var biggestCarpet = fcarpets[0];
		var biggestCarpetSize = new THREE.Vector3(0, 0, 0);
		biggestCarpet.getSize(biggestCarpetSize);
		var carpetSize = new THREE.Vector3(0, 0, 0);
		for (var i = 1; i < fcarpets.length; i++) { // choose biggest of all carpets
			fcarpets[i].getSize(carpetSize);
			if (biggestCarpetSize.lengthSq() < carpetSize.lengthSq()) {
				biggestCarpet = fcarpets[i];
				biggestCarpetSize.copy(carpetSize);
			}
		}

		var carpet = biggestCarpet;
		carpet.getSize(carpetSize);
		var items = [];

		const opt = this.options;
		opt.minHGapBtnChairs = opt.minHGapBtnChairs / 100;
		opt.distanceBtnChairAndTable = opt.distanceBtnChairAndTable / 100;

		var cBox = new THREE.Box3().setFromObject(chairModel.scene);
		var cSize = new THREE.Vector3();
		cBox.getSize(cSize);

		var tBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tSize = new THREE.Vector3();
		tBox.getSize(tSize);

		// var chairsPerTable = Math.floor(tSize.x/(cSize.x + opt.minHGapBtnChairs));
		var itemSize = tSize.x/chairsPerTable; //cSize.x + hGapBtnChairs

		if (carpetSize.z > carpetSize.x) {
			var maxNoOfTablesOnLongSide = Math.floor((carpetSize.z - 2 * (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
			var maxNoOfTablesOnShortSide = Math.floor((carpetSize.x - 2 * (cSize.z + opt.distanceBtnChairAndTable))/tSize.x);
		} else {
			var maxNoOfTablesOnLongSide = Math.floor((carpetSize.x - 2 * (cSize.z + opt.distanceBtnChairAndTable))/tSize.x);
			var maxNoOfTablesOnShortSide = Math.floor((carpetSize.z - 2 * (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
		}

		var maxTables = 2 * (maxNoOfTablesOnLongSide + maxNoOfTablesOnShortSide);

		var maxChairs = maxTables * chairsPerTable;

		var debugInfo = {
			"chairs-required": peopleCount,
			"maxChairs": maxChairs,
			"maxTables": maxTables,
			"tableCount": tableCount,
			"chairsPerTable": chairsPerTable,
			"maxNoOfTablesOnLongSide": maxNoOfTablesOnLongSide,
			"maxNoOfTablesOnShortSide": maxNoOfTablesOnShortSide,
			"biggestCarpetSize": carpetSize,
			"biggestCarpet": carpet
		};
		console.log("BoardroomLayout: ", JSON.stringify(debugInfo));

		var chairCount = peopleCount;
		if (chairCount > maxChairs) {
			console.warn("Can not seat given number of people. Placing", maxChairs, "Required", peopleCount);
			chairCount = maxChairs;
		}

		var idealNoOfPeopleOnEachSide = chairCount/4;
		var noOfTablesNeededOnShorterSide  = Math.floor(idealNoOfPeopleOnEachSide/chairsPerTable);

		if (noOfTablesNeededOnShorterSide === 0 && chairCount !== 0) { /* if idealNoOfPeopleOnEachSide < chairsPerTable, noOfTablesNeededOnShorterSide === 0.
		That time we are adding 1 table to shorter side if maxNoOfTablesOnShortSide > 0 */
			console.warn("calculated shorter side tables are zero so assigning one");
			noOfTablesNeededOnShorterSide = 1;
		}

		if (noOfTablesNeededOnShorterSide > maxNoOfTablesOnShortSide) {
			console.warn("noOfTablesNeededOnShorterSide crossed maxNoOfTablesOnShortSide");
			noOfTablesNeededOnShorterSide = maxNoOfTablesOnShortSide;
		}
		var noOfPeopleSeatedOnShorterSide = 2 * noOfTablesNeededOnShorterSide * chairsPerTable;

		var minNoOfPeopleSeatedOnLongerSide = chairCount - noOfPeopleSeatedOnShorterSide;
		var noOfTablesNeededOnLongerSide = Math.ceil(minNoOfPeopleSeatedOnLongerSide/ (2 * chairsPerTable));

		if (noOfTablesNeededOnLongerSide === 0 && chairCount !== 0) {  /* if minNoOfPeopleSeatedOnLongerSide < chairsPerTable, noOfTablesNeededOnLongerSide === 0.
		That time we are adding 1 table to longer side if maxNoOfTablesOnLongSide > 0 */
			console.warn("calculated longer side tables are zero so assigning one");
			noOfTablesNeededOnLongerSide = 1;
		}

		if (noOfTablesNeededOnLongerSide > maxNoOfTablesOnLongSide) {
			console.warn("noOfTablesNeededOnLongerSide crossed maxNoOfTablesOnLongSide");
			noOfTablesNeededOnLongerSide = maxNoOfTablesOnLongSide;
		}

		var totalNoOfTables =  2 * (noOfTablesNeededOnLongerSide + noOfTablesNeededOnShorterSide);
		var totalNoOfPeopleSeated = totalNoOfTables * chairsPerTable;

		debugInfo = {
			"noOfTablesNeededOnLongerSide": noOfTablesNeededOnLongerSide,
			"noOfTablesNeededOnShorterSide": noOfTablesNeededOnShorterSide,
		};
		console.log("BoardroomLayout: ", JSON.stringify(debugInfo));

		var center = new THREE.Vector3();
		carpet.getCenter(center);

		if (carpetSize.x > carpetSize.z) {
			var temp = noOfTablesNeededOnLongerSide;
			noOfTablesNeededOnLongerSide = noOfTablesNeededOnShorterSide;
			noOfTablesNeededOnShorterSide = temp;
		}

		var padding = 0;
		if (noOfTablesNeededOnShorterSide === 0) {
			padding = tSize.z;
		}

		//initial table positions
		var tPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		tPos.y -= tBox.min.y;

		var FRtPos = tPos.clone();
		FRtPos.x += (noOfTablesNeededOnShorterSide * tSize.x - tSize.z)/2 + padding;
		FRtPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - tSize.x)/2;

		var BRtPos = tPos.clone();
		BRtPos.x += (noOfTablesNeededOnShorterSide * tSize.x - tSize.x)/2;
		BRtPos.z += (noOfTablesNeededOnLongerSide * tSize.x + tSize.z)/2;

		var BLtPos = tPos.clone();
		BLtPos.x -= (noOfTablesNeededOnShorterSide * tSize.x - tSize.z)/2 + padding;
		BLtPos.z += (noOfTablesNeededOnLongerSide * tSize.x - tSize.x)/2;

		var FLtPos = tPos.clone();
		FLtPos.x -= (noOfTablesNeededOnShorterSide * tSize.x - tSize.x)/2;
		FLtPos.z -= (noOfTablesNeededOnLongerSide * tSize.x + tSize.z)/2;

		//initial chair positions
		var cPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		cPos.y -= cBox.min.y;

		var FRcPos = cPos.clone();
		FRcPos.x += (noOfTablesNeededOnShorterSide * tSize.x/2 + opt.distanceBtnChairAndTable + cSize.z/2 + padding);
		FRcPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - itemSize)/2;

		var BRcPos = cPos.clone();
		BRcPos.x += (noOfTablesNeededOnShorterSide * tSize.x - itemSize)/2;
		BRcPos.z += noOfTablesNeededOnLongerSide * tSize.x/2 + tSize.z + opt.distanceBtnChairAndTable + cSize.z/2;

		var BLcPos = cPos.clone();
		BLcPos.x -= (noOfTablesNeededOnShorterSide * tSize.x/2 + opt.distanceBtnChairAndTable + cSize.z/2 + padding);
		BLcPos.z += (noOfTablesNeededOnLongerSide * tSize.x - itemSize)/2;

		var FLcPos = cPos.clone();
		FLcPos.x -= (noOfTablesNeededOnShorterSide * tSize.x - itemSize)/2;
		FLcPos.z -= noOfTablesNeededOnLongerSide * tSize.x/2 + tSize.z + opt.distanceBtnChairAndTable + cSize.z/2;

		var tableID = 0;
		var chairID = 0;
		var peopleCounter = 0;
		var tableCounter = 0;

		tableCounter = this.addItem(noOfTablesNeededOnLongerSide, tableModel, "table", tSize.x, BLtPos, -Math.PI/2, items, tableID, "forward", tableCounter, tableCount);
		tableID += noOfTablesNeededOnLongerSide;

		peopleCounter = this.addItem(noOfTablesNeededOnLongerSide * chairsPerTable, chairModel, "chair", itemSize, BLcPos, -Math.PI/2, items, chairID, "forward", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnLongerSide * chairsPerTable;

		tableCounter = this.addItem(noOfTablesNeededOnShorterSide, tableModel, "table", tSize.x, FLtPos, 0, items, tableID, "right", tableCounter, tableCount);
		tableID += noOfTablesNeededOnShorterSide;

		peopleCounter = this.addItem(noOfTablesNeededOnShorterSide * chairsPerTable, chairModel, "chair", itemSize, FLcPos, -Math.PI, items, chairID, "right", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnShorterSide * chairsPerTable;

		tableCounter = this.addItem(noOfTablesNeededOnLongerSide, tableModel, "table", tSize.x, FRtPos, -Math.PI/2, items, tableID, "backward", tableCounter, tableCount);
		tableID += noOfTablesNeededOnLongerSide;

		peopleCounter = this.addItem(noOfTablesNeededOnLongerSide * chairsPerTable, chairModel, "chair", itemSize, FRcPos, Math.PI/2, items, chairID, "backward", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnLongerSide * chairsPerTable;

		tableCounter = this.addItem(noOfTablesNeededOnShorterSide, tableModel, "table", tSize.x, BRtPos, 0, items, tableID, "left", tableCounter, tableCount);
		tableID += noOfTablesNeededOnShorterSide;

		peopleCounter = this.addItem(noOfTablesNeededOnShorterSide * chairsPerTable, chairModel, "chair", itemSize, BRcPos, 0, items, chairID, "left", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnShorterSide * chairsPerTable;

		if (peopleCounter < peopleCount) {
			console.warn("BoardroomLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount, {
				"chairsPerTable": chairsPerTable,
				"chairs-required": peopleCount,
				"chairs-finalised": chairCount,
				"chairs-added": peopleCounter,
				"tables-finalised": tableCount,
				"tables-added": tableCounter,
				"options": JSON.stringify(this.options)
			});
		}

		return items;

	}

});

var ULayout = function (options) {
	this.options = {
		minHGapBtnChairs: 5,  // not-used
		distanceBtnChairAndTable: 5
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

ULayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: ULayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets) {

		const opt = this.options;
		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];
		var items = [];

		opt.minHGapBtnChairs = opt.minHGapBtnChairs / 100;
		opt.distanceBtnChairAndTable = opt.distanceBtnChairAndTable / 100;

		var cBox = new THREE.Box3().setFromObject(chairModel.scene);
		var cSize = cBox.getSize();

		var tBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tSize = tBox.getSize();

		if ( ! fcarpets.length) {
			console.error("No fcarpets provided");
			return [];
		}

		var biggestCarpet = fcarpets[0];
		var biggestCarpetSize = new THREE.Vector3(0, 0, 0);
		biggestCarpet.getSize(biggestCarpetSize);
		var carpetSize = new THREE.Vector3(0, 0, 0);
		for (var i = 1; i < fcarpets.length; i++) { // choose biggest of all carpets
			fcarpets[i].getSize(carpetSize);
			if (biggestCarpetSize.lengthSq() < carpetSize.lengthSq()) {
				biggestCarpet = fcarpets[i];
				biggestCarpetSize.copy(carpetSize);
			}
		}

		var carpet = biggestCarpet;
		carpet.getSize(carpetSize);

		// var chairsPerTable = Math.floor(tSize.x/(cSize.x + opt.minHGapBtnChairs));
		var itemSize = tSize.x/chairsPerTable; //ie: cSize.x + hGapBtnChairs

		if (carpetSize.z > carpetSize.x) {
			var maxNoOfTablesOnLongSide = Math.floor((carpetSize.z - (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
			var maxNoOfTablesOnShortSide = Math.floor((carpetSize.x - 2 * (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
		} else {
			var maxNoOfTablesOnLongSide = Math.floor((carpetSize.x - 2 * (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
			var maxNoOfTablesOnShortSide = Math.floor((carpetSize.z - (cSize.z + opt.distanceBtnChairAndTable + tSize.z))/tSize.x);
		}

		var maxTables = 2 * maxNoOfTablesOnLongSide + maxNoOfTablesOnShortSide;

		var maxChairs = maxTables * chairsPerTable;

		var debugInfo = {
			"chairs-required": peopleCount,
			"maxChairs": maxChairs,
			"maxTables": maxTables,
			"tableCount": tableCount,
			"chairsPerTable": chairsPerTable,
			"maxNoOfTablesOnLongSide": maxNoOfTablesOnLongSide,
			"maxNoOfTablesOnShortSide": maxNoOfTablesOnShortSide,
			"biggestCarpetSize": carpetSize,
			"biggestCarpet": carpet
		};
		console.log("ULayout: ", JSON.stringify(debugInfo));

		var chairCount = peopleCount;
		if (chairCount > maxChairs) {
			console.warn("Can not seat given number of people. Placing", maxChairs, "Required", peopleCount);
			chairCount = maxChairs;
		}

		var idealNoOfPeopleOnEachSide = chairCount/3;
		var noOfTablesNeededOnShorterSide  = Math.floor(idealNoOfPeopleOnEachSide/chairsPerTable);

		// if (noOfTablesNeededOnShorterSide == 0) {
		//   console.warn("calculated shorter side tables are zero so assigning one");
		//   noOfTablesNeededOnShorterSide = 1;
		// }

		if (noOfTablesNeededOnShorterSide > maxNoOfTablesOnShortSide) {
			console.warn("noOfTablesNeededOnShorterSide crossed maxNoOfTablesOnShortSide");
			noOfTablesNeededOnShorterSide = maxNoOfTablesOnShortSide;
		}
		var noOfPeopleSeatedOnShorterSide = noOfTablesNeededOnShorterSide * chairsPerTable;

		var minNoOfPeopleSeatedOnLongerSide = chairCount - noOfPeopleSeatedOnShorterSide;
		var noOfTablesNeededOnLongerSide = Math.ceil(minNoOfPeopleSeatedOnLongerSide/ (2 * chairsPerTable));

		// if (noOfTablesNeededOnLongerSide == 0) {
		//   console.warn("calculated longer side tables are zero so assigning one");
		//   noOfTablesNeededOnLongerSide = 1;
		// }

		if (noOfTablesNeededOnLongerSide > maxNoOfTablesOnLongSide) {
			console.warn("noOfTablesNeededOnLongerSide crossed maxNoOfTablesOnLongSide");
			noOfTablesNeededOnLongerSide = maxNoOfTablesOnLongSide;
		}

		var totalNoOfTables =  2 * noOfTablesNeededOnLongerSide + noOfTablesNeededOnShorterSide;
		var totalNoOfPeopleSeated = totalNoOfTables * chairsPerTable;

		var center = new THREE.Vector3();
		carpet.getCenter(center);

		if (carpetSize.x > carpetSize.z) {
			var temp = noOfTablesNeededOnLongerSide;
			noOfTablesNeededOnLongerSide = noOfTablesNeededOnShorterSide;
			noOfTablesNeededOnShorterSide = temp;
		}

		//initial table positions
		var tPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		tPos.y -= tBox.min.y;

    var FLtPos = tPos.clone();
    FLtPos.x -= (noOfTablesNeededOnShorterSide * tSize.x + tSize.z)/2;
    FLtPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - tSize.x)/2;

    var BLtPos = tPos.clone();
    BLtPos.x -= (noOfTablesNeededOnShorterSide * tSize.x - tSize.x)/2;
    BLtPos.z += (noOfTablesNeededOnLongerSide * tSize.x - tSize.z)/2;

    var BRtPos = tPos.clone();
    BRtPos.x += (noOfTablesNeededOnShorterSide * tSize.x + tSize.z)/2;
    BRtPos.z += (noOfTablesNeededOnLongerSide * tSize.x - tSize.x)/2;

    // var FRtPos = tPos.clone();
		// FRtPos.x += (noOfTablesNeededOnShorterSide * tSize.x + tSize.z)/2;
		// FRtPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - tSize.x)/2;

		//initial chair positions
		var cPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		cPos.y -= cBox.min.y;

    var FLcPos = cPos.clone();
    FLcPos.x = FLtPos.x - tSize.z/2 - opt.distanceBtnChairAndTable - cSize.z/2;
    FLcPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - itemSize)/2;

    var BLcPos = cPos.clone();
    BLcPos.x -= (noOfTablesNeededOnShorterSide * tSize.x - itemSize)/2;
    BLcPos.z = BLtPos.z + tSize.z/2 + opt.distanceBtnChairAndTable + cSize.z/2;

    var BRcPos = cPos.clone();
    BRcPos.x = BRtPos.x + tSize.z/2 + opt.distanceBtnChairAndTable + cSize.z/2;
    BRcPos.z += (noOfTablesNeededOnLongerSide * tSize.x - itemSize)/2;

    // var FRcPos = cPos.clone();
		// FRcPos.x += noOfTablesNeededOnShorterSide * tSize.x/2 + tSize.z + opt.distanceBtnChairAndTable + cSize.z/2;
		// FRcPos.z -= (noOfTablesNeededOnLongerSide * tSize.x - itemSize)/2;


		var tableID = 0;
		var chairID = 0;
		var peopleCounter = 0;
		var tableCounter = 0;

		tableCounter = this.addItem(noOfTablesNeededOnLongerSide, tableModel, "table", tSize.x, FLtPos, -Math.PI/2, items, tableID, "backward", tableCounter, tableCount);
		tableID += noOfTablesNeededOnLongerSide;

		peopleCounter = this.addItem(noOfTablesNeededOnLongerSide * chairsPerTable, chairModel, "chair", itemSize, FLcPos, -Math.PI/2, items, chairID, "backward", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnLongerSide * chairsPerTable;

		tableCounter = this.addItem(noOfTablesNeededOnShorterSide, tableModel, "table", tSize.x, BLtPos, 0, items, tableID, "right", tableCounter, tableCount);
		tableID += noOfTablesNeededOnShorterSide;

		peopleCounter = this.addItem(noOfTablesNeededOnShorterSide * chairsPerTable, chairModel, "chair", itemSize, BLcPos, 0, items, chairID, "right", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnShorterSide * chairsPerTable;

		tableCounter = this.addItem(noOfTablesNeededOnLongerSide, tableModel, "table", tSize.x, BRtPos, Math.PI/2, items, tableID, "forward", tableCounter, tableCount);
		tableID += noOfTablesNeededOnLongerSide;

		peopleCounter = this.addItem(noOfTablesNeededOnLongerSide * chairsPerTable, chairModel, "chair", itemSize, BRcPos, Math.PI/2, items, chairID, "forward", peopleCounter, chairCount);
		chairID += noOfTablesNeededOnLongerSide * chairsPerTable;

		if (peopleCounter < peopleCount) {
			console.warn("BoardroomLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount, {
				"chairsPerTable": chairsPerTable,
				"chairs-required": peopleCount,
				"chairs-finalised": chairCount,
				"chairs-added": peopleCounter,
				"tables-finalised": tableCount,
				"tables-added": tableCounter,
				"options": JSON.stringify(this.options)
			});
		}

		return items;
	}

});

var BanquetLayout = function (options) {
	this.options = {
		hGapBtnTables: 15,  // cm
		vGapBtnTables: 15,  // cm
		chairTableGap: 2,  // cm
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

BanquetLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: BanquetLayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets) {

		const opt = this.options;

		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];

		const hGapBtnTables = opt.hGapBtnTables / 100;
		const vGapBtnTables = opt.vGapBtnTables / 100;
		const chairTableGap = opt.chairTableGap / 100;

		var items = [];

		var tableBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tableSize = new THREE.Vector3();
		tableBox.getSize(tableSize);

		var chairBox = new THREE.Box3().setFromObject(chairModel.scene);
		var chairSize = new THREE.Vector3();
		chairBox.getSize(chairSize);

		var angle = (2 * Math.PI)/chairsPerTable;
		var yAxis = new THREE.Vector3(0, 1, 0);

    var maxTableSide = Math.max(tableSize.x, tableSize.z);

		var groupWidth = tableSize.x + 2 * chairSize.z + 2 * chairTableGap;
		var groupLength = tableSize.z + 2 * chairSize.z + 2 * chairTableGap;

		tableCount = Math.ceil(peopleCount/chairsPerTable);
		var carpetSize = new THREE.Vector3();
    var carpetCenter = new THREE.Vector3();
		var peopleCounter = 0;
		for (var fc=0; fc<fcarpets.length; fc++) {
			var carpet = fcarpets[fc];
			carpet.getSize(carpetSize);
      carpet.getCenter(carpetCenter);

      var xStep = groupWidth+hGapBtnTables;  // furniture group + gap btn groups
      var zStep = groupLength+vGapBtnTables;

			var maxColumns = Math.floor(carpetSize.x/xStep);
			if (maxColumns === 0) { // group width is greater than carpet size
				if (carpetSize.x >= groupWidth) {
					maxColumns = 1;
          xStep = carpetSize.x;
				}
			}

			var maxRows = Math.floor(carpetSize.z/zStep);
			if (maxRows === 0) { // group length is greater than carpet size
				if (carpetSize.z >= groupLength) {
					maxRows = 1;
          zStep = carpetSize.z;
				}
			}

			var columns = Math.min(Math.ceil(Math.sqrt(tableCount)), maxColumns);
			var rows = Math.ceil(tableCount/columns);
			if (rows > maxRows) {
				rows = maxRows;  // load max rows possible
				// we didn't have required number of rows. So, increase columns as much as possible
				columns = Math.min(Math.ceil(tableCount/rows), maxColumns);
			}

			var debugInfo = {
				"rows": rows,
				"maxRows": maxRows,
				"columns": columns,
				"maxColumns": maxColumns,
				"carpetSize": carpetSize,
				"carpet": carpet,
				"tableSize": tableSize,
				"chairSize": chairSize,
				"groupWidth": groupWidth,
				"xStep": xStep,
				"zStep": zStep,
			};
			console.log("BanquetLayout: carpet["+fc+"]", JSON.stringify(debugInfo));

			var tablePos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z); //table position is the old group position

			// center in z-axis
			var rowsLength = rows * zStep;
      tablePos.z = carpetCenter.z - (rowsLength/2) + (zStep/2);

			for (var r=1; r<=rows; r++) {
				if (peopleCounter === peopleCount) {
					break;
				}
        // center in x-axis
        var rowWidth = columns * xStep;
        tablePos.x = carpetCenter.x - (rowWidth/2) + (xStep/2);

				for (var c=1; c<=columns; c++) {

					if (peopleCounter === peopleCount) {
						break;
					}

					var mTable = Utils.cloneObject3D(tableModel.scene);
					mTable.name = "T-"+r+c;
					this.setObjectOnFloor(mTable, tablePos);

					var iTable = new TableItem(mTable, tableModel.data);

					items.push(iTable);
					tableCount--;

					var chairStartPos = new THREE.Vector3();
					chairStartPos.x = maxTableSide/2 + chairTableGap + chairSize.z/2;

					for (var ch=0; ch<chairsPerTable; ch++) {
						var mChair = Utils.cloneObject3D(chairModel.scene);
						mChair.name = "CH-"+r+c;

						var chPos = chairStartPos.clone();

						var chPosAngle = angle*ch;
						// console.log("chairStartPos: ", JSON.stringify(chairStartPos), " chPosAngle", chPosAngle);
						chPos.applyAxisAngle(yAxis, chPosAngle);
						chPos.x += tablePos.x;
						chPos.z += tablePos.z;
						// console.log("chPos: ", JSON.stringify(chPos));
						this.setObjectOnFloor(mChair, chPos);
						mChair.rotation.y = (Math.PI/2);
						mChair.rotation.y += chPosAngle;

						var iChair = new ChairItem(mChair, chairModel.data);

						items.push(iChair);

						peopleCounter++;
						if (peopleCounter === peopleCount) {
							break;
						}

					}

					tablePos.x += xStep;
					tablePos.y = carpet.min.y; //why?
				}

				tablePos.z += zStep;
			}
			if (peopleCounter === peopleCount || tableCount <= 0) 	{
				break;
			}
		}
		if (peopleCounter < peopleCount) {
			console.warn("BanquetLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount, {
				"chairsPerTable": chairsPerTable,
				"chairs-required": peopleCount,
				"chairs-added": peopleCounter,
				"tableSize": tableSize,
				"chairSize": chairSize,
				"options": JSON.stringify(this.options)
			});
		}
		return items;
	}

});

var CabaretLayout = function (options) {
	this.options = {
    hGapBtnTables: 15,  // cm
    vGapBtnTables: 15,  // cm
    chairTableGap: 2,  // cm
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

CabaretLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: CabaretLayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets) {

		const opt = this.options;

		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];

    const hGapBtnTables = opt.hGapBtnTables / 100;
    const vGapBtnTables = opt.vGapBtnTables / 100;
    const chairTableGap = opt.chairTableGap / 100;

		var items = [];

		var tableBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tableSize = new THREE.Vector3();
		tableBox.getSize(tableSize);

		var chairBox = new THREE.Box3().setFromObject(chairModel.scene);
		var chairSize = new THREE.Vector3();
		chairBox.getSize(chairSize);

		var angle = 0;
		if (chairsPerTable > 1) {
			angle = Math.PI/(chairsPerTable - 1);
		}

		var yAxis = new THREE.Vector3(0, 1, 0);

    var maxTableSide = Math.max(tableSize.x, tableSize.z);

		var groupWidth = tableSize.x + 2 * chairSize.z + 2 * chairTableGap;
		var groupLength = tableSize.z + chairSize.z + 2 * chairTableGap;

		tableCount = Math.ceil(peopleCount/chairsPerTable);
		var carpetSize = new THREE.Vector3();
    var carpetCenter = new THREE.Vector3();
		var peopleCounter = 0;
		for (var fc=0; fc<fcarpets.length; fc++) {
			var carpet = fcarpets[fc];
			carpet.getSize(carpetSize);
      carpet.getCenter(carpetCenter);

      var xStep = groupWidth+hGapBtnTables;  // furniture group + gap btn groups
      var zStep = groupLength+vGapBtnTables;

      var maxColumns = Math.floor(carpetSize.x/xStep);
      if (maxColumns === 0) { // group width is greater than carpet size
        if (carpetSize.x >= groupWidth) {
          maxColumns = 1;
          xStep = carpetSize.x;
        }
      }

      var maxRows = Math.floor(carpetSize.z/zStep);
      if (maxRows === 0) { // group length is greater than carpet size
        if (carpetSize.z >= groupLength) {
          maxRows = 1;
          zStep = carpetSize.z;
        }
      }

			var columns = Math.min(Math.ceil(Math.sqrt(tableCount)), maxColumns);
			var rows = Math.ceil(tableCount/columns);
			if (rows > maxRows) {
				rows = maxRows;  // load max rows possible
				// we didn't have required number of rows. So, increase columns as much as possible
				columns = Math.min(Math.ceil(tableCount/rows), maxColumns);
			}

			var debugInfo = {
				"rows": rows,
				"maxRows": maxRows,
				"columns": columns,
				"maxColumns": maxColumns,
				"carpetSize": carpetSize,
				"carpet": carpet,
				"tableSize": tableSize,
				"chairSize": chairSize,
				"groupLength": groupLength,
				"groupWidth": groupWidth,
				"maxTableSide": maxTableSide,
        "xStep": xStep,
        "zStep": zStep,
			};
			console.log("CabaretLayout: carpet["+fc+"]", JSON.stringify(debugInfo));

			var tablePos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z); //table position is the old group position

			// center in z-axis
      var rowsLength = rows * zStep;
      tablePos.z = carpetCenter.z - (rowsLength/2) + (zStep/2) - (groupLength/2) + maxTableSide/2;
      // this calculation required because table will not be at the center of group. Chairs are present only on one side

			for (var r=1; r<=rows; r++) {
				if (peopleCounter === peopleCount) {
					break;
				}
        // center in x-axis
        var rowWidth = columns * xStep;
        tablePos.x = carpetCenter.x - (rowWidth/2) + (xStep/2);

				for (var c=1; c<=columns; c++) {

					if (peopleCounter === peopleCount) {
						break;
					}

					var mTable = Utils.cloneObject3D(tableModel.scene);
					mTable.name = "T-"+r+c;
					this.setObjectOnFloor(mTable, tablePos);

					var iTable = new TableItem(mTable, tableModel.data);

					items.push(iTable);
					tableCount--;

					var chairStartPos = new THREE.Vector3();
					chairStartPos.x = maxTableSide/2 + chairTableGap + chairSize.z/2;

					for (var ch=0; ch<chairsPerTable; ch++) {
						var mChair = Utils.cloneObject3D(chairModel.scene);
						mChair.name = "CH-"+r+c;

						var chPos = chairStartPos.clone();

						var chPosAngle = -angle*ch;
						// console.log("chairStartPos: ", JSON.stringify(chairStartPos), " chPosAngle", chPosAngle);
						chPos.applyAxisAngle(yAxis, chPosAngle);
						chPos.x += tablePos.x;
						chPos.z += tablePos.z;
						// console.log("chPos: ", JSON.stringify(chPos));
						this.setObjectOnFloor(mChair, chPos);
						mChair.rotation.y = (Math.PI/2);
						mChair.rotation.y += chPosAngle;

						var iChair = new ChairItem(mChair, chairModel.data);

						items.push(iChair);

						peopleCounter++;
						if (peopleCounter === peopleCount) {
							break;
						}

					}

          tablePos.x += xStep;
					tablePos.y = carpet.min.y; //why?
				}

        tablePos.z += zStep;
			}
			if (peopleCounter === peopleCount || tableCount <= 0) {
				break;
			}
		}

		if (peopleCounter < peopleCount) {
			console.warn("CabaretLayout: Could not place all chairs. Placed:", peopleCounter, "Required:", peopleCount, {
				"chairsPerTable": chairsPerTable,
				"chairs-required": peopleCount,
				"chairs-added": peopleCounter,
				"tableSize": tableSize,
				"chairSize": chairSize,
				"options": JSON.stringify(this.options)
			});
		}

		return items;
	}

});

var MeetingroomLayout = function (options) {
	this.options = {
		minHGapBtnChairs: 10,
		distanceBtnChairAndTable: 15
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

MeetingroomLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: MeetingroomLayout,

	load: function (carpet, peopleCount, tableCount, chairsPerTable) {

		const opt = this.options;
		const chairModel = this.models['chair'];
		const tableModel = this.models['table'];
		const carpetSize = carpet.getSize();
		var items = [];

		opt.minHGapBtnChairs = 10 * PIXEL_TO_SCENE;
		opt.distanceBtnChairAndTable = 10 * PIXEL_TO_SCENE;

		var cBox = new THREE.Box3().setFromObject(chairModel.scene);
		var cSize = cBox.getSize();

		var tBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tSize = tBox.getSize();

		var maxChairsOnLongSide = Math.floor(tSize.x/(cSize.x + opt.minHGapBtnChairs));

		var maxChairs = 2 * maxChairsOnLongSide;

		if (peopleCount > maxChairs) {
				console.warn("Can not seat given number of people");
				peopleCount = maxChairs;
		}

		var noOfPeopleOnEachSide = Math.ceil(peopleCount/2);
		var itemSize = tSize.x/noOfPeopleOnEachSide; //ie: cSize.x + hGapBtnChairs

		var totalNoOfPeopleSeated = 2 * noOfPeopleOnEachSide;

		//initial table position
		var FRtPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		FRtPos.y -= cBox.min.y;
		FRtPos.x += 0;
		FRtPos.z += 0;

		//initial chair positions
		var FRcPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		FRcPos.y -= cBox.min.y;
		FRcPos.x += tSize.z/2 + opt.distanceBtnChairAndTable + cSize.z/2;
		FRcPos.z -= (tSize.x - itemSize)/2;

		var BLcPos = new THREE.Vector3(center.x, carpet.min.y, center.z);
		BLcPos.y -= cBox.min.y;
		BLcPos.x -= tSize.z/2 + opt.distanceBtnChairAndTable + cSize.z/2;
		BLcPos.z += (tSize.x - itemSize)/2;

		var tableID = 0;
		var chairID = 0;

		this.addItem( 1, tableModel, "table", tSize.x, FRtPos, -Math.PI/2, items, tableID, "backward");
		tableID += 1;

		this.addItem( noOfPeopleOnEachSide, chairModel, "chair", itemSize, FRcPos, Math.PI/2, items, chairID, "backward");
		chairID += noOfPeopleOnEachSide;

		this.addItem( noOfPeopleOnEachSide, chairModel, "chair", itemSize, BLcPos, -Math.PI/2, items, chairID, "forward");
		chairID += noOfPeopleOnEachSide;

		return items;

	}

});

var CocktailLayout = function (options) {
	this.options = {
    hGapBtnTables : 100,
    vGapBtnTables : 100
	};
	this.models = {
		chair: null,
		table: null
	};

	Layout.call(this, options);
};

CocktailLayout.prototype = Object.assign(Object.create(Layout.prototype), {
	constructor: CocktailLayout,

	load: function (peopleCount, tableCount, chairsPerTable, fcarpets) {
		// we are considering noOfPeople equal to noOfTables needed because we don't have backend
		tableCount = peopleCount;

		const opt = this.options;
		// const chairModel = this.models['chair'];
		const tableModel = this.models['table'];
		var items = [];

		const hGapBtnTables = opt.hGapBtnTables / 100;
		const vGapBtnTables = opt.vGapBtnTables / 100;

		// var cBox = new THREE.Box3().setFromObject(chairModel.scene);
		// var cSize = cBox.getSize();

		var tBox = new THREE.Box3().setFromObject(tableModel.scene);
		var tSize = new THREE.Vector3();
		tBox.getSize(tSize);

		var tableID = 0;
		var tablesAdded = 0;

		for (var j = 0; j < fcarpets.length; j++) {

			var carpet = fcarpets[j];
			const carpetSize = new THREE.Vector3();
			carpet.getSize(carpetSize);
			var carpetCenter = new THREE.Vector3();
			carpet.getCenter(carpetCenter);

			var xStep = tSize.x+hGapBtnTables;
			var maxColumns = Math.floor(carpetSize.x / xStep);
			if (maxColumns <= 0) {
				if (carpetSize.x >= tSize.x) {
					maxColumns = 1;
					xStep = carpetSize.x;  // keep in center
				}
			}

			var zStep = tSize.z+vGapBtnTables;
			var maxRows = Math.floor(carpetSize.z / zStep);
			if (maxRows <= 0) {
				if (carpetSize.z >= tSize.z) {
					maxRows = 1;
					zStep = carpetSize.z;  // keep in center
				}
			}
			var debugInfo = {
				"maxRows": maxRows,
				"maxColumns": maxColumns,
				"minxStep": xStep,
				"minzStep": zStep,
				"carpet": carpet,
				"carpetSize": carpetSize,
			};
			console.log("CocktailLayout: carpet["+j+"]", JSON.stringify(debugInfo));

			var rows = maxRows;
			var columns = maxColumns;

			// if(maxColumns > maxRows) {
			// 	// room width > length
			// 	// first calculate number of rows that can be fit in
			//   var rows = Math.min(Math.ceil(Math.sqrt(tableCount)), maxRows);
			//   // then adjust number of columns
			//   var columns = Math.min(Math.ceil(tableCount/rows), maxColumns);
			// } else {
			//   var columns = Math.min(Math.ceil(Math.sqrt(tableCount)), maxColumns);
			//   var rows = Math.min(Math.ceil(tableCount/columns), maxRows);
			// }
			// xStep = carpetSize.x/columns;
			// zStep = carpetSize.z/rows;
			// console.log("VH: Cocktail", "rows", rows, "columns", columns, "xStep", xStep, "zStep", zStep);

			var tPos = new THREE.Vector3(carpet.min.x, carpet.min.y, carpet.min.z);
			tPos.z += zStep/2;
			for (var r=0; r<rows; r++) {
				var tablesInRow = columns;
				// tPos.x = carpet.min.x + xStep/2;
				if ((r%2) !== 0) {  // interleaving for odd indexed rows
					// tPos.x = carpet.min.x + xStep;
					tablesInRow = columns-1;  // 1 less table than even rows
				}
				// make sure centered in x-axis
				tablesInRow = Math.min((tableCount-tablesAdded), tablesInRow); // number of tables to be added in this row
				var rowWidth = tablesInRow * xStep;
				tPos.x = carpetCenter.x - (rowWidth/2) + (xStep/2);

				tablesAdded = this.addItem( tablesInRow, tableModel, "table", xStep, tPos, 0, items, tableID, "right", tablesAdded, tableCount);
				tableID += tablesAdded;

				// move to next row
				tPos.z += zStep;
			}
		}

		if (tablesAdded < tableCount) {
			var debugInfo = {
				"fcarpets": fcarpets,
				"tSize": tSize,
				"opt": opt
			};
			console.warn("CocktailLayout: Could not place all tables. Placed:", tablesAdded, "Required", tableCount, "debugInfo", JSON.parse(JSON.stringify(debugInfo)));
		}

		return items;

	}

});


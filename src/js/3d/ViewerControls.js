"use strict";

THREE.ViewerControls = function ( object, domElement, scene, viewer ) {

	var _this = this;

	var STATE = { NONE: - 1, ROTATE: 0, ZOOM: 1, PAN: 2, AUTO_PAN: 3, TOUCH_ROTATE: 4, TOUCH_ZOOM_PAN: 5, TOUCH_ZOOM: 6, TOUCH_PAN: 7, AUTO_ZOOM: 8, AUTO_ROTATE: 9 };
	var DIRECTION = {
		NONE: -1,
		UP: 0,
		DOWN: 1,
		LEFT: 2,
		RIGHT: 3,
		FORWARD: 4,
		BACKWARD: 5,
	};

	var KEYS = {
		LEFT: [37, 'Left', 'ArrowLeft'],
		UP: [38, 'Up', 'ArrowUp'],
		RIGHT: [39, 'Right', 'ArrowRight'],
		DOWN: [40, 'Down', 'ArrowDown'],

		CTRL: [17, 'Control', 91, 93, 'Meta'],  // ctrl and Meta keys are merged to give same effect
		DEL: [8, "Backspace", 46, "Delete"],

		CHAR_D: [68, 'd'],
	};

	var vector1 = {x:0, y:0, z:0};
	var vector2 = {x:0, y:0, z:0};

	this.object = object;
	this.viewer = viewer;
	this.domElement = ( domElement !== undefined ) ? domElement : document;

	this.enabled = true;

	this.screen = { left: 0, top: 0, width: 0, height: 0 };

	this.lookVertical = true;

	this.constrainVertical = false;
	this.verticalMin = 0;
	this.verticalMax = Math.PI;

	this.rotateSpeed = 120.00;
	this.zoomSpeed = 0.8; //1.2; //1.2;
	this.touchZoomSpeed = 0.5; //1.2; //1.2;
	this.panSpeed = 0.3;

	this.noRotate = false;
	this.noZoom = false;
	this.noPan = false;

	this.autoPanDirection = DIRECTION.NONE;
	this.autoZoomDirection = DIRECTION.NONE;
	this.autoRotateDirection = DIRECTION.NONE;

	this.autoPanSpeed = 0.1;
	this.autoZoomSpeed = 3.4;
	this.autoRotationSpeed = 48;  // rotation per second(in degrees)

	this.keyDown = null;
	this.isDragOn = false;

	this.tool = TOOLS.VIEW;

	this.staticMoving = true;
	this.dynamicDampingFactor = 0.2;

	this.view2d = false;
	this.viewMode = '3D';
	this.zoom2D = {
		min: 0.8,
		max: 10,
		speed: 1
	};

	// var _zoomDelta = 0;
	// this.zoomSpeed = 10.0;

	this.obstacles = {
		'3D': [],
		'2D': [],
	};

	this.isTouchDevice = false;

	var _touchZoomStartDistance = 0;
	var _touchZoomEndDistance = 0;

	this.target = new THREE.Vector3();

	var _state = STATE.NONE,
		_prevState = STATE.NONE,

		_eye = new THREE.Vector3(),
		_zoom = 1,

		_movePrev = new THREE.Vector2(),
		_moveCurr = new THREE.Vector2(),

		_lastAxis = new THREE.Vector3(),
		_lastAngle = 0,

		_zoomStart = new THREE.Vector2(),
		_zoomEnd = new THREE.Vector2(),

		_touchZoomDistanceStart = 0,
		_touchZoomDistanceEnd = 0,

		_panStart = new THREE.Vector2(),
		_panEnd = new THREE.Vector2(),

		_rayCaster = new THREE.Raycaster(),

		_moveToPos = null,
		_anim,

		_rulerStart = null,
		_rulerEnd = null,

		_rulerMateial =  new THREE.LineBasicMaterial( { color: 0x0000ff } ),
		_rulerGeometry = null,
		_ruler = null,

		_targets = {};

	// for reset

	this.target0 = this.target.clone();
	// this.position0 = this.object.position.clone();
	// this.up0 = this.object.up.clone();

	this.setEnabled = function (enabled) {
		_this.enabled = enabled;
		if ( ! enabled) {
			_state = STATE.NONE;
		}
	};

	this.setViewMode = function (mode, object) {

		_this.viewMode = mode;

		if (_this.viewMode === '3D') {
			_this.showFloorPointer(true);
		} else {
			_this.showFloorPointer(false);
		}
		// if (_targets[_this.viewMode]) {
		//   _this.target.copy(_targets[_this.viewMode]);
		// }
	};

	this.setTool = function (tool) {
		_this.tool = tool;
	};

	this.setTarget = function (target) {
		//console.log("setTarget: ", JSON.stringify(target));
		_this.target.copy(target);
		_this.target0.copy(target);
		// _targets[_this.viewMode] = target.clone();
	};

	this.setObject = function (object) {
		// _targets[_this.viewMode] = _this.object.lookAtPos.clone();
		_this.object = object;
		_this.setTarget(_this.object.lookAtPos);
	};

	this.setObstacles = function (obstacles, mode) {
		//console.log("setObstacles", obstacles);
		_this.obstacles[mode] = obstacles;

		// var wObstacles = {
		//   g: [],  // geometries
		//   m: []  // matrixWorld
		// };
		// for (var i=0; i<obstacles.length; i++) {
		//   var g = obstacles[i].geometry;
		//   wObstacles.g.push(g.toJSON());
		//   wObstacles.m.push(obstacles[i].matrixWorld.elements);
		// }
		// viewer.worker.postMessage({
		//   cmd: 'setObstacles',
		//   data: {
		//     'mode': mode,
		//     'obstacles': wObstacles
		//   }
		// });
	};

	this.checkCollision = (function(){

		var MIN_GAP = 0.5;
		var direction = new THREE.Vector3(),
			position = new THREE.Vector3(),
			intersectionPoint = new THREE.Vector3(),
			minDistance;

		return function(oldPosition, newPosition, oldTarget) {
			// return true;

			// viewer.worker.postMessage({
			//   cmd: 'checkCollision',
			//   data: {
			//     source: 'ViewerControls',
			//     mode: _this.viewMode,
			//     oldPosition: oldPosition.toArray(),
			//     newPosition: newPosition.toArray(),
			//     oldTarget: oldTarget.toArray()
			//   }
			// });
			//
			// return;

			direction.copy(newPosition).sub(oldPosition).normalize();
			minDistance = Math.max(oldPosition.distanceTo(newPosition), MIN_GAP);

			position.copy(oldPosition);
			if (_this.viewMode === '2D') {
				position.y = _this.target0.y;
			}
			_rayCaster.set(position, direction);

			if (_this.isTouchDevice) {
				// check only against bounding box
				// var bbox = _this.viewer.getBBox();
				var bbox = _this.viewer.fbox;  //  on touch devices, check collision against combined bounding box of fcarpets

				if ( ! bbox) {
					return true;  // no bounding box? we can move anywhere
				}

				if(_rayCaster.ray.intersectBox(bbox, intersectionPoint)) {
					if(_rayCaster.ray.origin.distanceTo(intersectionPoint) < minDistance) {
						newPosition.copy(oldPosition);
						return false;
					}
				}
			} else {

				//console.log("POS", oldPosition, newPosition, direction, minDistance);

				var intersects = _rayCaster.intersectObjects(_this.obstacles[_this.viewMode], true);
				//console.log("INTERSECTS", intersects);
				for (var i=0, l=intersects.length; i<l; i++) {
					if (intersects[i].distance < minDistance ) {
						// console.warn("CROSSING", intersects[i]);
						//newPosition.addVectors(oldPosition, direction.multiplyScalar(intersects[i].distance-minDistance));
						newPosition.copy(oldPosition);
						return false;
					}
				}
			}

			return true;
		}
	}());

	this.handleWorkerResult = function (cmd, result) {
		switch (cmd) {
			case 'checkCollision':
				_this.onCollisionResult(result);
				break;
		}
	};

	this.onCollisionResult = function (result) {
		// console.log("VC: onCollisionResult", result);
		if (result.collides) {
			console.log("VC: onCollisionResult", result);
			_this.object.position.x = result.newPosition[0];
			_this.object.position.z = result.newPosition[2];
			_this.object.lookAtPos.fromArray(result.oldTarget);
		}
	};

	this.handleResize = function () {

		if ( this.domElement === document ) {

			this.screen.left = 0;
			this.screen.top = 0;
			this.screen.width = window.innerWidth;
			this.screen.height = window.innerHeight;

		} else {

			var box = this.domElement.getBoundingClientRect();
			// adjustments come from similar code in the jquery offset() function
			var d = this.domElement.ownerDocument.documentElement;
			this.screen.left = box.left + window.pageXOffset - d.clientLeft;
			this.screen.top = box.top + window.pageYOffset - d.clientTop;
			this.screen.width = box.width;
			this.screen.height = box.height;

		}

	};

	this.showFloorPointer = function (visible) {
		if (scene.floorPointer) {
			scene.floorPointer.visible = visible;
		}
	};

	var getMouseOnScreen = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnScreen( pageX, pageY ) {

			vector.set(
				( pageX - _this.screen.left ) / _this.screen.width,
				( pageY - _this.screen.top ) / _this.screen.height
			);

			return vector;

		};

	}() );

	var getMouseOnCircle = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnCircle( pageX, pageY ) {

			vector.set(
				( ( pageX - _this.screen.width * 0.5 - _this.screen.left ) / ( _this.screen.width * 0.5 ) ),
				( ( _this.screen.height + 2 * ( _this.screen.top - pageY ) ) / _this.screen.width ) // screen.width intentional
			);

			return vector;

		};

	}() );

	this.zoomCamera = function ( delta ) {

		var factor;
		var modified = false;

		if ( _state === STATE.TOUCH_ZOOM_PAN || _state === STATE.TOUCH_ZOOM ) {

			// if (_this.viewMode === '2D') {
			//   const camera = _this.object;
			//
			//   if(event.deltaY >0 ){
			//     camera.zoom = Math.max(_this.zoom2D.min, camera.zoom-_this.zoom2D.speed);
			//   } else {
			//     camera.zoom = Math.min(_this.zoom2D.max, camera.zoom+_this.zoom2D.speed);
			//   }
			//   camera.updateProjectionMatrix();
			// }

			// factor = 1.0 + (_touchZoomDistanceStart / _touchZoomDistanceEnd) * _this.touchZoomSpeed * (_touchZoomDistanceStart<_touchZoomDistanceEnd? 1 : -1);
			factor = (_touchZoomDistanceStart / _touchZoomDistanceEnd);
			factor = 1.0 - (1.0-factor)*_this.touchZoomSpeed;
			// factor = THREE.Math.clamp(factor, 0.0, 1.1);
			if ( factor !== 1.0  && factor > 0.0 ) {
				if (_this.viewMode === '2D') {
					_zoom = _zoom/factor;
					_zoom = Math.min(_this.zoom2D.max, _zoom);
					_zoom = Math.max(_this.zoom2D.min, _zoom);
				} else {
					_eye.multiplyScalar( factor );
				}
				_touchZoomDistanceStart = _touchZoomDistanceEnd;
				modified = true;

				// console.log("TOUCH_ZOOM_PAN: ", {factor: factor, _zoom: _zoom});
			}

		} else {

			if (_state === STATE.AUTO_ZOOM) {
				delta = Math.min(Math.max(delta, 0), 0.1);
				var effectiveSpeed = _this.autoZoomSpeed * delta;
				if (_this.autoZoomDirection === DIRECTION.FORWARD) {
					// factor = 1.0 - effectiveSpeed;
					factor = -effectiveSpeed;
				} else {
					// factor = 1.0 + effectiveSpeed;
					factor = effectiveSpeed;
				}
				// let l = factor > 1.0 ? 1.0 : -1.0;
				let _dir = _eye.clone().normalize().multiplyScalar(factor);
				_eye.add(_dir);
				modified = true;

			} else {
				factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;
				if ( factor !== 1.0 && factor > 0.0 ) {
					_eye.multiplyScalar( factor );
					modified = true;
				}
			}

			// if ( factor !== 1.0 && factor > 0.0 ) {
			//
			//   // console.log("Z: ", _this.zoomSpeed, ( _zoomEnd.y - _zoomStart.y ), _zoomEnd.y, _zoomStart.y);
			//   // let dist = 0.1;
			//   let l = factor > 1.0 ? 1.0 : -1.0;
			//   let _dir = _eye.clone().normalize().multiplyScalar(l);
			//   _eye.add(_dir);
			//
			//   // if (factor > 1.0)
			//   //   factor = (l - 1.0)/l;
			//   // else
			//   //   factor = (l + 1.0)/l;
			//
			//   // _eye.multiplyScalar( factor );
			//   modified = true;
			//
			// }

			if ( _this.staticMoving ) {

				_zoomStart.copy( _zoomEnd );

			} else {

				_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

			}

		}

		return modified;

	};

	var lookDirection = new THREE.Vector3(),
		angle;

	this.rotateCamera = function ( delta ) {

		if (_state === STATE.AUTO_ROTATE) {
			// console.log("Z:R:", _this.autoRotationSpeed, delta);
			if (_this.autoRotateDirection === DIRECTION.LEFT) {
				_this.lon -= _this.autoRotationSpeed * delta;
			} else {
				_this.lon += _this.autoRotationSpeed * delta;
			}
		} else {
			lookDirection.set( _moveCurr.x - _movePrev.x, _moveCurr.y - _movePrev.y, 0 );
			angle = lookDirection.length();

			if ( ! angle ) {
				return false;
			}

			var actualLookSpeed = this.rotateSpeed;  // delta * this.lookSpeed;

			// console.log("Z:RM:", actualLookSpeed, angle);

			var verticalLookRatio = 1;

			if ( _this.constrainVertical ) {
				verticalLookRatio = Math.PI / ( _this.verticalMax - _this.verticalMin );
			}

			_this.mouseX = (_moveCurr.x - _movePrev.x) * -1; // * _this.screen.width; //_this.screen.width * -1; //(_moveCurr.x - _movePrev.x);
			_this.mouseY = (_moveCurr.y - _movePrev.y) * -1;// * _this.screen.height; //_this.screen.height* -1; //(_moveCurr.y - _movePrev.y);

			_this.lon += _this.mouseX * actualLookSpeed;
			if ( _this.lookVertical ) _this.lat -= _this.mouseY * actualLookSpeed * verticalLookRatio;
		}

		_this.lat = Math.max( - 85, Math.min( 85, _this.lat ) );
		_this.phi = THREE.Math.degToRad( 90 - _this.lat );

		_this.theta = THREE.Math.degToRad( _this.lon );

		if ( _this.constrainVertical ) {

			_this.phi = THREE.Math.mapLinear( _this.phi, 0, Math.PI, _this.verticalMin, _this.verticalMax );

		}

		var targetPosition = _this.target,
			position = _this.object.position;

		targetPosition.x = position.x + 100 * Math.sin( _this.phi ) * Math.cos( _this.theta );
		targetPosition.y = position.y + 100 * Math.cos( _this.phi );
		targetPosition.z = position.z + 100 * Math.sin( _this.phi ) * Math.sin( _this.theta );

		_eye.copy( _this.object.position ).sub( _this.target );


		// //console.log("R: ", {
		//     'targetPosition':  JSON.stringify(targetPosition),
		//     'phi': _this.phi,
		//     'theta': _this.theta,
		//     'lat': _this.lat,
		//     'lon': _this.lon,
		//     'mouseX': _this.mouseX,
		//     'mouseY': _this.mouseY,
		//     '_movePrev': JSON.stringify(_movePrev),
		//     '_moveCurr': JSON.stringify(_moveCurr),
		// });
		_movePrev.copy(_moveCurr);

		return true;
	};

	this.panCamera = (function(){
		var mouseChange = new THREE.Vector2(),
			objectUp = new THREE.Vector3(),
			pan = new THREE.Vector3(),
			zAxis = new THREE.Vector3(0, 0, -1),
			xAxis = new THREE.Vector3(-1, 0, 0);

		return function panCamera( delta ) {

			if (_state === STATE.PAN || _state === STATE.TOUCH_ZOOM_PAN || _state === STATE.TOUCH_PAN) {
				mouseChange.copy( _panEnd ).sub( _panStart );
				// //console.log("TOUCH_PAN: mouseChange", mouseChange);

				if ( ! mouseChange.lengthSq() ) {
					return false;
				}

				//console.log("Pan camera");


			} else if (_state === STATE.AUTO_PAN) {
				var effectiveSpeed = _this.autoPanSpeed * delta;
				mouseChange.set(0, 0);
				switch (_this.autoPanDirection) {
					case DIRECTION.LEFT:
						mouseChange.setX(effectiveSpeed);
						break;
					case DIRECTION.UP:
						mouseChange.setY(effectiveSpeed);
						break;
					case DIRECTION.RIGHT:
						mouseChange.setX(-effectiveSpeed);
						break;
					case  DIRECTION.DOWN:
						mouseChange.setY(-effectiveSpeed);
						break;
				}
			} else {
				return false;
			}

			mouseChange.multiplyScalar( _eye.length() * _this.panSpeed );

			if (_this.viewMode === '2D') {
				pan.copy( _eye ).cross( zAxis ).setLength( mouseChange.x );
				pan.add( objectUp.copy( zAxis ).setLength( mouseChange.y ) );
			} else {
				pan.copy( _eye ).cross( _this.object.up ).setLength( mouseChange.x );
				pan.add( objectUp.copy( _this.object.up ).setLength( mouseChange.y ) );
			}

			// _eye.add( pan );
			_this.target.add( pan );

			if (_state === STATE.PAN || _state === STATE.TOUCH_ZOOM_PAN || _state === STATE.TOUCH_PAN) {
				if ( _this.staticMoving ) {

					_panStart.copy( _panEnd );

				} else {

					_panStart.add( mouseChange.subVectors( _panEnd, _panStart ).multiplyScalar( _this.dynamicDampingFactor ) );

				}
			}



			return true;

		};

	}() );

	this.update = (function(){

		var newPostion = new THREE.Vector3(),
			oldTarget = new THREE.Vector3(),
			rotated = false,
			zoomed = false,
			panned = false;

		return function ( delta ) {

			if ( _this.enabled === false ) return;

			if (_moveToPos) {
				_this.object.position.x = _moveToPos.x;
				_this.object.position.z = _moveToPos.z;
				_moveToPos = null;
				return;
			}

			oldTarget.copy(_this.target);
			_eye.subVectors( _this.object.position, _this.target );
			_zoom = _this.object.zoom;

			// _eye.copy( _this.object.position );

			rotated = zoomed = panned = false;

			if ( ! _this.noRotate ) {

				rotated = _this.rotateCamera( delta );

			}

			if ( ! _this.noZoom ) {

				zoomed = _this.zoomCamera( delta );

			}

			if ( ! _this.noPan ) {

				panned = _this.panCamera( delta );

			}

			if (rotated || panned || zoomed) {

				newPostion.addVectors(_this.target, _eye); // why?
				if (panned || zoomed) {
					// _this.checkCollision(_this.object.position, newPostion, oldTarget);

					if( ! _this.checkCollision(_this.object.position, newPostion) && panned) {
						_this.target.copy(oldTarget);
					}
				}

				if (_this.object.zoom !== _zoom) {  // used for 2D camera
					_this.object.zoom = _zoom;
					_this.object.updateProjectionMatrix();

					_this.dispatchEvent({
						type: EVENT.ZOOM,
						value: ((_this.object.zoom-_this.zoom2D.min)/(_this.zoom2D.max-_this.zoom2D.min)) * 100,  // zoom percent
					});
				}

				_this.object.position.x = newPostion.x;
				// _this.object.position.y = newPostion.y;
				_this.object.position.z = newPostion.z;
				_this.object.lookAtPosition( _this.target );

			}
		}
	}());

	function addRuler (event) {
		_rulerStart = getFloorIntersection(event);
		if (_rulerStart) {
			_rulerEnd = _rulerStart.clone();

			_rulerGeometry = new THREE.Geometry();
			_rulerGeometry.vertices.push(_rulerStart);
			_rulerGeometry.vertices.push(_rulerEnd);

			_ruler = new THREE.Line(_rulerGeometry, _rulerMateial);
			scene.add(_ruler);
		}
	}

	function updateRuler (event) {
		if (_rulerStart) {
			_rulerEnd = getFloorIntersection(event);
			if (_rulerEnd) {
				_rulerGeometry.vertices[1].copy(_rulerEnd);
				_rulerGeometry.verticesNeedUpdate = true;
			}
		}
	}

	function endRuler(event) {
		if(_rulerStart) {

			if (event) {
				_rulerEnd = getFloorIntersection(event);
			}

			if (_rulerEnd) {
				_this.viewer.addRuler(_rulerStart, _rulerEnd);
			}
		}

		if (_ruler) {
			scene.remove(_ruler);
			_ruler.geometry.dispose();
			_ruler = null;
		}
		_rulerStart = null;
		_rulerEnd = null;
	}

	function disposeRuler() {
		_rulerStart = null;
		_rulerEnd = null;
		if (_ruler) {
			scene.remove(_ruler);
			_ruler.geometry.dispose();
			_ruler = null;
		}
	}

	function touchstart( event ) {
		if ( ! _this.enabled) return;

		event.preventDefault();

		_this.isTouchDevice = true;

		if (_this.viewMode === '2D' && _this.tool === TOOLS.RULER) {
			if (event.touches.length === 1) {  // start ruler for only one finger
				addRuler(event.touches[ 0 ]);
			}
			return;
		}

		switch ( event.touches.length ) {

			case 1:
				// look around

				if (_this.viewMode === "3D") {
					_state = STATE.TOUCH_ROTATE;
					_moveCurr.copy( getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
					_movePrev.copy( _moveCurr );
				} else {
					 _state = STATE.TOUCH_PAN;
					var x = event.touches[ 0 ].pageX;
					var y = event.touches[ 0 ].pageY;
					_panStart.copy( getMouseOnScreen( x, y ) );
					_panEnd.copy( _panStart );
				}

				break;

			default:
				// zoom and pan

					_state = STATE.TOUCH_ZOOM;
					var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
					var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
					_touchZoomDistanceStart = _touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy )

				break;
		}

	}

	function touchmove( event ) {
		if ( ! _this.enabled) return;

		event.preventDefault();
		// event.stopPropagation();

		if(_this.viewMode === '2D' && (_this.tool === TOOLS.RULER)) {
			if(event.touches.length) {  // update ruler with first touch
				updateRuler(event.touches[ 0 ]);
			}
			return;
		}

		switch ( event.touches.length ) {

			case 1:
				// look around
				if (_this.viewMode === "3D") {
					_movePrev.copy( _moveCurr );
					_moveCurr.copy( getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
				} else {
					var x = event.touches[ 0 ].pageX;
					var y = event.touches[ 0 ].pageY;
					_panEnd.copy( getMouseOnScreen( x, y ) );
				}

				break;

			default:
				// zoom and pan
				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				_touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );

				// var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
				// var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
				// _panEnd.copy( getMouseOnScreen( x, y ) );
				//
				// if (_panEnd.distanceTo(_panStart) > 0.01) {
				//     _state = STATE.PAN
				// } else {//if (Math.abs(_touchZoomDistanceEnd-_touchZoomDistanceStart) > 10.0) {
				//     _state = STATE.ZOOM;
				// }

				// if (_state === STATE.NONE) {
				//     //console.log("TOUCH: ", _panEnd.distanceTo(_panStart), _panEnd.distanceTo(_panStart)*_this.screen.width, Math.abs(_touchZoomDistanceEnd-_touchZoomDistanceStart));
				//     if (_panEnd.distanceTo(_panStart) > 2) {
				//         // _state = STATE.PAN;
				//     } else if (Math.abs(_touchZoomDistanceEnd-_touchZoomDistanceStart) > 1) {
				//         // _state = STATE.ZOOM;
				//     }
				// }

				break;
		}
	}

	function touchend( event ) {
		if ( ! _this.enabled) {
			disposeRuler();
			return;
		}

		if(_this.viewMode === '2D' && (_this.tool === TOOLS.RULER)) {
			if ( ! event.touches.length) {  // end ruler when all fingers lifted
				endRuler();
			}
			return;
		}

		switch ( event.touches.length ) {

			case 0:
				_state = STATE.NONE;
				break;

			case 1:
				_state = STATE.TOUCH_ROTATE;
				_moveCurr.copy( getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
				_movePrev.copy( _moveCurr );
				break;

		}
	}

	function onMouseDown( event ) {
		if ( ! _this.enabled) return;

		event.preventDefault();
		// event.stopPropagation();

		if ( _state === STATE.NONE ) {
			if ((_this.viewMode === '3D' && (event.ctrlKey || event.metaKey))
					|| (_this.viewMode === '2D' && _this.tool === TOOLS.VIEW)
			){
				_state = STATE.PAN;
			} else {
				_state = STATE.ROTATE;
			}
		}

		if ( _state === STATE.ROTATE && ! _this.noRotate ) {

			_moveCurr.copy( getMouseOnScreen( event.pageX, event.pageY ) );
			_movePrev.copy( _moveCurr );

		} else if ( _state === STATE.ZOOM && ! _this.noZoom ) {

			_zoomStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
			_zoomEnd.copy( _zoomStart );

		} else if ( _state === STATE.PAN && ! _this.noPan ) {

			_panStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
			_panEnd.copy( _panStart );

		}

		if(_this.viewMode === '2D' && (_this.tool === TOOLS.RULER) && ! (event.ctrlKey || event.metaKey)){
			addRuler(event);
		}

		_this.isDragOn = true;

	}

	function onMouseMove( event ) {
		if ( ! _this.enabled) return;

    // avoid e.preventDefault when mousemove event is triggered by DragDropTouch library
    if ( ! event.isDDT) {
      // console.log("VC:onMouseMove: preventDefault isDDT", event.isDDT);
      event.preventDefault();
		}

		if(_this.viewMode === '2D' && (_this.tool === TOOLS.RULER)) {
			updateRuler(event);
			return;
		}

		if ( _state === STATE.ROTATE && ! _this.noRotate ) {

			// _movePrev.copy( _moveCurr );
			_moveCurr.copy( getMouseOnScreen( event.pageX, event.pageY ) );

			// //console.log("STATE: ROTATE", _moveCurr, event.pageX, event.pageY);

		} else if ( _state === STATE.ZOOM && ! _this.noZoom ) {

			_zoomEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );

		} else if ( _state === STATE.PAN && _this.isDragOn && ! _this.noPan ) {

			_panEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );

		}

		if ( ! _this.isDragOn) {
			updateFloorPointer(event);
		} else {
			_this.showFloorPointer(false);
		}

	}

	function onMouseEnter( event ) {
		if ( ! _this.enabled) return;

		window.addEventListener( 'keydown', onKeyDown, false );
		window.addEventListener( 'keyup', onKeyUp, false );

	}

	function onMouseLeave( event ) {
		if ( ! _this.enabled) return;

		window.removeEventListener( 'keydown', onKeyDown, false );
		window.removeEventListener( 'keyup', onKeyUp, false );

		onMouseUp( event );

	}

	function onMouseUp( event ) {
		if ( ! _this.enabled) {
		 disposeRuler();
			return;
		}

		event.preventDefault();
		// event.stopPropagation();

		_state = STATE.NONE;

		if(_this.viewMode === '2D' && (_this.tool === TOOLS.RULER)){
			endRuler(event);
		}
		_this.isDragOn = false;

	}

	function onMousewheel( event ) {
		// console.log("VC: MouseWheel");

		if ( ! _this.enabled) return;

		if ( _this.noZoom === true ) return;

		// zoom only in left half of screen
		// var m = getMouseOnScreen( event.pageX, event.pageY );
		// if (m.x > 0.7) {
		//   console.warn("Scrolling");
		//   return;
		// }

		var isFullScreen = (document.fullscreenElement || document.webkitFullscreenElement || document.msFullscreenElement)
		if ( ! isFullScreen && ! (event.ctrlKey || event.metaKey)) {
			return;
		}

		event.preventDefault();

		if(_this.viewMode === '2D'){

			const camera = _this.object;

			if(event.deltaY >0 ){
				camera.zoom = Math.max(_this.zoom2D.min, camera.zoom-_this.zoom2D.speed);
			} else {
				camera.zoom = Math.min(_this.zoom2D.max, camera.zoom+_this.zoom2D.speed);
			}
			camera.updateProjectionMatrix();

			_this.dispatchEvent({
				type: EVENT.ZOOM,
				value: ((camera.zoom-_this.zoom2D.min)/(_this.zoom2D.max-_this.zoom2D.min)) * 100,  // zoom percent
			});
		} else {
			switch ( event.deltaMode ) {

				case 2:
					// Zoom in pages
					_zoomStart.y -= event.deltaY * 0.025;
					break;

				case 1:
					// Zoom in lines
					_zoomStart.y -= event.deltaY * 0.01;
					break;

				default:
					// undefined, 0, assume pixels
					_zoomStart.y -= event.deltaY * 0.00025;
					break;

			}
		}


	}

	function onKeyDown( event ) {
		if ( _this.enabled === false ) {
			return;
		}

		var key = event.keyCode || event.key;

		var captured = false;

		if (_this.viewMode === '2D' && (event.metaKey || event.ctrlKey) && KEYS.CHAR_D.indexOf(key) >= 0) {
			// ctrl+D -> clone objects in 2D
			// do not clone again if key is long pressed
			if( ! event.repeat) {
				_this.viewer.cloneSelectedItem();
			}
			captured = true;
		}
		/*else if (KEYS.DEL.indexOf(key) >= 0) {
			// delete -> delete selected object
			captured = _this.viewer.deleteSelectedItem();
		} */
		else if ( _state !== STATE.NONE ) {

			if (_this.keyDown === key) {
				// long press triggers multiple keydown events
				// so prevent propagation if same key event triggered multiple times
				// this is to avoid page scrolling while zooming/panning
				captured = true;
			} else if (_this.keyDown && KEYS.CTRL.indexOf(key) >= 0) {
				// when command key is pressed, reset the state.
				// bcz, when command key is pressed, we wont get any other keyup event to reset the state,
				// leading to infinite rotation/pan
				_state = _prevState;
				_this.keyDown = null;
			}
			captured = true;
		}
		else if ( ! (event.ctrlKey || event.metaKey)) {
			// when ctrl/meta keys are pressed, key-up is not triggered for other keys.
			// This can result in non stop auto* state
			// so lets ignore when either of these keys are pressed

			var oldState = _state;
			if ( KEYS.LEFT.indexOf(key) >= 0) { // left arrow
				if (_this.viewMode === '2D') {
					_state = STATE.AUTO_PAN;
					_this.autoPanDirection = DIRECTION.LEFT;
				} else if (_this.viewMode === '3D') {
					_state = STATE.AUTO_ROTATE;
					_this.autoRotateDirection = DIRECTION.LEFT;
				}
			}
			else if ( KEYS.UP.indexOf(key) >= 0 ) { // up arrow
				if (_this.viewMode === '2D') {
					_state = STATE.AUTO_PAN;
					_this.autoPanDirection = DIRECTION.UP;
				} else {
					_state = STATE.AUTO_ZOOM;
					_this.autoZoomDirection = DIRECTION.FORWARD;
				}
			}
			else if ( KEYS.RIGHT.indexOf(key) >= 0 ) { // right arrow
				if (_this.viewMode === '2D') {
					_state = STATE.AUTO_PAN;
					_this.autoPanDirection = DIRECTION.RIGHT;
				} else if (_this.viewMode === '3D') {
					_state = STATE.AUTO_ROTATE;
					_this.autoRotateDirection = DIRECTION.RIGHT;
				}
			}
			else if ( KEYS.DOWN.indexOf(key) >= 0 ) { // down arrow
				if (_this.viewMode === '2D') {
					_state = STATE.AUTO_PAN;
					_this.autoPanDirection = DIRECTION.DOWN;
				} else {
					_state = STATE.AUTO_ZOOM;
					_this.autoZoomDirection = DIRECTION.BACKWARD;
				}
			}

			_this.viewer.unselectItem();

			if (oldState !== _state) {
				// event is caught
				// save the old state and key used
				_prevState = oldState;
				_this.keyDown = key;
				captured = true;
			}
		}

		if (captured) {
			event.preventDefault();
			event.stopPropagation();
			return false;
		}
		return true;

		//console.log("onKeyDown: ", _state, event.keyCode);
	}

	function onKeyUp( event ) {
		if ( _this.enabled === false ) {
			return;
		}

		var key = event.keyCode || event.key;

		if (_this.keyDown === key) {
			_state = _prevState;
			// if ([STATE.PAN, STATE.AUTO_PAN, STATE.AUTO_ZOOM, STATE.AUTO_ROTATE].indexOf(_state) >= 0) {
			//   console.log("STATE RESET: ", _state, _prevState);
			//
			// }
			_this.keyDown = null;
		}

		//console.log("onKeyUp: ", _state, _prevState);

	}

	function onContextMenu( event ) {

		if ( _this.enabled === false ) return;

		event.preventDefault();

	}

	function updateFloorPointer( event ) {
		if (!scene.floorPointer) {
			_this.viewer.createFloorPointer();
		}
		if (scene.floorPointer) {
			var mouse = getFloorIntersection(event);
			if (mouse) {
				scene.floorPointer.position.x = mouse.x;
				scene.floorPointer.position.z = mouse.z;
				scene.floorPointer.visible = true;
			}
		}
	}

	function getFloorIntersection( event ) {
		var mouse = getMouseOnScreen( event.pageX, event.pageY );
		mouse.x = mouse.x * 2 -1;
		mouse.y = -mouse.y * 2 +1;
		var vector = mouseToVec3(mouse);

		var direction = vector.sub(_this.object.position).normalize();
		_rayCaster.set(_this.object.position, direction);

		var intersections = _rayCaster.intersectObjects(scene.floors, true);

		if (intersections.length) {
			return intersections[0].point;
		}

		return null;
	}
	this.getFloorIntersection = getFloorIntersection;

	this.clicks = 0;
	function onClick( event ) {
		// console.log("CLICK: ", event);
		if (_this.clicks === 0) {
			_this.clicks = 1;
			setTimeout(function () {
				if (_this.clicks === 1) {
					// console.log("SINGLE_CLICK");
				}
				_this.clicks = 0;
			}, 1000);
		}
	}

	function onDblClick( event ) {
		_this.clicks = 0;

		if ( ! _this.enabled) return;

		if(_this.viewMode === '2D') return;


		if (_state === STATE.NONE) {
			event.preventDefault();

			var itemExist = viewer.dragControls.doesIntersectingItemExist();
			if (itemExist) {
				return;
			}

			var endPos = getFloorIntersection(event);
			if ( ! endPos) {
				return;
			}
			endPos.y = _this.object.position.y;
			if( ! _this.checkCollision(_this.object.position, endPos)) {
				return;
			}
			_this.animateCamera(endPos, _this.object);
		}
	}

	this.animateCamera = function (toPos, camera) {
		camera = camera || _this.object;
		_anim = new TWEEN.Tween(camera.position.clone())
			.to(toPos, 500)
			.easing(TWEEN.Easing.Sinusoidal.InOut)
			.onUpdate(function ( pos ) {
				camera.position.copy(pos);
			})
			.start();
	};

	function mouseToVec3(mouse) {
		var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
		vector.unproject(_this.object);
		return vector;
	}

	this.attachEventListeners = function (attach) {
		var func = attach ? 'addEventListener' : 'removeEventListener';
		// this.domElement[func]( 'wheel', onMousewheel, false );
		this.domElement[func]( 'wheel', onMousewheel, false );
		// this.domElement[func]( 'DOMMouseScroll', onMousewheel, false );

		this.domElement[func]( 'mousedown', onMouseDown, false );
		this.domElement[func]( 'mousemove', onMouseMove, false );
		this.domElement[func]( 'mouseenter', onMouseEnter, false );
		this.domElement[func]( 'mouseleave', onMouseLeave, false );
		this.domElement[func]( 'mouseup', onMouseUp, false );
		this.domElement[func]( 'contextmenu', onContextMenu, false );

		this.domElement[func]( 'touchstart', touchstart, false );
		this.domElement[func]( 'touchmove', touchmove, false );
		this.domElement[func]( 'touchend', touchend, false );

		// this.domElement.addEventListener( 'click', onClick, false );
		this.domElement[func]( 'dblclick', onDblClick, false );

		// window[func]( 'keydown', onKeyDown, false );
		// window[func]( 'keyup', onKeyUp, false );
	};
	this.attachEventListeners(true);

	this.handleResize();

	this.dispose = function () {

		this.attachEventListeners(false);

		this.object = null;
		this.domElement = null;
		this.obstacles = null;
		this.target = null;
		this.target0 = null;

//         this.domElement.removeEventListener( 'wheel', onMousewheel, false );
//
//         this.domElement.removeEventListener( 'mousedown', onMouseDown, false );
//         this.domElement.removeEventListener( 'mousemove', onMouseMove, false );
//         this.domElement.removeEventListener( 'mouseup', onMouseUp, false );
//
//         this.domElement.removeEventListener( 'touchstart', touchstart, false );
//         this.domElement.removeEventListener( 'touchmove', touchmove, false );
//         this.domElement.removeEventListener( 'touchend', touchend, false );
//
// // this.domElement.addEventListener( 'click', onClick, false );
//         this.domElement.removeEventListener( 'dblclick', onDblClick, false );
//
//         window.removeEventListener( 'keydown', onKeyDown, false );
//         window.removeEventListener( 'keyup', onKeyUp, false );
	}


};

THREE.ViewerControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.ViewerControls.prototype.constructor = THREE.ViewerControls;

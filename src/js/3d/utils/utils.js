"use strict";

function setRandomColors(object, scale) {
	var children = object.children;
	if (children && children.length > 0) {
		children.forEach(function (e) {
			setRandomColors(e, scale)
		});
	} else {
		// no children assume contains a mesh
		if (object instanceof THREE.Mesh) {
			if (object.material instanceof Array) {
				object.material.forEach(function(m) {
					m.color = new THREE.Color(scale(Math.random()).hex());
					if (m.name.indexOf("building") == 0) {
						m.emissive = new THREE.Color(0x444444);
						m.transparent = true;
						m.opacity = 0.8;
					}
				});
			} else {
				object.material.color = new THREE.Color(scale(Math.random()).hex());
				if (object.material.name.indexOf("building") == 0) {
					object.material.emissive = new THREE.Color(0x444444);
					object.material.transparent = true;
					object.material.opacity = 0.8;
				}
			}
		}
	}
}

var Utils = (function () {
	function Utils() {}

	Utils.angle= function (x1, y1, x2, y2) {
		var tDot = x1 * x2 + y1 * y2;
		var tDet = x1 * y2 - y1 * x2;
		var tAngle = -Math.atan2(tDet, tDot);
		return tAngle;
	};

	Utils.removeIf = function(array, func) {
		var tResult = [];
		for(var i=0, l=array.length; i<l; i++) {
			if (!func(array[i])) {
				tResult.push(array[i])
			}
		}
		return tResult;
	};

	Utils.planesFromBox3 = function (box) {
		var planes = []
		var vertices = []
		// front
		vertices.push([
			{x: box.min.x, y: box.min.y, z: box.max.z},
			{x: box.max.x, y: box.min.y, z: box.max.z}
		]);

		planes.push(new THREE.Plane().setFromCoplanarPoints())
	};

	Utils.box3Corners = function (box) {
		return [
			new THREE.Vector3( box.min.x, box.min.y, box.min.z ), // 000
			new THREE.Vector3( box.min.x, box.min.y, box.max.z ), // 001
			new THREE.Vector3( box.min.x, box.max.y, box.min.z ), // 010
			new THREE.Vector3( box.min.x, box.max.y, box.max.z ), // 011
			new THREE.Vector3( box.max.x, box.min.y, box.min.z ), // 100
			new THREE.Vector3( box.max.x, box.min.y, box.max.z ), // 101
			new THREE.Vector3( box.max.x, box.max.y, box.min.z ), // 110
			new THREE.Vector3( box.max.x, box.max.y, box.max.z )  // 111
		];
	};

	Utils.objectBBoxOnScreen = function (obj, camera, screen) {

		obj.updateMatrixWorld(true);
		var box3 = new THREE.Box3().setFromObject(obj);

		var vertices = Utils.box3Corners(box3);

		var vertex = new THREE.Vector3();
		var min = new THREE.Vector3(1, 1, 1);
		var max = new THREE.Vector3(-1, -1, -1);

		for (var i = 0; i < vertices.length; i++) {
			vertex.copy(vertices[i]);
			var vertexScreenSpace = vertex.project(camera);
			min.min(vertexScreenSpace);
			max.max(vertexScreenSpace);
		}

		var box = new THREE.Box2(min, max);

		box.min.x = (box.min.x+1) * (screen.width/2.0)/window.devicePixelRatio;
		box.min.y = (-box.min.y+1) * (screen.height/2.0)/window.devicePixelRatio;

		box.max.x = (box.max.x+1) * (screen.width/2.0)/window.devicePixelRatio;
		box.max.y = (-box.max.y+1) * (screen.height/2.0)/window.devicePixelRatio;

		return box;
	};

	Utils.disposeObject3D = function(object) {

		object.traverse(function (child) {
			if ( ! child.isMesh) return;

			Utils.disposeMesh(child);
		});

		if (object.isMesh) {
			Utils.disposeMesh(object);
		}

		if (object.parent) {
			object.parent.remove(object);
		}
	};

	Utils.disposeMesh = function(mesh) {
		mesh.geometry.dispose();

		if (mesh.material.isMaterial) {
			Utils.cleanMaterial(mesh.material)
		} else {
			// array of materials
			for (var i=0; i<mesh.material.length; i++) {
				Utils.cleanMaterial(mesh.material[i]);
			}
		}
	};

	Utils.cleanMaterial = function (material) {
		material.dispose();

		for (var k in material) {
			var v = material[k];
			if(v && typeof v === 'object' && 'minFilter' in v) {
				v.dispose();
			}
		}
	};

	Utils.itemForObject3D = function (mesh, filterItems) {
		var item = null;
		while (true) {
			if (mesh.item) {
				// found an item
				if ( ! filterItems || filterItems.indexOf(mesh.item) >= 0) {
					// this is the intersecting Item
					item = mesh.item;
				} else if(mesh.item.parent) {
					// may be a child item in a Group
					console.warn("Intersecting a Group child Item?", item);
					// move to parent item
					item = mesh.item.parent;
				}
				break;
			} else if (mesh.parent) {
				// move to parent
				mesh = mesh.parent;
			} else {
				// reached top most. its not a Item
				// highly unlikely
				console.warn("Found a mesh without Item", mesh);
				break;
			}
		}
		return item;
	};

	Utils.getCameraLookAtVector = function (camera) {

		let v = new THREE.Vector3(0, 0, -1);
		v.applyQuaternion(camera.quaternion);

		return v;
	};

	Utils.getMouseOnScreen = ( function () {

		var vector = new THREE.Vector2();

		return function getMouseOnScreen( screen, pageX, pageY, inDOM ) {
			if (inDOM) {
				vector.set(
					( pageX - screen.left ),
					( pageY - screen.top )
				);
			} else {
				// mouse co-ordinates normalized to the range -1 to 1
				// Top left corner of the domElement is (-1, 1) and center is (0, 0)
				vector.set(
					(( pageX - screen.left ) / screen.width) * 2 - 1,
					-(( pageY - screen.top ) / screen.height) * 2 + 1
				);
			}
			return vector;
		};
	}() );

	Utils.createRuler = function (start, end, font, camera, _box) {

		var dist = end.distanceTo(start);
		if ( ! dist) {
			return null;
		}

		var lineMaterial = new THREE.LineBasicMaterial( { color: 0X000000 } );
		var lineGeometry = new THREE.Geometry();
		lineGeometry.vertices.push(
			end.clone(),
			start.clone()
		);

		var lineMesh = new THREE.Line( lineGeometry, lineMaterial );
		// lineMesh.mode = THREE.LineStrip;
		lineMesh.position.y = _box.min.y-1;

		var group = new THREE.Group();
		group.add(lineMesh);

		if (start.z < end.z) {
			var t = start;
			start = end;
			end = t;
		}

		var lineDirection = end.clone().sub(start);
		var xAxis = new THREE.Vector3(1, 0, 0);
		var angle = xAxis.angleTo(lineDirection);

		var text = dist.toFixed(1)+"m";
		var textPos = end.clone().add(start).multiplyScalar(0.5);

		var shapes = font.generateShapes(text, 0.5);
		var textGeometry = new THREE.ShapeBufferGeometry(shapes);

		var textMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });
		var textMesh = new THREE.Mesh(textGeometry, textMaterial);
		textMesh.lookAt(0, 1, 0);  // look up

		textGeometry.computeBoundingBox();
		var textBox = textGeometry.boundingBox.clone();

		textMesh.position.copy(textPos);
		textMesh.position.y = _box.min.y-1;
		if (angle < Math.PI/2) {
			// move down
			textMesh.position.z += textBox.max.y;
		} else {
			// move up
			textMesh.position.z -= textBox.min.y;
		}
		group.add(textMesh);

		return group;
	};

	Utils.deDuplicateArray = function (arr) {
		// if (typeof Set !== "undefined") {
		//   return [...new Set(arr)];
		// }

		// fallback for non ES5
		var out = [];
		for(var i=0, j=0, l=arr.length; i<l; i++) {
			var item = arr[i];
			if (out.indexOf(item) < 0) {
				out[j++] = item;
			}
		}
		return out;
	};

	Utils.getLocalBBox = function (object) {
		object.updateMatrix();

		var oldMatrix = object.matrix;
		var oldMatrixAutoUpdate = object.matrixAutoUpdate;

		object.updateMatrixWorld();
		let m = new THREE.Matrix4();
		if (object.parent !== null)
		{
			m.getInverse(object.parent.matrixWorld, false);
		}
		// to prevent matrix being reassigned
		object.matrixAutoUpdate = false;
		object.matrix = m;
		object.updateMatrixWorld();

		var bounds = new THREE.Box3();
		bounds.setFromObject(object);

		object.matrix = oldMatrix;
		object.matrixAutoUpdate = oldMatrixAutoUpdate;
		object.updateMatrixWorld();

		return bounds;
	};

	Utils.cloneObject3D = function (object) {
		var clone = object.clone();
		clone.convexHull = object.convexHull;
		return clone;
	};

	/**
	 *  Traverse the scene,
	 *  for each node, generate EdgesGeometry with 10 degree threshold
	 *  From generated EdgesGeometry accumulate vertices(points)
	 *
	 *  With the accumulated points, generate ConvexGeometry (which uses convex hull internally)
	 */
	Utils.setConvexHull = function( scene ) {
		// return null;
		if (scene.convexHull) {
			return scene.convexHull;
		}

		var points = [];
		var scenePosition = scene.position.clone();
		var sceneRotation = scene.rotation.clone();
		scene.position.set(0, 0, 0);
		scene.rotation.set(0, 0, 0);
		scene.updateMatrixWorld( true );
		scene.traverse( function ( node ) {

			if ( ! node.geometry) {
				return;
			}

			var i, l, point;

			var geometry = new THREE.EdgesGeometry(node.geometry, 10);

			if ( geometry !== undefined ) {

				if ( geometry.isGeometry ) {

					var vertices = geometry.vertices;

					for ( i = 0, l = vertices.length; i < l; i ++ ) {

						point = vertices[ i ].clone();
						point.applyMatrix4( node.matrixWorld );

						points.push( point );

					}

				} else if ( geometry.isBufferGeometry ) {

					var attribute = geometry.attributes.position;

					if ( attribute !== undefined ) {

						for ( i = 0, l = attribute.count; i < l; i ++ ) {

							point = new THREE.Vector3();

							point.fromBufferAttribute( attribute, i ).applyMatrix4( node.matrixWorld );

							points.push( point );

						}

					}

				}

			}

		} );


		var geometry = new THREE.ConvexGeometry( points );
		var material = new THREE.MeshBasicMaterial( {
			color: 0xffff00,
			// shading: THREE.FlatShading,
			opacity: 0.3,
			transparent: true,
			side: THREE.DoubleSide,
		} );
		scene.convexHull = new THREE.Mesh( geometry, material );
		scene.convexHull.position.copy(scenePosition);
		scene.convexHull.rotation.copy(sceneRotation);
		scene.position.copy(scenePosition);
		scene.rotation.copy(sceneRotation);
		// scene.add(scene.convexHull);

		return scene.convexHull
	};

	return Utils;
})();


var SceneReference = function(box) {
	this.position = new THREE.Vector3();
	this.size = new THREE.Vector3(1, 1, 1);
	if (box) {
		box.getCenter(this.position);
		this.position.y = box.min.y;

		box.getSize(this.size);
		this.box = box;
	} else {
		this.box = new THREE.Box3();
	}
};
SceneReference.prototype.normalise = function(vec) {
	return vec.sub(this.position).divide(this.size);
};
SceneReference.prototype.denormalise = function(vec) {
	return vec.multiply(this.size).add(this.position);
};

Object.assign(THREE.PerspectiveCamera.prototype, {

	lookAtPosition: function (x, y, z) {

		if( ! this.lookAtPos) {
			this.lookAtPos = new THREE.Vector3();
		}

		if (x.isVector3) {
			this.lookAtPos.copy(x);
		} else {
			this.lookAtPos.set(x, y, z);
		}
		this.lookAt(x, y, z);
	}

});

Object.assign(THREE.Box3.prototype, {
	setFromObjectBoxes: function () {
		// Computes the world-axis-aligned bounding box of an object (including its children),
		// accounting for both the object's, and children's, world transforms
		var box;
		return function ( object ) {
			if ( box === undefined ) box = new THREE.Box3();

			var scope = this;
			this.makeEmpty();
			object.updateMatrixWorld( true );
			object.traverse( function ( node ) {
				var geometry = node.geometry;
				if ( geometry !== undefined ) {
					if ( geometry.boundingBox === null ) {
						geometry.computeBoundingBox();
					}
					box.copy( geometry.boundingBox );
					box.applyMatrix4( node.matrixWorld );
					scope.union( box );

				}
			});
			return this;
		};

	}(),

	setFromObjectTranasform: function( object, transform ) {
		this.makeEmpty();
		return this.expandByObject( object, transform );
	},

	expandByObjectTransform: function () {
		var v1 = new THREE.Vector3();
		return function expandByObject( object, transform ) {
			var scope = this;
			object.updateMatrixWorld( true );
			object.traverse( function ( node ) {
				var i, l;
				var geometry = node.geometry;
				if ( geometry !== undefined ) {
					if ( geometry.isGeometry ) {
						var vertices = geometry.vertices;
						for ( i = 0, l = vertices.length; i < l; i ++ ) {
							v1.copy( vertices[ i ] );
							v1.applyMatrix4( node.matrixWorld );
							if ( transform !== undefined ) v1.applyMatrix4( transform );
							scope.expandByPoint( v1 );
						}
					} else if ( geometry.isBufferGeometry ) {
						var attribute = geometry.attributes.position;
						if ( attribute !== undefined ) {
							for ( i = 0, l = attribute.count; i < l; i ++ ) {
								v1.fromBufferAttribute( attribute, i ).applyMatrix4( node.matrixWorld );
								if ( transform !== undefined ) v1.applyMatrix4( transform );
								scope.expandByPoint( v1 );
							}
						}
					}
				}
			} );
			return this;
		};
	}(),

	/**
	 * Check if furniture is inside a box. Check for only x and z axis. y not required, it will be on floor
	 * @param itemBox
	 * @returns {boolean}
	 */
	containsItemBox: function ( itemBox ) {

		return this.min.x <= itemBox.min.x && itemBox.max.x <= this.max.x &&
			// this.min.y <= itemBox.min.y && itemBox.max.y <= this.max.y &&
			this.min.z <= itemBox.min.z && itemBox.max.z <= this.max.z;

	},
});

var EVENT = {
	READY: 'ready',
	SAVE: 'save',
	ITEM_ADDED: 'itemadded',
	ITEM_UPDATED: 'itemupdated',
	ITEM_ENTER: 'itementer',
	ITEM_LEAVE: 'itemleave',
	ITEM_SELECTED: 'itemselected',
	ITEM_UNSELECTED: 'itemunselected',
	ITEM_COUNTCHANGED: 'itemcountchanged',
	ITEM_ROTATED: 'itemrotated',
	ITEM_DRAG_START: 'itemdragstart',
	ITEM_DRAG_MOVE: 'itemdragmove',
	ITEM_DRAG_END: 'itemdragend',
	ZOOM: 'zoom',
};
"use strict";

var MESH_TYPE = {
	FLOOR: 'floor',
	WALL: 'wall',
	CHAIR: 'chair',
	TABLE: 'table'
};

var TOOLS = {
	VIEW: 'view',
	SELECT: 'select',
	RULER: 'ruler'
};

var PIXEL_TO_SCENE = 1;  // factor for converting pixels to scene units

var SceneViewer = function(domElement, progressElement) {

	var _this = this;
	window.viewer = this;

	this.domElement = domElement;
	this.progressElement = progressElement;
	this.canvas = null;

	this.roomScene = null;
	this.chair = null;
	this.table = null;

	this.scene = null;
	this.camera = null;
	this.renderer = null;
	this.clock = null;

	this.controls = null;
	this.controlType = 'ViewerControls';

	this.dragControls = null;

	this.sceneType = 'basic'; // 'obj', 'gltf', basic'

	this.screen = {
		x: 0,
		y: 0,
		width: 0,
		height: 0,
		halfWidth: 0,
		halfHeight: 0
	};

	this.isRotatingItem = false;
	this.itemRotationDirection = 1;
	this.itemRotationSpeed = Math.PI; // rotation angle per second
	this.walkThroughHeight = 1.83; // 1.83 meter or 6 feet

	this.animReq = null;

	this.viewMode = ''; //2D/3D/DH
	this.isSwitchingCamera = false;

	this.zoom2D = {
		min: 0.8,
		max: 10,
		speed: 1
	};

	this.fcarpets = [];
	this.fbox = new THREE.Box3();
	this.worker = null;

	this.ready = false;

	var _hud,
		_box,
		_furnitures = [],
		_meshes = [],
		_selectedObject = [],
		_textureLoader = new THREE.TextureLoader(),
		_camera3D = null,
		_camera2D = null,
		_cameraDH = null;

	var defaults = {
		editing: true,
		viewMode: "3D"
	};

	this.datControls = new function () {
		this.ambientLight = {
			intensity: 2.0,
			color: 0xffffff
		};
		this.topLight = {
			intensity: 2.5,
			color: 0xffffff
		};
		this.pointLight = {
			intensity: 2.5,
			color: 0xffffff
		};
		this.FurnitureColor = 0xffffff;
	};

	this.init = function (opts) {
		console.log("Init");

		// this.initWorker();

		opts = opts || {};
		this.opts = Object.assign({}, defaults, opts);

		this.initRenderer();
		this.initCamera(new THREE.Vector3(0, 10, 20));

		this.initScene();

		// this.addPostProcessors();

		this.clock = new THREE.Clock();

		window.addEventListener('resize', this.onResize, false);
		// this.renderer.domElement.addEventListener('wheel', function (e) {
		//     _this.mouseWheel(e);
		// }, false);

		this.initLights(this.scene);
		this.initControls(this.controlType);

		this.loadScene();
		this.preload();

		//this.initStats();

		this.updateScreenSize();

		this.render();
	};

	this.initWorker = function() {
		this.worker = new Worker('src/js/3d/worker.js');
		this.worker.onmessage = function (e) {
			// console.log("Worker said ", e.data, e);
			_this.handleWorkerMessage(e.data);
		};
	};

	this.handleWorkerMessage = function (data) {
		// console.log("Worker data ", data);
		switch (data.source) {
			case 'ViewerControls':
				_this.controls.handleWorkerResult(data.cmd, data.data);
				break
		}
	};

	this.initScene = function () {
		// this.scene = new THREE.Scene();
		this.scene = new THREE.Scene();
		this.scene.background = new THREE.Color(0XFFFFFF);
		this.scene.walls = [];
		this.scene.floors = [];
	};

	this.initRenderer = function(opts) {
		var defaults = {
			antialias: true,
			// alpha: true
		};

		opts = (typeof opts !== 'undefined' && opts) ? opts : {};

		var props = Object.assign({}, defaults, opts);

		var renderer = new THREE.WebGLRenderer(props);

		renderer.autoClear = false;

		renderer.physicallyCorrectLights = true;
		renderer.gammaOutput = true;
		renderer.gammaFactor = 2.2;
		renderer.setPixelRatio(window.devicePixelRatio);

		renderer.shadowMap.enabled = true;
		renderer.shadowMapSoft = true;
		renderer.shadowMap.type = THREE.PCFSoftShadowMap;

		renderer.setClearColor(new THREE.Color(0xFFFFFF));
		// renderer.setSize(window.innerWidth, window.innerHeight);
		renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
		renderer.shadowMap.enabled = true;

		this.renderer = renderer;
		this.canvas = renderer.domElement;

		console.log('CANVAS: ', this.canvas.clientWidth, this.canvas.clientHeight);
		console.log('CONTAINER: ', this.domElement.clientWidth, this.domElement.clientHeight);

		this.domElement.appendChild(this.canvas);
	};

	this.initCamera = function(initialPosition) {
		var position = (initialPosition !== undefined) ? initialPosition : new THREE.Vector3(-30, 40, 30);

		_camera3D = new THREE.PerspectiveCamera(45, this.canvas.clientWidth/ this.canvas.clientHeight, 0.1, 1000);
		_camera3D.position.copy(position);
		_camera3D.lookAt(new THREE.Vector3(0, 0, 0));

		_camera2D = new THREE.PerspectiveCamera(45, this.canvas.clientWidth/ this.canvas.clientHeight, 0.1, 1000);
		_camera2D.position.set(0,50,0);
		_camera2D.lookAtPosition(0,0,0);

		_cameraDH = new THREE.PerspectiveCamera(45, this.canvas.clientWidth/ this.canvas.clientHeight, 0.1, 1000);
		_cameraDH.position.set(0,50,0);
		_cameraDH.lookAtPosition(0,0,0);

		this.camera = _camera3D;
	};

	this.initLights = function(scene, initialPosition) {
		// var position = (initialPosition !== undefined) ? initialPosition : new THREE.Vector3(-40, 30, 30);
		// var b = new THREE.Box3();
		// b.set(new THREE.Vector3( -6.0238318443298375, 0, -16.198741912841797 ),
		//     new THREE.Vector3( 6.2557258605957, 3.39528489112854, 16.167579650878906 ));
		// var c = b.getCenter();
		//
		// var pointLight = new THREE.PointLight(_this.datControls.pointLight.color, _this.datControls.pointLight.intensity);
		// // pointLight.position.set(c.x, b.max.y-0.5, c.z);
		// pointLight.position.copy(c);
		// pointLight.target = _this.camera;
		// pointLight.decay = 2;
		// pointLight.name = "DefaultPointLight";
		//
		// scene.add(pointLight);

		// directional light
		// var b = {
		//     "min":{"x":-6.0238318443298375,"y":0,"z":-16.198741912841797},
		//     "max":{"x":6.2557258605957,"y":3.39528489112854,"z":16.167579650878906}
		// };

		var topLight = _this.topLight = new THREE.DirectionalLight(new THREE.Color(_this.datControls.topLight.color));
		topLight.position.set(0, 50, 0);
		// topLight.position.set(b.max.x, c.y, c.z);
		// topLight.target.position.set(b.min.x, c.y, c.z);
		topLight.castShadow = true;
		topLight.shadow.mapSize.width = 2048;
		topLight.shadow.mapSize.height = 2048;
		// topLight.shadow.camera.left = b.min.z;
		// topLight.shadow.camera.right = b.max.z;
		// topLight.shadow.camera.top = b.max.y;
		// topLight.shadow.camera.bottom = b.min.y;
		topLight.name = 'TopLight';
		scene.add(topLight);

		_this.ambientLight = new THREE.AmbientLight(new THREE.Color(_this.datControls.ambientLight.color), _this.datControls.ambientLight.intensity);
		_this.ambientLight.name = "DefaultAmbientLight";

		scene.add(_this.ambientLight);
	};

	this.initControls = function(controlsType) {

		if ( ! controlsType) {
			controlsType = 'ViewerControls';
		}

		var controls = null;
		if (controlsType === 'ViewerControls') {
			controls = new THREE.ViewerControls(this.camera, this.renderer.domElement, _this.scene, _this);
			controls.staticMoving = true;
			controls.dynamicDampingFactor = 0.3;
			controls.lat = 0;
			controls.lon = -90;
			controls.constrainVertical = true;
			controls.verticalMin = THREE.Math.degToRad(75);
			controls.verticalMax = THREE.Math.degToRad(115);

			controls.addEventListener( EVENT.ZOOM, _this.onZoom );
		}

		_this.controls = controls;
		_this.controls.enabled = false;  // enable after 'ready'

		this.setupDragControls();

	};

	this.initDATControls = function () {
		var gui = new dat.GUI();

		// light controls
		function addLightControl(controller, target, title) {
			var folder = gui.addFolder(title);

			// intensity
			var intensity = folder.add(controller, 'intensity', 0, 5);
			intensity.onChange(function ( val ) {
				target.intensity = val;
			});

			// color
			var colorCtrl = folder.addColor(controller, 'color');
			colorCtrl.onChange(function ( val ) {
				target.color = new THREE.Color( val );
			});
		}

		// ambient light controls
		addLightControl(this.datControls.ambientLight, _this.ambientLight, 'Ambient Light');
		addLightControl(this.datControls.topLight, _this.topLight, 'Top Light');

		// Go To Home
		var miscFolder = gui.addFolder('Misc');
		miscFolder.add(_this.renderer, 'gammaOutput').onChange(function () {
			_this.scene.traverse(function (node) {
				if (node.isMesh && node.material) {
					node.material.needsUpdate = true;
				}
			});
		});

		miscFolder.add(_this.renderer, 'physicallyCorrectLights').onChange(function () {
			_this.scene.traverse(function (node) {
				if (node.isMesh && node.material) {
					node.material.needsUpdate = true;
				}
			});
		});

		// add model controls

		var AddModelControls = {
			AddChair: function () {
				_this.addChair();
			},
			AddTable: function () {
				_this.addTable();
			},
			TableWithGlass: function () {
				_this.addTableWithGlass();
			}
		};

		// add chair
		gui.add(AddModelControls, 'AddChair');
		gui.add(AddModelControls, 'AddTable');
		gui.add(AddModelControls, 'TableWithGlass');


		// material colors
		// gui.addColor(this.datControls, 'FurnitureColor').onChange(function(val) {
		//     for(var i=0; i<_furnitures.length; i++) {
		//         _this.setMaterialColor(_furnitures[i], val);
		//     }
		// });

		// Go To Home
		gui.add(new function() {
			this.HOME = function() {
				_this.resetCamera();
			}
		}, 'HOME');

		gui.add(new function() {
			this.ViewIn2D = function() {
				_this.ViewIn2D();
			}
		}, 'ViewIn2D');
		gui.add(new function() {
			this.ViewIn3D = function() {
				_this.ViewIn3D();
			}
		}, 'ViewIn3D');

		var ssaoFolder = gui.addFolder('SSAO');

		if (_this.ssaoPass) {
			ssaoFolder.add( _this.ssaoPass, 'output', {
				'Default': THREE.SSAOPass.OUTPUT.Default,
				'SSAO Only': THREE.SSAOPass.OUTPUT.SSAO,
				'SSAO Only + Blur': THREE.SSAOPass.OUTPUT.Blur,
				'Beauty': THREE.SSAOPass.OUTPUT.Beauty,
				'Depth': THREE.SSAOPass.OUTPUT.Depth,
				'Normal': THREE.SSAOPass.OUTPUT.Normal
			} ).onChange( function ( value ) {
				_this.ssaoPass.output = parseInt( value );
			} );
			ssaoFolder.add( _this.ssaoPass, 'kernelRadius' ).min( 0 ).max( 32 );
			ssaoFolder.add( _this.ssaoPass, 'minDistance' ).min( 0.001 ).max( 0.02 );
			ssaoFolder.add( _this.ssaoPass, 'maxDistance' ).min( 0.01 ).max( 0.3 );
		}

		_this.datgui = gui;

	};

	this.addPostProcessors = function() {
		_this.composer = new THREE.EffectComposer(_this.renderer);

		_this.ssaoPass = new THREE.SSAOPass(_this.scene, _this.camera, _this.domElement.clientWidth, _this.domElement.clientHeight);
		_this.ssaoPass.kernelRadius = 16;
		_this.ssaoPass.renderToScreen = true;
		_this.composer.addPass(_this.ssaoPass);

		// _this.renderPass = new THREE.RenderPass(_this.scene, this.camera);
		// _this.composer.addPass(_this.renderPass);
		// _this.saoPass = new THREE.SAOPass(_this.scene, _this.camera, false, true);
		// _this.saoPass.renderToScreen = true;
		// _this.composer.addPass(_this.saoPass);
	};

	this.onResize = function () {

		if (_this.disposed) return;

		if (_box) {
			PIXEL_TO_SCENE = (_box.getSize().x/_this.domElement.clientWidth) || 1;
		}

		_camera2D.aspect = (_this.domElement.clientWidth / _this.domElement.clientHeight);
		_camera2D.updateProjectionMatrix();

		_camera3D.aspect = (_this.domElement.clientWidth / _this.domElement.clientHeight);
		_camera3D.updateProjectionMatrix();

		_cameraDH.aspect = (_this.domElement.clientWidth / _this.domElement.clientHeight);
		_cameraDH.updateProjectionMatrix();

		_this.renderer.setSize(_this.domElement.clientWidth, _this.domElement.clientHeight);
		if(_this.ssaoPass) {
			_this.ssaoPass.setSize(_this.domElement.clientWidth, _this.domElement.clientHeight);
		}
		_this.updateScreenSize();
		_this.controls.handleResize();
		_this.dragControls.handleResize();

		// console.log("ONRESIZE: ", _this.domElement.clientWidth, _this.domElement.clientHeight);
	};

	this.updateScreenSize = function () {
		var canvas = _this.renderer.domElement;
		_this.screen.width = canvas.width;
		_this.screen.height = canvas.height;
		_this.screen.halfWidth = canvas.width/2;
		_this.screen.halfHeight = canvas.height/2;
	};

	this.render = function( time ) {
		TWEEN.update(time);

		let delta = _this.clock.getDelta();
		_this.animReq = requestAnimationFrame(_this.render);
		//_this.stats.update();
		if ( ! _this.ready) {
			return;
		}

		_this.controls.update(delta);
		if(_this.composer) {
			_this.composer.render();
		} else {
			// _this.scene.simulate();
			_this.renderer.clear();
			if (_this.renderer.getPixelRatio() !== window.devicePixelRatio) {
				_this.renderer.setPixelRatio(window.devicePixelRatio);
			}
			_this.renderer.render(_this.scene, _this.camera);
			if(_this.needsScreenShot) {
				_this.needsScreenShot = false;
				_this.screenShot = {
					data: _this.renderer.domElement.toDataURL('image/png'),
					width: _this.renderer.domElement.width,
					height: _this.renderer.domElement.height,
				};
				if(_this.screenShotCB) {
					setTimeout(function () {
						try {
							_this.screenShotCB(_this.screenShot);
							_this.screenShotCB = null;
						} catch(err) {
							console.error("Error while processing 2d image.", err);
						}
					}, 0);
				}
			}
		}

		// _this.renderer.clearDepth();
		// if (_this.dragControls) {
		//     _this.dragControls.render(_this.renderer, _this.camera);
		// }

		if (_this.isRotatingItem) {
			console.log('rotateSelectedItem called');
			_this.rotateSelectedItem(delta);
		}
	};

	this.initStats = function() {
		this.stats = new Stats();
		document.body.appendChild(this.stats.domElement);

		return this.stats;
	};

	this.preload = function() {
    let models = [];
		if (this.opts.json) {
      models = this.extractFurnitureModels(this.opts.json);
		} else if (this.opts.models) {
			let iModels = this.opts.models;
			for(var k in iModels) {
				if (iModels.hasOwnProperty(k)) {
          models.push(iModels[k]);
				}
			}
		}
		console.log("PRE_LOAD", models);
		models.map(function (model) {
			ModelLoader.load(model, null, null);
    })
	};

	this.loadScene = function() {

		if (this.opts.model3d.fileType === 'basic') {
			// for testing purposes
			this.loadBasicScene();
			return;
		}

		let loaders = [];
    loaders.push(new Promise(this.load2DScene.bind(this)));
		loaders.push(new Promise(this.load3DScene.bind(this)));

    Promise.all(loaders).then(this.onSceneLoad.bind(this));

		// if (_this.opts.viewMode === "2D") {
		// 	_this.load2DScene(function () {
		// 		_this.load3DScene(function () {
		// 			_this.onSceneLoad();
		// 		});
		// 	});
		// } else  {
		// 	_this.load3DScene(function () {
		// 		_this.load2DScene(function () {
		// 			_this.onSceneLoad();
		// 		});
		// 	});
		// }

	};

	this.load3DScene = function (cb) {
		if (this.opts.model3d) {
			ModelLoader.load(this.opts.model3d, function (m, scene, error) {
				console.log("3D Model Loaded", m, scene, error);

				_this.scene3D = scene.clone();
				_this.scene3D.name = "3Dfloor";

				// _this.roomScene = scene.clone();
				// _this.roomScene.name="3Dfloor";

				// _this.loadRoomScene(_this.roomScene);

				if (cb) {
					cb();
				}

			}, this.onSceneLoadProgress);
		} else {
			console.warn("3D Model is not available");
			setTimeout(function () {
				if (cb) {
					cb();
				}
			});
		}
	};

	this.load2DScene = function (cb) {
		if (this.opts.model2d) {
			ModelLoader.load(this.opts.model2d, function (m, scene, error) {
				console.log("2D Model Loaded", m, scene, error);

				_this.scene2D = scene.clone();
				_this.scene2D.name = "2Dfloor";
				_this.scene2D.visible = false;

				_this.scene2D.traverse( function ( child ) {
					if(child.material){
						child.material.color.setHex(0x000000);
					}
				});

				if (cb) {
					cb();
				}

			}, this.onSceneLoadProgress);
		} else {
			console.warn("2D Model is not available");
			setTimeout(function () {
				if (cb) {
					cb();
				}
			});
		}
	};

	this.onSceneLoad = function () {
		var scenes = {
			'3D': _this.scene3D,
			'2D': _this.scene2D,
		};
		var firstAvailableModel = true;
		for (var viewMode in scenes) {
			var scene = scenes[viewMode];
			if (scene) {
				console.log("Adding scene ", viewMode);
				_this.scene.add(scene);

				if (firstAvailableModel) {
					// update box
					_this.setBoxFromScene(scene);
				}

				var obstacles = [];
				scene.traverse(function (child) {
					if (child.isMesh) {
						obstacles.push(child);
					}
				});
				try{
					_this.controls.setObstacles(obstacles, viewMode);
				} catch (e) {
					console.error(e);
				}

				// update furniture carpets
				if ( ! _this.fcarpets.length ) {
					_this.buildFurnitureCarpets(scene);
					_this.dragControls.setRoomBoundingBoxes(_this.fcarpets);
				}

				// if 3d
				if (viewMode === '3D') {
					// set walls and floors
					_this.setWallsAndFloors(scene);

					// adjust 3D camera
					console.log("resetCamera");
					_this.resetCamera(obstacles);
				} else {
					console.log("adjustCamera");
					_this.adjustCamera();
				}

				firstAvailableModel = false;
			}
		}

		// hide progress element
		if (_this.progressElement) {
			_this.progressElement.style.display = 'none';
		}

		// enable controls
		_this.controls.enabled = true;
		_this.dragControls.enabled = _this.opts.editing;

		console.log("Set View Mode", _this.opts.viewMode);
		_this.setViewMode(_this.opts.viewMode, true, function () {
			_this.ready = true;
			console.log("READY");
			_this.dispatchEvent( { type: EVENT.READY } );
		});

	};

	this.setViewMode = function (viewMode, force, cb) {
		if (_this.viewMode === viewMode && ! force) {
			setTimeout(function () {
				if (cb) {
					cb();
				}
			});
			return;
		}
		switch (viewMode) {
			case '3D':
				_this.ViewIn3D(cb);
				break;

			case '2D':
				_this.ViewIn2D(force, cb);
				break;

			case 'DH':
				_this.viewInDollhouse(cb);
				break;
		}
	};

	this.setBoxFromScene = function (scene) {
		scene.updateWorldMatrix();
		_box = new THREE.Box3().setFromObject(scene);
		PIXEL_TO_SCENE = (_box.getSize().x/_this.domElement.clientWidth) || 1;
	};

	// this.on3DSceneLoad = function (m, scene, error) {
	//   console.log("Viewer:on3DSceneLoad", m, scene, error);
	//   if (error) {
	//     return;
	//   }
	//
	//   _this.roomScene = scene.clone();
	//   _this.roomScene.name="3Dfloor";
	//
	//   _this.loadRoomScene(scene);
	//
	//   _this.load2DFloor();
	//
	// };

	this.onSceneLoadProgress = function (m, progress) {
		_this.displayProgress(progress);
	};

	this.displayProgress = function (progress) {
		_this.progressElement.setAttribute('value', progress);
	};

	// this.loadRoomScene = function(scene) {
	//   // var scene = _this.roomScene;  // OBJ
	//
	//   console.log("Viewer:onSceneLoad", scene, _this.scene);
	//
	//   // scene.rotation.y = (Math.PI/2);
	//
	//   scene.updateWorldMatrix();
	//   _box = new THREE.Box3().setFromObject(scene);
	//   console.log("BOX:", JSON.stringify(_box));
	//   console.log("SCENE:", scene);
	//
	//   PIXEL_TO_SCENE = (_box.getSize().x/_this.domElement.clientWidth) || 1;
	//
	//   if (_this.progressElement) {
	//     _this.progressElement.style.display = 'none';
	//   }
	//
	//   // load controls
	//   // _this.initDATControls();
	//
	//   // set objects for collision detection
	//
	//   if (_this.viewMode === "3D") {
	//     var obstacles = [];
	//     scene.traverse(function (child) {
	//       if (child.isMesh) {
	//         obstacles.push(child);
	//       }
	//     });
	//     try{
	//       _this.controls.setObstacles(obstacles, '3D');
	//     } catch (e) {
	//       console.error(e);
	//     }
	//
	//     _this.setWallsAndFloors(scene);
	//   } else  {
	//
	//   }
	//
	//   _this.buildFurnitureCarpets(scene);
	//
	//
	//
	//   _this.resetCamera(obstacles);
	//
	//   _this.adjustCamera();
	//
	//   _this.scene.add(scene);
	//
	//   _this.dispatchEvent( { type: EVENT.READY } );
	//
	// };

	this.loadLayout = function (layout, peopleCount, tableCount, chairsPerTable, modelOptions, callback) {

		_this.clearFurnitures();

		ModelLoader.loads(modelOptions, function (models) {

			console.time("LOAD_LAYOUT");

			let items = LayoutFactory.load(layout, peopleCount, tableCount, chairsPerTable, models, _this.fcarpets);
			for(let i=0, l=items.length; i<l; i++) {
				_this.scene.add(items[i].scene);
				if(items[i].convexHull) {
					items[i].convexHull.position.copy(items[i].scene.position);
					items[i].convexHull.rotation.copy(items[i].scene.rotation);
					// _this.scene.add(items[i].convexHull);
				}
				_furnitures.push(items[i]);
			}

			_this.dragControls.clearCollidingFurnitures();

			console.timeEnd("LOAD_LAYOUT");

			if (callback) {
				callback();
			}

		});

	};

	this.getFutureCount = function (layout, maxPeopleCount, peopleCount, tableCount, chairsPerTable, modelOptions, getCount, callback) {

		ModelLoader.loads(modelOptions, function (models) {
			console.time("GET_COUNT");

			let itemsCount = LayoutFactory.load(layout, peopleCount, tableCount, chairsPerTable, models, _this.fcarpets, getCount);

			console.timeEnd("GET_COUNT");
			callback(itemsCount);
		});

	};

	this.resetCamera = function (obstacles){
		var center = new THREE.Vector3();
		_box.getCenter(center);
		var size = new THREE.Vector3();
		_box.getSize(size);

		_this.walkThroughHeight = Math.min(_this.walkThroughHeight, (size.y*3/4));

		var offset = 1;  // 1 meter from wall

		var position = new THREE.Vector3(center.x, _box.min.y+_this.walkThroughHeight, _box.max.z+1);

		var target = new THREE.Vector3();
		target.x = center.x;
		target.y = position.y;
		target.z = _box.min.z+offset;
		// target.copy(center);
		// target.z = _box.min.z - 100;

		if (_this.fbox) {
			position.z = _this.fbox.max.z-offset;  // offset from nearest wall
		} else {
			if (obstacles) {
				var rayCaster = new THREE.Raycaster();
				var direction = position.clone();
				direction.sub(target).normalize();
				rayCaster.set(target, direction);
				var intersects = rayCaster.intersectObjects(obstacles);
				if (intersects.length) {
					position.z = intersects[intersects.length-1].point.z-offset;  // 1 feet from nearest wall
				} else {
					position.z = _box.max.z-offset;  // 1 feet from nearest wall
				}
			}
		}

		console.log("CAMERA_POS: ", JSON.stringify({
			position: position,
			target: target,
			_box: _box,
		}));

		_camera3D.position.copy(position);
		_camera3D.updateProjectionMatrix();
		_camera3D.lookAtPosition(target);

		if (_this.controls && _this.controls.setTarget) {
			_this.controls.setTarget(target);
		}

		_this.controls.lat = 0;
		_this.controls.lon = -90;
	};

	// this.load2DFloor = function () {
	//   ModelLoader.load(this.opts.model2d, this.on2DFloorLoad, this.onSceneLoadProgress);
	// };
	//
	// this.on2DFloorLoad = function (m, sceneObj, error) {
	//   if (error) {
	//     return;
	//   }
	//   var scene = sceneObj.clone();
	//   scene.name = "2Dfloor";
	//   scene.visible = false;
	//
	//   var obstacles = [];
	//   scene.traverse( function ( child ) {
	//     if(child.material){
	//       child.material.color.setHex(0x000000);
	//     }
	//     if (child.isMesh) {
	//       obstacles.push(child);
	//     }
	//   });
	//
	//   if (_this.viewMode === "2D") {
	//     _this.roomScene = scene.clone();
	//     _this.loadRoomScene();
	//     _this.ViewIn2D(true);
	//     return;
	//   }
	//
	//   _this.scene.add( scene );
	//
	// };

	this.loadBasicScene = function() {
		// create the ground plane
		var planeGeometry = new THREE.PlaneGeometry(60, 20, 1, 1);
		var planeMaterial = new THREE.MeshLambertMaterial({color: 0x11FF22, side: THREE.DoubleSide});
		var plane = new THREE.Mesh(planeGeometry, planeMaterial);
		plane.receiveShadow = true;

		// rotate and position the plane
		plane.rotation.x = -0.5 * Math.PI;
		plane.position.x = 0;
		plane.position.y = 0;
		plane.position.z = 0;

		// add the plane to the scene
		this.scene.add(plane);

		// var group = new THREE.Group();

		// create a cube
		var cubeGeometry = new THREE.BoxGeometry(4, 4, 4);
		var cubeMaterial = new THREE.MeshLambertMaterial({color: 0xff0000, side: THREE.DoubleSide});
		var cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
		cube.castShadow = true;

		// position the cube
		cube.position.x = 2; //-4;
		cube.position.y = 2; //3;
		cube.position.z = 2;

		// add the cube to the scene
		this.scene.add(cube);
		_furnitures.push(new Item(cube));
		// group.add(cube);

		var sphereGeometry = new THREE.SphereGeometry(4, 20, 20);
		var sphereMaterial = new THREE.MeshLambertMaterial({color: 0x7777ff, side: THREE.DoubleSide});
		var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);

		// position the sphere
		sphere.position.x = 20;
		sphere.position.y = 5;
		sphere.position.z = 2;
		sphere.castShadow = true;

		// add the sphere to the scene
		this.scene.add(sphere);
		_furnitures.push(new Item(sphere));
		// group.add(sphere);
		// this.scene.add(group);
		// _furnitures.push(new Item(group));
		//  group.position.x = -2;

		// show axes in the screen
		var axes = new THREE.AxesHelper(20);
		this.scene.add(axes);

		// position and point the camera to the center of the scene
		// this.camera.position.set(-30, 40, 30);
		// this.camera.lookAtPosition(this.scene.position);

		this.adjustCamera(this.scene);

		PIXEL_TO_SCENE = (60/_this.domElement.clientWidth);

		_box = new THREE.Box3(new THREE.Vector3(-10, 0, 0), new THREE.Vector3(30, 10, 40));

		// _this.initDATControls();
	};

	this.adjustCamera = function (scene) {
		if (scene) {
			// basic scene for testing
			this.camera.lookAtPosition(scene.position);
			if (_this.controls && _this.controls.setTarget) {
				_this.controls.setTarget(scene.position);
			}
			console.log("SCENE_POS: ", JSON.stringify(scene.position));

			this.camera.position.set(0, 20, 30);
			this.camera.lookAtPosition(0, -10, -30);
			if (_this.controls && _this.controls.setTarget) {
				_this.controls.setTarget(new THREE.Vector3(0, -10, -30));
			}

			console.log("CAMERA_POS: ", JSON.stringify(this.camera.position));
			console.log("LookAt: ", JSON.stringify([0, -10, -30]));

			_this.controls.lat = 0;
			_this.controls.lon = -90;
			return;
		}

		// adjust _camera2D to fit scene
		const size = _box.getSize();
		const center = _box.getCenter();
		const maxDim = Math.max( size.x, size.z ) + 1;
		const fov = _camera2D.fov * ( Math.PI / 180 );
		const dist = (maxDim/2)/Math.abs(Math.tan(fov/2)) + 1;
		_camera2D.position.set(center.x, dist, center.z);
		_camera2D.zoom = _this.zoom2D.min;
		_camera2D.lookAtPosition(center.x, _box.min.y, center.z);
		_camera2D.updateProjectionMatrix();

		_cameraDH.position.set(_box.min.x, dist, _box.max.z);
		_cameraDH.zoom = 1;
		_cameraDH.lookAtPosition(center.x, _box.min.y, center.z);
		_cameraDH.updateProjectionMatrix();

		console.log("CAMERA_2D: center:", JSON.stringify(center), " dist:", dist, "_box", JSON.stringify(_box));


	};

	this.switchCamera = function (camera, animated, callback) {
		// console.log("switchCamera", camera, _this.camera, _camera3D, _camera2D);
		if (this.isSwitchingCamera) return false;

		this.isSwitchingCamera = true;

		if (animated) {
			let cCamera = _this.camera;
			let nCamera = camera;

			const from = {
				position: cCamera.position.clone(),
				lookAt: cCamera.lookAtPos.clone()
			};
			const to = {
				position: nCamera.position.clone(),
				lookAt: nCamera.lookAtPos.clone()
			};

			_this.animateCamera(cCamera, from, to, function () {
				_this.camera = nCamera;
				cCamera.position.copy(from.position);
				cCamera.lookAtPosition(from.lookAt);
				_this.isSwitchingCamera = false;
				if (callback) {
					callback();
				}
			});
		} else {
			_this.camera = camera;
			_this.isSwitchingCamera = false;
			if (callback) {
				callback();
			}
		}

		_this.controls.setObject(camera);
		_this.dragControls.setCamera(camera);
	};

	this.ViewIn2D = function(force, callback) {

		if (_this.viewMode === '2D' && !force) {
			console.warn("Already in 2D mode");
			return false;
		} else if (_this.isSwitchingCamera) {
			console.warn("Switching view");
			return false;
		}
		// make sure player is stopped
		_this.pause();

		let fromViewMode = _this.viewMode;
		_this.viewMode = '2D';

		_this.scene.background = new THREE.Color(0xFFFFFF);

		_this.switchCamera(_camera2D, false, function () {
			if(_this.dhControls) {
				_this.dhControls.enabled = false;
			}

			_this.controls.enabled = true;
			_this.controls.noZoom = false;
			_this.controls.noPan = false;
			_this.controls.noRotate = true;
			_this.controls.setViewMode(_this.viewMode);

			_this.dragControls.setViewMode(_this.viewMode);
			_this.dragControls.enabled = _this.opts.editing;

			_this.setTool(_this.tool);

			var floor2D = _this.scene.getObjectByName( "2Dfloor" );
			if(floor2D){
				floor2D.visible = true;
			}
			var floor3D = _this.scene.getObjectByName( "3Dfloor" );
			if(floor3D){
				floor3D.visible = false;
			}
			// _this.scene.getObjectByName( "DefaultAmbientLight2D" ).visible = true;
			_this.scene.getObjectByName( "DefaultAmbientLight" ).visible = false;
			// _this.scene.getObjectByName( "TopLight2D" ).visible = true;
			// _this.scene.getObjectByName( "TopLight" ).visible = false;
			// _this.camera.position.set(0,50,0);
			// _this.camera.lookAtPosition(0,0,0);
			// _this.scene.updateWorldMatrix(true);

			_this.showRulers(true);
			_this.showFurnitureCarpets(true);
			// _this.enableRuler(true);

			if (callback) {
				callback();
			}
		});

		return true;
	};

	this.ViewIn3D = function(callback) {

		if (_this.viewMode === '3D') {
			console.warn("Already in 3D mode");
			return false;
		} else if (_this.isSwitchingCamera) {
			console.warn("Switching view");
			return false;
		}
		// make sure player is stopped
		_this.pause();

		let fromViewMode = _this.viewMode;
		_this.viewMode = '3D';

		let animate = (fromViewMode === 'DH');
		_this.switchCamera(_camera3D, animate, function () {
			if(_this.dhControls) {
				_this.dhControls.enabled = false;
			}

			_this.controls.enabled = true;
			_this.controls.noZoom = false;
			_this.controls.noPan = false;
			_this.controls.noRotate = false;
			_this.controls.setViewMode(_this.viewMode);

			_this.dragControls.setViewMode(_this.viewMode);
			_this.dragControls.enabled = _this.opts.editing;

			_this.dragControls.dragSelection = false;

			var floor2D = _this.scene.getObjectByName( "2Dfloor" );
			if(floor2D){
				floor2D.visible = false;
			}
			var floor3D = _this.scene.getObjectByName( "3Dfloor" );
			if(floor3D){
				floor3D.visible = true;
			}
			// _this.scene.getObjectByName( "DefaultAmbientLight2D" ).visible = false;
			_this.scene.getObjectByName( "DefaultAmbientLight" ).visible = true;
			// _this.scene.getObjectByName( "TopLight2D" ).visible = false;
			// _this.scene.getObjectByName( "TopLight" ).visible = true;
			_this.showRulers(false);
			_this.showFurnitureCarpets(false);

			if (callback) {
				callback();
			}
		});

		return true;
	};

	this.setTool = function (tool) {
		if( ! _this.opts.editing) return;

		this.tool = tool;
		this.controls.setTool(tool);
		this.dragControls.setTool(tool);
	};

	this.addChair = function() {
		ModelLoader.load(DataProvider.getModel(1), function (cm, chairModel) {
			var mChair = chairModel.clone();
			mChair.name = "SingleChair-"+(Math.random()*1000+1);

			var pos = new THREE.Vector3(0, 0, 0);
			if (_box) {
				var center = _box.getCenter();
				var size = _box.getSize();
				pos.x = center.x;
				pos.y = _box.min.y;
				pos.z = _box.min.z + size.z/4;
			}
			_this.setObjectOnFloor(mChair, pos);

			var iChair = new ChairItem(mChair, cm);
			// var iChair = new WallItem(mChair, cm);
			_this.scene.add(mChair);
			_furnitures.push(iChair);
		});
	};

	this.addTable = function () {

		ModelLoader.load(DataProvider.getModel(2), function (tm, tableModel) {
			var mTable = tableModel.clone();
			mTable.name = "SingleTable-"+(Math.random()*1000+1);

			var pos = new THREE.Vector3(0, 0, 0);
			if (_box) {
				var center = _box.getCenter();
				var size = _box.getSize();
				pos.x = _box.max.x - size.x/4;
				pos.y = _box.min.y;
				pos.z = _box.min.z + size.z/4;
			}
			_this.setObjectOnFloor(mTable, pos);

			var iTable = new TableItem(mTable, tm);
			_this.scene.add(mTable);
			_furnitures.push(iTable);
		});
	};

	this.addTableWithGlass = function () {
		ModelLoader.load(DataProvider.getModel(3), function (tm, tableModel) {
			var mTable = tableModel.clone();
			mTable.name = "SingleGlassTable-"+(Math.random()*1000+1);

			var pos = new THREE.Vector3(0, 0, 0);
			if (_box) {
				var size = _box.getSize();
				pos.x = _box.min.x + size.x/4;
				pos.y = _box.min.y;
				pos.z = _box.min.z + size.z/4;
			}
			_this.setObjectOnFloor(mTable, pos);

			var iTable = new TableItem(mTable, tm);
			_this.scene.add(mTable);
			_furnitures.push(iTable);
		});
	};

	this.clearFurnitures= function() {
		_this.dragControls.setItemUnSelected(null);

		for(var i=0, l=_furnitures.length; i<l; i++) {
			if (_furnitures[i].convexHull) {
					if (_this.scene.children.indexOf(_furnitures[i].convexHull) > 0) {
						_this.scene.remove(_furnitures[i].convexHull);
					}
					Utils.disposeObject3D(_furnitures[i].convexHull);
				}
			_furnitures[i].delete(_this.scene);
		}
		_furnitures.splice(0, _furnitures.length);

	};

	this.deleteSelectedItem = function () {
		if ( ! _selectedObject.length) return false;

		for (var i=0; i<_selectedObject.length; i++) {
			var idx = _furnitures.indexOf(_selectedObject[i]);
			if (idx >= 0) {
				console.log("REMOVE", _furnitures[idx]);
				if (_furnitures[idx].convexHull) {
					if (_this.scene.children.indexOf(_furnitures[idx].convexHull) > 0) {
						_this.scene.remove(_furnitures[idx].convexHull);
					}
					Utils.disposeObject3D(_furnitures[idx].convexHull);
				}
				_furnitures[idx].delete();
				_furnitures.splice(idx, 1);
			}
		}
		let deletedItems = _selectedObject.slice();
		_this.dragControls.setItemUnSelected(null);
		_this.dragControls.removeDeletedFurnituresFromCollidingList(deletedItems);

		var itemsData = _this.getTableAndChairCount();
		_this.dispatchEvent({ type: EVENT.ITEM_COUNTCHANGED, data: itemsData });

		return true;
	};

	this.getTableAndChairCount = function () {
		var noOfChairs = 0;
		var noOfTables = 0;
		_furnitures.forEach(function (item) {
			if (item.itemType === "ChairItem") {
				noOfChairs += 1;
			} else if (item.itemType === "TableItem") {
				noOfTables += 1;
			}
		})

		return {"noOfChairs" : noOfChairs, "noOfTables" : noOfTables};

	};

	this.setObjectOnFloor = function (object, pos, itemBox) {

		var box = itemBox || new THREE.Box3().setFromObject(object);
		pos.y -= box.min.y;
		object.position.copy(pos);

		console.log("OBJECT: Box: ", JSON.stringify(box), " POS:", JSON.stringify(pos));

	};

	this.createFloorPointer = function () {
		if ( ! _this.ready) return;

		var curve = new THREE.EllipseCurve(
			0,  0,            // ax, aY
			10*PIXEL_TO_SCENE, 10*PIXEL_TO_SCENE,           // xRadius, yRadius
			0,  2 * Math.PI,  // aStartAngle, aEndAngle
			false,            // aClockwise
			0                 // aRotation
		);

		var points = curve.getPoints( 50 );
		var geometry = new THREE.BufferGeometry().setFromPoints( points );

		var material = new THREE.LineBasicMaterial( { color : 0xffffff } );

		var floorPointer = new THREE.Line( geometry, material );
		floorPointer.position.y = _box.min.y + 2*PIXEL_TO_SCENE;
		floorPointer.rotation.x = -Math.PI/2;

		floorPointer.visible = false;

		this.scene.floorPointer = floorPointer;

		this.scene.add(this.scene.floorPointer);
	};

	// drag controls

	this.setupDragControls = function () {
		console.log('setupDragControls: ', _furnitures, _this.camera, _this.renderer.domElement);

		_this.dragControls = new THREE.DragControls(_this.domElement, _furnitures, _this.camera, _this.renderer, _this.controls, _this.scene);

		_this.dragControls.addEventListener( EVENT.ITEM_SELECTED, _this.onObjectSelected );
		_this.dragControls.addEventListener( EVENT.ITEM_UNSELECTED, _this.onObjectUnselected );
		_this.dragControls.addEventListener( EVENT.ITEM_DRAG_MOVE, _this.onObjectMoved );
		_this.dragControls.addEventListener( EVENT.ITEM_DRAG_END, _this.onObjectMoved );
		_this.dragControls.setViewer(_this);
		_this.setTool(TOOLS.VIEW);
		_this.dragControls.enabled = false; // enable after 'ready' && _this.opts.editing;

		// _this.dragControls.addEventListener( 'dragstart', function ( event ) { _this.controls.enabled = false; } );
		// _this.dragControls.addEventListener( 'dragend', function ( event ) { _this.controls.enabled = true; } );
		// _this.dragControls.addEventListener( 'selected', _this.onObjectSelected );
		// _this.dragControls.addEventListener( 'deselected', _this.onObjectDeselected );
	};

	this.onObjectSelected = function ( event ) {
		_selectedObject = event.object;


		var data = {
			'itemType': _selectedObject[0].itemType,
			'model': _selectedObject[0].modelData,
			'bbox': _this.objectBBoxOnScreen(_selectedObject[_selectedObject.length-1].scene),
			'multiSelection': event.multiSelection
		};

		_this.dispatchEvent({ type: EVENT.ITEM_SELECTED, data: data })

		// showControlsOverlay(true);
	};

	this.onObjectUnselected = function ( event ) {
		// console.log("onObjectUnselected ", event.object);
		_selectedObject = event.object;

		if ( ! _selectedObject.length) {
			_this.dispatchEvent({ type: EVENT.ITEM_UNSELECTED});
		}

		_this.isRotatingItem = false;
		// showControlsOverlay(false);
	};

	this.onObjectMoved = function ( event ) {
		if (_selectedObject.length) {
			var data = {
				'model': _selectedObject[0].modelData,
				'bbox': _this.objectBBoxOnScreen(_selectedObject[_selectedObject.length-1].scene)
			};
			_this.dispatchEvent({type: event.type, data: data})
		}
	};

	this.applyTexture = function ( textureData, applyto ) {
		console.log("ApplyTexture",  textureData, applyto, _selectedObject);

		if ( ! _selectedObject.length) return;

		_textureLoader.load(textureData.path, function (texture) {
			texture.flipY = false;
			texture.encoding = THREE.sRGBEncoding;

			for(var i=0; i<_selectedObject.length; i++) {
				if (_selectedObject[i].itemType === 'ItemGroup') {
					var children = _selectedObject[i].children;
					for(var c=0; c<children.length; c++) {
						if (children[c].itemType === applyto) {
							children[c].applyTexture(texture, textureData);
						}
					}
				} else if (_selectedObject[i].itemType === applyto) {
					_selectedObject[i].applyTexture(texture, textureData);
				}
			}



			// for(var i=0, l=_furnitures.length; i<l; i++) {
			//
			//     if (_furnitures[i].itemType === 'ItemGroup') {
			//         var children = _furnitures[i].children;
			//         for(var c=0; c<children.length; c++) {
			//             if (children[c].itemType === applyto) {
			//                 children[c].applyTexture(texture, textureData);
			//             }
			//         }
			//     } else if (_furnitures[i].itemType === applyto) {
			//         _furnitures[i].applyTexture(texture, textureData);
			//     }
			//
			// }
		});
	};

	this.buildFurnitureCarpets = function (scene) {
		// creating manual hot spots
		_this.fcarpets = [];
		var originalCarpetMeshes = [];
		// load fcarpets from scene
		scene.traverse(function (child) {
			if (child.isMesh && child.name.startsWith("fcarpet")) {
				var box = new THREE.Box3().setFromObject(child);
				// dirty fix
				box.max.y = _box.max.y; // we are giving some height to help collision detection (without height THREE.Box3..intersectsBox ( box : Box3 ) : Boolean misses some intersections)
				_this.fcarpets.push(box);
				originalCarpetMeshes.push(child);
			}
		});

		console.log("FCARPTES: original", originalCarpetMeshes, " new ", _this.fcarpets);

		// remove original carpets from scene
		originalCarpetMeshes.map(function (mesh) {
			if (mesh.parent) {
				mesh.parent.remove(mesh);
			}
		});

		if ( ! _this.fcarpets.length) {
			// fallback to hardcoded values
			var carpet = _box.clone();
			carpet.min.x += 1;
			carpet.max.x -= 1;
			carpet.min.z += 2;
			carpet.max.z -= 1;

			_this.fcarpets.push(carpet);
		}

		// create carpet mesh
		_this.fcarpetGroup = new THREE.Group();
		_this.fcarpetGroup.visible = false;
		var material = new THREE.LineDashedMaterial({
			color: 0xff3333,
			dashSize: 0.3,
			gapSize: 0.2,
		});
		for (var i=0; i<_this.fcarpets.length; i++) {
			var fc = _this.fcarpets[i];
			var geometry = new THREE.Geometry();
			geometry.vertices.push(new THREE.Vector3(fc.min.x, fc.min.y, fc.min.z));
			geometry.vertices.push(new THREE.Vector3(fc.max.x, fc.min.y, fc.min.z));
			geometry.vertices.push(new THREE.Vector3(fc.max.x, fc.min.y, fc.max.z));
			geometry.vertices.push(new THREE.Vector3(fc.min.x, fc.min.y, fc.max.z));
			geometry.vertices.push(new THREE.Vector3(fc.min.x, fc.min.y, fc.min.z));  // close path

			var mesh = new THREE.Line( geometry, material );
			mesh.computeLineDistances();
			_this.fcarpetGroup.add(mesh);

			// also update the bounding carpet
			_this.fbox.union(fc);
		}

		_this.scene.add(_this.fcarpetGroup);

		// var helper = new THREE.Box3Helper( _this.fbox, 0xffff00 );
		// _this.scene.add(helper);

	};

	this.showFurnitureCarpets = function(show) {
		_this.fcarpetGroup.visible = show;
	};

	this.setWallsAndFloors = function(scene) {
		var wallNames = [
			'Box025',
			'Box026',
			'Box027'
		];
		scene.traverse(function (child) {
			if (child.isMesh) {
				if ( child.name.startsWith('floor')
					|| (child.material && child.material.name.startsWith('floor'))
				) {
					_this.scene.floors.push(child);

				}
				else if ( child.name.startsWith('wall')
					|| (child.material && child.material.name.startsWith('wall'))
				) {
					// else if (wallNames.lastIndexOf(child.name) >= 0) {
					_this.scene.walls.push(child);

				}
			}
		});

    if( ! _this.scene.floors.length) {
      scene.traverse(function (child) {
        if (child.isMesh) {
          if ( child.name.startsWith('carpet')
            || (child.material && child.material.name.startsWith('carpet'))
          ) {
            _this.scene.floors.push(child);

          }
        }
      });
		}


		console.log("FLOORS", _this.scene.floors);
		console.log("WALLS", _this.scene.walls);
	};

	this.createWallsPlanes = function() {
		_this.scene.wallsPlanes = [];

		for(var i=0; i<_this.scene.walls.length; i++) {
			var wall = _this.scene.walls[i];

		}

	};

	this.objectBBoxOnScreen = function (obj, camera) {

		camera = camera || _this.camera;

		obj.updateMatrixWorld(true);
		var box3 = new THREE.Box3().setFromObject(obj);

		var vertices = Utils.box3Corners(box3);

		var vertex = new THREE.Vector3();
		var min = new THREE.Vector3(1, 1, 1);
		var max = new THREE.Vector3(-1, -1, -1);

		for (var i = 0; i < vertices.length; i++) {
			vertex.copy(vertices[i]);
			var vertexScreenSpace = vertex.project(camera);
			min.min(vertexScreenSpace);
			max.max(vertexScreenSpace);
		}

		var box = new THREE.Box2(min, max);
		// box.multiply()
		//
		//
		// var vector = new THREE.Vector3();
		//
		//
		// obj.updateMatrixWorld();
		// var box = new THREE.Box3().setFromObject(obj);
		//
		// if (! _this.selectedBoxHelper) {
		//     _this.selectedBoxHelper = new THREE.BoxHelper(obj, 0xffff00);
		//     _this.scene.add(_this.selectedBoxHelper);
		// }
		//
		//
		//
		// console.log("objectBBoxOnScreen", box.clone(), _this.screen, obj);
		//
		// // vector.setFromMatrixPosition(obj.matrixWorld);
		// box.min.project(camera);
		// box.max.project(camera);
		//
		// console.log("objectBBoxOnScreen ofter project", box.clone());

		// vector.x = ( vector.x * _this.screen.halfWidth ) + _this.screen.halfWidth;
		// vector.y = - ( vector.y * _this.screen.halfHeight ) + _this.screen.halfHeight;

		box.min.x = (box.min.x+1) * _this.screen.halfWidth/window.devicePixelRatio;
		box.min.y = (-box.min.y+1) * _this.screen.halfHeight/window.devicePixelRatio;

		box.max.x = (box.max.x+1) * _this.screen.halfWidth/window.devicePixelRatio;
		box.max.y = (-box.max.y+1) * _this.screen.halfHeight/window.devicePixelRatio;

		return box;
	};

	this.toJSON = function () {
		var data = {
			'furnitures': []
		};

		var ref = new SceneReference(_box);
		for (var i=0, l=_furnitures.length; i<l; i++) {
			data.furnitures.push(_furnitures[i].toJSON(ref));
		}

		return data;

	};

	this.fromJSON = function (data, callback) {
		if ( ! data) return;

		_this.clearFurnitures();

		var furnitures = data.furnitures || [];
		var ref = new SceneReference(_box);

		var furnitureLoaders = furnitures.map(function (furniture) {
			return new Promise(function (resolve) {
				loadItem(furniture, resolve, ref);
			});
		});

		Promise.all(furnitureLoaders).then(function (result) {
			console.log("ALL FURNITURES LOADED");
			var collidingFurnitures = [];
			for (var i = 0; i < result.length; i++) {
				_this.scene.add(result[i].scene);
				_furnitures.push(result[i]);
				if (result[i].isColliding) {
					collidingFurnitures.push(result[i]);
				}
			}
			_this.dragControls.updateCollidingFurnitures(collidingFurnitures);
			if(callback){
				callback();
			}
		})

	};

	function loadItem(data, itemResolve, ref) {

		if (data.modelData) {
			// console.log("Loading model", data.modelData);
			// load model, create item and then resolve
			ModelLoader.load(data.modelData, function (m, lodedModel) {
				var model = lodedModel.clone();
				// model.position.fromArray(data.position);
				// model.rotation.fromArray(data.rotation);
				// create new item with loaded model
				var item = new Items[data.itemType](model, m).fromJSON(data, ref);
				// console.log("Model loaded", data.itemType, data.modelData);
				loadChildren(item, data, itemResolve, ref);
			});
		} else {
			// no model required. This might be a group
			// create the group item
			// position it
			// and then, load children
			var item = new Items[data.itemType]().fromJSON(data, ref, _camera2D);
			// if (item.scene) {
			//     item.scene.position.fromArray(data.position);
			//     item.scene.rotation.fromArray(data.rotation);
			// }
			// console.log("Item created", data.itemType);

			loadChildren(item, data, itemResolve, ref);
		}
	}

	function loadChildren(item, data, resolve, ref) {
		if (data.children && data.children.length) {
			// call loadItem on all children
			// add them to this item and then resolve
			// console.log("Loading model children..", data.itemType, data.children);
			var itemLoaders = data.children.map(function (childData) {
				return new Promise(function (resolve) {
					loadItem(childData, resolve, ref);
				});
			});
			Promise.all(itemLoaders).then(function (result) {
				console.log("All model children loaded", data.itemType, result);
				for (var i = 0; i < result.length; i++) {
					item.add(result[i]);
				}
				resolve(item);
			});
		} else {
			resolve(item);
		}
	}

	this.extractFurnitureModels = function(data) {
		let models = [];
		if( ! data || ! data.furnitures) {
			return models;
		}
		for (let i=0; i<data.furnitures.length; i++) {
			this.extractModelData(models, data.furnitures[i]);
		}
		return models;
	};

	this.extractModelData = function(models, data) {
		if (data.modelData) {
      models.push(data.modelData);
		} else {
			if (data.children && data.children.length) {
				for(let i=0; i<data.children.length; i++) {
					this.extractModelData(models, data.children[i]);
				}
			}
		}
	};

	this.rotateSelectedItem = function (delta) {
		if ( ! _selectedObject.length) return;

		console.log('rotated');
		for (var i=0; i<_selectedObject.length; i++) {
			_selectedObject[i].rotateByAngle(delta*_this.itemRotationSpeed*(-1*_this.itemRotationDirection));
		}

		if (_this.dragControls.hud) {
			_this.dragControls.hud.update();
		}
	};

	/**
	 *
	 * @param direction int 1(clockwise) or -1(anti clockwise)
	 */
	this.startItemRotation = function(direction) {
		direction = direction || 1;
		console.log('rotate started');
		if ( ! _selectedObject.length) return;

		_this.isRotatingItem = true;
		_this.itemRotationDirection = direction;

	};

	this.stopItemRotation = function() {
		console.log('rotate stoped')
		_this.isRotatingItem = false;
	};

	this.setItemTexture = function(data, callback) {
		console.log("setItemTexture", data);
		if ( ! _selectedObject.length) return;

		_textureLoader.load(data.img, function (texture) {
			texture.flipY = false;
			texture.encoding = THREE.sRGBEncoding;

			for (var i=0; i<_selectedObject.length; i++) {
				if (_selectedObject[i].itemType === 'ItemGroup') {
					var children = _selectedObject[i].children;
					for(var c=0; c<children.length; c++) {
						// if (children[c].itemType === applyto) {
						children[c].applyTexture(texture, data);
						// }
					}
				} else { //if (_selectedObject.itemType === applyto) {
					_selectedObject[i].applyTexture(texture, data);
				}
			}

			if(callback) {
				callback()
			}
		});

	};

	this.setItemColor = function(color, callback) {
		console.log("setItemColor", color);

		if ( ! _selectedObject.length) return;

		if ( ! (color instanceof THREE.Color)) {
			color = new THREE.Color(color);
		}

		for (var i=0; i<_selectedObject.length; i++) {
			if (_selectedObject[i].itemType === 'ItemGroup') {
				var children = _selectedObject[i].children;
				for(var c=0; c<children.length; c++) {
					// if (children[c].itemType === applyto) {
					children[c].applyColor(color);
					// }
				}
			} else { //if (_selectedObject.itemType === applyto) {
				_selectedObject[i].applyColor(color);
			}
		}

		if(callback) {
			callback()
		}

	};

	this.cloneSelectedItem = function () {
		if( ! _selectedObject.length) return;

		var selectedItems = _selectedObject.slice(0, _selectedObject.length);
		_this.dragControls.setItemUnSelected(null);

		var clones = [];

		for (var i=0; i<selectedItems.length; i++) {
			var clone = selectedItems[i].clone();

			// adjust position
			clone.scene.position.x += 0.5;
			clone.scene.position.z += 0.5;
			if (clone.convexHull) {
				clone.convexHull.position.x += 0.5;
				clone.convexHull.position.z += 0.5;
			}
			clone.updateSize(true);

			_this.scene.add(clone.scene);
			// if (clone.convexHull) {
			//  _this.scene.add(clone.convexHull);
			// }
			_furnitures.push(clone);
			_this.dragControls.setItemSelected(clone, true);
			clones.push(clone);
		}

		for (var j = 0; j <= 0.5; j += 0.05) {
			for (var k = 0; k < clones.length; k++) {
				var clonedItem = clones[k];
				clonedItem.scene.position.x -= j;
				if (clone.convexHull) {
				clonedItem.convexHull.position.x -= j;
				}
			}
			var sucesses = _this.doItemsGoOutOfBounds(clones);
			if (!sucesses) {
				break;
			}

		}

		for (var x = 0; x <= 0.5; x += 0.05) {
			for (var y = 0; y < clones.length; y++) {
				var clonedItem = clones[y];
				clonedItem.scene.position.z -= x;
				if (clone.convexHull) {
					clonedItem.convexHull.position.z -= x;
				}
			}
			var sucesses = _this.doItemsGoOutOfBounds(clones);
			if (!sucesses) { // if clones are not going out of bound 'stop'
				break;
			}

		}

		_this.dragControls.detectCollisionBetweenFurnitures();

		var itemsData = _this.getTableAndChairCount();
		_this.dispatchEvent({ type: EVENT.ITEM_COUNTCHANGED, data: itemsData });

	};

	this.doItemsGoOutOfBounds = function (items) {
		if (!_this.fcarpets.length) {
			return false;
		}

		var sucesses = false;

		for (var i = 0; i < items.length; i++) {

			var item = items[i];
			var itemSize = item.getSize();

			var roomBoundingBox = new THREE.Box3();
			var oldRoomBoundingBox = roomBoundingBox;
			for (var j = 0; j < _this.fcarpets.length; j++) {
				var boundingBox = _this.fcarpets[j];
				if (boundingBox.intersectsBox(item.getBox3())) {
					roomBoundingBox = boundingBox;
				}
			}

			if (oldRoomBoundingBox === roomBoundingBox) { // if can not find bounding box of selected items then return
				return false;
			}

			var pos = item.scene.position.clone();
			var xAxisIntersected = (pos.x + itemSize.x / 2) > roomBoundingBox.max.x;
			var negativeXAxisIntersected = (pos.x - itemSize.x / 2) < roomBoundingBox.min.x;
			var zAxisIntersected = (pos.z + itemSize.z / 2) > roomBoundingBox.max.z;
			var negativeZAxisIntersected = (pos.z - itemSize.z / 2) < roomBoundingBox.min.z;

			var isIntersected = xAxisIntersected || negativeXAxisIntersected || zAxisIntersected || negativeZAxisIntersected;

			if (isIntersected) {
				sucesses = true;
			}
		}

		return sucesses;
	};

	this.addItems = function(data, callback) {
		console.log("Dragged data", data);

		if (data instanceof Array) {
			var onAddItems = data.map(function (itemData) {
				return new Promise(function (resolve) {
					_this.addItem(itemData, resolve);
				});
			});
			Promise.all(onAddItems).then(function () {
				if(callback) {
					callback();
				}
			})
		} else {
			_this.addItem(data, callback);
		}
	};

	this.addItem = function (modelData, callback) {
		var pos = null;
		if (modelData.loc) {
			console.log("Drop location provided", modelData.loc);
			let pointOnFloor = _this.controls.getFloorIntersection(modelData.loc);
			if ( ! pointOnFloor) {
				var err = new Error("Floor pointer missing. Possibly missing Floor- mesh in metting room model.");
				console.log(err.message);
				if(callback) {
					callback(modelData, err);
				}
				return;
			}
			pos = pointOnFloor;
		}

		ModelLoader.load(modelData, function (modelData, model, error) {
			if (error) {
				if(callback) {
					callback(modelData, error);
				}
				return;
			}

			var itemsToAdd = modelData.count || 1;
			var itemsAdded = 0;
			var row = 0,
				col = 0;
			var boxCenter = new THREE.Vector3();
			_box.getCenter(boxCenter);
			var boxSize = new THREE.Vector3();
			_box.getSize(boxSize);

			if ( ! pos) {
				console.warn("Drop location not provided. will default to box center");
				pos = boxCenter.clone();
			}

			_this.unselectItem(); //unselect previous items

			console.log("Start position", JSON.stringify(pos));

			var itemBox = new THREE.Box3().setFromObject(model);
			var itemSize =  new THREE.Vector3();
			itemBox.getSize(itemSize);

			var maxRows = Math.floor(boxSize.z/itemSize.z) || 1;
			var maxCols = Math.floor(boxSize.x/itemSize.x) || 1;

			var cols = Math.min(Math.ceil(Math.sqrt(itemsToAdd)), maxCols) || 1;
			var rows = Math.ceil(itemsToAdd/cols) || 1;
			// total area required to place all items
			var sizeRequired = {
				x: cols*itemSize.x,
				z: rows*itemSize.z,
			};

			// anchor wil be the start position from where items will be placed
			var anchor = {
				x: _box.min.x,
				z: _box.min.z,
			};
			// calculating anchor position so that items are placed 'around' `pos` point
			for (var axis in anchor) {
				if (anchor.hasOwnProperty(axis)) {
					if (pos[axis] < boxCenter[axis]) {
						anchor[axis] = Math.max(pos[axis]-sizeRequired[axis]/2, _box.min[axis]);
					} else {
						var maxPt = Math.min(pos[axis]+sizeRequired[axis]/2, _box.max[axis]);  // get max point on the right
						// get left anchro by subtracting required width from the right
						// and make sure it stays within box boundries
						anchor[axis] = Math.max(maxPt-sizeRequired[axis], _box.min[axis]);
					}
				}
			}

			console.log("COLSROWS",cols, rows, "sizeRequired", sizeRequired, "anchor", anchor);

			// set on floor
			pos.y = _box.min.y - itemBox.min.y;

			for(var r=0; r<rows; r++) {

				if (itemsAdded >= itemsToAdd) {
					break;
				}

				for (var c=0; c<cols; c++) {

					var mItem = Utils.cloneObject3D(model);
					mItem.name = "Item-R"+row+"C"+col+"-"+(Math.random()*1000+1);

					pos.x = anchor.x + itemSize.x*c + itemSize.x/2;
					pos.z = anchor.z + itemSize.z*r + itemSize.z/2;

					mItem.position.copy(pos);

					var modelType = modelData.type;
					switch (modelData.type) {
						case 'tables':
							modelType = 'TableItem';
							break;
						case 'chairs':
							modelType = 'ChairItem';
							break;
						default:
							modelType = modelData.type;
					}
					var item;
					if (Items[modelType]) {
						modelData.type = modelType;
						item = new Items[modelType](mItem, modelData);
					} else {
						item = new Items['Item'](mItem, modelData);
					}
					if (item) {
						_this.dragControls.setItemSelected(item, true);

						if(item.convexHull) {
							item.convexHull.position.copy(item.scene.position);
							item.convexHull.rotation.copy(item.scene.rotation);
							// _this.scene.add(item.convexHull);
						}

						_furnitures.push(item);
					}
					itemsAdded++;

					var furniture = _this.getCollidingFurniture(item);
					var originalPos = item.scene.position.clone();
					item.updateSize(true);
					var minDistance = 0.05;

					if (furniture && modelData.count == 1 && modelData.auto_placement == true) {
						var furnitureBox  = furniture.getBox3();

						for (var count = 0; count < 8; count++) {
							itemSize = item.getSize();

							switch (count) {
								case 0:
									item.scene.position.x = furnitureBox.min.x - ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furniture.scene.position.z;
									break;
								case 1:
									item.scene.position.x = furnitureBox.min.x - ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furnitureBox.min.z - ((itemSize.z / 2) + minDistance);
									break;
								case 2:
									item.scene.position.x = furniture.scene.position.x;
									item.scene.position.z = furnitureBox.min.z - ((itemSize.z / 2) + minDistance);
									break;
								case 3:
									item.scene.position.x = furnitureBox.max.x + ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furnitureBox.min.z - ((itemSize.z / 2) + minDistance);
									break;
								case 4:
									item.scene.position.x = furnitureBox.max.x + ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furniture.scene.position.z;
									break;
								case 5:
									item.scene.position.x = furnitureBox.max.x + ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furnitureBox.max.z + ((itemSize.z / 2) + minDistance);
									break;
								case 6:
									item.scene.position.x = furniture.scene.position.x;
									item.scene.position.z = furnitureBox.max.z + ((itemSize.z / 2) + minDistance);
									break;
								case 7:
									item.scene.position.x = furnitureBox.min.x - ((itemSize.x / 2) + minDistance);
									item.scene.position.z = furnitureBox.max.z + ((itemSize.z / 2) + minDistance);
									break;
								default:
									break;
							}

							item.convexHull.position.copy(item.scene.position);
							item.updateSize(true);
							var collidingFurniture = _this.getCollidingFurniture(item);
							var isItemOutOfBound = _this.isItemOutOfBound(item);
							if (!collidingFurniture && !isItemOutOfBound) {
								// var randomNumForXAxis = _this.getRandomNumber(0.01, 0.02);
								// var randomNumForZAxis = _this.getRandomNumber(0.01, 0.02);
								//
								// /* move x and z position very slightly so there won't be a problem of overlap due to collision detection failure
								// (our collision detection won't work in 2 items have same size and position) */
								// item.scene.position.x += randomNumForXAxis;
								// item.scene.position.z += randomNumForZAxis;
								//
								// item.convexHull.position.copy(item.scene.position);
								break;
							}
						}

						if (collidingFurniture || isItemOutOfBound) {
							item.scene.position.copy(originalPos);
							item.convexHull.position.copy(item.scene.position);
							item.updateSize(true);
						}

					}

					_this.scene.add(item.scene);

					if (itemsAdded >= itemsToAdd) {
						break;
					}
				}

			}

			_this.dragControls.detectCollisionBetweenFurnitures();

			if (callback) {
				callback(modelData);
			}

			var itemsData = _this.getTableAndChairCount();
			_this.dispatchEvent({ type: EVENT.ITEM_COUNTCHANGED, data: itemsData });
		});
	};

	this.getRandomNumber = function(min, max) { // The returned value is no lower than (and may possibly equal) min, and is less than (and not equal) max.
	  return Math.random() * (max - min) + min;
	}

	this.getCollidingFurniture = function(item) {
		for (var count = 0; count < _furnitures.length; count++) {
			var furniture = _furnitures[count];
			var colliding = _this.dragControls.detectCollision(item, furniture);
			if (colliding) {
				return furniture;
			}
		}
	};

	this.isItemOutOfBound = function(item) {
		var isOutOfBound = true;
		var itemBox = item.getBox3();

		for (var count = 0; count < _this.fcarpets.length; count++) {
			var carpet = _this.fcarpets[count];
			var containsItem = carpet.containsItemBox(itemBox);
			if (containsItem) {
				isOutOfBound = false;
				return isOutOfBound;
			}

			return isOutOfBound;
		}
	};

	this.addRuler = function (start, end) {
		if( ! _this.opts.editing) return;

		FontLoader.load('fonts/helvetiker_regular.typeface.json', function ( font ) {
			var mRuler = Utils.createRuler(start, end, font, _camera2D, _box);
			if ( ! mRuler) {
				return;
			}

			var data = {
				start: start.clone(),
				end: end.clone(),
			};
			var ruler = new RulerItem(mRuler, data);
			_this.scene.add(ruler.scene);
			_furnitures.push(ruler);

			var data = {
				'itemType': ruler.itemType,
				'model': ruler.modelData,
				'bbox': _this.objectBBoxOnScreen(ruler.scene),
				'multiSelection': false
			};

			_this.dispatchEvent({ type: EVENT.ITEM_ADDED, data: data });
		});
	};

	this.showRulers = function (show) {
		for (var i=0, l=_furnitures.length; i<l; i++) {
			if (_furnitures[i].isRuler) {
				_furnitures[i].setVisible(show);
			}
		}
	};

	this.dispose = function () {
		if (_this.disposed) return;
    _this.disposed = true;

		// cancel animation frame
		cancelAnimationFrame(_this.animReq);

		// remove event listners
		window.removeEventListener('resize', this.onResize, false);

		if (_this.datgui) {
			_this.datgui.destroy();
		}

		// stop camera player
		if (_this.player) {
			_this.player.reset();
			_this.player = null;
		}

		// remove viewer controls
		_this.controls.dispose();
		_this.controls = null;

		// remove drag controls
		_this.dragControls.dispose();
		_this.dragControls = null;

		// remove stats
		// this.stats.domElement.remove();
		// this.stats = null;

		// remove all furnitures
		for (var i=0; i<_furnitures.length; i++) {
			_furnitures[i].delete();
			_furnitures[i] = null;
		}
		_furnitures = null;

		Utils.disposeObject3D(_this.scene);
		_this.roomScene = null;

		if (_this.renderer.dispose) {
			_this.renderer.dispose();
		}
		_this.renderer.domElement.remove();
		_this.renderer = null;
		_this.camera = null;
		_camera2D = null;
		_camera3D = null;

	};

	this.unselectItem = function(people) {
		_this.dragControls.setItemUnSelected(null);
	};

	this.onZoom = function ( event ) {
		_this.dispatchEvent({
			type: EVENT.ZOOM,
			value: event.value,
		});
	};

	this.setZoom2D = function(value) {
		_this.camera.zoom = (_this.zoom2D.min+(_this.zoom2D.max-_this.zoom2D.min)*value/100);
		_this.camera.updateProjectionMatrix();
	};

	this.viewInDollhouse = function (callback) {

		if (_this.viewMode === 'DH') {
			if(callback) {
				callback();
			}
			return false;
		} else if (_this.isSwitchingCamera) {
			console.warn("Switching view");
			return false;
		}
		let fromViewMode = _this.viewMode;
		_this.viewMode = 'DH';

		if ( ! _this.dhControls) {
			_this.dhControls = new THREE.OrbitControls(_cameraDH, _this.renderer.domElement);
			_this.dhControls.minPolarAngle = 0; // radians
			_this.dhControls.maxPolarAngle = Math.PI/2;
			_this.dhControls.enablePan = false;
			_this.dhControls.enableKeys = false;

			var boxSize = _box.getSize();
			var maxDim = Math.max(boxSize.x, boxSize.y, boxSize.z);
			_this.dhControls.maxDistance = maxDim*2;
			_this.dhControls.minDistance = 0.2;
		}
		_this.controls.setViewMode('DH');
		_this.dragControls.setViewMode('DH');
		_this.controls.enabled = false;
		_this.dragControls.enabled = false;
		_this.dhControls.enabled = true;

		_this.scene.background = new THREE.Color(0x000000);

		var floor2D = _this.scene.getObjectByName( "2Dfloor" );
		if(floor2D){
			floor2D.visible = false;
		}
		var floor3D = _this.scene.getObjectByName( "3Dfloor" );
		if(floor3D){
			floor3D.visible = true;
		}

		// _this.scene.getObjectByName( "DefaultAmbientLight2D" ).visible = false;
		_this.scene.getObjectByName( "DefaultAmbientLight" ).visible = true;

		_this.showRulers(false);
		_this.showFurnitureCarpets(false);

		let animate = (fromViewMode === '3D');
		_this.switchCamera(_cameraDH, animate, callback);

		return true;
	};

	this.enterFullScreen = (function(){
		let fsReqFunc = null;
		if (document.documentElement.requestFullscreen) {
			fsReqFunc = 'requestFullscreen';
		} else if (document.documentElement.mozRequestFullScreen) { /* Firefox */
			fsReqFunc = 'mozRequestFullScreen';
		} else if (document.documentElement.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
			fsReqFunc = 'webkitRequestFullscreen';
		} else if (document.documentElement.msRequestFullscreen) { /* IE/Edge */
			fsReqFunc = 'msRequestFullscreen';
		}

		let fsExitFunc = null;
		if (document.exitFullscreen) {
			fsExitFunc = 'exitFullscreen';
		} else if (document.mozCancelFullScreen) { /* Firefox */
			fsExitFunc = 'mozCancelFullScreen';
		} else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
			fsExitFunc = 'webkitExitFullscreen';
		} else if (document.msExitFullscreen) { /* IE/Edge */
			fsExitFunc = 'msExitFullscreen';
		}

		return function(enter) {
			console.log("FS", fsReqFunc, fsExitFunc);
			if (enter) {
				_this.domElement[fsReqFunc]();
			} else if (document.fullscreenElement){
				document[fsExitFunc]();
			}

		}
	})();

	this.captureScreenShot = function(cb) {
		console.log("captureScreenShot");
		this.needsScreenShot = true;
		this.screenShotCB = cb;
	};

	this.getBBox = function () {
		return _box;
	};

};


SceneViewer.prototype = Object.create( THREE.EventDispatcher.prototype );
SceneViewer.prototype.constructor = SceneViewer;

SceneViewer.prototype.play = function () {
	this.viewInDollhouse(function () {
		this._play();
	}.bind(this));
};
SceneViewer.prototype._play = function() {
	if ( ! this.playbackTrack) {
		this.createPlaybackTrack();
	}

	if ( ! this.player) {
		this.player = CameraPlayer
			.create(this.camera, this.playbackTrack)
			.onEnd(function () {
				this.dispatchEvent({type: 'playbackend'});
			}.bind(this));
	}
	this.player.play();
};
SceneViewer.prototype.pause = function() {
	if (this.player) {
		this.player.pause();
	}
};
SceneViewer.prototype.createPlaybackTrack = function () {
	this.playbackTrack = [];

	const _box = this.getBBox();
	const size = _box.getSize();
	const center = _box.getCenter();
	const maxDim = Math.max( size.x, size.z ) + 1;
	const fov = this.camera.fov * ( Math.PI / 180 );
	const dist = (maxDim/2)/Math.abs(Math.tan(fov/2)) + 1;

	const lookAtCenter = new THREE.Vector3(center.x, _box.min.y, center.z);
	// start at dollhouse position
	this.playbackTrack.push({
		position: new THREE.Vector3(_box.min.x, dist, _box.max.z),
		lookAt: lookAtCenter
	});
	const roomTop = this.walkThroughHeight;
	const room = _box.clone();
	const inset = 1; // inset from room boundary
	room.min.x += inset;
	room.min.z += inset;
	room.max.x -= inset;
	room.max.z -= inset;
	room.max.y = this.walkThroughHeight;
	// move to front-top-left corner with easing function inout
	this.playbackTrack.push({
		position: new THREE.Vector3(room.min.x, room.max.y, room.max.z),
		lookAt: lookAtCenter,
		easing: TWEEN.Easing.Sinusoidal.InOut
	});
// move to front-top-right corner
	this.playbackTrack.push({
		position: new THREE.Vector3(room.max.x, room.max.y, room.max.z),
		lookAt: lookAtCenter
	});
// move to back-top-right corner
	this.playbackTrack.push({
		position: new THREE.Vector3(room.max.x, room.max.y, room.min.z),
		lookAt: lookAtCenter
	});
// move to back-top-left corner
	this.playbackTrack.push({
		position: new THREE.Vector3(room.min.x, room.max.y, room.min.z),
		lookAt: lookAtCenter
	});
// back to front-top-left corner
	this.playbackTrack.push({
		position: new THREE.Vector3(room.min.x, room.max.y, room.max.z),
		lookAt: lookAtCenter,
	});
	// back to dollhouse position with easing function inout
	this.playbackTrack.push({
		position: new THREE.Vector3(_box.min.x, dist, _box.max.z),
		lookAt: lookAtCenter,
		easing: TWEEN.Easing.Sinusoidal.InOut
	});

	// update timings
	var from = this.playbackTrack[0];
	for (var i=1; i<this.playbackTrack.length; i++) {
		var to = this.playbackTrack[i];
		var distance = to.position.distanceTo(from.position);
		to.timing = Math.max(2, Math.min(5, distance*0.2)) * 1000;  // 0.2 seconds per meter clamped to 2-5 seconds
		from = to;
	}
};

SceneViewer.prototype.animateCamera = function (camera, from, to, onComplete) {
	let start = {
		px: from.position.x,
		py: from.position.y,
		pz: from.position.z,
		tx: from.lookAt.x,
		ty: from.lookAt.y,
		tz: from.lookAt.z,
	};

	let end = {
		px: to.position.x,
		py: to.position.y,
		pz: to.position.z,
		tx: to.lookAt.x,
		ty: to.lookAt.y,
		tz: to.lookAt.z,
	};

	let anim = new TWEEN.Tween(start)
		.to(end, 2000)
		.easing(TWEEN.Easing.Sinusoidal.InOut)
		.onUpdate(function ( val ) {
			camera.position.set(val.px, val.py, val.pz);
			camera.lookAtPosition(val.tx, val.ty, val.tz);
		})
		.onComplete(function () {
			if (onComplete) {
				onComplete();
			}
		})
		.start();

	return anim;
};

"use strict";
var MODEL_FILE_TYPE = {
	GLTF: 'gltf',
	OBJ: 'obj'
};

var ModelLoader = (function () {

	var ModelLoader = function () {
		/**
		 * @type {{
		 *     status: 'loading', 'loaded', 'failed'
		 *     scene: object
		 *     onProgress: [<callback-func-list>]
		 *     onLoad: [<callback-func-list>]
		 * }}
		 */
		this.models = {};
	};

	Object.assign(ModelLoader.prototype, {

		loads: function(models, callback) {

			var scope = this;
			var loaders = [];
			var loadedModels = [];
			for (let k in models) {
				if (models.hasOwnProperty(k)) {
					let m = models[k];
					m.k_ = k;
					loaders.push(new Promise(function (resolve) {
						scope.load(m, function (mData, model) {
							loadedModels[m.k_] = {
								data: mData,
								scene: model,
							};
							delete mData.k_;
							resolve();
						});
					}));
				}
			}

			Promise.all(loaders).then(function (result) {
				if (callback) {
					callback(loadedModels);
				}
			});
		},

		load: function (m, callback, onProgress) {
			var scope = this;

			if ( ! this.models[m.id]) {
				this.models[m.id] = {
					status: 'none',
					scene: null,
					onProgress: [],
					onLoad: [],
				}
			}

			if (this.models[m.id].status === 'loaded') {
				// trigger callback immediately
				if (callback) {
					setTimeout(function () {
						callback(m, scope.models[m.id].scene);
					});
				}
			} else if (this.models[m.id].status === 'none' || this.models[m.id].status === 'failed') {
				// add callbacks and trigger download
				this._add_callbacks(this.models[m.id], callback, onProgress);
				this._load(m);
			} else if (this.models[m.id].status === 'loading') {
				// add callbacks
				this._add_callbacks(this.models[m.id], callback, onProgress);
			}

			// if (this.models[m.id]) {
			//     if (callback) {
			//         setTimeout(function () {
			//             callback(m, scope.models[m.id]);
			//         });
			//     } else {
			//         return scope.models[m.id];
			//     }
			// } else {
			//     this._load(m, function (m, scene) {
			//         scope.models[m.id] = scene;
			//         if (callback) {
			//             callback(m, scene);
			//         }
			//     });
			// }
		},

		_load: function (m) {
			var scope = this;

			scope.models[m.id].status = 'loading';

			var managerProgress = 0;
			var modelProgress = 0;
			var manager = new THREE.LoadingManager();
			manager.onProgress = function (item, loaded, total) {
				// console.log('onLoadModelManagerProgress ', loaded, total, item);
				managerProgress = loaded / total * 100;
				scope._fire_onprogress(m, scope.models[m.id], modelProgress, managerProgress);
			};

			var loader = (m.fileType === MODEL_FILE_TYPE.OBJ) ? new THREE.OBJLoader( manager ) : new THREE.GLTFLoader( manager );

			var mPath = (m.path || m.model_path);
			loader.load(mPath, function ( obj ) {
				// on load
				var scene = m.fileType === MODEL_FILE_TYPE.OBJ ? obj : (obj.scene || obj.scenes[0]);

				mPath = mPath.toLowerCase();
				if (mPath.indexOf("chair") >= 0 || mPath.indexOf("table") >= 0) {
					// var gScene = new Physijs.ConvexMesh(new THREE.Geometry(),  new THREE.MeshBasicMaterial( {visible: true}));
					// console.log("VH: loading", scene);
					// scene.traverse(function (child) {
					//   console.log("VH: loading", child, scene);
					//   gScene.add(child);
					// });

					// for (var i=scene.children.length-1; i>=0; i--) {
					//   gScene.add(scene.children[i]);
					// }
					// // gScene.children = scene.children;
					// scene = gScene;
					// console.log("VH: Loaded", scene);

					// var geometry = new THREE.ConvexGeometry( scene );
					// var material = new THREE.MeshBasicMaterial( {
					//   color: 0xffff00,
					//   // shading: THREE.FlatShading,
					//   opacity: 0.5,
					//   transparent: true
					// } );
					// var gScene = new THREE.Mesh( geometry, material );
					// scene.add(gScene);

					Utils.setConvexHull(scene);

				}

				scope.models[m.id].scene = scene;
				scope.models[m.id].status = 'loaded';

				scope._fire_onload(m, scope.models[m.id]);
			}, function (xhr) {
				// on progress
				if ( xhr.lengthComputable ) {
					modelProgress = xhr.loaded / xhr.total * 100;
					scope._fire_onprogress(m, scope.models[m.id], modelProgress, managerProgress);
				}
			}, function ( err ) {
				// on error
				scope.models[m.id].status = 'failed';
				console.error("Could not load model ", m, err);
				scope._fire_onload(m, scope.models[m.id], err);
			});
		},

		_add_callbacks: function (model, onLoad, onProgress) {
			if (onLoad && model.onLoad.indexOf(onLoad) < 0) {
				model.onLoad.push(onLoad);
			}

			if (onProgress && model.onProgress.indexOf(onProgress) < 0) {
				model.onProgress.push(onProgress);
			}
		},

		remove_callbacks: function(m, onLoad, onProgress) {
			var model = this.models[m.id];
			if (model) {
				if (onLoad) {
					var i = model.onLoad.indexOf(onLoad);
					if (i >= 0) {
						model.onLoad.splice(i, 1);
					}
				}
				if (onProgress) {
					var i = model.onProgress.indexOf(onProgress);
					if (i >= 0) {
						model.onProgress.splice(i, 1);
					}
				}
			}
		},

		_fire_onload: function (m, model, error) {
			for (var i=0; i<model.onLoad.length; i++) {
				try{
					model.onLoad[i](m, model.scene, error);
				} catch (e) {
					console.error(e);
				}
			}
			model.onLoad.splice(0);
			model.onProgress.splice(0);
		},

		_fire_onprogress: function (m, model, modelProgress, managerProgress) {
			var progress = (managerProgress*0.5) + (modelProgress*0.5);

			for (var i=0; i<model.onProgress.length; i++) {
				try{
					model.onProgress[i](m, progress);
				} catch (e) {
					console.error(e);
				}
			}
		},

		clearCache: function (id) {
			if (id) {
				delete this.models[id]
			} else {
				this.models = {};
			}
		}

	});

	return new ModelLoader();
})();

var FontLoader = (function () {
	var FontLoader = function () {
		this.fonts = {};
	};

	Object.assign(FontLoader.prototype, {
		load: function (fontPath, callback) {
			if (this.fonts[fontPath]) {
				var font = this.fonts[fontPath];
				if (callback) {
					setTimeout(function () {
						callback(font);
					});
				}
			} else {
				var scope = this;
				var loader = new THREE.FontLoader();
				loader.load(fontPath, function ( font ) {
					scope.fonts[fontPath] = font;
					if (callback) {
						callback(font);
					}
				});
			}
		},
	});

	return new FontLoader();
})();


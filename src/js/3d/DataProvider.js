var DataProvider = (function () {

    var TEXTURE_DB = {
        1: {
            id: 1,
            // path: "static/textures/grey-brown_wood.jpg"
        },
        2: {
            id: 2,
            // path: "static/textures/oak_wood.jpg"
        },
        3: {
            id: 3,
            // path: "static/textures/ps_7.jpg"
        },
        4: {
            id: 4,
            // path: "static/textures/walnut-marin.jpg"
        },
        5: {
            id: 5,
            // path: "static/textures/white_wood.jpg"
        }
    };

    var MODEL_DB = {
        1: {
            id: 1,
            fileType: MODEL_FILE_TYPE.GLTF,
            path: 'models/3D_PACK_FURNITURE/Chair/chair.gltf'
        },
        2: {
            id: 2,
            fileType: MODEL_FILE_TYPE.GLTF,
            path: 'models/3D_PACK_FURNITURE/Table/table_no_glass/table-no glass.gltf'
        },
        3: {
            id: 3,
            fileType: MODEL_FILE_TYPE.GLTF,
            path: 'models/3D_PACK_FURNITURE/Table/table_with_glass_material/table_with_glass.gltf'
        }
    };

    var DataProvider = function () {

        var scope = this;

        this.textures = {};

        // set up textures for chair
        this.textures['chair'] = {};
        [3, 4].map(function (i) {
            scope.textures['chair'][i] = TEXTURE_DB[i];
        });

        // setup table textures
        this.textures['table'] = {};
        [1, 2, 5].map(function (i) {
            scope.textures['table'][i] = TEXTURE_DB[i];
        });

    };

    Object.assign(DataProvider.prototype, {

        getTexture: function (id) {
            return TEXTURE_DB[id];
        },

        getTexturesFor: function (itemType) {
            return this.textures[itemType];
        },

        getModel: function (id) {
            return MODEL_DB[id];
        },

        storeData: function (key, data) {
            console.log("STORE", key, data);
            if (window.localStorage) {
                window.localStorage.setItem(key, JSON.stringify(data));
            }
        },

        getData: function (key) {
            var data = null;
            if (window.localStorage) {
                data = window.localStorage.getItem(key);
                data = JSON.parse(data);
            }
            return data;
        }

    });

    return new DataProvider();
})();

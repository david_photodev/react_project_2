"use strict";

var Item = function (scene, modelData) {
	this.scene = scene || new THREE.Object3D();
	if (scene) {
		this.setScene(scene);

		// console.log("VH: INIT With Scene", this.scene);
		// var cHandler = this.handleCollision.bind(this);
		// this.scene.traverse(function (child) {
		//   if (child._physijs) {
		//     child.addEventListener( 'collision',  cHandler );
		//   }
		// });
		// this.scene.addEventListener( 'collision',  this.handleSceneCollision.bind(this) );
	}
	this.box3 = new THREE.Box3();
	this.lbox3 = null;
	this.size = new THREE.Vector3();
	this.center = new THREE.Vector3();
	this.scene.item = this;
	this.hover = false;
	this.selected = false;
	this.isColliding = false;
	this.isOutOfBound = false;
	this.visible = true;
	this.selectedColor = 0x444444;
	this.errorColor = 0xff0000; // 0xff6347;
	this.emissiveColor = 0x000000;
	this.emissiveIntensity = 0.5;
	this.drag = {
		enabled: true,
		x: true,
		y: false,
		z: true
	};
	this.allowRotate = true;
	this.fixed = false;
	this.dragOffset = new THREE.Vector3();
	this.needsSizeUpdate = true;

	this.modelData = modelData || null;
	this.parent = null;
	this.children = [];
	this.textureData = null;
	this.color = null;
	this.snapToFloor = true;
	this.snapToWall = false;  // @TODO

	this.itemType = 'Item';
};

Object.assign(Item.prototype, {

	setScene: function(scene, move) {
		if (this.scene) {
			this.scene.item = null;

			if (move) {
				// add new scene to parent of existing scene
				if ( ! scene.parent && this.scene.parent) {
					this.scene.parent.add(scene);
				}
				if (this.scene.parent) {
					this.scene.parent.remove(this.scene);
				}
			}
		}

		this.scene = scene;
		scene.item = this;

		var hull = Utils.setConvexHull(this.scene);
		this.convexHull = hull ? hull.clone() : null;

		// this.scene.addEventListener( 'collision', this.handleCollision.bind(this) );
		// console.log("VH: SET Scene", this.scene);

		this.lbox3 = null;
		this.needsSizeUpdate = true;
	},

	handleCollision: function(collided_with, linearVelocity, angularVelocity) {
		console.log("VH: Collided! ", this, "collided_with:", collided_with, " linearVelocity:", linearVelocity, " angularVelocity:", angularVelocity);
	},

	handleSceneCollision: function(collided_with, linearVelocity, angularVelocity) {
		console.log("VH: Scene Collided! ", this, "collided_with:", collided_with, " linearVelocity:", linearVelocity, " angularVelocity:", angularVelocity);
	},

	updateSize: function(force) {
		if (this.scene && (force || this.needsSizeUpdate)) {
			this.box3.setFromObject(this.scene);
			if (! this.lbox3) {
				this.lbox3 = Utils.getLocalBBox(this.scene);
				this.lbox3.getSize(this.size);
			}
			this.box3.getCenter(this.center);
			this.needsSizeUpdate = false;
		}
	},

	// append child item
	add: function (item) {
		this.scene.add(item.scene);
		this.children.push(item);
		item.parent = this;
		this.needsSizeUpdate = true;
	},

	// remove child item
	remove: function (item) {
		var i = this.children.indexOf(item);
		if (i >= 0) {
			this.children.splice(i, 1);
		}
		this.scene.remove(item.scene);
		item.parent = null;

		this.needsSizeUpdate = true;
	},

	// remove this item from the parent
	delete: function(parent) {

		Utils.disposeObject3D(this.scene);

		// if (this.scene.parent) {
		//     this.scene.parent.remove(this.scene);
		// }
		if (this.parent) {
			this.parent.remove(this);
		}
		this.scene.item = null;
		this.scene = null;
	},

	copy: function( source ) {
		if (source.scene) {
			this.scene = source.scene.clone();
			if (this.scene.isMesh && this.scene.material && this.scene.material.map) {
				this.scene.material = this.scene.material.clone();
				this.scene.material.isClone = true;
			}
			this.scene.traverse(function ( child ) {
				if (child.isMesh && child.material && child.material.map) {
					child.material = child.material.clone();
					child.material.isClone = true;
				}
			});
			this.convexHull = source.convexHull.clone()
		} else {
			this.scene = new THREE.Object3D();
		}
		this.box3 = new THREE.Box3();
		this.scene.item = this;
		this.hover = false;
		this.selected = false;
		this.visible = true;
		this.selectedColor = 0x444444;
		this.errorColor = 0xff0000;
		this.emissiveColor = 0x000000;
		this.emissiveIntensity = 0.5;
		this.drag = Object.assign({}, source.drag);
		this.allowRotate = source.allowRotate;
		this.fixed = source.fixed;
		this.dragOffset = new THREE.Vector3();
		this.needsSizeUpdate = true;

		if (source.modelData) {
			this.modelData = JSON.parse(JSON.stringify(source.modelData));
		}
		this.parent = null;
		this.children = [];
		if (source.textureData) {
			this.textureData = JSON.parse(JSON.stringify(source.textureData));
		}
		this.color = source.color ? source.color.clone() : null;
		this.snapToFloor = source.snapToFloor;
		this.snapToWall = source.snapToWall;  // @TODO

		return this;
	},

	clone: function() {
		return new this.constructor().copy( this );
	},

	mouseOver: function () {
		this.hover = true;
		// this.updateHighlight();
	},

	mouseOff: function () {
		this.hover = false;
		// this.updateHighlight();
	},

	setSelected: function ( selected ) {
		if (this.selected !== selected) {
			this.selected = selected;
			this.updateHighlight();
		}
	},

	setColliding: function( colliding ) {
		if (this.isColliding !== colliding) {
			this.isColliding = colliding;
			this.updateHighlight();
		}
	},

	setOutOfBound: function( outOfBound ) {
		if (this.isOutOfBound !== outOfBound) {
			this.isOutOfBound = outOfBound;
			this.updateHighlight();
		}
	},

	updateHighlight: function () {

		var color = 0x000000;
		if (this.isColliding || this.isOutOfBound) {
			color = this.errorColor;
		// } else if (this.hover || this.selected) {
		} else if (this.selected) {
			color = this.selectedColor;
		}

		if (this.emissiveColor === color) {
			return;
		}

		this.emissiveColor = color;
		var emissiveIntensity = this.emissiveIntensity;

		if ( ! this.scene) return;

		this.scene.traverse(function (child) {
			if (child.isMesh) {
				if (child.material && child.material.emissive) {
					if ( ! child.material.isClone) {
						child.material = child.material.clone();
						child.material.isClone = true;
					}
					child.material.emissive.setHex(color);
					child.material.emissiveIntensity = emissiveIntensity;
					child.material.needsUpdate = true;
				} else if (child.materials) {
					var materials = child.materials.map(function (m) {
						if ( ! m.isClone) {
							m = m.clone();
							m.isClone = true;
						}
						return m;
					});
					materials.forEach(function (material) {
						if (material && material.emissive) {
							material.emissive.setHex(color);
							material.emissiveIntensity = emissiveIntensity;
							material.needsUpdate = true;
						}
					});
				}
			}
		});
	},

	clickPressed: function(intersection) {
		this.dragOffset.copy(intersection.point).sub(this.scene.position);
	},

	clickDragged: function(intersection) {
		if (intersection) {
			this.moveToPosition( intersection.point.clone().sub(this.dragOffset), intersection);
			// this.moveToPosition( intersection.point, intersection);
		}
	},

	clickReleased: function () {
		// @TODO
		// hide error if any
	},

	moveToPosition: function (vec3, intersection) {
		// this.scene.position.copy(vec3);moveToPosition
		this.scene.position.x = vec3.x;
		this.scene.position.z = vec3.z;
		if ( ! this.snapToFloor) {
			this.scene.position.y = vec3.y;
		}
		// this.scene.position.copy(vec3);

		if(this.convexHull) {
			this.convexHull.position.copy(vec3);
		}

		// this.scene.traverse(function (child) {
		//   if (child._physijs) {
		//     child.__dirtyPosition = true;
		//     console.log('VH: child moved', child)
		//   }
		// });
		// this.scene.__dirtyPosition = true;
		// }
		this.needsSizeUpdate = true;
	},

	getBox3: function() {
		this.updateSize();
		return this.box3;
	},

	getSize: function() {
		this.updateSize();
		return this.size;
	},

	getCenter: function() {
		this.updateSize();
		return this.center;
	},

	getHalfSize: function() {
		if (this.needsSizeUpdate) {
			this.updateSize();
		}
		return this.halfSize;
	},

	objectHalfSize: function () {
		// console.log("OBOX", JSON.stringify(objectBox));
		// console.log("OSCENE", JSON.stringify(this.scene.position));
		return this.box3.max.clone().sub(this.box3.min).divideScalar(2);
	},

	rotate: function (intersection) {
		if (intersection) {
			var angle = Utils.angle(
				0,
				1,
				intersection.point.x - this.scene.position.x,
				intersection.point.z - this.scene.position.z);

			var snapTolerance = Math.PI / 16.0;
			for (var i = -4; i <= 4; i++) {
				if (Math.abs(angle - (i * (Math.PI / 2))) < snapTolerance) {
					angle = i * (Math.PI / 2);
					break;
				}
			}
			this.scene.rotation.y = angle;
			if(this.convexHull) {
				this.convexHull.rotation.y = angle;
			}

			this.needsSizeUpdate = true;
		}
	},

	rotateByAngle: function(stepAngle, force) {
		// if (snap) {
		//     var snapTolerance = Math.PI / 16.0;
		//     for (var i = -4; i <= 4; i++) {
		//         if (Math.abs(angle - (i * (Math.PI / 2))) < snapTolerance) {
		//             angle = i * (Math.PI / 2);
		//             break;
		//         }
		//     }
		// }

		// let angle_ = this.angle_ || 0;

		let pi2 = (Math.PI*2);
		let angle = stepAngle + this.scene.rotation.y;

		// let angle = angle_ + stepAngle + this.scene.rotation.y;

		// if (angle > pi2) {
		// 	angle -= pi2;
		// } else if(angle < -pi2) {
		// 	angle += pi2;
		// }

		angle = angle % pi2;

		// let snap = false;
		// if ( ! force) {
		// 	var snapTolerance = Math.PI / 16.0;
		// 	for (var i = -4; i <= 4; i++) {
		// 		if (Math.abs(angle - (i * (Math.PI / 2))) < snapTolerance) {
		// 			angle = i * (Math.PI / 2);
		// 			snap = true;
		// 			break;
		// 		}
		// 	}
		// 	if (snap) {
		// 		// console.log("SNAP", angle_, stepAngle, snapTolerance);
		// 		angle_ = stepAngle;
		// 	} else {
		// 		// console.log("SNAP_RESET", angle_, stepAngle, angle, snapTolerance);
		// 		angle_ = 0;
		// 	}
		// }

		// this.angle_ = angle_;

		this.scene.rotation.y = angle;
		if(this.convexHull) {
			this.convexHull.rotation.y = angle;
		}
		// this.needsSizeUpdate = true;
	},

	setVisible: function(visible) {
		this.visible = visible;
		if (this.scene) {
			this.scene.visible = visible;
		}
		for (var i=0, l=this.children.length; i<l; i++) {
			this.children[i].setVisible(visible);
		}
	},

	// setTexture: function (textureData, loader, recursive, filterByItemType) {
	//
	//     var scope = this;
	//     loader = loader || new THREE.TextureLoader();
	//     loader.load(textureData.path, function (texture) {
	//         texture.flipY = false;
	//         texture.encoding = THREE.sRGBEncoding;
	//         scope.textureData = textureData;
	//
	//         // apply to this scene
	//
	//
	//         for (var i=0, l=scope.scene.children.length; i<l; i++) {
	//             var item = scope.scene.children[i].item;
	//             if (item && ( ! itemType || item.itemType === itemType)) {
	//                 console.log("SELECTED", item);
	//                 item.scene.traverse(function ( child ) {
	//                     if (child.isMesh && child.material) {
	//                         if ( ! child.material.isClone) {
	//                             child.material = child.material.clone();
	//                             child.material.isClone = true;
	//                         }
	//
	//                         child.material.map = texture;
	//                         child.material.color = new THREE.Color('#FFFFFF');
	//                         // child.material.blending = THREE.NoBlending;
	//                         child.material.needsUpdate = true;
	//                     }
	//                 })
	//             }
	//         }
	//     }, undefined, function ( err ) {
	//         console.error("Failed to load texture", textureData, err)
	//     });
	//
	// },

	applyTexture: function(texture, textureData) {
		var scope = this;
		if (this.scene.isMesh && this.scene.material && this.scene.material.map) {
			scope.applyTextureToMesh(texture, this.scene);
		}
		this.scene.traverse(function ( child ) {
			if (child.isMesh && child.material && child.material.map) {
				scope.applyTextureToMesh(texture, child);
			}
		});
		this.textureData = textureData;
	},

	applyTextureToMesh: function(texture, mesh) {
		if( ! mesh.material) {
			return;
		}

		if ( ! mesh.material.isClone) {
			mesh.material = mesh.material.clone();
			mesh.material.isClone = true;
		}

		mesh.material.map = texture;
		mesh.material.color = this.color || (new THREE.Color('#FFFFFF'));
		// child.material.blending = THREE.NoBlending;
		mesh.material.needsUpdate = true;
	},

	applyColor: function(color) {
		var scope = this;
		if (this.scene.isMesh && this.scene.material && this.scene.material.map) {
			scope.applyColorToMesh(color, this.scene);
		}
		this.scene.traverse(function ( child ) {
			if (child.isMesh && child.material && child.material.map) {
				scope.applyColorToMesh(color, child);
			}
		});
		this.color = color;
	},

	applyColorToMesh: function(color, mesh) {
		if( ! mesh.material) {
			return;
		}

		if ( ! mesh.material.isClone) {
			mesh.material = mesh.material.clone();
			mesh.material.isClone = true;
		}

		mesh.material.color = color;
		// child.material.blending = THREE.NoBlending;
		mesh.material.needsUpdate = true;
	},

	toJSON: function (ref) {
		var data = {
			itemType: this.itemType,
			position: ref.normalise(this.scene.position.clone()).toArray(),
			rotation: this.scene.rotation.toArray(),
			children: [],
			modelData: this.modelData,
			textureData: this.textureData,
			isColliding: this.isColliding,
			isOutOfBound: this.isOutOfBound,
		};

		if (this.color) {
			data.color = {
				r: this.color.r,
				g: this.color.g,
				b: this.color.b,
			}
		}
		if (this.isRuler) {
			data.start = ref.normalise(this.start.clone()).toArray();
			data.end = ref.normalise(this.end.clone()).toArray();
		}

		for (var i=0, l=this.children.length; i<l; i++) {
			data.children.push(this.children[i].toJSON(ref));
		}

		return data;
	},

	fromJSON: function (data, ref, camera) {

		var scope = this;

		ref.denormalise(this.scene.position.fromArray(data.position));
		if (this.convexHull) {
			ref.denormalise(this.convexHull.position.fromArray(data.position));
		}

		// place it on the floor
		// console.log("Y1", this.scene.position.y);
		// this.scene.position.y = container.min.y; // set `position` on the floor
		// console.log("Y2", this.scene.position.y);
		// var box = new THREE.Box3().setFromObject(this.scene);
		// console.log("box", box);
		// this.scene.position.y += (box.min.y-this.scene.position.y); // move up until the legs are on the floor
		// console.log("Y", this.scene.position.y);

		this.scene.rotation.fromArray(data.rotation);
		if (this.convexHull) {
			this.convexHull.rotation.fromArray(data.rotation);
		}

		if (data.textureData) {
			var loader = new THREE.TextureLoader();
			loader.load((data.textureData.path || data.textureData.img), function (texture) {
				texture.flipY = false;
				texture.encoding = THREE.sRGBEncoding;
				scope.applyTexture(texture, data.textureData);
			});
		}

		if (data.color) {
			scope.applyColor(new THREE.Color(data.color.r, data.color.g, data.color.b));
		}

		if (this.isRuler) {
			FontLoader.load('fonts/helvetiker_regular.typeface.json', function ( font ) {
				scope.start = ref.denormalise(new THREE.Vector3().fromArray(data.start));
				scope.end = ref.denormalise(new THREE.Vector3().fromArray(data.end));
				var mRuler = Utils.createRuler(scope.start, scope.end, font, camera, ref.box);
				if (mRuler) {
					scope.setScene(mRuler, true);
				}
			});
		}

		scope.setColliding(data.isColliding);
		scope.setOutOfBound(data.isOutOfBound);

		return this;
	},

});


var ItemGroup = function (scene) {
	scene = scene || new THREE.Group();

	Item.call(this, scene);

	this.isItemGroup = true;
	this.itemType = 'ItemGroup';
};

ItemGroup.prototype = Object.assign(Object.create(Item.prototype), {
	constructor: ItemGroup,
});


var TableItem = function (scene, modelData) {
	Item.call(this, scene, modelData);

	this.isTable = true;
	this.itemType = 'TableItem';
};

TableItem.prototype = Object.assign(Object.create(Item.prototype), {
	constructor: TableItem,
});


var ChairItem = function (scene, modelData) {
	Item.call(this, scene, modelData);

	this.isChair = true;
	this.itemType = 'ChairItem';
};

ChairItem.prototype = Object.assign(Object.create(Item.prototype), {
	constructor: ChairItem,
});


var WallItem = function (scene, modelData) {
	Item.call(this, scene, modelData);

	this.isWallItem = true;
	this.itemType = 'WallItem';
	this.snapToFloor = false;
	this.snapToWall = true;
};

WallItem.prototype = Object.assign(Object.create(Item.prototype), {
	constructor: WallItem,
});

Object.assign(WallItem.prototype, {
	moveToPosition: function (vec3, intersection) {

		this.scene.lookAt(intersection.face.normal);

		var halfSize = this.getHalfSize();

		var newPos = new THREE.Vector3();
		newPos.addVectors(vec3, intersection.face.normal.clone().multiply(halfSize))

		this.scene.position.x = vec3.x;
		this.scene.position.z = vec3.z;
		this.scene.position.y = vec3.y;
	}
});

var RulerItem = function (scene, data) {
	Item.call(this, scene);

	this.isRuler = true;
	this.itemType = 'RulerItem';
	this.start = data ? data.start : null;
	this.end = data ? data.end : null;
};

RulerItem.prototype = Object.create(Item.prototype);
RulerItem.prototype.constructor = RulerItem;

RulerItem.prototype.moveToPosition = function () {
	console.warn("Cannot move a RulerItem");
};

var Items = Object.freeze({
	'ItemGroup': ItemGroup,
	'TableItem': TableItem,
	'ChairItem': ChairItem,
	'RulerItem': RulerItem,
	'Item': Item
});
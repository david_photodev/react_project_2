var FloorPlan = function (domElement) {
	this.domElement = domElement;
	this.renderer = null;
	this.canvas = null;
	this.scene = null;
	this.camera = null;
	this.animReq = null;
	this.rooms = [];
	this.box = null;
	this.size = new THREE.Vector3();
	this.center = new THREE.Vector3();
	// this.bgColor = new THREE.Color(0xFFFFFF);
	this.color1 = 0x930F3C;
	// this.checkMarkColor = new THREE.Color(0xFCFCFC);
	// this.checkMarkColorSelected = new THREE.Color(0xAB5975);
	// this.pixelToScene = 1;

	this.screen = {
		left: 0,
		top: 0,
		width: 0,
		height: 0,
		canvas: {
			width: 0,
			height: 0
		}
	};
	this.mouse = new THREE.Vector3();
	this.rayCaster = new THREE.Raycaster();
	this.rayCaster.linePrecision = 0.1;

	this.hideMaterial = null;
	this.showMaterial = null;

	this.mouseOverRoom = null;
	this.selectedRoom = null;

	this.defaults = {};
	this.opts = Object.assign({}, this.defaults);

	this.ready = false;
};
FloorPlan.PIXEL_TO_SCENE = 1;

FloorPlan.prototype = Object.create( THREE.EventDispatcher.prototype );
FloorPlan.prototype.constructor = FloorPlan;

FloorPlan.prototype.init = function (options) {
	this.opts = Object.assign({}, this.defaults, options);

	console.log("FloorPlan.init", options, this.opts);

	this.initRenderer();
	this.computeScreen();
	this.initCamera();
	this.initScene();
	this.initLights();
	window.addEventListener('resize', this.onResize.bind(this), false);

	this.render();

	this.loadFloorPlan();
};

FloorPlan.prototype.initScene = function () {
	this.scene = new THREE.Scene();
	this.scene.background = new THREE.Color(0XFFFFFF);
};

FloorPlan.prototype.initRenderer = function () {
	var renderer = new THREE.WebGLRenderer({
		antialias: true,
	});
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setClearColor(new THREE.Color(0xFFFFFF));
	renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
	renderer.physicallyCorrectLights = true;
	// renderer.gammaOutput = true;
	// renderer.gammaFactor = 2.2;

	this.renderer = renderer;
	this.canvas = renderer.domElement;
	this.domElement.appendChild(this.canvas);
};

FloorPlan.prototype.initCamera = function () {
	var camera = new THREE.PerspectiveCamera(45, this.canvas.clientWidth/this.canvas.clientHeight, 0.1, 1000);
	camera.position.set(0,10,0);
	camera.lookAtPosition(0,0,0);
	this.camera = camera;

	// var camera = new THREE.OrthographicCamera( 1, 1, 1, 1, 0.1, 1000 );
	// camera.position.set(0,10,0);
	// camera.lookAt(0,0,0);
	// this.camera = camera;

};

FloorPlan.prototype.initLights = function() {
	this.topLight = new THREE.DirectionalLight(new THREE.Color(0xffffff));
	this.topLight.position.set(0, 50, 0);
	this.scene.add(this.topLight);

	this.ambientLight = new THREE.AmbientLight(new THREE.Color(0xffffff), 3);
	this.ambientLight.name = "DefaultAmbientLight";
	this.scene.add(this.ambientLight);
};

FloorPlan.prototype.onResize = function () {

	this.adjustCamera();

	if (this.renderer.getPixelRatio() !== window.devicePixelRatio) {
		this.renderer.setPixelRatio(window.devicePixelRatio);
	}
	this.renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);

	this.computeScreen();

	// if (this.box) {
	//   var x = this.box.getSize(this.size).x;
	//   // FloorPlan.PIXEL_TO_SCENE = (x/this.domElement.clientWidth) || 1;
	//   // console.log("onResize: ", FloorPlan.PIXEL_TO_SCENE, x, this.domElement.clientWidth, (x/this.domElement.clientWidth), JSON.stringify(this.size));
	//   console.log("onResize: canvas:", JSON.stringify(this.screen.canvas));
	// } else {
	//   console.error("onResize: canvas:", JSON.stringify(this.screen.canvas));
	// }
};

FloorPlan.prototype.computeScreen = function () {
	var box = this.domElement.getBoundingClientRect();
	// adjustments come from similar code in the jquery offset() function
	var d = this.domElement.ownerDocument.documentElement;
	this.screen.left = box.left + window.pageXOffset - d.clientLeft;
	this.screen.top = box.top + window.pageYOffset - d.clientTop;
	this.screen.width = box.width;
	this.screen.height = box.height;

	var canvas = this.renderer.domElement;
	this.screen.canvas.width = canvas.width;
	this.screen.canvas.height = canvas.height;
};

FloorPlan.prototype.render = function () {
	this.animReq = requestAnimationFrame(this.render.bind(this));
	if ( ! this.ready) {
		return;
	}
	if (this.renderer.getPixelRatio() !== window.devicePixelRatio) {
		this.renderer.setPixelRatio(window.devicePixelRatio);
		this.renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
		this.computeScreen();
	}
	this.renderer.render(this.scene, this.camera);
};

FloorPlan.prototype.loadFloorPlan = function () {
	if ( ! this.opts.floorplan) {
		console.error("floorplan not provided");
		return;
	}

	if (this.fpObj) {
		console.error("floorplan already loaded", this.fpObj);
		return;
	}

	ModelLoader.load(this.opts.floorplan, this.onFloorPlanLoaded.bind(this));
};

FloorPlan.prototype.onFloorPlanLoaded = function (m, fpObj, error) {
	console.log("onFloorPlanLoaded", m, fpObj, error);

	if (error) return;

	this.clear();
	this.fpObj = fpObj.clone();

	this.box = new THREE.Box3().setFromObject(fpObj);
	this.box.getSize(this.size);
	this.box.getCenter(this.center);
	this.adjustCamera();

	this.scene.add(this.fpObj);

  console.log("FP: opt.rooms", this.opts.rooms);
	// load rooms
	let models = {};
	for (let i=0, l=this.opts.rooms.length; i<l; i++) {
		const room = this.opts.rooms[i];
		models[room.id] = room.model;
	}
	console.log("FP: models", models);
	ModelLoader.loads(models, this.onRoomModelsLoaded.bind(this));
};

FloorPlan.prototype.onRoomModelsLoaded = function(models) {
	for (let i=0, l=this.opts.rooms.length; i<l; i++) {
		const room = this.opts.rooms[i];
		if (models[room.id] && models[room.id].scene) {
			let mScene = models[room.id].scene;
			let mRoom = this._extractRoomMesh(mScene);
			if ( ! mRoom) {
				console.log("Could no extract room model", mScene);
				continue;
			}

			mRoom.userData['room'] = room;
			this.rooms.push(mRoom);
			this.updateRoomMaterial(mRoom);
			this.scene.add(mRoom);
		} else {
			console.error("Room model not loaded. ", room.id, JSON.stringify(room), models[room.id]);
		}
	}

	this.attachEventListeners(true);

	this.onResize();

	this.ready = true;
	this.dispatchEvent({type: EVENT.READY});
};

FloorPlan.prototype._extractRoomMesh = function(scene) {
	let mRoom = null;
	for ( var i = 0, l = scene.children.length; i < l; i ++ ) {
		let child = scene.children[i];
		if (child.isMesh) {
			mRoom = child.clone();
			break;
		}
	}
	return mRoom;
};

FloorPlan.prototype.createMaterials = function() {
	this.hideMaterial = new THREE.MeshBasicMaterial({visible: false});
	this.showMaterial = new THREE.MeshBasicMaterial({color: this.color1});
};

FloorPlan.prototype.adjustCamera = function() {
	if (this.camera.isOrthographicCamera) {
		if (this.box) {
			var size = this.size;
			var width = this.domElement.clientWidth;
			var height = this.domElement.clientHeight;
			var offset = 1;

			var aspect = width / height;
			var frustumSizeW = size.x+offset*2;
			var frustumSizeH = size.z+offset*2;
			var frustumAspect = frustumSizeW/frustumSizeH;

			if (frustumAspect > aspect) {
				this.camera.left = frustumSizeW / -2;
				this.camera.right = frustumSizeW / 2  ;
				this.camera.top = (frustumSizeW/aspect) / 2;
				this.camera.bottom = (frustumSizeW/aspect) / - 2;
				FloorPlan.PIXEL_TO_SCENE = (frustumSizeW/this.domElement.clientWidth) || 1;
			} else {
				this.camera.left = frustumSizeH * aspect / - 2;
				this.camera.right = frustumSizeH * aspect / 2;
				this.camera.top = frustumSizeH / 2;
				this.camera.bottom = frustumSizeH / - 2;
				FloorPlan.PIXEL_TO_SCENE = (frustumSizeH/this.domElement.clientHeight) || 1;
			}

			// console.log("THIS_CENTER", JSON.stringify(this.center));
			// console.log("THIS_SIZE", JSON.stringify(this.size));
			// console.log("THIS_BOX", JSON.stringify(this.box));
			// console.log("THIS_CAMERA",  {
			//   aspect: aspect,
			//   left: this.camera.left,
			//   right: this.camera.right,
			//   top: this.camera.top,
			//   bottom: this.camera.bottom,
			// });
			this.camera.position.set(this.center.x, this.box.max.y+this.camera.near+10, this.center.z);
			this.camera.lookAt(this.center.x, this.box.min.y, this.center.z);
		}
	} else {
		if (this.box) {
			// this.box.getSize(this.size);
			// this.box.getCenter(this.center);
			var maxDim = Math.max( this.size.x, this.size.z );
			// var maxDim = this.box.min.distanceTo(this.box.max);
			var fov = this.camera.fov * ( Math.PI / 180 );
			var dist = (maxDim/2)/Math.abs(Math.tan(fov/2)) + 2;

			this.camera.position.set(this.center.x, this.box.max.y+dist, this.center.z);
			// this.camera.position.set(this.box.min.x, dist, this.box.max.z);
			this.camera.zoom = 1;
			this.camera.lookAtPosition(this.center.x, this.box.min.y, this.center.z);
		}

		this.camera.aspect = (this.domElement.clientWidth/this.domElement.clientHeight);
		if (this.size.x >= this.size.z) {
			FloorPlan.PIXEL_TO_SCENE = (this.size.x/this.domElement.clientWidth) || 1;
		} else {
			FloorPlan.PIXEL_TO_SCENE = (this.size.z/this.domElement.clientHeight) || 1;
		}

	}
	this.camera.updateProjectionMatrix();
};

FloorPlan.prototype.clear = function () {
	for (var i=0; i<this.rooms.length; i++) {
		var room = this.rooms[i];
		Utils.disposeObject3D(room);
		room.userData = {};
	}
	this.rooms.splice(0, this.rooms.length);
};

FloorPlan.prototype.createRoom = function(srcMesh) {
	var useOriginal = false;
	var mesh = srcMesh.clone();
	if (useOriginal) {
		mesh.material = this.hideMaterial;
	} else {
		mesh.material = this.showMaterial.clone();
		// mesh.material.needsUpdate = true;

		var box = new THREE.Box3();
		box.setFromObject(mesh);
		var size = new THREE.Vector3();
		box.getSize(size);
		size.addScalar(-2*FloorPlan.PIXEL_TO_SCENE);

		var center = new THREE.Vector3();
		box.getCenter(center);

		var shape = new THREE.Shape();
		console.log("BOX ", JSON.stringify(box));
		// shape.moveTo(box.min.x, box.min.z);
		// shape.lineTo(box.min.x, box.max.z);
		// shape.lineTo(box.max.x, box.max.z);
		// shape.lineTo(box.max.x, box.min.z);
		// shape.lineTo(box.min.x, box.min.z);  // closes path
		shape.moveTo(0, 0);
		shape.lineTo(0, size.z);
		shape.lineTo(size.x, size.z);
		shape.lineTo(size.x, 0);
		shape.lineTo(0, 0);  // closes path

		// shape.moveTo(-5, -15);
		// shape.lineTo(-5, 15);
		// shape.lineTo(5, 15);
		// shape.lineTo(5, -15);
		// shape.lineTo(-5, -15);  // closes path
		// shape.moveTo(0, 0);
		// shape.lineTo(0, 30);
		// shape.lineTo(10, 30);
		// shape.lineTo(10, 0);
		// shape.lineTo(0, 0);  // closes path

		var geometry = new THREE.ShapeBufferGeometry( shape );
		geometry.center();

		var fillMesh = new THREE.Mesh(geometry, this.hideMaterial);
		// fillMesh.rotation.x = -Math.PI/2;
		// fillMesh.rotateX(-Math.PI/2);
		fillMesh.lookAt(0, 1, 0);
		// fillMesh.position.set(center.x, box.max.y, center.z);
		// fillMesh.position.set(size.x/2.0, box.max.y, size.z/2.0);
		// fillMesh.position.set(0, box.max.y, 0);
		fillMesh.position.set(center.x, box.max.y, center.z);
		console.log("POSITION ", JSON.stringify(fillMesh.position));

		mesh.userData.fillMesh = fillMesh;
		mesh.add(fillMesh);
		// mesh.material.visible = false;

		// add edge geometry
		// var edges = new THREE.EdgesGeometry( fillMesh.geometry, 90 );
		// var line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0xccF800 } ) );
		// line.lookAt(0, 1, 0);
		// line.position.copy(fillMesh.position);
		// line.position.y += 0.1;
		// mesh.add( line );

	}
	mesh.userData.selected = false;

	return mesh;
};

// get room obj with given room id
FloorPlan.prototype.getRoom = function(roomId) {
	for (var i=0; i<this.rooms.length; i++) {
		if (this.rooms[i].userData.room.id === roomId) {
			return this.rooms[i];
		}
	}
	return null;
};

FloorPlan.prototype.getRoomBBox = function(room) {
	return Utils.objectBBoxOnScreen(room, this.camera, this.screen.canvas);
};

FloorPlan.prototype.addCheckMark = function(room) {
	var checkMark = new SelectIndicator({
	});
	var box = new THREE.Box3().setFromObject(room);

	console.log("SelectIndicator: ROOMBOX", JSON.stringify(box));
	var offset = 5*FloorPlan.PIXEL_TO_SCENE;

	checkMark.scene.position.set(box.min.x+checkMark.options.size/2+offset, box.max.y, box.max.z-checkMark.options.size/2-offset);
	// checkMark.scene.position.set(0, box.max.y, 0);
	checkMark.scene.rotation.x = -Math.PI/2;
	// checkMark.scene.lookAt(0, 1, 0);

	room.add(checkMark.scene);
	room.checkMark = checkMark;
};

/**
 *
 * @param roomId => room id
 * @param selected
 */
FloorPlan.prototype.setRoomSelected = function (roomId, selected) {
	console.log("setRoomSelected", selected, roomId);
	const room = this.getRoom(roomId);
	if ( ! room) {
		console.error("Could not find room with id", roomId);
		return;
	}

	this._setRoomSelected(room, selected);
};

/**
 *
 * @param room => Mesh
 * @param selected
 * @private
 */
FloorPlan.prototype._setRoomSelected = function(room, selected) {
	room.userData.selected = selected;
	// update room background
	this.updateRoomMaterial(room);

	console.log("_setRoomSelected", selected, room);
};

FloorPlan.prototype.updateRoomMaterial = function(room) {
	if (room.material) {
		const visible = (room.userData.hover || room.userData.selected);
		const opacity = (room.userData.selected ? 1.0 : (room.userData.hover?0.5:1.0));
		let materials = room.material;
		if ( ! Array.isArray(materials)) {
			materials = [materials];
		}
		for(let i=0; i<materials.length; i++) {
			materials[i].visible = visible;
			materials[i].opacity = opacity;
			materials[i].needsUpdate = true;
		}
	}
};

// event handlers
FloorPlan.prototype.attachEventListeners = function(attach) {
	var func = attach ? 'addEventListener' : 'removeEventListener';
	if ( ! this.listners) {
		this.listners = {
			'mousemove': this.onMouseMove.bind(this),
			'mousedown': this.onMouseDown.bind(this),
			'mouseleave': this.onMouseLeave.bind(this),
			'touchend': this.onMouseDown.bind(this),
		};
	}

	for (var event in this.listners) {
		if (this.listners.hasOwnProperty(event)) {
			this.domElement[func]( event, this.listners[event], false );
		}
	}
};

FloorPlan.prototype.onMouseMove = function ( event ) {
	var m = Utils.getMouseOnScreen(this.screen, event.pageX, event.pageY);
	var room = this.getIntersectingRoom(m);
	this.setMouseOverItem(room);
};

FloorPlan.prototype.onMouseDown = function ( event ) {
	var m = null;
	if (event.touches && event.touches.length) {  // touch screen
		m = Utils.getMouseOnScreen(this.screen, event.touches[0].pageX, event.touches[0].pageY);
	} else {  // desktop
		m = Utils.getMouseOnScreen(this.screen, event.pageX, event.pageY);
	}

	if ( ! m) {
		return;
	}

	var room = this.getIntersectingRoom(m);

	if (room) {
		if (room.userData.selected) {
			this._setRoomSelected(room, false);
			this.dispatchEvent({type: EVENT.ITEM_UNSELECTED, data: {
				room: room.userData.room
			}});
		} else {
			this._setRoomSelected(room, true);
			this.dispatchEvent({type: EVENT.ITEM_SELECTED, data: {
				room: room.userData.room,
				bbox: this.getRoomBBox(room),
			}});
		}
	}
};

FloorPlan.prototype.onMouseLeave = function ( event ) {
	this.setMouseOverItem(null);
};

FloorPlan.prototype.setMouse = function ( event ) {
	this.mouse.copy( Utils.getMouseOnScreen(this.screen, event.pageX, event.pageY) );
};

FloorPlan.prototype.setMouseOverItem = function( room ) {
	if (room) {
		if (this.mouseOverRoom === room) {
			// within same room
			return;
		} else {
			// entered a different room
			if (this.mouseOverRoom) {
				this.mouseOverRoom.userData.hover = false;
				this.updateRoomMaterial(this.mouseOverRoom);
				this.dispatchEvent({type: EVENT.ITEM_LEAVE, data: {
					room: this.mouseOverRoom.userData.room
				}});
			}
			this.mouseOverRoom = room;
			this.mouseOverRoom.userData.hover = true;
			this.updateRoomMaterial(this.mouseOverRoom);
			this.dispatchEvent({type: EVENT.ITEM_ENTER, data: {
				room: this.mouseOverRoom.userData.room,
				bbox: this.getRoomBBox(this.mouseOverRoom)
			}});
		}
		this.domElement.style.cursor = 'pointer';
	} else {
		if (this.mouseOverRoom) {
			this.mouseOverRoom.userData.hover = false;
			this.updateRoomMaterial(this.mouseOverRoom);
			this.dispatchEvent({type: EVENT.ITEM_LEAVE, data: {
				room:this.mouseOverRoom.userData.room
			}});
			this.mouseOverRoom = null;
		}
		this.domElement.style.cursor = 'auto';
	}
};

FloorPlan.prototype.getIntersectingRoom = function (mouse) {
	// var rayCaster = this.rayCaster;
	// var intersects = [];
	// var rooms = this.rooms;
	// for (var i=0, l=rooms.length; i<l; i++) {
	//   var room = rooms[i];
	//   room.raycast(rayCaster, intersects );
	//   if (intersects.length) {
	//     return room;
	//   }
	//
	// }
	this.rayCaster.setFromCamera(mouse, this.camera);
	var intersections = this.rayCaster.intersectObjects(this.rooms, true);
	if (intersections.length) {
		var obj = intersections[0].object;
		while (true) {
			if ( ! obj || this.rooms.indexOf(obj) >= 0) {
				break;
			}
			obj = obj.parent;
		}
		return obj;

		// if (this.rooms.indexOf(obj) >= 0) {
		//   return obj;
		// } else {
		//   obj = obj.parent;
		//   while (true) {
		//     if ( ! obj || this.rooms.indexOf(obj) >= 0) {
		//       break;
		//     }
		//     obj = obj.parent;
		//   }
		//   return obj;
		// }
	}
	return null;
};

FloorPlan.prototype.dispose = function () {
	if (this.disposed) return;

	// cancel animation frame
	cancelAnimationFrame(this.animReq);

	// remove event listners
	this.attachEventListeners(false);
	window.removeEventListener('resize', this.onResize.bind(this), false);

	// remove all rooms
	this.clear();

	Utils.disposeObject3D(this.scene);
	this.scene = null;

	if (this.renderer.dispose) {
		this.renderer.dispose();
	}
	this.renderer.domElement.remove();
	this.renderer = null;
	this.camera = null;
};


var SelectIndicator = function (options) {

	var defaults = {
		// background: 0xFF0000,
		background: 0xFCFCFC,
		backgroundSelected: 0xAB5975,
		// border: 0x0000FF,
		border: 0x930F3C,
		foreground: 0xFCFCFC,
		size: 42*FloorPlan.PIXEL_TO_SCENE,
	};

	this.options = Object.assign(defaults, options || {});
	var size = this.options.size;

	var group = new THREE.Group();

	// add background circle
	var bgMaterial = new THREE.MeshBasicMaterial({ color: this.options.background });
	var bgGeometry = new THREE.CircleGeometry(size/2, 32);
	var bgMesh = new THREE.Mesh(bgGeometry, bgMaterial);

	group.add(bgMesh);

	// add outline circle
	// var olTexture = new THREE.TextureLoader().load( "dist/img/textures/icon-circle.png" );
	// olTexture.encoding = THREE.sRGBEncoding;
	// var olMaterial = new THREE.SpriteMaterial( { map: olTexture, color: this.options.border} );
	// var olSprite = new THREE.Sprite(olMaterial);
	// olSprite.scale.set( size, size, size );
	//
	// group.add(olSprite);
	var olThickness = 2*FloorPlan.PIXEL_TO_SCENE;
	var olSpriteMaterial = new THREE.MeshBasicMaterial({ color: this.options.border });
	var olSpriteGeometry = new THREE.RingGeometry(size/2-olThickness, size/2, 32);
	var olSprite = new THREE.Mesh(olSpriteGeometry, olSpriteMaterial);

	group.add(olSprite);

	// add check mark
	var cmTexture = new THREE.TextureLoader().load( "dist/img/textures/icon-checkmark.png" );
	cmTexture.encoding = THREE.sRGBEncoding;
	var cmMaterial = new THREE.SpriteMaterial( { map: cmTexture, color: this.options.foreground} );
	var cmSprite = new THREE.Sprite(cmMaterial);
	cmSprite.scale.set( size, size, size );
	cmSprite.visible = false;

	group.add(cmSprite);

	console.log("SelectIndicator: SIZE:", size);

	this.scene = group;
	this.bgMesh = bgMesh;
	this.olSprite = olSprite;
	this.cmSprite = cmSprite;

	this.selected = false;
};

SelectIndicator.prototype.setSelected = function (selected) {
	this.selected = selected;
	this.cmSprite.visible = selected;
	this.olSprite.visible = ! selected;
	this.bgMesh.material.color.setHex(selected ? this.options.backgroundSelected : this.options.background);
	// this.bgMesh.material.color.setHex(0x930F3C);
	this.bgMesh.material.needsUpdate = true;
};

SelectIndicator.prototype.dispose = function () {
	if (this.disposed) return;

	this.attachEventListeners(false);

	Utils.disposeObject3D(this.scene);
	this.scene = null;
	this.bgMesh = null;
	this.olSprite = null;
	this.cmSprite = null;

	this.disposed = true;
};


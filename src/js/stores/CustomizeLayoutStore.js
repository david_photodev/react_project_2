import StoreInterface from './StoreInterface';

class CustomizeLayoutStore extends StoreInterface {
  constructor() {
    super({
      data: [
        {
          id: 1,
          name: 'mod. 01',
          type: 'ChairItem',
          thumbnail_path: 'dist/img/chairs/chair_001.png',
          model_path: 'models/3D_PACK_FURNITURE/Chair/chair_001/chair_001.gltf',
        },
        {
          id: 2,
          name: 'mod. 02',
          type: 'ChairItem',
          thumbnail_path: 'dist/img/chairs/chair_002.png',
          model_path: 'models/3D_PACK_FURNITURE/Chair/chair_002/chair_002.gltf',
        },
        {
          id: 3,
          name: 'mod. 03',
          type: 'ChairItem',
          thumbnail_path: 'dist/img/chairs/chair_003.png',
          model_path: 'models/3D_PACK_FURNITURE/Chair/chair_003/chair_003.gltf',
        },
        {
          id: 4,
          name: 'mod. 04',
          type: 'ChairItem',
          thumbnail_path: 'dist/img/chairs/chair_004.png',
          model_path: 'models/3D_PACK_FURNITURE/Chair/chair_004/chair_004.gltf',
        },
        {
          id: 5,
          name: 'mod. 05',
          type: 'ChairItem',
          thumbnail_path: 'dist/img/chairs/chair_005.png',
          model_path: 'models/3D_PACK_FURNITURE/Chair/chair_005/chair_005.gltf',
        },
        {
          id: 6,
          name: 'mod. 06',
          type: 'TableItem',
          thumbnail_path: 'dist/img/tables/table_002.png',
          model_path: 'models/3D_PACK_FURNITURE/Table/table_002/table_002.gltf',
        },
        {
          id: 7,
          name: 'mod. 07',
          type: 'TableItem',
          thumbnail_path: 'dist/img/tables/table_003.png',
          model_path: 'models/3D_PACK_FURNITURE/Table/table_003/table_003.gltf',
        },
        {
          id: 8,
          name: 'mod. 08',
          type: 'TableItem',
          thumbnail_path: 'dist/img/tables/table_004.png',
          model_path: 'models/3D_PACK_FURNITURE/Table/table_004/table_004.gltf',
        },
        {
          id: 9,
          name: 'mod. 09',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/atril-001/atril-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/atril-001/atril-001.gltf',
        },
        {
          id: 10,
          name: 'mod. 10',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/pallet-001/pallet-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/pallet-001/pallet-001.gltf',
        },
        {
          id: 11,
          name: 'mod. 11',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/rollup-001/rollup-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/rollup-001/rollup-001.gltf',
        },
        {
          id: 12,
          name: 'mod. 12',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/table-007/table-007-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/table-007/table-007.gltf',
        },
        {
          id: 14,
          name: 'mod. 14',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/table-008/table-008-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/table-008/table-008.gltf',
        },
        {
          id: 15,
          name: 'mod. 15',
          type: 'TableItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/others/tableprojector-001/table-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/others/tableprojector-001/table-001.gltf',
        },
        {
          id: 16,
          name: 'mod. Bar',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/bar-001/bar-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/bar-001/bar-001.gltf',
        },
        {
          id: 17,
          name: 'mod. Car',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/car-001/car-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/car-001/car-001.gltf',
        },
        {
          id: 18,
          name: 'mod. Coatrack',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/coatrack-001/coatrack-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/coatrack-001/coatrack-001.gltf',
        },
        {
          id: 19,
          name: 'mod. Dancefloor',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/dancefloor-001/dancefloor-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/dancefloor-001/dancefloor-001.gltf',
        },
        {
          id: 20,
          name: 'mod. Dj stand',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/dj-001/dj-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/dj-001/dj-001.gltf',
        },
        {
          id: 21,
          name: 'mod. Flipchart',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/flipchart-002/flipchart-002-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/flipchart-002/flipchart-002.gltf',
        },
        {
          id: 22,
          name: 'mod. Foodtruck',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/foodtruck-001/foodtruck-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/foodtruck-001/foodtruck-001.gltf',
        },
        {
          id: 23,
          name: 'mod. Heatlamp',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/heatlamp-001/heatlamp-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/heatlamp-001/heatlamp-001.gltf',
        },
        {
          id: 24,
          name: 'mod. 15',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/table-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/table-001.gltf',
        },
        {
          id: 25,
          name: 'mod. Loveseat',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/loveseat-001/loveseat-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/loveseat-001/loveseat-001.gltf',
        },
        {
          id: 26,
          name: 'mod. Podium Mic',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/podiummic-001/podiummic-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/podiummic-001/podiummic-001.gltf',
        },
        {
          id: 27,
          name: 'mod. Portable Restroom',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/portablerestroom-001/portablerestroom-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/portablerestroom-001/portablerestroom-001.gltf',
        },
        {
          id: 28,
          name: 'mod. Sofa',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/sofa-001/sofa-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/sofa-001/sofa-001.gltf',
        },
        {
          id: 29,
          name: 'mod. Speaker',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/speaker-001/speaker-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/speaker-001/speaker-001.gltf',
        },
        {
          id: 30,
          name: 'mod. Staff',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/staff-001/staff-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/staff-001/stuff-001.gltf',
        },
        {
          id: 31,
          name: 'mod. Stage',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stage-001/stage-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stage-001/stage-001.gltf',
        },
        {
          id: 32,
          name: 'mod. Stand 1',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-001.gltf',
        },
        {
          id: 33,
          name: 'mod. Stand 2',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-002-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-002.gltf',
        },
        {
          id: 34,
          name: 'mod. Stand 3',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-003-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-003.gltf',
        },
        {
          id: 35,
          name: 'mod. Stand 4',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-004-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-004.gltf',
        },
       {
          id: 36,
          name: 'mod. Stand 5',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-005-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-005.gltf',
        },
        {
          id: 37,
          name: 'mod. Stand 6',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-006-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-006.gltf',
        },
        {
          id: 38,
          name: 'mod. Stand 7',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-007-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-007.gltf',
        },
        {
          id: 39,
          name: 'mod. Stand 8',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-008-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-008.gltf',
        },
        {
          id: 40,
          name: 'mod. Stand 9',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-009-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-009.gltf',
        },
        {
          id: 41,
          name: 'mod. Stand 10',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-010-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/stands/stand-010.gltf',
        },
        {
          id: 42,
          name: 'mod. Tent',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/tent-001/tent-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/tent-001/tent-001.gltf',
        },
        {
          id: 43,
          name: 'mod. Translation Booth',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/translationbooth-001/translationbooth-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/translationbooth-001/translationbooth-001.gltf',
        },
        {
          id: 44,
          name: 'mod. Portable TV',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/tv-001/tv-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/tv-001/tv-001.gltf',
        },
        {
          id: 45,
          name: 'mod. Vending Machine',
          type: 'ChairItem',
          thumbnail_path: 'models/3D_PACK_FURNITURE/convention_furnitures/vendingmachine-001/vendingmachine-001-img.png',
          model_path: 'models/3D_PACK_FURNITURE/convention_furnitures/vendingmachine-001/vendingmachine-001.gltf',
        },
      ],
      filteredData: [],
      searchVal: '',
    }, 'furnitureInfo');
  }

  getFurnitures(action) {
    let { furniture } = action;
    if (furniture.length) {
      this._store.data = furniture.map(data => {
        const params = {
          id: data.id,
          name: data.name,
          thumbnail_path: data.thumbnail,
          model_path: data.model_3d,
          type: data.item_type,
          category: data.category,
        };
        return params;
      });
    }
    this._store.filteredData = this._store.data;
  }

  getFurnitureCategories(action) {
    const { categories } = action;
    this._store.categories = categories;
  }

  // getData() {
  //   this._store.filteredData = this._store.data;
  // }

  setFilter(action) {
    const { id, name } = action;
    this._store.searchVal = name;
    if (action.id) {
      this._store.filteredData = this._store.data.filter(item => item.category.id === id);
      this._store.filteredData = this._store.filteredData.filter(item => item.name.toLowerCase().indexOf(name.toLowerCase()) >= 0);
    } else {
      this._store.filteredData = this._store.data;
      this._store.filteredData = this._store.filteredData.filter(item => item.name.toLowerCase().indexOf(name.toLowerCase()) >= 0);
    }
  }

  // searchData(action) {
  //   const { filterBy, name } = action;
  //   this._store.searchVal = name;
  //   this._store.filteredData = this._store.data.filter(item => item.name.toLowerCase().indexOf(name.toLowerCase()) >= 0);
  // }

  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      // case 'GET_DATA':
      //   this.getData();
      //   break;
      case 'FILTER':
        this.setFilter(action);
        break;
      case 'SEARCH':
        this.searchData(action);
        break;
      case 'GET_FURNITURE':
        this.getFurnitures(action);
        break;
      case 'GET_FURNITURE_CATEGORIES':
        this.getFurnitureCategories(action);
        break;
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new CustomizeLayoutStore();

import StoreInterface from './StoreInterface';
import MeetingSpaceConstants from '../constants/MeetingSpaceConstants';

class RFPManagementStore extends StoreInterface {
  constructor() {
    super({
      data: [],
    	hotelListDetails: [],
      chainList: [],
      allChainList: [],
      searchVal: '',
      selectedPage: 1,
      setUpImages: {
        theatre: 'dist/img/hotelProfile/theatre.svg',
        ushape: 'dist/img/meeting_space/room_setup/u-shape.svg',
        classroom: 'dist/img/meeting_space/room_setup/classroom.svg',
        boardroom: 'dist/img/meeting_space/room_setup/boardroom.svg',
        cabaret: 'dist/img/meeting_space/room_setup/cabaret.svg',
        banquet: 'dist/img/meeting_space/room_setup/banquet.svg',
        round: 'dist/img/meeting_space/room_setup/banquet.svg',
        cocktail: 'dist/img/meeting_space/room_setup/cocktail.svg',
      },
      notificationMsg: [],
    }, 'hotelListDetails');
  }

  getHotelList(action) {
    const { hotelList } = action;
    this._store.totalCount = hotelList.count;
    this._store.data = hotelList.results;
    this._store.hotelListDetails = hotelList.results;
  }

  getHotelChainList(chainList) {
    const chains = chainList.filter(chain => chain.status !== 2);
    this._store.chainList = chains;
  }

  searchBy(action) {
    const { name } = action;
    this._store.searchVal = name;
    // this._store.hotelListDetails = this._store.data.filter(item => item.name.toLowerCase().indexOf(action.name.toLowerCase()) >= 0);
  }

  getNotificationMsg(msg, type) {
    const { notificationMsg } = this._store;
    const index = notificationMsg.indexOf(msg);
    if (type === 'remove' && index > -1) {
      notificationMsg.splice(index, 1);
    } else if (type === 'add') {
      if (index > -1) {
        notificationMsg.splice(index, 1);
      }
      notificationMsg.push(msg);
    }
    this._store.notificationMsg = notificationMsg;
  }

  statusChange(action) {
    const { hotel, isActive } = action;
    this._store.hotelListDetails = this._store.hotelListDetails.map((hotelInfo) => {
      if (hotelInfo.id === hotel.id) {
        hotelInfo['status'] = isActive ? 'Active' : 'Inactive';
        return hotelInfo;
      }
      return hotelInfo;
    });
  }

  paginationClick(action) {
    const { page } = action;
    this._store.selectedPage = parseInt(page, 10);
  }


  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_HOTEL_LIST':
        this.getHotelList(action);
        break;
      case 'HOTEL_LIST_SORT':
        this._store.hotelListDetails = action.hotelListDetails;
        break;
      case 'HOTEL_LIST_SEARCH':
        this.searchBy(action);
        break;
      case 'GET_HOTEL_CHAIN_LIST':
        this._store.chainList = action.chainList.results;
        break;
      case 'STATUS_CHANGE':
        this.statusChange(action);
        break;
      case 'GET_NOTIFICATION_MSG':
        this.getNotificationMsg(action.msg, action.type);
        break;
      case 'HOTEL_LIST_PAGE':
        this.paginationClick(action);
        break;
      case 'CHAIN_LIST':
        this._store.allChainList = this._store.allChainList.concat(action.chainList.results);
        this.getHotelChainList(this._store.allChainList);
        break;
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new RFPManagementStore();

import StoreInterface from './StoreInterface';


class HotelBrandStore extends StoreInterface {
  constructor() {
    super({
    	brandLists: {},
    	profileInfo : {},
      searchVal: '',
      allChainList: [],
      fontsList: [],
      getUserHotel: null,
      selectedProfile: {},
      notificationMsg:[],
      allChainList: [],
    }, 'HotelBrandInfo');
  }

  getHotelBrandList(chainList) {
    const { brandLists } = this._store;
    brandLists['results'] = chainList;
  	this._store.brandLists = brandLists;
  }
  
  getSortedBrandList(sortBy) {
    const ascBrandLists = this._store.brandLists;
    const ascBrandListsResults = ascBrandLists.results.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
    if (sortBy) {
      ascBrandLists.results = ascBrandListsResults;
      this._store.brandLists = ascBrandLists;
    } else {
      ascBrandLists.results = ascBrandListsResults.reverse();
      this._store.brandLists = ascBrandLists;
    }
  }

  searchBy(action) {
    const { name } = action;
    this._store.searchVal = name;
    console.log('getHotelBrandList', this._store.allChainList);
    // this._store.brandLists = this._store.allChainList.filter(item => item.name.toLowerCase().indexOf(action.name.toLowerCase()) >= 0);
    const searchList  = this._store.brandLists;
    const searchResult = this._store.allChainList.filter(item => item.name.toLowerCase().indexOf(action.name.toLowerCase()) >= 0);
    searchList.results = searchResult;
    this._store.brandLists = searchList;
  }

  getNotificationMsg(msg, type) {
    const { notificationMsg } = this._store;
    const index = notificationMsg.indexOf(msg);
    if (type === 'remove' && index > -1) {
      notificationMsg.splice(index, 1);
    } else if (type === 'add') {
      notificationMsg.push(msg);
    }
    this._store.notificationMsg = notificationMsg;
  }

  saveProfile(profileInfo) {
    if (typeof(profileInfo) === 'undefined'){
      this._store.profileInfo = {};
    } else {
      this._store.profileInfo = profileInfo;
    }
  }

  setProfileInfoAsEmpty() {
  	this._store.profileInfo = {};
    this._store.brandLists = {};
    this._store.allChainList = [];
  }

  getTypography(typography) {
    const fontsList = []
    typography.items.map(item=>{fontsList.push(item.family)})
    this._store.fontsList = fontsList;
  }

  statusChange(action) {
    const { brandList, isActive } = action;
    const tempBrandLists = this._store.brandLists;
    const tempBrandListsResults = this._store.brandLists.results.map((brandInfo) => {
      if (brandInfo.id === brandList.id) {
        brandInfo['status'] = isActive ? 1 : 2;
        return brandInfo;
      }
      return brandInfo;
    });
    tempBrandLists.results = tempBrandListsResults;
    this._store.brandLists = tempBrandLists;
  }

  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_HOTEL_BRAND_LIST':
        // this.getHotelBrandList(action.brandList);
        break;
      case 'SINGLE_CHAIN_LIST':
        this._store.brandLists = action.brandList;
        this._store.allChainList = this._store.allChainList.concat(action.brandList.results);
        this.getHotelBrandList(this._store.allChainList);
        break;
      case 'GET_PROFILE_INFO':
        this.saveProfile(action.profileInfo);
        break;
      case 'SET_PROFILE_INFO_AS_EMPTY':
        this.setProfileInfoAsEmpty();
        break;
      case 'GET_SORTED_HOTEL_BRAND_LIST':
        this.getSortedBrandList(action.sortBy);
        break;
      case 'HOTEL_BRAND_SEARCH':
        this.searchBy(action);
        break;
      case 'BRAND_STATUS_CHANGE':
        this.statusChange(action);
        break;
      case 'GET_TYPOGRAPHY':
        this.getTypography(action.typography);
        break;
      case 'GET_NOTIFICATION_MSG':
        this.getNotificationMsg(action.msg, action.type);
        break;
      case 'SHOW_PROFILE':
        this._store.selectedProfile = action.brand;
        break;
      case 'GA_TRACKING':
        this._store.getUserHotel = action.hotelResponse.results;
        this._store.gaTrackingId = action.hotelResponse.results.length >= 1 ? action.hotelResponse.results[0].ga_tracking : null;
        break;
      default:
        emitChange = false;
        break;
    }

    if (emitChange) {
      this._emitChange(action.actionType);
    }

   }
}
export default new HotelBrandStore();
import StoreInterface from './StoreInterface';
import MeetingSpaceConstants from '../constants/MeetingSpaceConstants';

class MeetingSpaceStore extends StoreInterface {
  constructor() {
    super({
      data: {},
      layout: {},
      eventId: null,
      eventUuId:null,
      capacity: null,
      totalMeetingRooms: null,
      meetingRoomInfo: [],
      allMeetingRooms: [],
      guestInfo: [],
      setUpImages: {
        theatre: 'dist/img/meeting_space/room_setup/theatre.svg',
        ushape: 'dist/img/meeting_space/room_setup/u-shape.svg',
        classroom: 'dist/img/meeting_space/room_setup/classroom.svg',
        boardroom: 'dist/img/meeting_space/room_setup/boardroom.svg',
        cabaret: 'dist/img/meeting_space/room_setup/cabaret.svg',
        banquet: 'dist/img/meeting_space/room_setup/banquet.svg',
        round: 'dist/img/meeting_space/room_setup/banquet.svg',
        cocktail: 'dist/img/meeting_space/room_setup/cocktail.svg',
      },
      selectedRooms: [],
      eventDetails: {},
      contactDetails: {
        name: '',
        email: '',
        phone: '',
        contact_type: 'personal',
        company_name: '',
        name_of_event: '',
        country: '',
        address: '',
        address_line_2: '',
        city: '',
        state: '',
        postal_code: '',
        attendees: '',
        additional_information: '',
      },
      uploadedDocuments: [],
      furnitureData: null,
      viewer: null,
      floorPlan: null,
      notificationMsg: [],
      viewLayout: {},
      selectedLayout: {},
      defaultChairsPerTable: {
        theatre: 0,
        cocktail: 0,
        ushape: 3,
        classroom: 3,
        boardroom: 3,
        cabaret: 7,
        banquet: 11,
        round: 11,
        custom: 0,
      },
      scenes: [],
      eventInfo: {},
      available_setup: [],
      documentLength: null,
      uploadedFile: false,
      defaultViewRoom: {
        dimensions: '-',
        ceiling_height: '-',
        room_space: '-',
      },
    }, 'meetingStyle');
  }

  restoreData() {
    this._store = {
      data: {},
      layout: {},
      eventId: null,
      eventUuId:null,
      capacity: null,
      totalMeetingRooms: null,
      meetingRoomInfo: [],
      allMeetingRooms: [],
      guestInfo: [],
      setUpImages: {
        theatre: 'dist/img/meeting_space/room_setup/theatre.svg',
        ushape: 'dist/img/meeting_space/room_setup/u-shape.svg',
        classroom: 'dist/img/meeting_space/room_setup/classroom.svg',
        boardroom: 'dist/img/meeting_space/room_setup/boardroom.svg',
        cabaret: 'dist/img/meeting_space/room_setup/cabaret.svg',
        banquet: 'dist/img/meeting_space/room_setup/banquet.svg',
        round: 'dist/img/meeting_space/room_setup/banquet.svg',
        cocktail: 'dist/img/meeting_space/room_setup/cocktail.svg',
      },
      selectedRooms: [],
      eventDetails: {},
      contactDetails: {
        name: '',
        email: '',
        phone: '',
        contact_type: 'personal',
        company_name: '',
        name_of_event: '',
        country: '',
        address: '',
        address_line_2: '',
        city: '',
        state: '',
        postal_code: '',
        attendees: '',
        additional_information: '',
      },
      uploadedDocuments: [],
      furnitureData: null,
      viewer: null,
      floorPlan: null,
      notificationMsg: [],
      viewLayout: {},
      selectedLayout: {},
      eventInfo: {},
      defaultChairsPerTable: {
        theatre: 0,
        cocktail: 0,
        ushape: 3,
        classroom: 3,
        boardroom: 3,
        cabaret: 7,
        banquet: 11,
        round: 11,
        custom: 0,
      },
      scenes: [],
      available_setup: [],
      documentLength: null,
      uploadedFile: false,
      defaultViewRoom: {
        dimensions: '-',
        ceiling_height: '-',
        room_space: '-',
      },
    };
  }

  getMeetingRoomInfo(meetingInfo, capacity) {
    const meetingRoomList = meetingInfo.map((item) => {
      const selectedItem = item;
      selectedItem['showSetup'] = false;
      selectedItem['room_selected'] = false;
      return selectedItem;
    });

    // Order the room setups by the available setups
    const meetingRooms = meetingRoomList.map((room) => {
      const orderedSetup = [];
      const filteredSetup = this._store.available_setup.filter(o => !room.setup.find(o2 => o.id === o2.id));
      this._store.available_setup.map((available) => {
        room.setup.map((setup) => {
          if (setup.id === available.id) {
            orderedSetup.push(setup);
          } else if (filteredSetup.length >= 1) {
            const filterVal = filteredSetup.filter(val => val.id === available.id);
            if (filterVal.length >= 1) {
              const found = orderedSetup.some(el => el.id === filterVal[0].id);
              if (!found) {
                orderedSetup.push(available);
              }
            }
          }
        });
      });
      room['setup'] = orderedSetup;
      return room;
    });
    this._store.allMeetingRooms = this._store.allMeetingRooms.concat(meetingRooms);
    this._store.meetingRoomInfo = this._store.meetingRoomInfo.concat(meetingRooms);
    this._store.capacity = capacity;
    if (this._store.meetingRoomInfo.length >= 1) {
      this._store.totalMeetingRooms = this._store.meetingRoomInfo.length;
    }
  }

  updateMeetingRoomConfig(action) {
    console.log("ACTION!!", action);
    const { id, setupId, data } = action;
    const meetingInfo = this._store.meetingRoomInfo.map((item) => {
      const isRoomSelected = data.filter(setup => setup.active === true);
      const selectedItem = item;
      if (item.id !== id) {
        return selectedItem;
      }
      selectedItem['setup'] = data;
      selectedItem['room_selected'] = isRoomSelected.length >= 1;
      return selectedItem;
    });
    this._store.meetingRoomInfo = meetingInfo;
    const selectedSetup = this._store.meetingRoomInfo.filter(roomSetupConfigs => roomSetupConfigs.id === id)[0];
    const layoutInfo = selectedSetup.setup.filter(val => val.id === setupId)[0];
    layoutInfo['activity_name'] = '';
    layoutInfo['event_date_starts'] = '';
    layoutInfo['event_date_ends'] = '';
    layoutInfo['event_time_starts'] = '';
    layoutInfo['event_time_ends'] = '';
    layoutInfo['event_hour'] = '';
    layoutInfo['chairs_count'] = layoutInfo.count;
    layoutInfo['active'] = true;
    const availableRoom = this._store.selectedRooms.filter(room => room.id === id);
    if (availableRoom.length >= 1) {
      const availableSetup = availableRoom[0].setup.filter(active => active.id === setupId);
      let selectedRooms = this._store.selectedRooms.map((layout) => {
        const setupConfig = layout;
        if (layout.id !== id) {
          return setupConfig;
        }
        setupConfig.setup = setupConfig.setup.map((config) => {
          const setup = config;
          if (config.id !== setupId) {
            return setup;
          }
          setup['active'] = !setup['active'];
          return setup;
        });
        if (availableSetup.length >= 1) {
          setupConfig['setup'] = setupConfig.setup.filter(layoutActive => layoutActive.active === true);
          setupConfig['setup'].sort((a, b) => a.setup_id - b.setup_id);
          return setupConfig;
        }
        if (setupConfig['setup'].length >= 1) {
          layoutInfo['setup_id'] = setupConfig['setup'][setupConfig['setup'].length - 1].setup_id + 1;
        } else {
          layoutInfo['setup_id'] = 1;
        }
        setupConfig['setup'].push(layoutInfo);
        return setupConfig;
      });
      if (selectedRooms.length === 1 && selectedRooms[0].setup.length < 1) {
        selectedRooms = [];
      }
      this._store.selectedRooms = selectedRooms;
    } else {
      const roomSetup = {};
      roomSetup['id'] = selectedSetup.id;
      roomSetup['available_model'] = selectedSetup.available_model;
      roomSetup['room_name'] = selectedSetup.room_name;
      roomSetup['setup'] = [];
      layoutInfo['setup_id'] = 1;
      roomSetup['setup'].push(layoutInfo);
      roomSetup['allLayouts'] = selectedSetup.setup;
      roomSetup['scenes'] = selectedSetup.scenes;
      roomSetup['available_layout'] = layoutInfo;
      this._store.selectedRooms.push(roomSetup);
    }
    this.MaxAttendeeCount();
  }

  updateLayout(action) {
    // const layout = {};
    const updateMeetingRoomList = this._store.selectedRooms.filter((selectedRoom) => {
      const listedRoom = selectedRoom;
      listedRoom['setup'] = listedRoom.setup.map((setup) => {
        const highlightSetup = JSON.parse(JSON.stringify(setup));
        if (listedRoom.id === action.room.id && highlightSetup.setup_id === action.setup.setup_id) {
          highlightSetup['enable3D'] = true;
          return highlightSetup;
        }
        highlightSetup['enable3D'] = false;
        return highlightSetup;
      });
      return listedRoom;
    });
    // layout['type'] = action.setup.type;
    // layout['columns'] = [3, 3];
    const layout = action.setup;
    layout['droppedItems'] = layout.droppedItems || [];
    if (action.setup.hasOwnProperty('model_3d') && action.setup.model_3d.hasOwnProperty('data')) {
      layout['furnitureData'] = Object.keys(action.setup.model_3d.data).length >= 1 ? action.setup.model_3d.data : null;
    }
    const peopleCount = layout['chairs_count'] >= 0 ? layout['chairs_count'] : layout['count'];
    layout['chairs_per_table'] = (layout.config && layout.config.hasOwnProperty('defaultChairsPerTable') &&
      layout.config.defaultChairsPerTable) || this._store.defaultChairsPerTable[layout.type.toLowerCase()];
    layout['tables_count'] = layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'cocktail' ? 0
      : Math.ceil(peopleCount / layout['chairs_per_table']);
    let viewLayout;
    let selectedLayoutRoom;
    if (action.type) {
      viewLayout = action.room;
      selectedLayoutRoom = viewLayout;
    } else {
      viewLayout = this._store.allMeetingRooms.filter(room => room.id === action.room.id);
      selectedLayoutRoom = viewLayout[0];
    }
    this._store.viewLayout = selectedLayoutRoom;
    layout['roomId'] = selectedLayoutRoom.id;
    if (action.setup.type.toLowerCase() === 'custom') {
      const customInfo = action.setup;
      layout['chairs_per_table'] = 0;
      layout['tables_count'] = 0;
      layout['chairs_count'] = customInfo.count || 0;
      layout['count'] = customInfo.count || 0;
      layout['droppedItems'] = customInfo.droppedItems || [];
      this._store.layout = layout;
    }
    else{
      this._store.layout = layout;
    }
    this._store.selectedRooms = updateMeetingRoomList;
  }

  removeLayout(action) {
    const updateMeetingRoomList = this._store.selectedRooms.filter((room) => {
      const listedRoom = room;
      if (room.id !== action.room.id) {
        return listedRoom;
      }
      listedRoom['setup'] = listedRoom.setup.filter(setup => setup.setup_id !== action.setup.setup_id);
      if (listedRoom['setup'].length < 1) {
        return false;
      }
      return listedRoom;
    });
    const meetingInfo = this._store.meetingRoomInfo.map((item) => {
      const selectedItem = item;
      if (item.id !== action.room.id) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = roomConfig;
        if (roomEventType.id === action.setup.id) {
          roomEventType['active'] = false;
          return roomEventType;
        }
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      const isRoomSelected = updateMeetingRoomList.filter(setup => setup.id === item.id);
      selectedItem['room_selected'] = isRoomSelected.length >= 1;
      return selectedItem;
    });
    this._store.selectedRooms = updateMeetingRoomList;
    this._store.allMeetingRooms = meetingInfo;
    this._store.meetingRoomInfo = meetingInfo;
  }

  duplicateLayout(action) {
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== action.room.id) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (roomEventType.setup_id !== action.setup.setup_id) {
          roomEventType['addon'] = false;
          return roomEventType;
        }
        roomEventType['enable3D'] = false;
        roomEventType['active'] = true;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      selectedItem['setup'].push(action.setup);
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  updateAddons(action) {
    let clickedDropDown = 'meetingRoomInfo';
    if (action.type === 'services'){
      clickedDropDown = 'addon';
    }
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = JSON.parse(JSON.stringify(room));
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (selectedItem.id === action.room.id && roomEventType.setup_id === action.setup.setup_id) {
          roomEventType[clickedDropDown] = roomEventType.hasOwnProperty(clickedDropDown) ? !roomEventType[clickedDropDown] : true;
          return roomEventType;
        }
        roomEventType[clickedDropDown] = clickedDropDown === 'addon' ? false :  true;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  updateFoodAndServices(action) {
    const {
      room: choosedRoom, setup, addon, type,
    } = action.params;
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id === choosedRoom.id) {
        const updateSetup = selectedItem.setup.map((roomConfig) => {
          const roomEventType = roomConfig;
          if (roomEventType.setup_id === setup.setup_id) {
            let selectedServices = [];
            if (roomEventType.hasOwnProperty(type)) {
              selectedServices = roomEventType[type];
              const index = selectedServices.indexOf(addon);
              if (index > -1) {
                selectedServices.splice(index, 1);
              } else {
                selectedServices.push(addon);
              }
            } else {
              selectedServices.push(addon);
            }
            roomEventType[type] = selectedServices;
            return roomEventType;
          }
          return roomEventType;
        });
        selectedItem['setup'] = updateSetup;
        return selectedItem;
      }
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  addNewPeriod() {
    const newPeriod = {
      id: this._store.guestInfo.length + 1,
      start: '',
      end: '',
      single_rooms: '',
      double_rooms: '',
      isNew: true,
    };
    this._store.guestInfo.push(newPeriod);
  }

  deletePeriod(action) {
    const guestInfo = this._store.guestInfo.filter(item => item.id !== action.id);
    this._store.guestInfo = guestInfo;
  }

  editPeriod(action) {
    const { id, item, type } = action;
    const guestInfo = this._store.guestInfo.map((guest) => {
      const updateGuest = guest;
      updateGuest['edit'] = updateGuest.hasOwnProperty('edit') ? updateGuest['edit'] : [];
      if (guest.id === id) {
        if (type === 'read') {
          const index = updateGuest['edit'].indexOf(item);
          updateGuest['edit'].splice(index, 1);
        } else {
          updateGuest['edit'].push(item);
        }
        return updateGuest;
      }
      return updateGuest;
    });
    this._store.guestInfo = guestInfo;
  }

  savePeriod(action) {
    this._store.guestInfo = action.guestInfo;
  }

  editContact(action) {
    const { item, type } = action;
    const { contactDetails } = this._store;
    contactDetails['edit'] = contactDetails.hasOwnProperty('edit') ? contactDetails['edit'] : [];
    if (type === 'read') {
      const index = contactDetails['edit'].indexOf(item);
      contactDetails['edit'].splice(index, 1);
    } else {
      contactDetails['edit'].push(item);
    }
    this._store.contactDetails = contactDetails;
  }

  addNotification(action) {
    const { notificationMsg } = this._store;
    const { msg, val, type } = action;
    const index = notificationMsg.indexOf(msg);
    if (val === 'remove' && index > -1) {
      notificationMsg.splice(index, 1);
    } else if (val === 'add') {
      if (type === 'presetup' && index > -1) {
        notificationMsg.splice(index, 1);
      }
      notificationMsg.push(msg);
    }
    this._store.notificationMsg = notificationMsg;
  }

  addEventDate(date) {
    console.log(date);
  }

  updateLayoutInputs(params) {
    const {
      roomId, setupId, chairsPerTable, peopleCount, tablesCount, columnsPerRow,
    } = params;
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== roomId) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = roomConfig;
        if (roomEventType.setup_id !== setupId) {
          return roomEventType;
        }
        roomEventType['chairs_count'] = peopleCount;
        roomEventType['chairs_per_table'] = chairsPerTable;
        roomEventType['tables_count'] = tablesCount;
        if (columnsPerRow) {
          roomEventType['columns_per_row'] = columnsPerRow;
        }
        this._store.layout = roomEventType;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
    this.MaxAttendeeCount();
  }

  updateActivityName(params) {
    const {
      roomId, setupId, name, type,
    } = params;
    const createdEvent = this._store.selectedRooms.map((room) => {
      const selectedItem = room;
      if (room.id !== roomId) {
        return selectedItem;
      }
      const updateSetup = selectedItem.setup.map((roomConfig) => {
        const roomEventType = JSON.parse(JSON.stringify(roomConfig));
        if (roomEventType.setup_id !== setupId) {
          return roomEventType;
        }
        roomEventType[type] = name;
        return roomEventType;
      });
      selectedItem['setup'] = updateSetup;
      return selectedItem;
    });
    this._store.selectedRooms = createdEvent;
  }

  submitLayout(action) {
    const { layout, roomId } = action;
    const selectedRooms = this._store.selectedRooms.map((room) => {
      if (room.id !== roomId) return room;
      const updateRoomLayout = room;
      updateRoomLayout['setup'] = updateRoomLayout.setup.map((layoutRoom) => {
        if (layoutRoom.setup_id !== layout.setup_id) return layoutRoom;
        layout['config'] = layout['columns_per_row'] ? {columns: layout['columns_per_row']} : layout['config'];
        return layout;
      });
      return updateRoomLayout;
    });
    this._store.layout = layout;
    this._store.selectedRooms = selectedRooms;
  }

  showSetupView(item) {
    this._store.config3D = item;
  }

  createOwnSetup() {
    const { config3D } = this._store;
    const customSetup = config3D.setup.filter((selectedSetup)=> selectedSetup.type.toLowerCase() === 'custom');
    const newLayout = {};
    newLayout['id'] = customSetup[0].id;
    newLayout['type'] = customSetup[0].type;
    newLayout['shape'] = customSetup[0].shape;
    newLayout['enable3D'] = true;
    newLayout['active'] = true;
    const availableRoom = this._store.selectedRooms.filter(room => room.id === config3D.id);
    if (availableRoom.length >= 1) {
      const selectedRooms = this._store.selectedRooms.map((layout) => {
        const setupConfig = JSON.parse(JSON.stringify(layout));
        setupConfig.setup = setupConfig.setup.map((config) => {
          const setup = JSON.parse(JSON.stringify(config));
          setup['enable3D'] = false;
          return setup;
        });
        if (layout.id !== config3D.id) {
          return setupConfig;
        }
        if (setupConfig['setup']) {
          newLayout['setup_id'] = setupConfig['setup'][setupConfig['setup'].length - 1].setup_id + 1;
        } else {
          newLayout['setup_id'] = 1;
        }
        setupConfig['setup'].push(newLayout);
        return setupConfig;
      });
      this._store.selectedRooms = selectedRooms;
    } else {
      const selectedRooms = this._store.selectedRooms.map((layout) => {
        const setupConfig = layout;
        setupConfig.setup = setupConfig.setup.map((config) => {
          const setup = JSON.parse(JSON.stringify(config));
          setup['enable3D'] = false;
          return setup;
        });
        return setupConfig;
      });
      this._store.selectedRooms = selectedRooms;
      const roomSetup = {};
      roomSetup['id'] = config3D.id;
      roomSetup['room_name'] = config3D.room_name;
      roomSetup['setup'] = [];
      newLayout['setup_id'] = 1;
      roomSetup['setup'].push(newLayout);
      roomSetup['available_layout'] = newLayout;
      roomSetup['allLayouts'] = config3D.setup;
      roomSetup['available_model'] = config3D.available_model || false;
      roomSetup['scenes'] = config3D.scenes;
      this._store.selectedRooms.push(roomSetup);
    }
  }

  saveContact(contact) {
    this._store.contactDetails = contact;
  }

  saveAvailableSetup(setUp) {
    this._store.currentViewRoom = this._store.defaultViewRoom;
    this._store.floorPlanSetup = setUp;
    this._store.available_setup = setUp;
  }

  saveHotelInfo(data) {
    this._store.data = data;
  }

  saveActivityInfo(activityParams) {
    this._store.selectedRooms = activityParams;
  }

  saveEventDetails(eventsParams) {
    this._store.eventDetails = eventsParams;
    const contactInfo = this._store.contactDetails;
    contactInfo.attendees = eventsParams.attendees;
    this._store.contactDetails = contactInfo;
  }

  handleRoomSetupList(action) {
    const { room, list, setup_id } = action;
    const selectedRooms = this._store.selectedRooms.map((config) => {
      if (config.id !== room.id) return config;
      const updateRoomLayout = JSON.parse(JSON.stringify(config));
      updateRoomLayout['setup'] = updateRoomLayout.setup.map((layoutRoom) => {
        const updateSetup = JSON.parse(JSON.stringify(layoutRoom));
        if (updateSetup.setup_id !== setup_id) return updateSetup;
        updateSetup['id'] = list.id;
        updateSetup['shape'] = list.shape;
        updateSetup['type'] = list.type;
        updateSetup['count'] = list.count;
        updateSetup['img'] = list.img;
        updateSetup['config'] = list.config;
        updateSetup['furnitures'] = list.furnitures;
        updateSetup['active'] = true;
        return updateSetup;
      });
      return updateRoomLayout;
    });
    this._store.selectedRooms = selectedRooms;
  }

  filterSelectedRoom(action) {
    const { typeOfSetup, typeOfCapacity } = action;
    let filteredRooms = this._store.allMeetingRooms;
    let range;
    const rangeIndex = typeOfCapacity.indexOf('-');
    if (rangeIndex > -1) {
      range = typeOfCapacity.split('-');
    } else {
      range = typeOfCapacity.split('More than ');
    }
    const range1 = parseInt(range[0], 10);
    const range2 = parseInt(range[1], 10);
    if (typeOfSetup.toLowerCase() === 'all' && typeOfCapacity === 'all') {
      filteredRooms = this._store.allMeetingRooms;
    } else if (typeOfSetup.toLowerCase() === 'custom' && typeOfCapacity === 'all'){
      filteredRooms = this._store.allMeetingRooms;
    } else {
      filteredRooms = filteredRooms.filter((room) => {
        let filteredData = [];
        if (typeOfCapacity.indexOf('More than') > -1 && typeOfSetup.toLowerCase() !== 'all') {
          filteredData = room.setup.filter(setup => setup.count > 0 && setup.count > range2 && setup.type.toLowerCase() === typeOfSetup.toLowerCase());
        } else if (typeOfCapacity.indexOf('More than') > -1 && typeOfSetup.toLowerCase() === 'all') {
          filteredData = room.setup.filter(setup => setup.count > 0 && setup.count > range2);
        } else if (typeOfSetup.toLowerCase() === 'all' && typeOfCapacity !== 'all') {
          filteredData = room.setup.filter(setup => setup.count > 0 && setup.count >= range1 && setup.count <= range2);
        } else if (typeOfSetup.toLowerCase() !== 'all' && typeOfCapacity === 'all') {
          filteredData = room.setup.filter(setup => setup.count > 0 && setup.type.toLowerCase() === typeOfSetup.toLowerCase());
        } else {
          filteredData = room.setup.filter(setup => setup.count > 0 && setup.type.toLowerCase() === typeOfSetup.toLowerCase() && setup.count >= range1
            && setup.count <= range2);
        }
        if (filteredData.length >= 1) {
          return room;
        }
      });
    }
    this._store.meetingRoomInfo = filteredRooms;
  }

  getAttachmentTypes(action) {
    // const supportedFileTypes = {};
    // const types = action.data.results;
    // for (let i = 0; i < types.length; i++) {
    //   supportedFileTypes[types[i].mime_type] = types[i].id;
    // }
    this._store.attachmentTypes = action.supportedFileTypes;
  }

  getAttachedFiles(action) {
    this._store.uploadedDocuments = action.data.results;
  }

  MaxAttendeeCount() {
    const attendeesList = [];
    const selectedRooms = this._store.selectedRooms;
    if (selectedRooms.length >= 1) {
      selectedRooms.map((room) => {
        room.setup.map((setup) => {
          attendeesList.push(setup.chairs_count);
        });
      });
    }
    attendeesList.sort((a, b) => {
      return a - b;
    });
    const contact = this._store.contactDetails;
    contact.attendees = contact.attendees || attendeesList[attendeesList.length - 1];
    this._store.contactDetails = contact;
  }

  showSelectedFloorRoom(action) {
    const {
      setup, room_space, dimensions, ceiling_height,
    } = action.room;
    const roomInfo = {
      room_space,
      dimensions,
      ceiling_height,
    };
    this._store.currentViewRoom = roomInfo;
    this._store.floorPlanSetup = setup;
  }

  sortBy(action) {
    const { key, order, setup } = action;
    let meetingRooms;
    if (key === 'room_space') {
      meetingRooms = this._store.meetingRoomInfo.sort((a, b) => {
        a = parseFloat(a[key].split('m²')[0], 10);
        b = parseFloat(b[key].split('m²')[0], 10);
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        return a - b;
      });
    } else if (key === 'dimensions') {
      meetingRooms = this._store.meetingRoomInfo.sort((a, b) => {
        a = parseFloat(a[key].replace(/,/g, '.').split('m')[0], 10);
        b = parseFloat(b[key].replace(/,/g, '.').split('m')[0], 10);
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        return a - b;
      });
    } else if (key === 'ceiling_height') {
      meetingRooms = this._store.meetingRoomInfo.sort((a, b) => {
        a = parseFloat(a[key], 10);
        b = parseFloat(b[key], 10);
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        return a - b;
      });
    } else if (key === 'setup') {
      meetingRooms = this._store.meetingRoomInfo.sort((a, b) => {
        a = parseInt(a[key].filter(val => val.type.toLowerCase() === setup.type.toLowerCase())[0].count, 10);
        b = parseInt(b[key].filter(val => val.type.toLowerCase() === setup.type.toLowerCase())[0].count, 10);
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        return a - b;
      });
    }

    if (!order) {
      meetingRooms = meetingRooms.reverse();
    }
    this._store.meetingRoomInfo = meetingRooms;
  }

  storeCreatedEventInfo(action) {
    const { eventInfo } = action;
    this._store.eventInfo = eventInfo;
    const { contactDetails } = this._store;
    contactDetails['name_of_event'] = eventInfo.name;
    contactDetails['attendees'] = eventInfo.attendees;
    this._store.contactDetails = contactDetails;
  }

  saveDroppedItems(action) {
    const { droppedItems } = action;
    const { layout } = this._store;
    // layout.droppedItems = droppedItems;
    // this._store.layout = layout;
    const updateMeetingRoomList = this._store.selectedRooms.filter((room) => {
      const listedRoom = room;
      if (room.id !== layout.roomId) {
        return listedRoom;
      }
      const updateSetup = listedRoom.setup.map(setup => {
        const roomEventType = setup;
        if (roomEventType.setup_id === layout.setup_id) {
          roomEventType['droppedItems'] = droppedItems;
          return roomEventType;
        }
        return roomEventType;
      });
      listedRoom['setup'] = updateSetup;
      return listedRoom;
    });
    this._store.selectedRooms = updateMeetingRoomList;
  }

  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_MEETING_ROOM_INFO':
        this.getMeetingRoomInfo(action.meetingRoomDatas, action.capacity);
        break;
      case 'UPDATE_ROOM_CONFIG':
        this.updateMeetingRoomConfig(action);
        break;
      case 'UPDATE_LAYOUT':
        this.updateLayout(action);
        break;
      case 'REMOVE_LAYOUT':
        this.removeLayout(action);
        break;
      case 'SET_VIEWER':
        this._store.viewer = action.viewer;
        break;
      case 'DUPLICATE_LAYOUT':
        this.duplicateLayout(action);
        break;
      case 'ADDON_UPDATE':
        this.updateAddons(action);
        break;
      case 'UPDATE_FOOD_SERVICES':
        this.updateFoodAndServices(action);
        break;
      case 'ADD_NEW_PERIOD':
        this.addNewPeriod();
        break;
      case 'DELETE_PERIOD':
        this.deletePeriod(action);
        break;
      case 'EDIT_PERIOD':
        this.editPeriod(action);
        break;
      case 'SAVE_PERIOD':
        this.savePeriod(action);
        break;
      case 'EDIT_CONTACT':
        this.editContact(action);
        break;
      case 'ADD_NOTIFICATION':
        this.addNotification(action);
        break;
      case 'UPDATE_LAYOUT_INPUTS':
        this.updateLayoutInputs(action.params);
        break;
      case 'UPDATE_ACTIVITY_NAME':
        this.updateActivityName(action.params);
        break;
      case 'SUBMIT_LAYOUT':
        this.submitLayout(action);
        break;
      case 'SHOW_SETUP_VIEW':
        this.showSetupView(action.item);
        break;
      case 'CREATE_OWN_SETUP':
        this.createOwnSetup();
        break;
      case 'ADD_EVENTDATE':
        this.addEventDate(action.date);
        break;
      case 'SAVE_CONTACT':
        this.saveContact(action.contactDetails);
        break;
      case 'SAVE_HOTEL_DATA':
        this.saveHotelInfo(action.data);
        break;
      case 'SAVE_AVAILABLE_SETUP':
        this.saveAvailableSetup(action.available_setup);
        break;
      case 'SAVE_ACTIVITY':
        this.saveActivityInfo(action.ActivityInfos);
        break;
      case 'SAVE_EVENT':
        this.saveEventDetails(action.eventInfos);
        break;
      case 'ROOM_SETUP':
        this.handleRoomSetupList(action);
        break;
      case 'FILTER_SETUP':
        this.filterSelectedRoom(action);
        break;
      case 'SELECTED_LAYOUT':
        this._store.selectedLayout = action.params;
        break;
      case 'SAVED_FURNITURE_DATA':
        this._store.furnitureData = Object.keys(action.data).length >= 1 ? action.data : null;
        break;
      case 'REPLACE_IMAGE':
        this._store.replaceImage = action.bool;
        break;
      case 'STORE_SCENE':
        this._store.scenes = action.scenes;
        break;
      case MeetingSpaceConstants.CREATE_EVENT:
        this._store.eventId = action.data.id;
        this._store.eventUuId = action.data.uuid;
        break;
      case MeetingSpaceConstants.ATTACHMENT_TYPES:
        this.getAttachmentTypes(action);
        break;
      case MeetingSpaceConstants.ATTACHED_FILE:
        this.getAttachedFiles(action);
        break;
      case 'DOC_LENGTH':
        this._store.uploadedfile = action.length;
        break;
      case 'UPLOADED_FILE':
        this._store.uploadedDocuments.push(action.file);
        break;
      case 'SAVE_FLOOR_PLAN':
        this._store.floorPlan = action.fp;
        break;
      case 'SAVE_EVENT_ID':
        this._store.eventId = action.eventId;
        this._store.eventUuId = action.uuId;
        break;
      case 'SHOW_SELECTED_FLOOR_ROOM':
        this.showSelectedFloorRoom(action);
        break;
      case 'HIDE_SELECTED_FLOOR_ROOM':
        this._store.currentViewRoom = this._store.defaultViewRoom;
        this._store.floorPlanSetup = this._store.available_setup;
        break;
      case 'SORT_MEETING_ROOM':
        this.sortBy(action);
        break;
      case 'RESTORE_DATA':
        this.restoreData();
        break;
      case 'CREATE_EVENT_INFO':
        this.storeCreatedEventInfo(action);
        break;
      case 'SAVE_DROPPED_ITEMS':
        this.saveDroppedItems(action);
        break;
      default:
        emitChange = false;
        break;
    }

    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new MeetingSpaceStore();

import StoreInterface from './StoreInterface';

class NotificationStore extends StoreInterface {
  constructor() {
    super({
      unReadNotificationCount : 0,
      notificationList: [],
      allNotification: [],
      isApiCalled: false,
      searchVal : '',
    }, 'notificationDetails');
  }



  getNotificationList(notificationInfo) {
    const unReadNotification = notificationInfo.results.filter(result => result.status === 1).length
    this._store.unReadNotificationCount = this._store.unReadNotificationCount + unReadNotification  ;
    this._store.notificationList = this._store.notificationList.concat(notificationInfo.results);
    this._store.allNotification = this._store.allNotification.concat(notificationInfo.results);
  }

  searchBy(action) {
    const { name } = action;
    this._store.searchVal = name;
    const searchResult = this._store.allNotification.filter(item => item.message.toLowerCase().indexOf(action.name.toLowerCase()) >= 0);
    this._store.notificationList = searchResult;
  }

  checkNotificationApiCalled(checkNotificationApiCalled) {
    this._store.isApiCalled = checkNotificationApiCalled;
    if (!checkNotificationApiCalled) {
      this._store.notificationList = [];
      this._store.allNotification = [];
      this._store.unReadNotificationCount = 0;
      this._store.searchVal = '';
    }
  }

  clearStoreInfo() {
    this._store.notificationList = [];
    this._store.allNotification = [];
    this._store.unReadNotificationCount = 0;
    this._store.searchVal = '';
  }



    _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_NOTIFICATION_LIST':
        this.getNotificationList(action.notificationList);
        break;
      case 'CHECK_GET_NOTIFICATION_API_CALLED':
        this.checkNotificationApiCalled(action.isApiCalled);
        break;
      case 'CLEAR_STORE_NOTIFICATIONS_INFO':
        this.clearStoreInfo();
        break;
      case 'NOTIFICATION_SEARCH':
        this.searchBy(action);
        break;
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new NotificationStore();
import StoreInterface from './StoreInterface';

class RFPManagementStore extends StoreInterface {
  constructor() {
    super({
      data: [],
      userList: [],
      allUserList: [],
      searchVal: '',
    }, 'hotelListDetails');
  }

  getUserList(action) {
    // deleted user id is 3
    const activeUsers = action.filter(user => user.profile.status.id !== 3);
    this._store.data = activeUsers;
    this._store.userList = activeUsers;
  }

  sortBy(action) {
    const { key, asc } = action;
    const { userList } = this._store;
    let sortedList = userList.sort((a, b) => {
      let nameA;
      let nameB;
      if (key === 'role') {
        nameA = a.profile[key].toLowerCase();
        nameB = b.profile[key].toLowerCase();
      } else if (key === 'hotel_chain') {
        nameA = a.profile[key].name.toLowerCase();
        nameB = b.profile[key].name.toLowerCase();
      } else if (key === 'hotel') {
        nameA = a[key].length >= 1 ? a[key][0].name.toLowerCase() : '-';
        nameB = b[key].length >= 1 ? b[key][0].name.toLowerCase() : '-';
      } else {
        nameA = a[key].toLowerCase();
        nameB = b[key].toLowerCase();
      }
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    if (!asc) {
      sortedList = sortedList.reverse();
    }
    this._store.data = sortedList;
    this._store.hotelListDetails = sortedList;
  }

  _registerToActions(payload) {
    const { action } = payload;
    let emitChange = true;
    switch (action.actionType) {
      case 'GET_USER_LIST':
        break;
      case 'USER_LIST_SORT':
        this.sortBy(action);
        break;
      case 'USER_LIST_SEARCH':
        this._store.searchVal = action.name;
        this._store.userList = this._store.data.filter(item => item.username.toLowerCase().indexOf(action.name.toLowerCase()) >= 0);
        break;
      case 'DELETE_USER':
        this._store.userList = this._store.userList.filter(user => user.id !== action.user.id);
        break;
      case 'SELECTED_USER':
        this._store.selectedUser = action.user;
        break;
      case 'SINGLE_USER_LIST':
        this._store.allUserList = this._store.allUserList.concat(action.userList.results);
        this.getUserList(this._store.allUserList);
        break;
      case 'UPDATED_HOTEL_USER':
        this._store.selectedUser = action.user;
        const userList = this._store.userList.map(user => {
          if (user.id === action.user.id) {
            return action.user;
          }
          return user;
        });
        this._store.userList = userList;
        break;
      default:
        emitChange = false;
        break;
    }
    if (emitChange) {
      this._emitChange(action.actionType);
    }
  }
}
export default new RFPManagementStore();

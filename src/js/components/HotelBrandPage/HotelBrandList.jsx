import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/HotelBrand/HotelBrand.scss';
import HotelBrandStore from '../../stores/HotelBrandStore';
import HotelBrandAction from '../../actions/HotelBrandAction';


class HotelBrandList extends CommonLayout{
  constructor(props){
  	super(props);
  	this.state = {
         isLoggedIn : false,
         hotelBrandInfo : HotelBrandStore.getAll(),
         sortBy: false,
         setLoader: false,
  	};
    this.onHotelBrandStoreChange = this.onHotelBrandStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSorting = this.handleSorting.bind(this);
    this.deleteHotelBrand = this.deleteHotelBrand.bind(this);
    this.showProfile = this.showProfile.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
    
  componentWillMount(){
  	window.scrollTo(0, 0);
  	const isLoggedIn = AuthStore.isLoggedIn();
  	if (isLoggedIn) {
  		this.setState({ isLoggedIn });
  	}
    const { hotelBrandInfo } = this.state;
    const { brandLists } = hotelBrandInfo;
    
    if (Object.keys(brandLists).length < 1) {
      this.setState({ setLoader: true });
      HotelBrandAction.getHotelBrandList(res => {
        if (res) {
          this.setState({ setLoader: false });
        }
      });
    }

    HotelBrandStore.addStoreListener(this.onHotelBrandStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
  }

  onHotelBrandStoreChange(action) {
    console.log("onHotelBrandStoreChange", arguments);
    const hotelBrandInfo = HotelBrandStore.getAll();
    this.setState({ hotelBrandInfo });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }


  handleSorting() {
    const { sortBy } = this.state
    this.setState({sortBy: !sortBy});
    HotelBrandAction.getSortedBrandList(!sortBy)
  }

  handleSearch(e){
    console.log('target value',e.target.value);
    HotelBrandAction.searchBy(e.target.value);
  }

  showProfile(brand) {
    const { router } = this.context;
    HotelBrandAction.selectedProfile(brand);
    router.history.push(`/property/brand/profile/${brand.id}`);
  }

  deleteHotelBrand(hotelBrandId, hotelBrandStatusId) {
    const { hotelBrandInfo } = this.state;
    if (hotelBrandStatusId !== 2) {
      // HotelBrandAction.deleteHotelBrand(hotelBrandId);
      hotelBrandInfo.brandLists.results = hotelBrandInfo.brandLists.results.filter(result=>result.id !== hotelBrandId)
      HotelBrandAction.deleteHotelBrand(hotelBrandInfo.brandLists);
    }
  }

  handleToggle(brandList, e) {
    const active = e.target.checked;
    HotelBrandAction.statusChange(brandList, active);
  }

	render(){
		const {
      isLoggedIn, isRedirect, hotelBrandInfo, sortBy, isMobileSidebar, setLoader,
    } = this.state;
    const { brandLists, searchVal } = hotelBrandInfo;
    const isBrands = typeof(brandLists) !== 'undefined' && Object.keys(brandLists).length >=1 && brandLists.results.length >= 1;
    const isNoBrands = typeof(brandLists) === 'undefined' || Object.keys(brandLists).length === 0 || brandLists.results.length < 1;
    
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    
		return(
			<Grid className="primary-color hotel-brand-list-container layout-header">
              {this.sideBar()}
              {
                isMobileSidebar && (
                  this.mobileSideBar()
                )
              }
              <Row className="content-right">
                {this.pageHeader()}
                <Row>
                  <Col className="tmargin20" lg={12} md={12} sm={12} xs={12}>
                   <Col className="primary-border-color fright border-search-bar" lg={3} md={4} sm={5} xs={6}>
                    <Col className="lpadding0" sm={9} xs={9}>
                      <div>
                        <input
                        className="find-text text-font14"
                        type="text"
                        placeholder="Find Brand"
                        value={searchVal}
                        onChange={this.handleSearch}
                        />
                      </div>
                    </Col>
                    <Col className="fleft search-icon" lg={3} md={3} sm={3} xs={3}>
                      <span className="fright"><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" width="25px"/></span>
                    </Col>
                   </Col>
                  </Col>
                </Row>
                <Row className="row-title tpadding10 bpadding10">
                  <Col lg={10} md={9} sm={9} xs={6}>
                     <div className="column-header primary-color" >
                        <p className="header-title"><strong>Hotel brand list</strong></p>
                     </div>
                  </Col>
                  <Col lg={2} md={3} sm={3} xs={6}>
                     <p className="i-block fright tpadding10 pointer">
                        <Link to="/property/brand/create">
                        <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px" />
                        &nbsp;&nbsp;&nbsp;Add brand 
                        </Link>
                     </p>
                  </Col>
                </Row>
                <Row className="main-content">
                  <Row>
                    <Col lg={2} md={3}>
                    </Col>
                    <Col className="tpadding20" lg={8} md={6} sm={6} xs={6}>
                      <p className="lpadding10"><strong>Corporate brand</strong>
                        <div 
                        className="sort-icon"
                        onClick={this.handleSorting}>
                          <ReactSVG wrapper="span" 
                           className={sortBy ? "svg-wrapper lpadding10" :  "svg-wrapper transform lpadding10"}
                           src="dist/img/meeting_space/icons/arrow-down.svg"
                           alt="arrow up" 
                           width="10px"
                          />
                        </div>
                      </p>
                    </Col>
                    <Col lg={2} md={3} sm={6} xs={6}>
                      <p className="i-block tpadding20 fright pointer">
                        Inactive/<strong>Active</strong>
                      </p>
                    </Col>
                  </Row>
                { isBrands &&
                  (<div>
                  { brandLists.results.map(brandList => (
                    <Row className={"list-event"}> 
                      <Col
                        className={brandList.status === 2 ? "display-contents disable-hotel-brand" : "display-contents"}
                      >
                       <Col lg={2} md={3} sm={3} xs={4}
                       className={`pointer ${brandList.logo_positive ? "border-hotel-logo text-center" : "border-hotel-logo logo-hidden"}`}
                        onClick={() => this.showProfile(brandList)}
                       >
                         <img className="logo-size" src={brandList.logo_positive}/>
                       </Col>
                       <Col
                        className="brand-name-text pointer" lg={8} md={8} sm={6} xs={5}
                        onClick={() => this.showProfile(brandList)}
                       > 
                         <span className="primary-color text-font-16"><strong>{brandList.name}</strong></span>
                       </Col>
                       <Col lg={2} md={1} sm={3} xs={4}>
                         <label class="fright switch">
                           <input
                              type="checkbox"
                              checked={brandList.status === 1}
                              onChange={e => this.handleToggle(brandList, e)}
                           />
                           <span
                              class="slider round primary-background-color"
                           >
                           </span>
                         </label>
                       </Col>
                      </Col>
                    </Row> ))
                  }
                </div>
                )}
                {
                  isNoBrands && !setLoader && (
                    <Row>
                      <div className="empty-brand-msg">
                        <span>No Hotel Brands Found!</span>
                      </div>
                    </Row>
                  )
                }
                {
                  setLoader && (
                    <Row>
                      <div className="empty-brand-msg">
                        <span>Loading...</span>
                      </div>
                    </Row>
                  )
                }
                </Row>
                <div className="site-footer visible-xs">
                    <div className="logo-section">
                      <p className="rmargin10">Powered by</p>
                      <img src="dist/img/white-logo.png" alt="White Logo" />
                    </div>
                    <div className="footer-menu-section">
                      <span>Hotel</span>
                      <span>Meeting Rooms</span>
                    </div>
                </div>
                
              </Row>
            </Grid>
        )
    }
}

HotelBrandList.contextTypes = {
  router: PropTypes.object,
};

export default HotelBrandList;

import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/HotelBrand/CreateHotelBrand.scss';
import HotelBrandStore from '../../stores/HotelBrandStore';
import HotelBrandAction from '../../actions/HotelBrandAction';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import { SketchPicker } from 'react-color';
import Popup from "reactjs-popup";
import convertCssColorNameToHex from 'convert-css-color-name-to-hex';



class CreateHotelbrand extends CommonLayout{
	constructor(props){
		super(props);
		this.state = {
			isLoggedIn : false,
      hotelBrandInfo : HotelBrandStore.getAll(),
      primaryColorPicker: false,
      secondaryColorPicker: false,
      primaryColor: '',
      secondaryColor: '',
      isPrimaryDeleteColor: false,
      isSecondaryDeleteColor: false,
      isFontSearchBox:false,
      displayedFontSearchBox: '',
      validationStatus:false,
      isClickedDone:false,
      requiredFields:false,
      fonts: [],
      selectedFont: '',
      savingMessage: false,
		};
    this.handleProfile = this.handleProfile.bind(this);
    this.handleSaveProfile = this.handleSaveProfile.bind(this);
    this.createHotelBrand = this.createHotelBrand.bind(this);
    this.onHotelBrandStoreChange = this.onHotelBrandStoreChange.bind(this);
    this.showColorPicker = this.showColorPicker.bind(this);
    this.handleColorPicker = this.handleColorPicker.bind(this);
    this.closeColorPicker = this.closeColorPicker.bind(this);
    this.clearColors = this.clearColors.bind(this);
    this.selectFonts = this.selectFonts.bind(this);
    this.addSelectedFont = this.addSelectedFont.bind(this);
    this.fontSearchBox = this.fontSearchBox.bind(this);
    this.enableAllFontSearchBox = this.enableAllFontSearchBox.bind(this);
    this.clearFonts = this.clearFonts.bind(this);
    this.ProfileFieldValidation = this.ProfileFieldValidation;
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
	}
	componentWillMount(){
    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    if(isLoggedIn){
    	this.setState ({ isLoggedIn });
    }
    HotelBrandStore.addStoreListener(this.onHotelBrandStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
    HotelBrandAction.getTypography();
    setTimeout(() => {
      const myDropzone = new Dropzone('div#my-document-upload-zone', {
        url: ' ',
        paramName: 'file',
        acceptedFiles: ".pdf",
        accept: (file, done) => {
          this.setState({ dragImage: false });
          if (myDropzone.files.length>1) {
            myDropzone.removeFile(myDropzone.files[0])
          }
          const uploadZone = $('#my-document-upload-zone');
          if (uploadZone) {
            const fileUpload = $('#my-document-upload-zone')[0].childNodes.length;
            const dragMessageTop = `${20 + (fileUpload * 30)}px`;
            const dropZoneHeight = `${200 + (fileUpload * 30)}px`;
            $(`<style>.create-hotel-brand-list-container #upload-doc-section .drag-upload-file
            {top: ${dragMessageTop};}</style>`).appendTo('head');
            $(`<style>.create-hotel-brand-list-container #my-document-upload-zone {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
          }
          console.log('ACCEPT: ', file);
          done();
        },
        uploadMultiple: false,
        parallelUploads: 1,
        previewTemplate: document.querySelector('#file-upload-template').innerHTML,
        thumbnailWidth: 10,
        thumbnailHeight: 10,
        thumbnailMethod: 'contain',
        autoProcessQueue: false, // upload all files at once when submit button is clicked
      });

      myDropzone.on('error', (file, message, xhr) => {
        if (xhr == null) myDropzone.removeFile(file); // Remove the unsupported files
        const msg = `Unsupported file format. Please upload only pdf format`;
        HotelBrandAction.getNotificationMsg(msg, 'add');
        this.setState({ isClickedDone :true});
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedDone :false });
        }, 5000);
      });

      myDropzone.on('removedfile', (file) => {
        const uploadZone = $('#my-document-upload-zone');
        if (uploadZone) {
          const fileUpload = $('#my-document-upload-zone')[0].childNodes.length;
          const dragMessageTop = `${20 + (fileUpload * 30)}px`;
          const dropZoneHeight = `${180 + (fileUpload * 30)}px`;
          $(`<style>.create-hotel-brand-list-container #upload-doc-section .drag-upload-file
          {top: ${dragMessageTop};}</style>`).appendTo('head');
          $(`<style>.create-hotel-brand-list-container #my-document-upload-zone {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
        }
        this.setState({ acceptedFiles: myDropzone.getAcceptedFiles() });
        if (myDropzone.files.length <= 0) {
          console.log('REMOVED: ', file);
          this.setState({ uploadedFile: false });
          this.handleProfile('','file', 'privacy_policy');
        }
      });
      myDropzone.on('addedfile', (file) => {
        this.handleProfile('', 'file', 'privacy_policy');
        this.setState({ uploadedFile: true });
        console.log('ADDED: ', file);
      });
      this.setState({ myDropzone });
    }, 3000);


	}

  onHotelBrandStoreChange(action) {
    console.log("onHotelBrandStoreChange", arguments);
    const hotelBrandInfo = HotelBrandStore.getAll();
    this.setState({ hotelBrandInfo });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  handleProfile(e, fieldType, fileAtribute) {
    const { hotelBrandInfo } = this.state;
    const { profileInfo } = hotelBrandInfo;
    if (fieldType === 'country'){
      profileInfo[fieldType] = e;
    } else if(fieldType === 'file') {
      if (fileAtribute === 'logo_positive') {
        const fileInput = document.getElementById("logo_positive");
        const file = fileInput.files[0];
        if (file) {
          // Image Preview
          var reader = new FileReader();
          reader.onload = function (e) {
          $('#upload-logo-positive')
            .attr('src', e.target.result)
            .height(39);
          };
          reader.readAsDataURL(file);
          profileInfo[fileAtribute] = file;
          HotelBrandAction.saveProfile(profileInfo);
        }
      } else if(fileAtribute === 'logo_negative') {
        const fileInput = document.getElementById("logo_negative");
        const file = fileInput.files[0];
        if (file) {
          // Image Preview
          var reader = new FileReader();
          reader.onload = function (e) {
          $('#upload-logo-negative')
            .attr('src', e.target.result)
            .height(39);
          };
          reader.readAsDataURL(file);
          profileInfo[fileAtribute] = file;
          HotelBrandAction.saveProfile(profileInfo);
        }
      } else {
        const {myDropzone} = this.state;
        if (myDropzone.files.length >=1) {
          profileInfo[fileAtribute] = myDropzone.files[myDropzone.files.length-1];
        } else {
          profileInfo.hasOwnProperty('privacy_policy') ? delete profileInfo.privacy_policy : '';
        }
        HotelBrandAction.saveProfile(profileInfo);
      }
    } else {
      const { name, value } = e.target;
      profileInfo[name] = value;
    }
    this.setState({ profileInfo });
  }

  handleSaveProfile() {
    const { profileInfo } = this.state;
    HotelBrandAction.saveProfile(profileInfo);
  }

  createHotelBrand(){
    const { hotelBrandInfo } = this.state;
    const { profileInfo } = hotelBrandInfo;
    const {myDropzone} = this.state;
    if (Object.keys(profileInfo).length !==0) {
      let isClickedDone = true;
      const validationStatus = this.ProfileFieldValidation(profileInfo)
      this.setState({ isClickedDone });
      if (validationStatus) {
        const address = profileInfo['address'].replace(/ /g,"+")+'+'+profileInfo['city'].replace(/ /g,"+");
        HotelBrandAction.getGeometryLocation(address, (GeometryLocation)=>{
          if (GeometryLocation.status === "OK") {
            const location = GeometryLocation.results[0].geometry.location.lng+', '+GeometryLocation.results[0].geometry.location.lat;
            profileInfo['location'] = location;
            if (profileInfo.hasOwnProperty('primary_color')) {
              profileInfo['primary_color'] = convertCssColorNameToHex(profileInfo['primary_color']);
            }
            if (profileInfo.hasOwnProperty('secondary_color')) {
              profileInfo['secondary_color'] = convertCssColorNameToHex(profileInfo['secondary_color']);
            }
            profileInfo['description'] = profileInfo['name'];
            HotelBrandAction.getNotificationMsg('Saving...', 'add');
            this.setState({ savingMessage:true });
            HotelBrandAction.createHotelBrand(profileInfo,(response)=>{
              // HotelBrandAction.getHotelBrandList();
              this.setState({ savingMessage:false });
              HotelBrandAction.getNotificationMsg('Saving...', 'remove');
              const { router } = this.context;
              router.history.push('/property/brand/list');
              this.setState({requiredFields:false})
              if (myDropzone.files.length >=1) {
                for (var file=0; file<myDropzone.files.length; file++) {
                  myDropzone.removeFile(myDropzone.files[file]);
                }
              }   
            });
          } else {
              const msg = `Invalid address and city. Please check`;
              HotelBrandAction.getNotificationMsg(msg, 'add');
              this.setState({ isClickedDone :true, validationStatus:false});
              setTimeout(() => {
                HotelBrandAction.getNotificationMsg(msg, 'remove');
                this.setState({ isClickedDone :false, validationStatus:false});
              }, 5000);
          }
        });
      }
    } else {
      const msg = 'Please fill the required fields'
      HotelBrandAction.getNotificationMsg(msg, 'add');
      this.setState({ isClickedDone :true, validationStatus:false});
      setTimeout(() => {
        HotelBrandAction.getNotificationMsg(msg, 'remove');
        this.setState({ isClickedDone :false, validationStatus:false});
      }, 5000);
      this.setState({requiredFields:true})
    }
    
  }


  ProfileFieldValidation (profileInfo) {
    let validationStatus = true
    if ((!profileInfo.hasOwnProperty('name') || profileInfo['name'] === '') || (!profileInfo.hasOwnProperty('address') || profileInfo['address'] === '') || (!profileInfo.hasOwnProperty('city') || profileInfo['city'] === '')) {
      const msg = 'Please fill the required fields'
      HotelBrandAction.getNotificationMsg(msg, 'add');
      setTimeout(() => {
        HotelBrandAction.getNotificationMsg(msg, 'remove');
        this.setState({ isClickedDone :false });
      }, 5000);
      validationStatus = false;
      this.setState({requiredFields:true})
    } else if (profileInfo.hasOwnProperty('name')) {
      if (!(/\S/.test(profileInfo['name']))) {
        const msg = 'Please enter the valid corporate brand name'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedDone :false });
        }, 5000);
        validationStatus = false;
      }
    }

    if (profileInfo.hasOwnProperty('sender_email') && (/\S/.test(profileInfo['sender_email']))) {
      if (!profileInfo['sender_email'].match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/)) {
        const msg = 'Please enter the valid email'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedDone :false });
        }, 5000);
        validationStatus = false;
      }
    }

    if (profileInfo.hasOwnProperty('primary_color') && profileInfo['primary_color'] !=='') {
      if (!(/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(convertCssColorNameToHex(profileInfo['primary_color'])))) {
        const msg = 'Please enter the valid main color'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedDone :false });
        }, 5000);
        validationStatus = false;
      }
    }

    if (profileInfo.hasOwnProperty('secondary_color') && profileInfo['secondary_color'] !=='') {
      if (!(/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(convertCssColorNameToHex(profileInfo['secondary_color'])))) {
        const msg = 'Please enter the valid secondary color'
        HotelBrandAction.getNotificationMsg(msg, 'add');
        setTimeout(() => {
          HotelBrandAction.getNotificationMsg(msg, 'remove');
          this.setState({ isClickedDone :false });
        }, 5000);
        validationStatus = false;
      }
    }

    this.setState({ validationStatus });
    return validationStatus    
  }

  showColorPicker(colorPickerType) {
    const { secondaryColorPicker, primaryColorPicker } = this.state;
    this.setState({
      secondaryColorPicker: colorPickerType === 'secondary_color',
      primaryColorPicker: colorPickerType === 'primary_color',
    });
    
  }

  closeColorPicker() {
    this.setState({
      secondaryColorPicker: false,
      primaryColorPicker:false,
    });
  }

  handleColorPicker(color, event, colorPickerType) {
    const { hotelBrandInfo } = this.state;
    const { profileInfo } = hotelBrandInfo;
    
    if(colorPickerType === 'primary_color') {
      $('.primary-input-color').val(color.hex);
      profileInfo[colorPickerType] = color.hex;
      HotelBrandAction.saveProfile(profileInfo);
      this.setState({ primaryColor: color.rgb, isPrimaryDeleteColor: false });
    }else {
      $('.secondary-input-color').val(color.hex);
      profileInfo[colorPickerType] = color.hex;
      HotelBrandAction.saveProfile(profileInfo);
      this.setState({ secondaryColor: color.rgb, isSecondaryDeleteColor: false });
    }
    HotelBrandAction.saveProfile(profileInfo);
    this.setState({ profileInfo });
  
  }

  clearColors(colorPickerType) {
    const { hotelBrandInfo } = this.state;
    const { profileInfo } = hotelBrandInfo;

    if(colorPickerType === 'primary_color') {
      $('.primary-input-color').val('');
      this.setState({isPrimaryDeleteColor:true});
      if(profileInfo.hasOwnProperty(colorPickerType)){
        delete profileInfo[colorPickerType];
      }
    }else{
      $('.secondary-input-color').val('');
      this.setState({isSecondaryDeleteColor:true});
      if(profileInfo.hasOwnProperty(colorPickerType)){
        delete profileInfo[colorPickerType];
      }
    }
    HotelBrandAction.saveProfile(profileInfo);
    this.setState({ profileInfo });

  }

  clearFonts(isMainFonts) {
    const { hotelBrandInfo } = this.state;
    const { profileInfo } = hotelBrandInfo;
    
    if(isMainFonts && profileInfo.hasOwnProperty('main_typography')) {
      delete profileInfo['main_typography'];
    } else if(profileInfo.hasOwnProperty('secondary_typography')) {
      delete profileInfo['secondary_typography'];
    }
    HotelBrandAction.saveProfile(profileInfo);
    this.setState({ profileInfo });

  }

  selectFonts(e, isMainFonts) {
    this.setState({ selectedFont: e.target.value });
    const  value  = isMainFonts ? $('.font-search-input-main').val() : $('.font-search-input-secondary').val();
    const { hotelBrandInfo } = this.state;
    const { fontsList } = hotelBrandInfo;
    let fonts = [];
    for (var i = 0; i<fontsList.length; i++) { 
      if (value.length >=3 && (new RegExp(value.toLowerCase())).test(fontsList[i].toLowerCase()))
      { 
        fonts.push(fontsList[i]);
        this.setState({ fonts });
      } else if((fontsList[i].toLowerCase()).startsWith(value.toLowerCase())) {
        fonts.push(fontsList[i]);
        this.setState({ fonts });
      }
    } 

  }

  addSelectedFont(font, isEnteredAddFont, isMainFonts){
      console.log('select font', font);
      this.setState ({ selectedFont : font});
      const { hotelBrandInfo } = this.state;
      const { profileInfo } = hotelBrandInfo;
      const Typography = font ;
      if(Typography !==''){
        if (isMainFonts){
          profileInfo['main_typography'] = Typography;
        } else {
          profileInfo['secondary_typography'] = Typography;
        }
        this.setState({ profileInfo, isFontSearchBox: true });
        HotelBrandAction.saveProfile(profileInfo);
        if (isEnteredAddFont) {
          this.enableAllFontSearchBox();
        }
      }
  }

  fontSearchBox(whichFontSearchBox) {
    const { hotelBrandInfo } = this.state;
    const { fontsList } = hotelBrandInfo;
    this.setState({
      displayedFontSearchBox: whichFontSearchBox,
      selectedFont: '',
      fonts: fontsList,
    })
  }

  enableAllFontSearchBox() {
    this.setState({ 
      displayedFontSearchBox : '',
      isFontSearchBox: false,
    })
  }

  getFontsList(fonts, isEnteredAddFont, isMainFonts) {
      const fontsList = (
      <ul id="datalist">
        {
          fonts.map((font, i) => {
            return (
              <li
                className="pointer primary-color-light"
                key={i}
                onClick={() => this.addSelectedFont(font, isEnteredAddFont, isMainFonts)}
              >
                {font}
              </li>
            )
          })
        }
      </ul>
    );
    return fontsList;
  }

	render(){
		const { isLoggedIn, isRedirect, hotelBrandInfo, primaryColorPicker, secondaryColorPicker, primaryColor, secondaryColor,
     isPrimaryDeleteColor, isSecondaryDeleteColor, isFontSearchBox, displayedFontSearchBox, validationStatus, isClickedDone, 
     isMobileSidebar, requiredFields, fonts, selectedFont, savingMessage } = this.state;
    const { profileInfo, fontsList, notificationMsg } = hotelBrandInfo;
    const cover = {
      position: 'fixed',
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px',
    }    
    const onDeleteStyle = {
      border: '1px solid #ad0b3a33',
      color: 'black',
    }
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    
    return(
      <Grid className="primary-color create-hotel-brand-list-container layout-header">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <Row className="content-right">
              {this.pageHeader( savingMessage ? savingMessage : ((!validationStatus) && isClickedDone), notificationMsg)}
              <div>
                <Button
                  className="back-button primary-color"
                  onClick={this.context.router.history.goBack}
                >
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/back-arrow.svg" width="25px"/>
                  <span className="hidden-xs">&nbsp;&nbsp;Back</span>
                </Button>
              </div>
              <Row className="row-title">
                <Col>
                  <div className="column-header lmargin20 primary-color" >
                    <p className="header-title"><strong>Create hotel brand profile</strong></p>
                  </div>
                </Col>
              </Row>
              <Row className="main-content">
                <div className="primary-color tmargin20 lmargin45 text-font-12 hidden-xs hidden-sm"><strong>Corporate brand</strong></div>
                <Row className="list-event">
                  <div className="display-contents">
                    <Col className="column-background" lg={4} md={4} sm={12} xs={12}>
                      <div className="primary-color text-font-16 lmargin10">Corporate brand name
                      </div>
                      <div>
                        <input 
                          className={`input-brand-name tmargin10 ${requiredFields && (!profileInfo.hasOwnProperty('name') || profileInfo['name'] === '') ? 'required-field' : 'primary-border-color-light'}`}
                          type="text" 
                          placeholder=""
                          onChange={this.handleProfile}
                          onBlur={() => this.handleSaveProfile('name')}
                          name="name"
                          value={profileInfo.hasOwnProperty('name') ? profileInfo.name : ''}
                        />
                      </div> 
                      <div className="primary-color text-font-16 lmargin10 tmargin10">Address
                      </div>
                      <div>
                        <input 
                          className={`input-brand-name tmargin10 ${requiredFields && (!profileInfo.hasOwnProperty('address') || profileInfo['address'] === '') ? 'required-field' : 'primary-border-color-light'}`} 
                          type="text" 
                          placeholder=""
                          onChange={this.handleProfile}
                          onBlur={() => this.handleSaveProfile('address')}
                          name="address"
                          value={profileInfo.hasOwnProperty('address') ? profileInfo.address : ''}
                        />
                      </div>   
                      <div className="primary-color text-font-16 lmargin10 tmargin10">Country / Region
                      </div>
                      <div className="country-dropdown-background tmargin10 primary-border-color-light">
                        <CountryDropdown className="country-select primary-border-color-light tmargin5 profile-country"
                          defaultOptionLabel = "- -"
                          onChange={(e)=>this.handleProfile(e, 'country')}
                          value={profileInfo.hasOwnProperty('country') ? profileInfo.country : ''}
                          name="country"
                        />

                      </div>
                      <Row className="tpadding20">
                        <Col className="align-content-city" lg={7} md={7} sm={12} xs={12}>
                          <div className="text-font-16 lmargin5">City
                          </div>
                          <div>
                            <input 
                              className={`input-brand-name tmargin10 ${requiredFields && (!profileInfo.hasOwnProperty('city') || profileInfo['city'] === '') ? 'required-field' : 'primary-border-color-light'}`}
                              type="text" 
                              placeholder=""
                              onChange={this.handleProfile}
                              onBlur={() => this.handleSaveProfile('city')}
                              name="city"
                              value={profileInfo.hasOwnProperty('city') ? profileInfo.city : ''}
                            />
                          </div>
                        </Col> 
                        <Col className="align-content-zip-code" lg={5} md={5} sm={12} xs={12}>
                          <div className="text-font-16 lmargin5">Zip code
                          </div>
                          <div>
                            <input 
                              className="input-brand-name primary-border-color-light tmargin10" 
                              type="text" 
                              placeholder=""
                              onChange={this.handleProfile}
                              onBlur={() => this.handleSaveProfile('zipcode')}
                              name="zipcode"
                              value={profileInfo.hasOwnProperty('zipcode') ? profileInfo.zipcode : ''}
                            />
                          </div>
                        </Col>
                      </Row>
                      <div className="primary-color text-font-16 lmargin10 tmargin10">e-mail
                      </div>
                      <div>
                        <input 
                          className="input-brand-name primary-border-color-light tmargin10" 
                          type="text" 
                          placeholder=""
                          onChange={this.handleProfile}
                          onBlur={() => this.handleSaveProfile('sender_email')}
                          name="sender_email"
                          value={profileInfo.hasOwnProperty('sender_email') ? profileInfo.sender_email : ''}
                        />
                      </div>
                    </Col>
                    <Col className="primary-border-left column-background" lg={4} md={4} sm={12} xs={12}>
                      <Row className="primary-color text-font-12 lmargin10"><strong>Hotel chain logos</strong>
                      </Row>
                      <Row className="tpadding15"> 
                       <Col className="add-logo-padding" lg={6} md={6} sm={6} xs={6}>
                         <div className="primary-border-color-light add-logo-background shadow">
                         <div>
                          <input 
                            type="file"
                            accept="image/x-png,image/jpeg"
                            className="input-file-class visibilty-hidden"
                            id="logo_positive"
                            onChange={(e) => this.handleProfile(e, 'file', 'logo_positive')} />
                          <label for="logo_positive" class="btn wrap-text">
                           <p className="i-block">
                           <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px" />
                           &nbsp;&nbsp;&nbsp;Add logo 1 </p>
                          </label>
                         </div>
                         {profileInfo.hasOwnProperty('logo_positive') ? 
                          (<div className="img text-center"> 
                            <img id="upload-logo-positive" src="#" alt="logo positive" />
                          </div>) :
                          (<div className="background-light tmargin10"> 
                            <strong>Logo</strong>
                          </div>)
                          }
                         <div className="text-font-12 text-center tmargin10">
                           On light background
                         </div>
                         </div>
                       </Col>
                       <Col className="add-logo-padding" lg={6} md={6} sm={6} xs={6}>
                         <div className="primary-border-color-light add-logo-background shadow">
                           <div>
                           <input 
                              type="file"
                              accept="image/x-png,image/jpeg"
                              className="input-file-class visibilty-hidden"
                              id="logo_negative"
                              onChange={(e) => this.handleProfile(e, 'file', 'logo_negative')} />
                            <label for="logo_negative" class="btn wrap-text">
                             <p className="i-block">
                             <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px" />
                             &nbsp;&nbsp;&nbsp;Add logo 2 </p>
                            </label>
                            </div>
                            {profileInfo.hasOwnProperty('logo_negative') ? 
                              (<div className="img text-center"> 
                                <img id="upload-logo-negative" src="#" alt="logo negative" />
                              </div>) :
                              (<div className="background-dark tmargin10"> 
                                <strong>Logo</strong>
                              </div>)
                            }
                           <div className="text-font-12 text-center tmargin10">
                             On dark background
                           </div>
                         </div>
                       </Col>
                      </Row>
                      <Row className="primary-color text-font-12 tpadding20"><strong>Hotel chain colors</strong>
                      </Row>
                      <Row className="tpadding25"> 
                        <Col className="lpadding0" lg={5} md={4} sm={6} xs={5}>
                          <input 
                            className="input-brand-name input-color primary-input-color primary-border-color-light text-font-12" 
                            type="text" 
                            placeholder="Enter #ffffff value"
                            onChange={this.handleProfile}
                            onBlur={() => this.handleSaveProfile('primary_color')}
                            name="primary_color"
                            value={profileInfo.hasOwnProperty('primary_color') ? profileInfo.primary_color : ''}
                          />
                        </Col>
                        { !profileInfo.hasOwnProperty('primary_color') && isPrimaryDeleteColor ?
                        (<Col className="background-main background-cross text-center text-font-12" lg={3} md={3} sm={3} xs={3} style={onDeleteStyle}>Main
                          
                        </Col>)
                        :(<Col className="background-main text-center text-font-12" lg={3} md={3} sm={3} xs={3} style={{'background-color' : (profileInfo.hasOwnProperty('primary_color') && profileInfo['primary_color'] !=='') ? profileInfo.primary_color : '#b67a00'}}>Main
                        </Col>)
                        }
                        <Col className="tmargin5" lg={2} md={3} sm={2} xs={2}>
                          <div 
                            className="primary_color"
                            onClick={()=> {this.showColorPicker('primary_color')}}
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper button-substitute primary-border-color-light" src="dist/img/hotelbrand/substitute.svg"/>
                          </div>
                            {primaryColorPicker ? 
                              (<div>
                                <div 
                                  className="close-color-picker"
                                  style={cover}
                                  onClick={this.closeColorPicker}
                                  >
                                </div>
                                <div className="color-picker">
                                  <SketchPicker
                                  color={ primaryColor } 
                                  onChange={(color, event)=> {this.handleColorPicker(color, event, 'primary_color') }} />
                                </div>
                              </div>) : ''
                            }                         
                        </Col>
                        <Col className="rpadding0 tmargin5" lg={2} md={2} sm={1} xs={2}>
                        <div 
                        className="delete-primary-color"
                        onClick={()=>{this.clearColors('primary_color')}}
                        >
                          <ReactSVG wrapper="span" className="svg-wrapper primary-border-left fright" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />                          
                        </div>
                        </Col>
                      </Row>
                      <Row className="tpadding25"> 
                        <Col className="lpadding0" lg={5} md={4} sm={6} xs={5}>
                          <input 
                            className="input-brand-name input-color secondary-input-color primary-border-color-light text-font-12" 
                            type="text" 
                            placeholder="Enter #ffffff value"
                            onChange={this.handleProfile}
                            onBlur={() => this.handleSaveProfile('secondary_color')}
                            name="secondary_color"
                            value={profileInfo.hasOwnProperty('secondary_color') ? profileInfo.secondary_color : ''}
                          />
                        </Col>
                        {!profileInfo.hasOwnProperty('secondary_color') && isSecondaryDeleteColor ?
                        (<Col className="background-secondary background-cross text-center text-font-12" lg={3} md={3} sm={3} xs={3} style={onDeleteStyle}>Secondary
                          
                        </Col>)
                        :(<Col className="background-secondary text-center text-font-12" lg={3} md={3} sm={3} xs={3} style={{'background-color' : (profileInfo.hasOwnProperty('secondary_color') && profileInfo['secondary_color'] !=='') ? profileInfo.secondary_color : '#96accd'}}>Secondary
                        </Col>)
                        }
                        <Col className="tmargin5" lg={2} md={3} sm={2} xs={2}>
                          <div 
                            className="secondary_color"
                            onClick={()=> {this.showColorPicker('secondary_color')}}
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper button-substitute primary-border-color-light" src="dist/img/hotelbrand/substitute.svg"/>
                          </div>
                            {secondaryColorPicker ? 
                              (<div>
                                <div 
                                  className="close-color-picker"
                                  style={cover}
                                  onClick={this.closeColorPicker}
                                  >
                                </div>
                                <div className="color-picker">
                                  <SketchPicker 
                                  color={ secondaryColor }
                                  onChange={(color, event) => {this.handleColorPicker(color, event, 'secondary_color') }} />
                                </div>
                              </div>) : ''
                            }      
                        </Col>
                        <Col className="rpadding0 tmargin5" lg={2} md={2} sm={1} xs={2}>
                        <div 
                        className="delete-secondary-color"
                        onClick={()=>{this.clearColors('secondary_color')}}
                        >
                          <ReactSVG wrapper="span" className="svg-wrapper primary-border-left fright" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                        </div>
                        </Col>
                      </Row>
                      <Row>
                        <div className="text-font-12 tmargin20 font-color-hint">( 2 colors maximum ).</div>
                      </Row>
                    </Col>
                    <Col className="primary-border-left column-background" lg={4} md={4} sm={12} xs={12}>
                      <Row className="primary-color text-font-12"><strong>Hotel typography</strong>
                      </Row>
                      <Row className="tpadding25 dflex">
                        <Col className="primary-color lpadding0 tmargin5 text-font-12" lg={3} md={3} sm={3} xs={4}>
                         <strong>Main</strong>
                        </Col>
                        <Col className="primary-color-light no-padding primary-border-left text-font-14" lg={5} md={5} sm={6} xs={4}>
                        { profileInfo.hasOwnProperty('main_typography') ? (<p className="i-block">{profileInfo.main_typography.length >= 12 ? profileInfo.main_typography.substring(0,12)+'...' : profileInfo.main_typography}</p>) :
                          (<Popup trigger={
                              <p className="i-block font-selector">
                              <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="20px" />
                              &nbsp;&nbsp;&nbsp;Add </p>
                            } 
                            position="top center"
                            onOpen={() => {this.fontSearchBox('isMainAddFontSearchBox')}}
                            onClose={this.enableAllFontSearchBox}
                            disabled={(displayedFontSearchBox === '' || displayedFontSearchBox === 'isMainAddFontSearchBox') && (!isFontSearchBox) ? false : true}>
                            <div className = "font-search-input">
                              <img src="dist/img/event_list/search-black.png" width="20px" />
                              <input
                                className="font-search-input-main" 
                                onChange={(e)=>{this.selectFonts(e, true)}}
                                placeholder="Search fonts"
                                type="text" 
                                value={selectedFont}
                              /> 
                            </div>

                            { this.getFontsList(fonts, true, true) }
                          </Popup>)
                        }
                        </Col>
                         <Col className="tmargin5 align-button-ipad align-button" lg={3} md={3} sm={2} xs={3}>
                         {
                         <Popup 
                            trigger={<ReactSVG wrapper="span" className="font-selector svg-wrapper button-substitute primary-border-color-light" src="dist/img/hotelbrand/substitute.svg"/>} 
                            position="top center"
                            onOpen={() => {this.fontSearchBox('isMainRefreshFontSearchBox')}}
                            onClose={this.enableAllFontSearchBox}
                            disabled={(displayedFontSearchBox === '' || displayedFontSearchBox === 'isMainRefreshFontSearchBox') && (!isFontSearchBox) ? false : true}>
                            <div className = "font-search-input">
                              <img src="dist/img/event_list/search-black.png" width="20px" />  
                              <input
                              className="font-search-input-main" 
                              onChange={(e)=>{this.selectFonts(e, true)}}
                              type="text" 
                              placeholder="Search fonts"
                              value={selectedFont}
                              /> 
                            </div>
                            { this.getFontsList(fonts, false, true) }
                          </Popup>
                          } 
                        </Col>
                        <Col className="rpadding0" lg={1} md={1} sm={1} xs={1}>
                          <div
                          className="font-selector"
                          onClick={()=>{this.clearFonts(true)}} 
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper primary-border-left fright" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                          </div>
                        </Col>
                      </Row>
                      <Row className="tpadding25 dflex">
                        <Col className="primary-color word-wrap lpadding0 text-font-12 tmargin5" lg={3} md={3} sm={3} xs={4}>
                         <strong>Secondary</strong>
                        </Col>
                        <Col className="primary-color-light no-padding primary-border-left text-font-14" lg={5} md={5} sm={6} xs={4}>
                        { profileInfo.hasOwnProperty('secondary_typography') ? (<p className="i-block">{profileInfo.secondary_typography.length >= 12 ? profileInfo.secondary_typography.substring(0,12)+'...' : profileInfo.secondary_typography}</p>) :
                          (<Popup trigger={
                              <p className="i-block font-selector">
                              <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="20px" />
                              &nbsp;&nbsp;&nbsp;Add </p>
                            } 
                            position="top center"
                            onOpen={() => {this.fontSearchBox('isSecAddFontSearchBox')}}
                            onClose={this.enableAllFontSearchBox}
                            disabled={(displayedFontSearchBox === '' || displayedFontSearchBox === 'isSecAddFontSearchBox') && (!isFontSearchBox) ? false : true}>
                            <div className = "font-search-input">
                              <img src="dist/img/event_list/search-black.png" width="20px" />  
                              <input
                                className="font-search-input-secondary" 
                                onChange={(e)=>{this.selectFonts(e, false)}}
                                type="text" 
                                placeholder="Search fonts"
                                value={selectedFont}
                              />
                            </div> 
                            { this.getFontsList(fonts, true, false) }
                          </Popup>)
                        }
                        </Col>
                        <Col className="tmargin5 align-button-ipad align-button" lg={3} md={3} sm={2} xs={3}>
                         {
                         <Popup 
                            trigger={<ReactSVG wrapper="span" className="font-selector svg-wrapper button-substitute primary-border-color-light" src="dist/img/hotelbrand/substitute.svg"/>} 
                            position="top center"
                            onOpen={() => {this.fontSearchBox('isSecRefreshFontSearchBox')}}
                            onClose={this.enableAllFontSearchBox}
                            disabled={(displayedFontSearchBox === '' || displayedFontSearchBox === 'isSecRefreshFontSearchBox') && (!isFontSearchBox) ? false : true}>
                            <div className = "font-search-input">
                              <img src="dist/img/event_list/search-black.png" width="20px" />  
                              <input
                              className="font-search-input-secondary" 
                              onChange={(e)=>{this.selectFonts(e, false)}}
                              type="text" 
                              placeholder="Search fonts"
                              value={selectedFont}
                              /> 
                            </div>
                            { this.getFontsList(fonts, false, false) }
                          </Popup>
                          } 
                        </Col>
                        <Col className="rpadding0 tmargin5" lg={1} md={1} sm={1} xs={1}>
                          <div
                          className="font-selector"
                          onClick={()=>{this.clearFonts(false)}} 
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper primary-border-left fright" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                          </div>
                        </Col>
                      </Row>
                      <hr className="primary-border-color-light"></hr>
                      <Row>
                        <div className="text-font-12 font-color-hint">( 2 fonts maximum ).</div>
                      </Row>
                      <Col className="tpadding10 bpadding120">
                      <Row className="text-font-12 tmargin20"><strong>Privacy policy</strong>
                      </Row>
                      <Row className="add-doc-row box-rfp bpadding20">
                        <Col md={12} lg={12} xs={12} id="upload-doc-section" className="text-center tmargin10">
                          <div id="my-document-upload-zone" />
                          <div id="file-upload-template">
                            <div className="dz-preview dz-file-preview">
                              <div className="dz-details">
                                <img src="" data-dz-thumbnail />
                                <span data-dz-name className="file-dz-name" />
                                (
                                <span data-dz-size />
                                )
                                <span data-dz-remove className="dz-remove pointer">✘</span>
                                <span className="dz-error-message" data-dz-errormessage />
                              </div>
                              <div className="dz-progress">
                                <span className="dz-upload" data-dz-uploadprogress />
                              </div>
                            </div>
                          </div>
                          <div className="drag-upload-file block">
                            <ReactSVG wrapper="div" className="svg-wrapper text-center" src="dist/img/upload-icon.svg" alt="Upload Icon" />
                            <span className="block tmargin20 font16 text-center">
                              Drag and drop files here
                              <br />
                              or click to upload
                            </span>
                          </div>
                        </Col>
                      </Row>
                      </Col>
                    </Col>
                  </div>
                </Row>
                <Row className="tpadding20 bpadding20">
                 <Col lg={12} md={12} sm={12} xs={12}>
                  <Button 
                    className="fright button-done primary-color primary-border-color-light"
                    disabled={validationStatus? "disabled":""}
                    onClick={this.createHotelBrand}
                  >
                  Done
                  </Button>
                 </Col>
                </Row>
              </Row>
            </Row>
      </Grid>
    )
  }
}

CreateHotelbrand.contextTypes = {
  router: PropTypes.object,
};

export default CreateHotelbrand;
import React, { Component } from 'react';
import { Row, Col, Button, Alert } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import ReactSVG from 'react-svg';
import '../../../css/CommonLayout/Header.scss';
import '../../../css/CommonLayout/Sidebar.scss';
import '../../../css/HomePage/HomePage.scss';
import AuthStore from '../../stores/AuthStore';
import HotelBrandAction from '../../actions/HotelBrandAction';
import HotelBrandStore from '../../stores/HotelBrandStore';
import NotificationAction from '../../actions/NotificationAction';
import NotificationStore from '../../stores/NotificationStore';

class CommonLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settingsContainer: false,
      isRedirect: false,
      showSidebar: false,
      setSelectedColor : false,
    };
    this.logOut = this.logOut.bind(this);
    this.showSidebar =this.showSidebar.bind(this);
    this.openSidebar = this.openSidebar.bind(this);
    this.handleSettings = this.handleSettings.bind(this);
    this.closeAlert = this.closeAlert.bind(this);
  }

  handleSettings() {
    this.setState(prevState => ({
      settingsContainer: !prevState.settingsContainer,
    }));
  }

  closeAlert(msg) {
    HotelBrandAction.getNotificationMsg(msg, 'remove');
  }

  logOut() {
    AuthStore.logout((res) => {
      if (res) {
        this.setState({ isRedirect: true });
        NotificationAction.notificationApiSwitch(false);
      }
    });
  }

  showSidebar(){
    this.setState( prevState=> ({
       showSidebar: !prevState.showSidebar,
    }));
  }

  // setColor(id){
  //   HotelBrandAction.setSelectedColor(id);  
  // }

  openSidebar() {
    this.setState(prevState => ({
      isMobileSidebar: !prevState.isMobileSidebar,
    }));
  }

  loadCommonCSS(property, isHotelPage=false, gaTracking=null) {
    // 'property' param is the primary color based on hotel id from the public pages
    const userInfo = AuthStore.getAll();
    const { data } = userInfo.user;
    let primary_color = data.profile.hotel_chain.primary_color;
    // if (property) {
    //   primary_color = property;
    // }  
    const mapTip = primary_color.slice(1);
    const styles = {
        '.fill-primary': {
          'fill': `${primary_color}`
        },
        '.stroke-primary': {
          'stroke': `${primary_color}`
        },
        '.primary-color': {
          'color': `${primary_color}`
        },
        '.primary-background-color': {
          'background-color': `${primary_color}`
        },
        '.primary-background-color-light': {
          'background-color': `${primary_color}0d`
        },
        '.primary-background-color-light-add-visuals': {
          'background-color': `${primary_color}0d`
        },
        '.primary-border-color': {
          'border': `1px solid ${primary_color}`
        },
        '.primary-border-color-sidebar-header':{
          'border': `0.7px solid ${primary_color}1a`
        },
        '.primary-border-color-light':{
          'border': `1px solid ${primary_color}33 !important`
        },
        '.primary-border-left': {
           'border-left': `1px solid ${primary_color}`
        },
        '.primary-border-right': {
           'border-right': `1px solid ${primary_color}`
        },
        '.primary-border-left-light': {
           'border-left': `1px solid ${primary_color}33 !important`
        },
        '.primary-border-right-light': {
           'border-right': `1px solid ${primary_color}33 !important`
        },
        '.primary-border-bottom': {
           'border-bottom': `1px solid ${primary_color}`
        },
        '.primary-border-bottom-light': {
           'border-bottom': `1px solid ${primary_color}33 !important`
        },
        '.primary-border-top': {
           'border-top': `1px solid ${primary_color}33 !important`
        },
        '.border-color-primary': {
           'border-color': primary_color
        },
        '.border-hotel-logo': {
           'border': `0.5px solid ${primary_color}30`
        },
        '.navigation-sidebar-selected': {
          'background-color': `${primary_color}80`
        },
        '.sidebar-background-color:before': {
          'background-image':`linear-gradient(0deg, ${primary_color}e6 45%, ${primary_color}99 145%)`
        },
        'input[type="text"].enter-text::placeholder': {
          'color': `${primary_color}`
        },
        'input[type="text"].find-text::placeholder': {
          'color': `${primary_color}`
        },
        '[type="radio"]:checked + label:after, [type="radio"]:not(:checked) + label:after': {
          'background': `${primary_color}`
        },
        '.image-start-color': {
          'stop-color': `${primary_color}`
        },
        '.image-start-color-disabled': {
          'stop-color': `${primary_color}1a`
        },
        '.primary-color-light': {
           'color': `${primary_color}80`
        },
        '.meeting-room-wrapper': {
          'background-image': `linear-gradient(0deg, ${primary_color} 45%,
          rgb(185,146,146) 145%)`
        },
        '.meeting-room-wrapper:before': {
          'background-image': `linear-gradient(0deg, ${primary_color} 45%,
          rgb(185,146,146) 145%)`
        },
        '.sidebar-mobile-background:before':{
          'background-image':`linear-gradient(0deg, ${primary_color}e6 45%, ${primary_color}99 145%)`
        },
        '.edit-btn:hover':{
          'background-color' : `${primary_color}`
        },
        '.stroke-primary': {
          'stroke': `${primary_color}`
        },
        '.text-color-event-history': {
          'color': `${primary_color}`
        },
        '.daterangepicker .drp-calendar .calendar-table table thead': {
          color: primary_color
        },
        '.daterangepicker .drp-calendar .calendar-table thead th.month': {
          color: primary_color
        },
        '.daterangepicker .drp-calendar .calendar-table table tbody td.active': {
          'background-color': primary_color
        },
        '.slider':{
          'background-color': `${primary_color}1a`
        },
        'input:checked + .slider': {
          'background-color': `${primary_color}cc`
        },
        '.hotel-list-add-image': {
          'background-color':  `${primary_color}`
        },
        '.border-bottom-light-color': {
          'border-bottom': `1px solid ${primary_color}26`
        },
        '.chat-container.chat-background .rcw-widget-container .rcw-conversation-container .rcw-messages-container .rcw-message .rcw-client':{
          'background-color':  `${primary_color}1A !important`
        },
        '.chat-input-background':{
          'background-color':  `${primary_color}1A`
        },
        '.modal-content': {
          color: primary_color
        },
        '.modal-content .modal-body .modal-action-btns button': {
          'border-color': primary_color
        },
        '.modal-content .modal-body .modal-action-btns .change': {
          'background-color': primary_color
        },
        '.site-footer':{
          'background-image': `linear-gradient(0deg, ${primary_color} 45%, rgb(185,146,146) 145%)`
        }
  }

    let styles_entries = [];
    for (let selector in styles) {
      if (styles.hasOwnProperty(selector)) {
        let style = styles[selector];
        let s_attrs = [];
        for(let attr in style) {
          if (style.hasOwnProperty(attr)) {
            s_attrs.push(`${attr}: ${style[attr]};`);
          }
        }
        const s_attrs_txt = s_attrs.join("\n");
        styles_entries.push(`${selector} {\n${s_attrs_txt}\n}`);
      }
    }

    let $css = $('style#event-list');
    if ( ! $css.length) {
      $css = $("<style id='event-list'/>");
    }
    $css.text(styles_entries.join("\n"));
    $css.appendTo('head');
    var GaTag = $('script#ga-backend-page');
    var GaContentTag = $('script#ga-backend-page-content');
    if ((gaTracking !==null) && ((! GaTag.length) && (! GaContentTag.length)) && (!isHotelPage)) {
          var content =`window.dataLayer = window.dataLayer || [];
                        function gtag() { dataLayer.push(arguments); }
                        gtag('js', new Date());
                        gtag('config', '${gaTracking}');`
          var GaBackendPage = document.createElement("script");
          GaBackendPage.src = `https://www.googletagmanager.com/gtag/js?id=${gaTracking}`;
          GaBackendPage.async = true;
          GaBackendPage.id = "ga-backend-page";
          var GaBackendPageContent = document.createElement("script");
          GaBackendPageContent.id = "ga-backend-page-content";
          GaBackendPageContent.text = content;
          $("body").append(GaBackendPage);
          $("body").append(GaBackendPageContent);
    }
  
  }

  pageHeader(displayNotification = null, notificationMsg = [], property = null, type) {
    const { settingsContainer, showSidebar, isMobileSidebar } = this.state;
    const userInfo = AuthStore.getAll();
    this.loadCommonCSS(property);
    const { data } = userInfo.user;
    const { first_name, last_name, profile, username } = data;
    const fullName = `${first_name} ${last_name}`;
    const firstInitial =  first_name ? first_name[0] : '';
    const lastInitial = last_name ? last_name[0] : '';
    let userInitialName = `${firstInitial}${lastInitial}`;
    const usernameInitial = `${username[0]}${username[1]}`;
    userInitialName = userInitialName || usernameInitial;
    userInitialName = userInitialName.toUpperCase();
    
    let userRole = null;
    if (profile.role == 'Admin') {
       userRole = 'SA';
    } else {
       const firstInitialRole = profile.role[0];
       const lastInitialRole = profile.role.split(' ')[1] ? profile.role.split(' ')[1].charAt(0) : '';
       userRole = `${firstInitialRole}${lastInitialRole}`;
       userRole = userRole.toUpperCase();
    }

    const header = (
      <div>
        <Row className="header">
            <Col lg={7} md={7} sm={6} xs={10}>
                <div className="fleft padding10 i-block" onClick = {this.openSidebar}> 
                 <img src="dist/img/menu-icon.svg" className="header-margin-top menu-img pointer" alt="Menu"/>
                </div>
                <div className="fleft tmargin5">
                  <img src="dist/img/event_list/logo-blue.png" className="header-margin-top logo-blue" width="120px" alt="Hotel Logo"/>
                </div>
            </Col>
            { displayNotification && notificationMsg.length >= 1 && (notificationMsg.map((msg, i) => (
              <Alert key={`notification-msg${i}`} style={{ top: `${(i * 50) + 10}px` }} className={`alert-msg-${i} ${msg.includes('Please upload') ? 'alert-msg-upload-documents' : ''}`}>
                {msg}
                <span
                  className={`fa fa-times lmargin20 close-alert pointer ${type === 'update' ? 'hide' : ''}`}
                  onClick={() => this.closeAlert(msg)}
                />
              </Alert>
              )))
            }
            <Col lg={5} md={5} sm={6} xs={2}>
              <div className="fright">
              <div className="hidden-xs align-header-content fleft text-font16 i-block">
                <span className="header-text-color-1">{fullName}</span>
                <span className="user-image">{userInitialName}</span>
                <span className="header-text-color-2"><strong>{userRole}</strong></span>
              </div>
              <div
                className="settigs-icon lmargin50 margin-settings-icon inline pointer"
                onClick={this.handleSettings}
              >
                <img className="tmargin10" src="dist/img/event_list/settings.png" alt="settings icon" width="30px" />
              </div>
              </div>
            </Col>
          
        </Row>
        {
          settingsContainer && (
            <div
              className="settings-container"
              onClick={this.logOut}
            >
              Logout
            </div>
          )
        } 
        
      </div>
    );
    return header;
  }

  mobileSideBar(property = null, unReadNotificat=null){
    const { setSelectedColor } = this.state;

    const { showSidebar } = this.props;
    const HotelBrandInfo = HotelBrandStore.getAll();
    // const { selectedPage }  = HotelBrandInfo;
    let selectedPage = 'events';
    const currentPage = window.location.href;

    const userInfo = AuthStore.getAll();
    const { data } = userInfo.user;
    const role = data.profile.role;

    let sideBarInfo = [];

    //console.log(role);
      switch (role) {
          case 'Hotel Manager':
              //Sentencias ejecutadas cuando el resultado de expresion coincide con valor1
               sideBarInfo = [
                  {
                      id : 'events',
                      name : 'Events',
                      name_expand : 'Events',
                      src : 'dist/img/event_list/events.svg',
                      alt : 'events icon',
                      link : 'event/list',
                  },
                  {
                      id : 'notifications',
                      name : 'Notificat.',
                      name_expand : 'Notifications',
                      src : 'dist/img/event_list/notification-bell.svg',
                      alt : 'notifications icon',
                      link : 'notifications',
                  },
                  {
                      id : 'calendar',
                      name : 'Calendar',
                      name_expand : 'Calendar',
                      src : 'dist/img/event_list/calendar.svg',
                      alt : 'calendar icon',
                      link : '',
                  },
              ];
              break;

          default:
              //Sentencias_def ejecutadas cuando no ocurre una coincidencia con los anteriores casos
              console.log('Other profiles');

               sideBarInfo = [
                  {
                      id : 'events',
                      name : 'Events',
                      name_expand : 'Events',
                      src : 'dist/img/event_list/events.svg',
                      alt : 'events icon',
                      link : 'event/list',
                  },
                  {
                      id : 'notifications',
                      name : 'Notificat.',
                      name_expand : 'Notifications',
                      src : 'dist/img/event_list/notification-bell.svg',
                      alt : 'notifications icon',
                      link : 'notifications',
                  },
                  {
                      id : 'calendar',
                      name : 'Calendar',
                      name_expand : 'Calendar',
                      src : 'dist/img/event_list/calendar.svg',
                      alt : 'calendar icon',
                      link : '',
                  },
                  {
                      id : 'hotel',
                      name : 'Hotels',
                      name_expand : 'Hotels',
                      src : 'dist/img/event_list/hotels.svg',
                      alt : 'hotels icon',
                      link : 'hotel/list',
                  },
                  {
                      id : 'brand',
                      name : 'Brand',
                      name_expand : 'Brand',
                      src : 'dist/img/event_list/chain.svg',
                      alt : 'brand icon',
                      link : 'brand/list',
                  },
                  {
                      id : 'users',
                      name : 'Users',
                      name_expand : 'Users',
                      src : 'dist/img/event_list/users.svg',
                      alt : 'users icon',
                      link : 'users/list',
                  },
              ];
              break;
      }

    const selectedSidebar = sideBarInfo.filter(sidebar => currentPage.includes(sidebar.id));
    if (selectedSidebar.length >= 1) {
      selectedPage = selectedSidebar[0].id;
    }
    this.loadCommonCSS(property);

    const NotificationInfo = NotificationStore.getAll();
    const { isApiCalled } = NotificationInfo;
    let { unReadNotificationCount } = NotificationInfo;

    if (!isApiCalled) {
      NotificationAction.getNotificationInfos(data.id, (res)=>{
        const NotificationInfo = NotificationStore.getAll();
        let { unReadNotificationCount } = NotificationInfo;
        if ($('span#un-read-notification')[0] && unReadNotificationCount) {
          $('span#un-read-notification')[0].innerHTML = unReadNotificationCount ;
        }
      });
    }

    const mobileSideBar = (
        <Row className="sidebar-screen-background layout-sidebar visible-xs" onClick={this.openSidebar}> 
          <Row className="content-left-mobile visible-xs">
              <Col className="sidebar-mobile-background lpadding0" sm={3} xs={7}>
               <Row className="primary-border-color-sidebar-header sidebar-header-shadow">
                <div className="fleft tmargin10"> 
                 <img src="dist/img/menu-icon-mobile.svg" className="header-margin-top" alt="Menu"/>
                </div>
                <div className="fleft lmargin20 tmargin10">
                  <img className="lmargin10" src="dist/img/white-logo.png" alt="White Logo" width="80px" alt="Hotel Logo" />
                </div>
               </Row>
               {sideBarInfo.map(eachSideBar => (
               <Row className={`sidebar-headings ${selectedPage === eachSideBar.id ? 'navigation-sidebar-selected' : ''} ${eachSideBar.id === 'calendar' ? 'sidebar-disabled-background' : 'pointer'}`} > 
                 <Link to={`/property/${eachSideBar.link}`}>
                   <div className="fleft lmargin10 tmargin15"> 
                   <ReactSVG 
                      wrapper="span" 
                      className="svg-wrapper" 
                      src={eachSideBar.src}
                      alt={eachSideBar.alt}
                    />
                    {eachSideBar.id === 'notifications' && 
                       <span  
                       className={`primary-background-color notification-label ${unReadNotificationCount >= 1 ? '' : 'hide'}`}
                       id="un-read-notification">
                       {unReadNotificat ? unReadNotificat : ( unReadNotificationCount >= 1 ? unReadNotificationCount : '')}
                       </span>}
                   </div>
                   <div className="fleft lmargin20 tmargin20">
                     <p className={`${eachSideBar.id === 'calendar' ? 'disable-sidebar-option-mobile' : 'color-sidebar-mobile'}`}>{eachSideBar.name_expand}</p>
                   </div>
                     </Link>
               </Row>
               ))}

               </Col>
              <Col className="sidebar-transparent" sm={9} xs={5}>
              </Col>
            </Row>
        </Row>
    );
    return mobileSideBar;
  }

  sideBar(property = null, isHotelPage = false, unReadNotificat=null) {
    const { showSidebar } = this.state;
    // if (!isHotelPage) {
    //   $( "script#ga-hotel-page" ).remove();
    //   $( "script#ga-hotel-page-content" ).remove();
    // }
    const HotelBrandInfo = HotelBrandStore.getAll();
    const { getUserHotel } = HotelBrandInfo;
    // const { selectedPage }  = HotelBrandInfo;
    let selectedPage = 'events';
    const currentPage = window.location.href;

      const userInfo = AuthStore.getAll();
      const { data } = userInfo.user;
      const role = data.profile.role;
      console.log("ROLE",role);

      let sideBarInfo = [];

      switch (role) {
          case 'Hotel Manager':
              console.log('PASAMOS POR AQUI');
              //Sentencias ejecutadas cuando el resultado de expresion coincide con valor1

               sideBarInfo = [
                  {
                      id : 'events',
                      name : 'Events',
                      name_expand : 'Events',
                      src : 'dist/img/event_list/events.svg',
                      alt : 'events icon',
                      link : 'event/list',
                  },
                  {
                      id : 'notifications',
                      name : 'Notificat.',
                      name_expand : 'Notifications',
                      src : 'dist/img/event_list/notification-bell.svg',
                      alt : 'notifications icon',
                      link : 'notifications',
                  },
                  {
                      id : 'calendar',
                      name : 'Calendar',
                      name_expand : 'Calendar',
                      src : 'dist/img/event_list/calendar.svg',
                      alt : 'calendar icon',
                      link : '',
                  },
              ];
              break;

          default:
              //Sentencias_def ejecutadas cuando no ocurre una coincidencia con los anteriores casos
               sideBarInfo = [
                  {
                      id : 'events',
                      name : 'Events',
                      name_expand : 'Events',
                      src : 'dist/img/event_list/events.svg',
                      alt : 'events icon',
                      link : 'event/list',
                  },
                  {
                      id : 'notifications',
                      name : 'Notificat.',
                      name_expand : 'Notifications',
                      src : 'dist/img/event_list/notification-bell.svg',
                      alt : 'notifications icon',
                      link : 'notifications',
                  },
                  {
                      id : 'calendar',
                      name : 'Calendar',
                      name_expand : 'Calendar',
                      src : 'dist/img/event_list/calendar.svg',
                      alt : 'calendar icon',
                      link : '',
                  },
                  {
                      id : 'hotel',
                      name : 'Hotels',
                      name_expand : 'Hotels',
                      src : 'dist/img/event_list/hotels.svg',
                      alt : 'hotels icon',
                      link : 'hotel/list',
                  },
                  {
                      id : 'brand',
                      name : 'Brand',
                      name_expand : 'Brand',
                      src : 'dist/img/event_list/chain.svg',
                      alt : 'brand icon',
                      link : 'brand/list',
                  },
                  {
                      id : 'users',
                      name : 'Users',
                      name_expand : 'Users',
                      src : 'dist/img/event_list/users.svg',
                      alt : 'users icon',
                      link : 'users/list',
                  },
              ];
              console.log('Other profiles');
              break;
      }

      //console.log('ROLE!');
      //console.log(role);

    const selectedSidebar = sideBarInfo.filter(sidebar => currentPage.includes(sidebar.id));
    if (selectedSidebar.length >= 1) {
      selectedPage = selectedSidebar[0].id;
    }

    const gaPage = $("script#ga-hotel-page");
    const gaPageContent = $("script#ga-hotel-page-content");
    this.loadCommonCSS(property, isHotelPage);
    if (!isHotelPage) {
      $('link#font-link').remove();
      const backOfficeFontLink = $('link#font-link-backoffice');
      if (!backOfficeFontLink.length) {
        const mainTypography = 'Montserrat';
        const linkCss = `https://fonts.googleapis.com/css?family=${mainTypography}:400,700`;
        const $css = $(`<link id="font-link-backoffice" href=${linkCss} rel='stylesheet'/>`);
        $css.appendTo('head');
        $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
        $(`<style>.daterangepicker .drp-calendar {font-family: ${mainTypography}}</style>`).appendTo('head');
      }
      $( "script#ga-hotel-page" ).remove();
      $( "script#ga-hotel-page-content" ).remove();
      if (!getUserHotel) {
        HotelBrandAction.getGaTracking(data.hotels, (res) =>{
          if (res.results.length >= 1) {
            const gaTracking = res.results[0].ga_tracking;
            if (gaTracking !== null) {
              this.loadCommonCSS(property, isHotelPage, gaTracking);
            }
          }
        });
      }
    }

    const NotificationInfo = NotificationStore.getAll();
    const { isApiCalled } = NotificationInfo;
    let { unReadNotificationCount } = NotificationInfo;

    if (!isApiCalled) {
      NotificationAction.getNotificationInfos(data.id, (res)=>{
        const NotificationInfo = NotificationStore.getAll();
        let { unReadNotificationCount } = NotificationInfo;
        if ($('span#un-read-notification')[0] && unReadNotificationCount) {
          $('span#un-read-notification')[0].innerHTML = unReadNotificationCount ;
        }
      });
    }
    const sideBar = (
      <Row className="content-left hidden-xs">
        <Row className="sidebar sidebar-background-color">
        {sideBarInfo.map(eachSideBar => (
          <div id={`${eachSideBar.id}`} className={`navigation ${selectedPage === eachSideBar.id ? 'navigation-sidebar-selected' : ''} ${eachSideBar.id === 'calendar' ? 'sidebar-disabled-background' : 'pointer'} ${eachSideBar.id === 'notifications' ? 'align-notifications-icon' : ''}`} >
            <Link to={`/property/${eachSideBar.link}`}>
            <ReactSVG 
              wrapper="span" 
              className="svg-wrapper sidebar-icon" 
              src={eachSideBar.src}
              alt={eachSideBar.alt}
              width="40px"
            />
            {eachSideBar.id === 'notifications' && 
             <span 
             className={`primary-background-color notification-label ${unReadNotificationCount >= 1 ? '' : 'hide'}`} 
             id="un-read-notification">
             {unReadNotificat ? unReadNotificat : ( unReadNotificationCount >= 1 ? unReadNotificationCount : 's')}
             </span>}
            <p className={`${eachSideBar.id === 'calendar' ? 'disable-sidebar-option' : 'color-sidebar sidebar-label'}`}>{eachSideBar.name}</p>
            </Link>
          </div>
        ))}
        </Row>
      </Row>
    );
    return sideBar;
  }
}

export default CommonLayout;

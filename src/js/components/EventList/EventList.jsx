import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import RFPManagementAction from '../../actions/RFPManagementAction';
import RFPManagementStore from '../../stores/RFPManagementStore';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import PaginationContainer from '../Reusables/PaginationContainer';
import CommonLayout from '../CommonLayout/CommonLayout';
import '../../../css/HomePage/HomePage.scss';
import EventHistoryPopup from '../EventList/EventHistoryPopup';
import AuthStore from '../../stores/AuthStore';

class EventList extends CommonLayout {
  constructor(props) {
    super(props);
    this.state = {
      selectedVal: 'sent',
      selectedVal2:'approved',
      selectedVal3:'closed',
      fromDate: '--/--/--',
      toDate: '--/--/--',
      eventTypeId: null,
      hotelEventInfo: RFPManagementStore.getAll(),
      attendeesFrom: null,
      attendeesTo: null,
      sortByName: '',
      sortByStart: '',
      sortByLastActivity: '',
      sortByStatus: '',
      showEventHistoryPopup:false,
      showEventHistoryPopupId: null,
      sortBy:'',
      sortType:'',
      showFilters : true,
      filterOptionText : 'Less filters',
      isLoggedIn: false,
      setLoader: false,
      showEventDetailsDropdown: false,
      event: 'active',
      isClickedEventType: false,
    };
    this.handleDropdown = this.handleDropdown.bind(this);
    this.handleEventDetailDropdown = this.handleEventDetailDropdown.bind(this);
    this.handleDateEvent = this.handleDateEvent.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleEventTypes = this.handleEventTypes.bind(this);
    this.handleSortingDropdown = this.handleSortingDropdown.bind(this);
    this.handleAttendees = this.handleAttendees.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSettings = this.handleSettings.bind(this);
    this.onRFPManagementStoreChange = this.onRFPManagementStoreChange.bind(this);
    this.showHistoryPopup = this.showHistoryPopup.bind(this);
    this.handleHistoryClose = this.handleHistoryClose.bind(this);
    this.changeState = this.changeState.bind(this);
    this.handleEvents = this.handleEvents.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
  }

  componentWillMount() {
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    RFPManagementAction.clearStoreInfo();
    const { hotelEventInfo, event } = this.state;
    const { selectedPage } = hotelEventInfo;
    RFPManagementAction.paginationClick(1);
    this.setState({ setLoader: true });
    RFPManagementAction.getUserEventlist(event, (res) => {
      if (res) {
        this.setState({ setLoader : false });
      }
    });
    RFPManagementAction.getUserEventTypes();
    AuthStore.addStoreListener(this.onAuthStoreChange);
    RFPManagementStore.addStoreListener(this.onRFPManagementStoreChange);
    window.scrollTo(0, 0);
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  onRFPManagementStoreChange(action) {
    console.log("onRFPManagementStoreChange", arguments);
    const hotelEventInfo = RFPManagementStore.getAll();
    this.setState({ hotelEventInfo: hotelEventInfo});
  }

  handleDropdown(status, currentEvent, e) {
    e.preventDefault();
    const {hotelEventInfo} = this.state
    hotelEventInfo.eventInfo.results.map((event)=> {
      if(event.id === currentEvent.id){
        event.status = status;
      }
    });
    RFPManagementAction.updateEvent(currentEvent,(response) => {
      const orderedHistoryInfo = response[0].results.sort(function(a, b) {
        var keyA = new Date(a.date),
            keyB = new Date(b.date);
        // Compare the 2 dates
        if(keyA < keyB) return 1;
        if(keyA > keyB) return -1;
        return 0;
      });
      if (orderedHistoryInfo.length>=1) {
        hotelEventInfo.allEventHistoryInfo.map((EventHistoryInfo)=>{
          if (EventHistoryInfo.id === currentEvent.id) {
            EventHistoryInfo['orderedEventHistory']=orderedHistoryInfo;
          }
        });
        this.setState({ hotelEventInfo });
      }
      if (Object.keys(response[1]).length>=1) {
        hotelEventInfo.eventInfo.results.map((result,i)=>{
          if (result.id === currentEvent.id) {
            result.last_activity=response[1].last_activity;
          }
        });
        this.setState({ hotelEventInfo });
      }
    });

  } 

  handleDateEvent(e, picker, type){
    this.setState({date: picker})
    let { startDate } = picker;
    startDate = moment(startDate).format('DD/MM/YYYY');
    if (type === 'start') {
      this.setState({ fromDate: startDate });
    } else {
      this.setState({ toDate: startDate });
    }
    const {fromDate, toDate, eventTypeId, hotelEventInfo, event} = this.state
    const { selectedPage } = hotelEventInfo;
    if (moment(fromDate, "DD/MM/YYYY", true).isValid() && moment(toDate, "DD/MM/YYYY", true).isValid())
    {
      RFPManagementAction.paginationClick(1);
      RFPManagementAction.notificationMsg('Loading...', 'add');
      RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), '', '', 1, event, (res) => {
        if (res) {
          RFPManagementAction.notificationMsg('Loading...', 'remove');
        }
      });
    }
    
  }
  handleSelectedEvent(result){
    // const { router } = this.context;
    // router.history.push(`/property/event/view/${result.uuid}`);
    RFPManagementAction.getSelectedEventId(result.id, result.uuid);
  }

  handleSearch(e) {
    RFPManagementAction.search(e.target.value);
  }

  handleSortingDropdown(e){
    const sortingType = e.target.value;
    this.handleEventSorting(sortingType);
  }
  
  handleEventTypes(e){
    const typeId = e.target.value;

    this.setState({ eventTypeId: typeId === 'all' ? null : typeId});

    const {fromDate, toDate, hotelEventInfo, event} = this.state
    const { selectedPage } = hotelEventInfo;
    RFPManagementAction.paginationClick(1);
    RFPManagementAction.notificationMsg('Loading...', 'add');
    RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, typeId === 'all' ? null : typeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), '', '', 1, event, (res) => {
      if (res) {
        RFPManagementAction.notificationMsg('Loading...', 'remove');
      }
    });
  }

  handleEventSorting(type){
    const {fromDate, toDate, eventTypeId, event, attendeesFrom, attendeesTo, hotelEventInfo} = this.state
    const { selectedPage } = hotelEventInfo;
    let {sortByName, sortByStart, sortByLastActivity, sortByStatus} = this.state
    let sortBy;
    if(type === 'name'){
      sortBy = sortByName === ''? true : !sortByName;
      this.setState({sortByName: sortBy})
    }
    if(type === 'start'){
      sortBy = sortByStart === ''? true : !sortByStart;
      this.setState({sortByStart: sortBy})
    }
    if(type === 'last_activity'){
      sortBy = sortByLastActivity === ''? true : !sortByLastActivity;
      this.setState({sortByLastActivity: sortBy})
    }
    if(type === 'status'){
      sortBy = sortByStatus === ''? true : !sortByStatus;
      this.setState({sortByStatus: sortBy})
    }
    this.setState({sortBy : sortBy ? '+' : '-', sortType: type});
    
    if ((moment(fromDate, "DD/MM/YYYY", true).isValid() && moment(toDate, "DD/MM/YYYY", true).isValid()) || eventTypeId || attendeesFrom || attendeesTo) {
      RFPManagementAction.paginationClick(selectedPage);
      RFPManagementAction.notificationMsg('Loading...', 'add');
      RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), type, sortBy ? '+' : '-', selectedPage, event, (res) => {
        if (res) {
          RFPManagementAction.notificationMsg('Loading...', 'remove');
        }
      });
    }else{
      RFPManagementAction.notificationMsg('Loading...', 'add');
      RFPManagementAction.getSortedEventList(type, sortBy ? '+' : '-', event, selectedPage, (res) => {
        if (res) {
          RFPManagementAction.notificationMsg('Loading...', 'remove');
        }
      });
    }

  }

  handleAttendees(type,e){
    const {value} = e.target
    if (type === 'attendeesFrom') {
      this.setState({ attendeesFrom: parseInt(value, 10) });
    } else {
      this.setState({ attendeesTo: parseInt(value, 10) });
    }
    const {fromDate, toDate, eventTypeId, hotelEventInfo, event} = this.state
    const { selectedPage } = hotelEventInfo;
    clearTimeout(this.timer);
    if(!isNaN((parseInt(value))) || value === ''){
      this.timer = setTimeout(() => {
        RFPManagementAction.paginationClick(1);
        RFPManagementAction.notificationMsg('Loading...', 'add');
        RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), '', '', 1, event, (res) => {
          if (res) {
            RFPManagementAction.notificationMsg('Loading...', 'remove');
          }
        });
      }, 500);
    }
  }

  handlePageChange(selectedPage) {
    const {sortBy, sortType, event, fromDate, toDate, eventTypeId} = this.state;
    RFPManagementAction.paginationClick(selectedPage);
    RFPManagementAction.notificationMsg('Loading...', 'add');
    RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), sortType, sortBy, selectedPage, event, (res) => {
      if (res) {
        RFPManagementAction.notificationMsg('Loading...', 'remove');
      }
    });
  }

  resetFilter(filterType) {
    const {sortBy, sortType, event, fromDate, toDate, eventTypeId, hotelEventInfo} = this.state;
    const { selectedPage } = hotelEventInfo;
    let currentFromDate = fromDate;
    let currentToDate = toDate;
    let currentAttendeesFrom = $('.attendeesFrom').val();
    let currentAttendeesTo = $('.attendeesTo').val();
    if (filterType === "dateFilter") {
      currentFromDate = '--/--/--';
      currentToDate = '--/--/--';
      this.setState({fromDate: currentFromDate, toDate: currentToDate})
    } else {
      currentAttendeesFrom = '';
      currentAttendeesTo = '';
      this.setState({ attendeesFrom: null, attendeesTo: null, });
      $('.attendeesFrom').val('');
      $('.attendeesTo').val('');
    }
    RFPManagementAction.getUserEventlistBasedOnFilter(currentFromDate, currentToDate, eventTypeId, currentAttendeesFrom, currentAttendeesTo, sortType, sortBy, selectedPage, event, false);
  }

  showHistoryPopup(bool, eventId, e){
    e.preventDefault();
    this.setState({showEventHistoryPopup:true, showEventHistoryPopupId: eventId});
  }

  handleHistoryClose(){
    this.setState({showEventHistoryPopup:false});
  }

  changeState(){
    this.setState( prevState=> ({
       showFilters: !prevState.showFilters,
    }));

    const {showFilters} = this.state;
    if (!showFilters){
      this.setState({filterOptionText : 'Less filters'});
    }else{
      this.setState({filterOptionText : 'More filters'});
    }
  }

  handleEventDetailDropdown(e, id){ 
    e.preventDefault();
    const {hotelEventInfo} = this.state;
    const selectedEvent = hotelEventInfo.eventInfo.results.map((item) => {
      const selectedItem = item;
      if (item.id !== id) {
        selectedItem['showEventDetailsDropdown'] = false;
        return selectedItem;
      }
      selectedItem['showEventDetailsDropdown'] = !selectedItem['showEventDetailsDropdown'];
      return selectedItem;
    });
    this.setState({ hotelEventInfo });
  }

  handleEvents(eventType){
    this.setState({ event:eventType });
    const { hotelEventInfo, fromDate, toDate, eventTypeId, type, sortBy, sortType} = this.state;
    const { selectedPage } = hotelEventInfo;
    RFPManagementAction.paginationClick(1);
    this.setState({ setLoader : true, isClickedEventType: true });
    RFPManagementAction.getUserEventlistBasedOnFilter(fromDate, toDate, eventTypeId, $('.attendeesFrom').val(), $('.attendeesTo').val(), sortType, sortBy, 1, eventType, (res) => {
      if (res) {
        this.setState({ setLoader : false, isClickedEventType: false });
      }
    });
  }

  render() {
    const {
      selectedVal, selectedVal2, selectedVal3, date, fromDate, toDate, hotelEventInfo, attendeesFrom, attendeesTo,
      sortByName, sortByStart, sortByLastActivity, sortByStatus, showEventHistoryPopup, showEventHistoryPopupId,

      showFilters, showEventDetailsDropdown, filterOptionText, isRedirect, isLoggedIn, isMobileSidebar, event, setLoader, 
      isClickedEventType
    } = this.state;
    const {eventInfo, eventStatus, eventTypes, searchVal, allEventHistoryInfo, activeCount, pastCount, notificationMsg } = hotelEventInfo;
    const isEvents = typeof(eventInfo) !== 'undefined' && Object.keys(eventInfo).length !== 0 && eventInfo.results.length >= 1;
    const isNoEvents = typeof(eventInfo) === 'undefined' || Object.keys(eventInfo).length === 0 || eventInfo.results.length < 1;

    const updatedEventStatus = (name) => {
      if (name) {
        const selectedStatus = eventStatus.results.filter(status => status.name.toLowerCase() === name.toLowerCase());
        return selectedStatus[0].event_status.toUpperCase();
      }
      return '';
    }

    const recentHistoryInfo = (history) => {
      let message;
      const dateFields = ['start', 'end'];
      if ((Object.keys(history.changes).length >= 1) && (Object.keys(history.changes)[0] === 'created' || Object.keys(history.changes)[0] === 'updated')) {
        if (Object.keys(history.changes)[0] === 'created') {
          message = `${history.user.first_name} ${history.user.last_name} created an event ${history.changes.created.hasOwnProperty('name') ?  ': '+history.changes.created.name : ''}`
          return message;
        }
        if (Object.keys(history.changes)[0] === 'updated' && Object.keys(history.changes.updated).length >= 1) {
          if (dateFields.includes(Object.keys(history.changes.updated)[0])) {
            message = `${history.user.first_name} ${history.user.last_name} updated ${Object.keys(history.changes.updated)[0]} from ${moment(Object.values(history.changes.updated)[0]['previous'], 'YYYY-MM-DD').format('YYYY-MM-DD')} to ${moment(Object.values(history.changes.updated)[0]['current'], 'YYYY-MM-DD').format('YYYY-MM-DD')}`
          } else {
            message = `${history.user.first_name} ${history.user.last_name} updated ${Object.keys(history.changes.updated)[0]} from ${Object.values(history.changes.updated)[0]['previous']} to ${Object.values(history.changes.updated)[0]['current']}`
          }
          return message;
        } 
        return false;
      } else {
        message = `${history.user.first_name} ${history.user.last_name} ${Object.keys(Object.values(history.changes)[0])[0]} the ${Object.keys(history.changes)[0].replace('_', ' ')}`
        return message;
      }
    }

    const isPagingation = eventInfo && eventInfo.count >= 1;

    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <Grid className="primary-color home-container event-list-container layout-header">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
       <Row className="content-right">
        <Row>
          {this.pageHeader(true, notificationMsg)}
          <Row>
            <Button
              className="back-button primary-color"
              onClick={this.context.router.history.goBack}
            >
              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/back-arrow.svg" width="25px"/>
              &nbsp;&nbsp;Back
            </Button>
          </Row>
          <Row>
            <div 
                className="text-search header-search primary-color primary-border-color rmargin10 tmargin20 visible-xs">
                <form className="search primary-color">
                  <input
                    className="search primary-color find-text"
                    type="text"
                    placeholder="Find event"
                    name=""
                    onChange={this.handleSearch}
                  />
                  <span className="image-search">
                    <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" alt="search icon" width="25px"/>
                  </span>
                </form>
            </div>
          </Row>
          <Row className="row-title">
            <div className="tmargin20">
              <Col className="primary-color">
                 <div className="fleft i-block">
                  <span className="list-title text-font-22"><strong>Events list</strong></span>
                 </div>
                 <div className="fright i-block pointer"
                   onClick = {() => { this.changeState()} }>
                   <ReactSVG wrapper="span" 
                   className={`svg-wrapper fright ${showFilters ? 'arrow-class' : 'arrow-toggle'} visible-xs`}
                   src="dist/img/meeting_space/icons/arrow-icon.svg"
                   alt="arrow up" 
                   width="10px"
                   />
                   <span className="fright less-filter visible-xs">{filterOptionText}</span>
                 </div>
              </Col>
              <Col className="text-search header-search primary-color primary-border-color hidden-xs">
                  <input
                    className="search primary-color find-text"
                    type="text"
                    placeholder="Find event"
                    name=""
                    value={searchVal}
                    onChange={this.handleSearch}
                  />
                  <span className="image-search">
                    <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" alt="search icon" width="25px"/>
                  </span>
              </Col>
           </div>
          </Row>
        </Row>
          <Row>
            <table className={`table desktop-view ${showFilters ? 'show' : 'hide'}`}>
                <tr className="options">
                  <td className="table-column-1 col-md-4">
                    <Col className="div">
                      <Col className="align-middle" lg={8} md={9} sm={10} xs={9}>
                        <div className="date-from content-1 primary-color">
                          <div className="date-heading"> 
                            <p className="date-field primary-color">Dates from&nbsp;&nbsp;</p>
                          </div>
                          <div className="date">
                            <DateRangePicker
                                singleDatePicker
                                containerStyles={{ backgroundColor: '#ffffff' }}
                                // minDate={moment()}
                                onApply={(e, picker) => this.handleDateEvent(
                                  e, picker, 'start'
                                )}
                            >
                            <Button className="pick-date primary-color">{fromDate}</Button>
                            </DateRangePicker>
                          </div>
                        </div>
                        <div className="date-to primary-color">
                            <div className="date-heading"> 
                              <p className="primary-color">to&nbsp;&nbsp;</p>
                            </div>
                           <div className="date">
                             <DateRangePicker
                               singleDatePicker
                               containerStyles={{ backgroundColor: '#ffffff' }}
                               // minDate={moment()}
                               onApply={(e, picker) => this.handleDateEvent(
                                e, picker, 'end'
                               )}
                             >
                             <Button className="pick-date primary-color"> {toDate} </Button>
                             </DateRangePicker>
                           </div>
                        </div>
                      </Col>
                      <Col className="i-block align-reset-button lpadding0 primary-color" lg={4} md={3} sm={2} xs={3}>
                          <Button 
                          className="view reset-button primary-color primary-border-color" 
                          width="22px"
                          onClick={()=>{this.resetFilter('dateFilter')}}
                          >
                          Reset Dates</Button>
                      </Col>
                    </Col>
                  </td>
                  <td className="table-column-2 col-md-4">
                    <Col className="div">
                       <Col className="align-middle" lg={9} md={9} sm={10} xs={9}>
                        <div className="date-from content-1 primary-color">
                          <div className="date-heading attendees-frm"> 
                            <p className="primary-color"># of attendees from&nbsp;&nbsp;</p>
                          </div>
                          <div className="date">
                            <form>
                             <input 
                             type="text" 
                             className="enter-text primary-color attendeesFrom" 
                             placeholder="0000" 
                             size="6"
                             onChange={(e)=>{this.handleAttendees('attendeesFrom',e)}}
                             value={attendeesFrom >= 0? attendeesFrom : ''}
                             />
                            </form>
                          </div>
                        </div>
                        <div className="date-to primary-color">
                           <div className="date-heading"> 
                            <p className="primary-color">to&nbsp;&nbsp;</p>
                          </div>
                          <div className="date">
                            <form>
                             <input 
                             type="text" 
                             className="enter-text attendeesTo" 
                             placeholder="0000" 
                             size="6"
                             onChange={(e)=>{this.handleAttendees('attendeesTo',e)}}
                             value={attendeesTo >= 0? attendeesTo : ''}
                             />
                            </form>
                          </div>
                        </div>
                      </Col>
                      <Col className="i-block align-reset-button reset-count lpadding0 primary-color" lg={3} md={3} sm={2} xs={3}>
                          <Button 
                          className="view reset-button primary-color primary-border-color" 
                          width="22px"
                          onClick={()=>{this.resetFilter('attendeesFilter')}}
                          >
                          Reset</Button>
                      </Col>
                    </Col>
                  </td>
                  <td className="table-column-4 col-md-4">
                     <div className="div">
                       <div className="align-event-dropdown">

                        <div className="date-from content-1 primary-color">
                          <div className="date-heading event-type"> 
                           <p className="primary-color">Event type</p>
                          </div>
                        </div>
                        <div className="date-to primary-color">
                           <form className="primary-color">
                              <select
                                className="select-event align-right "
                                style={{'overflow':'hidden','white-space':'normal'}}
                                onChange={(e) => {this.handleEventTypes(e)}}
                              >
                                <option key="eventType" value="all" >All</option>

                                {
                                  Object.keys(eventTypes).length !== 0 && eventTypes.results.map(type => (
                                    <option
                                      value={type.id}
                                    >
                                      {type.name}
                                    </option>
                                  ))
                                }
                              </select>
                           </form>
                        </div>
                      </div>
                      </div>
                  </td>
                </tr>
              </table>
          </Row>
          <Row id="content">
            <div className="add-event add-device-specific primary-color visible-xs visible-sm" id="create-event">
              <Link to="/property/event/create">
                <p className="i-block">Create new event <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px" /></p>
              </Link>
            </div> 
            <div 
              className={`content-table primary-color ${event === 'active' ? event : ''}`}
              style={{'cursor': 'pointer'}}
              id="active-events"
              onClick={()=> {event === 'active' ? '' : this.handleEvents('active')}}
              >
             <div className="text-active-events">
              <p className="text-select"><strong>Active Events</strong>&nbsp;&nbsp;({activeCount})</p>
             </div>
            </div>
            <div 
              className={`content-table primary-color ${event === 'past' ? event : ''}`}
              style={{'cursor': 'pointer'}}
              id="past-events"
              onClick={()=> {event === 'past' ? '' : this.handleEvents('past')}}
            >
              <div className="text-past-events">
              <p className="text-select"><strong>Past Events</strong>&nbsp;&nbsp;({pastCount})</p>
              </div>
            </div>
            <div className="only-desktop primary-color hidden-xs hidden-sm" id="create-event">
              <Link to="/property/event/create">
                <p className="i-block">Create new event <img className="add-image primary-background-color" src="dist/img/meeting_space/icons/plus.png" width="30px" /></p>
              </Link>
            </div>
          </Row>
          <Row className="main-content">
            <Row className="table-title">
                <div className="visible-xs">
                  <form className="primary-color fright bpadding10 rmargin20">
                   <select className="dropdown-event text-bold" id="text-small"
                     onClick={(e) => {this.handleSortingDropdown(e)}}>
                      <optgroup label="Sort by:">
                        <option value="name">Event type</option>
                        <option value="start">Event Date</option>
                        <option value="last_activity">Last Activity</option>
                        <option value="status">Status</option>
                     </optgroup>
                   </select>
                  </form>
                </div>
                <Row className="hidden-xs">

                  <Col className="color-transparent content-table primary-color">
                    <p id="text-small">Sort by:</p>
                    <div>
                      <p id="text-small"><strong>Event Name</strong>&nbsp;&nbsp;
                        <div
                        type="button" 
                        className={sortByName ? "" : "transform"}
                        onClick={()=>{this.handleEventSorting('name')}}
                        style={{'display':'inline-block','cursor': 'pointer'}}
                        >
                          <ReactSVG 
                          wrapper="span" 
                          className="svg-wrapper" 
                          src="dist/img/meeting_space/icons/arrow-down.svg" 
                          alt="arrow down" width="5px"
                          />
                        </div>
                      </p>
                    </div>                  
                  </Col>
                  <Col className="event-date primary-color">
                    <p id="table-contents">Event Date&nbsp;&nbsp;
                      <div
                        type="button" 
                        className={sortByStart ? "" : "transform"}
                        onClick={()=>{this.handleEventSorting('start')}}
                        style={{'display':'inline-block','cursor': 'pointer'}}
                        >
                        <ReactSVG 
                        wrapper="span" 
                        className="svg-wrapper" 
                        src="dist/img/meeting_space/icons/arrow-down.svg" 
                        alt="arrow down" width="5px"
                        />
                      </div>
                    </p>
                  </Col>
                  <Col className="event-date primary-color">
                    <p id="table-contents">Last Activity&nbsp;&nbsp;
                      <div
                        type="button" 
                        className={sortByLastActivity ? "" : "transform"}
                        onClick={()=>{this.handleEventSorting('last_activity')}}
                        style={{'display':'inline-block','cursor': 'pointer'}}
                      >
                      <ReactSVG wrapper="span" className="svg-wrapper" 
                      src="dist/img/meeting_space/icons/arrow-down.svg" 
                      alt="arrow down" 
                      width="5px"
                      />
                      </div>
                    </p>
                  </Col>
                  <Col className="event-status primary-color">
                    <p id="table-contents-right">Status&nbsp;&nbsp;&nbsp;&nbsp;
                      <div
                        type="button" 
                        className={sortByStatus ? "" : "transform"}
                        onClick={()=>{this.handleEventSorting('status')}}
                        style={{'display':'inline-block','cursor': 'pointer'}}
                      >
                        <ReactSVG 
                        wrapper="span" 
                        className="svg-wrapper" 
                        src="dist/img/meeting_space/icons/arrow-down.svg" 
                        alt="arrow down"
                        />
                      </div>
                    </p>
                  </Col>
                </Row>
            </Row>
          { isEvents && (
            <div>
              {
                !isClickedEventType && eventInfo.results.map(result => (
                  <Row className="list-event">
                    <Link to={`/property/event/view/${result.uuid}`}>
                      <Row
                        className="display-contents pointer"
                        onClick={() => this.handleSelectedEvent(result)}
                      >
                          <Col className="event-name event-list-name" lg={2} md={2} sm={3} xs={6}> 
                             <div id="image-selected">
                              <img src={`dist/img/event_list/status_${result.status.id}.png`} width="10px"/>
                             </div>
                             <div id="event-selection">
                              <p className="event-width">{result.name.length >= 8 ? result.name.substring(0,8)+'...' : result.name}
                              <ReactSVG 
                                wrapper="span" 
                                className="svg-wrapper fright desktop-view-dropdown-icon" 
                                src={`dist/img/event_list/${result.showEventDetailsDropdown ? 'minus-icon' : 'plus-icon'}.svg`} 
                                alt="arrow down"
                                onClick={(e) => { this.handleEventDetailDropdown(e,result.id)} }
                              /></p>
                             </div>
                          </Col> 
                          <Col className="event-date hidden-xs" lg={2} md={2} sm={3}>
                            <p className="text primary-color">{moment(result.start, 'YYYY-MM-DD HH:mm').format('MMMM DD, YYYY')}</p>
                          </Col>
                          <Col className="event-date hidden-xs" lg={2} md={2} sm={3}>
                            <p className="text primary-color">{moment(result.last_activity, 'YYYY-MM-DD HH:mm').format('MMMM DD, YYYY')}</p>
                          </Col>
                        <Col className="event-status" lg={2} md={2} sm={3} xs={6}>
                          <div class="btn-group">
                            <button type="button" className={`select-button text-dropdown dropdown-item-1 dropdown-toggle status-${result.status.id}`} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              {result.status.event_status}&nbsp;&nbsp;<img class="select-icon" src="dist/img/event_list/drop-arrow-white.png" width="20px"/>
                            </button>
                            <div className="dropdown-menu">
                            { Object.keys(eventStatus).length !== 0 && eventStatus.results.map((status)=>(
                              status.name !== result.status.name && status.id !== 1 ?
                              (<div className="dropdown-item-1 ">
                                <Button className={`button status-${status.id}`} onClick={(e) => this.handleDropdown(status, result, e)}>{status.event_status}</Button>
                              </div>) : ''
                              ))
                            }
                            </div>
                          </div>
                        </Col>
                        { allEventHistoryInfo.length >= 1 && allEventHistoryInfo.filter(each => each.id === result.id).length >=1 && 

                        (<Col lg={4} md={4}>
                          <Col className="content-detail-left hidden-xs hidden-sm" lg={6} md={6}>
                          {
                            allEventHistoryInfo.filter(each => each.id === result.id)[0].orderedEventHistory.map((history, i)=>(
                            i == 0 ? recentHistoryInfo(history) && 
                            (<div>
                                <span className="header-text-color-1 leftmargin text-font12" style={{'font-size':'small'}}>{moment(history.date, 'YYYY-MM-DD').format('MMM.DD, YYYY')+' - '+moment(history.date, 'YYYY-MM-DD').format('hh:mm A')+' '}</span>
                                <div className="user-name">{history.user.first_name.charAt(0).toUpperCase()+history.user.last_name.charAt(0).toUpperCase()}</div>
                                <span className="text-detail">{recentHistoryInfo(history)}</span>
                            </div>): ''
                          ))
                          }

                          </Col>
                          <Col className="content-detail-right hidden-xs hidden-sm" lg={6} md={6}>
                            <div className="view-button primary-color">
                              <Button 
                              className="view text primary-color primary-border-color" 
                              width="22px"
                              onClick={(e) => this.showHistoryPopup(true, result.id, e)}
                              >
                              View Notifications</Button>
                            </div>
                          </Col>
                      </Col>)

                        }
                      
                        <div className={`lmargin10 primary-color rmargin20 ${result.showEventDetailsDropdown ? 'show' : 'hide'}`}>
                          <Row>
                           <Col sm={8} xs={8} className="hidden-lg hidden-md">
                            { allEventHistoryInfo.length >= 1 && allEventHistoryInfo.filter(each => each.id === result.id).length >=1 && 
                              (<div>
                                <div>
                                {
                                  allEventHistoryInfo.filter(each => each.id === result.id)[0].orderedEventHistory.map((history, i)=>(
                                  i == 0 ? recentHistoryInfo(history) && 
                                    (<div>
                                        <span className="header-text-color-1 leftmargin text-font12" style={{'font-size':'small'}}>{moment(history.date, 'YYYY-MM-DD').format('MMM.DD, YYYY')+' - '+moment(history.date, 'YYYY-MM-DD').format('hh:mm A')+' '}</span>
                                        <div className="user-name">{history.user.first_name.charAt(0).toUpperCase()+history.user.last_name.charAt(0).toUpperCase()}</div>
                                        <span className="text-detail">{recentHistoryInfo(history)}</span>
                                    </div>): ''
                                 ))
                                }
                                </div>
                              </div>)
                            }
                           </Col>
                           <Col sm={4} xs={4} className="hidden-lg hidden-md">
                             <Button 
                              className="view text primary-color fright primary-border-color" 
                              width="22px"
                              onClick={(e) => this.showHistoryPopup(true, result.id, e)}
                              >
                              View Notifications</Button>
                           </Col>
                          </Row>
                          <Row className="row-details-mobile-view bmargin10">
                           <Col className="padding10 list-details primary-border-right-light primary-border-bottom-light" lg={3} md={2} sm={2} xs={12}>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font14">Destination</span>
                             </div>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font16"><strong>{result.hotel.name}</strong></span>
                             </div>
                           </Col>
                           <Col className="padding10 list-details primary-border-right-light primary-border-bottom-light" lg={5} md={5} sm={5} xs={12}>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font14">Dates</span>
                             </div>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font16">
                                <strong>{moment(result.start, 'YYYY-MM-DD HH:mm').format('MMMM DD, YYYY')}</strong></span>
                              <span className="lmargin10">
                                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/arrow-right.svg" alt="to" />
                              </span>
                              <span className="text-font16 lmargin10">
                                <strong>{moment(result.end, 'YYYY-MM-DD HH:mm').format('MMMM DD, YYYY')}</strong></span>
                             </div>
                           </Col>
                           <Col className="padding10 list-details-attendees primary-border-right-light primary-border-bottom-light" lg={2} md={2} sm={2} xs={12}>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font14">Attendees</span>
                             </div>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font16"><strong>{result.attendees}</strong></span>
                             </div>
                           </Col>
                           <Col className="padding10" lg={2} md={3} sm={3} xs={12}>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font14">Type of Event</span>
                             </div>
                             <div className="tpadding10 lpadding10">
                              <span className="text-font16"><strong>{result.type.name}</strong></span>
                             </div>
                           </Col>
                          </Row>
                        </div>
                      </Row>
                    </Link>
                  </Row>
                ))
              }
              {
              showEventHistoryPopup
                &&(
                <EventHistoryPopup
                   handleClose={this.handleHistoryClose}
                   historyInfo={allEventHistoryInfo.filter(each => each.id == showEventHistoryPopupId)[0].orderedEventHistory}
                   selectedEventInfo={eventInfo.results.filter(each => each.id == showEventHistoryPopupId)[0]}
                   eventStatus = {eventStatus}
                  />
                )
              }              
            </div>
          )}
          {
            isPagingation && isNoEvents && !setLoader && (
              <Row>
                <div className="empty-event-msg-pagination">
                  <span>
                    No events found in this page!
                  </span>
                </div>
              </Row>
            )
          }
          {
            !isPagingation && isNoEvents && setLoader && (
              <Row>
                <div className="empty-event-msg-pagination">
                  <span>
                    Loading event list...
                  </span>
                </div>
              </Row>
            )
          }
          {
            !isClickedEventType && isPagingation && (
              <PaginationContainer
                count={eventInfo.count}
                paginationCB={this.handlePageChange}
                selectedPage={hotelEventInfo.selectedPage}
              />
            )
          }
          {
            isClickedEventType && setLoader && (
              <Row>
                <div className="empty-event-msg">
                  <span>
                    Loading event list...
                  </span>
                </div>
              </Row>
            )
          }
          {
            !isPagingation && isNoEvents && !setLoader && (
              <Row>
                <div className="empty-event-msg">
                  <span>
                    No Events Found!
                  </span>
                </div>
              </Row>
            )
          }
          </Row>
        </Row>
      </Grid>
    );
  }
}

EventList.contextTypes = {
  router: PropTypes.object,
};

export default EventList;

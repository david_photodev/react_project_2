import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Table,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import '../../../css/Chat/Chat.scss';
import { CountryDropdown } from 'react-country-region-selector';
import 'react-chat-widget/lib/styles.css';
import Datetime from 'react-datetime';
import ReactSVG from 'react-svg';
import { Redirect } from 'react-router-dom';
import EventHistoryPopup from '../EventList/EventHistoryPopup';
import HotelPage from '../HotelPage/HotelPage';
import LightBox from '../Reusables/LightBox';
import '../../../css/HomePage/HomePage.scss';
import '../../../css/HomePage/RFPManagement.scss';
import RFPManagementAction from '../../actions/RFPManagementAction';
import RFPManagementStore from '../../stores/RFPManagementStore';
import MeetingSpaceStore from '../../stores/MeetingSpaceStore';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import CommonLayout from '../CommonLayout/CommonLayout';
import MapBox from '../Reusables/MapBox';
import AuthStore from '../../stores/AuthStore';
import Helpers from '../Reusables/Helpers';

class RFPManagement extends CommonLayout {
  constructor(props) {
    super(props);
    this.state = {
      hotelEventInfo: RFPManagementStore.getAll(),
      hotelInfo: MeetingSpaceStore.getAll(),
      userInfo: AuthStore.getAll(),
      showEventHistoryPopup: false,
      showMapModal: false,
      viewMap: false,
      servicesDisplay: false,
      isRoomView: false,
      show3DPopup: false,
      selectedSetup: {},
      selected3DViewRoom: {},
      acceptedFiles: [],
      isLoggedIn: false,
      prevStateEventId: null,
      eventEditFields: [],
      acitivityProgramEditFields: {},
      guestRoomEditFields: {},
      updateEvents: {},
      editableAPs: [],
      activityPrograms: {},
      contactEditFields: [],
      selectedEventType: '',
      isSharePopup: false,
      hide3DView: false,
      validationStatus: true,
      editServices: false,
      cloneSetup: false,
      isScreenShot: false,
      showMaximumAlert: false,
      layoutSetup: {},
    };
    this.eventHeader = [
      'Choosen meeting spaces', 'Activity name', 'Event Date Starts', 'Event Date Ends', 'Attendees', 'Share',
      'Duplicate', 'View/Edit', '',
    ];
    this.showPopup = this.showPopup.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSetupInfo = this.handleSetupInfo.bind(this);
    this.viewInMap = this.viewInMap.bind(this);
    this.handleMapClose = this.handleMapClose.bind(this);
    this.handle3Dpage = this.handle3Dpage.bind(this);
    this.handle3DAlertClose = this.handle3DAlertClose.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.removeUploadedFile = this.removeUploadedFile.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.onEditEvent = this.onEditEvent.bind(this);
    this.onEditAP = this.onEditAP.bind(this);
    this.onEventChange = this.onEventChange.bind(this);
    this.handleEventTypes = this.handleEventTypes.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleDateAP = this.handleDateAP.bind(this);
    this.handleInputs = this.handleInputs.bind(this);
    this.handleShareInfo = this.handleShareInfo.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.onRFPManagementStoreChange = this.onRFPManagementStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.handleNewPeriod = this.handleNewPeriod.bind(this);
    this.onEditGuestRoom = this.onEditGuestRoom.bind(this);
    this.deleteGuest = this.deleteGuest.bind(this);
    this.deleteGuestInfo = this.deleteGuestInfo.bind(this);
    this.onEditContact = this.onEditContact.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.copyToClipboard = this.copyToClipboard.bind(this);
    this.handleSendMail = this.handleSendMail.bind(this);
    this.deleteAP = this.deleteAP.bind(this);
    this.closeAlert = this.closeAlert.bind(this);
    this.handleServices = this.handleServices.bind(this);
    this.selectMeetingRoom = this.selectMeetingRoom.bind(this);
    this.closeAlertMsg = this.closeAlertMsg.bind(this);
    this.changeCount = this.changeCount.bind(this);
  }

  componentWillMount() {
    ModelLoader.clearCache();
    localStorage.clear();
    window.scrollTo(0, 0);
    const { hotelEventInfo } = this.state;
    let { selectedEventId, eventTypes } = hotelEventInfo;
    let uuId;
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    const context = window.location.href;
    if (context.indexOf('/event/view/') > -1) {
      uuId = context.split('/event/view/')[1];
      this.setState({ eventUUID: uuId });
      MeetingSpaceAction.getEventID(uuId, eventId => {
        selectedEventId = eventId;
        this.loadRFPInfo(selectedEventId);
        this.setState({ selectedEventId });
      });
    }
    RFPManagementAction.search('');
    if (!Object.keys(eventTypes).length) {
      RFPManagementAction.getUserEventTypes();
    }
    RFPManagementStore.addStoreListener(this.onRFPManagementStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  loadRFPInfo(selectedEventId) {
    window.scrollTo(0, 0);
    const { hotelEventInfo, isLoggedIn } = this.state;
    setTimeout(() => {
      MeetingSpaceAction.getAttachementTypes((res) => {
        const attachmentTypes = res;
        const myDropzone = new Dropzone('div#my-document-upload-zone', {
          url: (files) => {
            return AuthStore.getApiHost() + `/api/events/${selectedEventId}/attachments`;
          },
          paramName: 'file',
          params: (files, xhr, chunk) => {
            console.log('PARAMS:', files);
            let params = {};
            if (chunk) {
              params = Dropzone.prototype.defaultOptions.params();
            }

            let fileTypeId = 1;
            if (files.length) {
              fileTypeId = (attachmentTypes[files[0].type] || fileTypeId);
            }
            params['type'] = fileTypeId;
            return params;
          },
          acceptedFiles: Object.keys(attachmentTypes).join(','),
          accept: (file, done) => {
            const msg = `Please upload only the following file types ${attachmentTypes.names.join(', ')}`;
            MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
            const uploadZone = $('#my-document-upload-zone');
            if (uploadZone) {
              const fileUpload = $('#my-document-upload-zone')[0].childNodes.length;
              const dragMessageTop = `${40 + (fileUpload * 30)}px`;
              const dropZoneHeight = `${224 + (fileUpload * 30)}px`;
              $(`<style>.rfp-management-container .submit-container #upload-doc-section .drag-upload-file
              {top: ${dragMessageTop};}</style>`).appendTo('head');
              $(`<style>.submit-container #my-document-upload-zone
                {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
            }
            console.log('ACCEPT: ', file);
            done();
            this.setState({ acceptedFiles: myDropzone.getAcceptedFiles() });
          },
          uploadMultiple: false,
          parallelUploads: 1,
          previewTemplate: document.querySelector('#file-upload-template').innerHTML,
          thumbnailWidth: 28,
          thumbnailHeight: 28,
          thumbnailMethod: 'contain',
          autoProcessQueue: false, // upload all files at once when submit button is clicked
        });
        myDropzone.on('sending', (file, xhr, formData) => {
          // set authorization header
          const headers = AuthStore.getHeaders();
          for (let headerName in headers) {
            if(headers.hasOwnProperty(headerName)) {
              let headerValue = headers[headerName];
              if (headerValue) {
                xhr.setRequestHeader(headerName, headerValue);
              }
            }
          }
        });
        myDropzone.on('error', (file, message, xhr) => {
          if (xhr == null) myDropzone.removeFile(file); // Remove the unsupported files
          const msg = `Unsupported file format. Please upload only ${attachmentTypes.names.join(', ')}`;
          MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
          setTimeout(() => {
            MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
          }, 5000);
        });
        myDropzone.on('removedfile', (file) => {
          this.setState({ acceptedFiles: myDropzone.getAcceptedFiles() });
          const uploadZone = $('#my-document-upload-zone');
          if (uploadZone) {
            const fileUpload = $('#my-document-upload-zone')[0].childNodes.length;
            const dragMessageTop = `${40 + (fileUpload * 30)}px`;
            const dropZoneHeight = `${224 + (fileUpload * 30)}px`;
            $(`<style>.rfp-management-container .submit-container #upload-doc-section .drag-upload-file
            {top: ${dragMessageTop};}</style>`).appendTo('head');
            $(`<style>.submit-container #my-document-upload-zone
              {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
          }
          if (myDropzone.files.length <= 0) {
            console.log('REMOVED: ', file);
            this.setState({ uploadedFile: false });
          }
        });
        myDropzone.on('addedfile', (file) => {
          this.setState({ uploadedFile: true });
          console.log('ADDED: ', file);
        });
        this.setState({ myDropzone });
      });
    }, 5000);
    if (isLoggedIn) {
      RFPManagementAction.getSelectedEventInfo(selectedEventId);
      MeetingSpaceAction.getActivityInformations(selectedEventId);
      MeetingSpaceAction.getContactInformations(selectedEventId);
      MeetingSpaceAction.getAttachmentFile(selectedEventId);
      RFPManagementAction.getHistoryInfo(selectedEventId);
      const { eventStatus } = hotelEventInfo;
      if (Object.keys(eventStatus).length === 0) {
        RFPManagementAction.getEventsStatus();
      }
    }
  }

  componentWillUnmount() {
    RFPManagementStore.removeStoreListener(this.onRFPManagementStoreChange);
  }

  componentWillUpdate(){
    const { eventUUID } = this.state;
    const uuId = window.location.href.split('/event/view/')[1];
    if (eventUUID && uuId && eventUUID.toString() !== uuId.toString()){
      this.componentWillMount();
    }
  }

  onRFPManagementStoreChange() {
    const hotelEventInfo = RFPManagementStore.getAll();
    const { eventEditFields, updateEvents } = this.state;
    const updateEventsInfo = JSON.parse(JSON.stringify(hotelEventInfo.selectedEventInfo));
    const activityPrograms = JSON.parse(JSON.stringify(hotelEventInfo.activityPrograms));
    if (updateEventsInfo.hotel) {
      updateEventsInfo['hotel'] = parseInt(updateEventsInfo.hotel.url.split('/hotel/')[1], 10);
    }
    updateEventsInfo['type'] = updateEventsInfo.type && updateEventsInfo.type.id ? updateEventsInfo.type.id : null;
    updateEventsInfo['status'] = updateEventsInfo.status && updateEventsInfo.status.id ? updateEventsInfo.status.id : null;
    this.setState({
      hotelEventInfo,
      activityPrograms, 
    });
    if ((!eventEditFields.length) && (!updateEvents.type) && (!updateEvents.status)) {
      this.setState({ updateEvents : updateEventsInfo});
    }
  }

  showPopup(bool){
    this.setState({showEventHistoryPopup:true});
  }

  handleClose(){
    this.setState({ showEventHistoryPopup:false });
  }

  viewInMap() {
    this.setState({ viewMap: true });
    this.setState({ showMapModal: true });
  }

  handleMapClose() {
    this.setState({
      showMapModal: false,
      viewMap: false,
    });
  }

  handleSetupInfo(room, setup, type) {
    this.setState({ editServices: true });
    RFPManagementAction.updateAddons(room, setup, type);
  }

  handle3Dpage(room, setup) {
    const {
      hotelEventInfo, selectedEventId, prevStateEventId, hotelInfo,
    } = this.state;
    const { hotelMeetingRooms } = hotelEventInfo;
    const show3dModel = room.available_model;
    const { scenes } = room;
    const { viewLayout, layout, setUpImages } = hotelInfo;
    this.onEditAP(setup, 'model_3d');
    this.setState(prevState => ({
      prevStateEventId: prevState.selectedEventId,
    }));
    if (viewLayout && layout && prevStateEventId && prevStateEventId === selectedEventId) {
      if (viewLayout.hasOwnProperty('id') && layout.hasOwnProperty('id')) {
        if (viewLayout.id === room.id && layout.setup_id === setup.setup_id
          && layout.chairs_count === setup.chairs_count && layout.type === setup.type
        ) {
          return;
        }
      }
    }
    const filteredRoom = hotelMeetingRooms.filter(hotelRoom => hotelRoom.id === room.id);

    // Changed the format to show the available setups in the presetup popup (viewLayout)
    const setupUpdate = [];
    if (filteredRoom.length && filteredRoom[0].room_setups.length) {
      filteredRoom[0].room_setups.map((room_setup) => {
        const updateFurniture = () => {
          const furnitures = {};
          if (room_setup.furnitures.length >= 1) {
            room_setup.furnitures.map((furniture => {
              if (furniture.item_type === 'ChairItem') {
                furnitures.chair = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
              if (furniture.item_type === 'TableItem') {
                furnitures.table = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
            }));
            return furnitures;
          }
          return furnitures;
        };
        let setupNameUpdated = room_setup.type.name.toLowerCase();
        setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
        const img = setUpImages.hasOwnProperty(setupNameUpdated) ? setUpImages[setupNameUpdated] : setUpImages['theatre'];
        setupUpdate.push({
          id: room_setup.type.id,
          shape: room_setup.type.name,
          type: setupNameUpdated,
          count: room_setup.capacity,
          img,
          furnitures: updateFurniture(),
          config: room_setup.config,
        });
      });
    }
    const updatedRoom = JSON.parse(JSON.stringify(room));
    updatedRoom.setup = setupUpdate;

    // Changed the format to highlight the selected Layout in the presetup popup (viewLayout)
    let updatedSetup = JSON.parse(JSON.stringify(setup));
    if (setupUpdate.length) {
      const filteredSetup = setupUpdate.filter(layoutSetup => layoutSetup.type.toLowerCase() === updatedSetup.type.toLowerCase());
      if (filteredSetup.length) {
        updatedSetup.id = filteredSetup[0].id;
      }
    }
    this.setState({ isRoomView: false });
    if (!show3dModel || scenes.length < 1) {
      this.setState({ show3DPopup: true, isRoomView: false });
      return false;
    } else {
      MeetingSpaceAction.storeSceneData(room.scenes);
      if (hotelInfo.viewer) {
        hotelInfo.viewer.dispose();
        MeetingSpaceAction.setViewer(null);
      }
      MeetingSpaceAction.showSetupView(room);
      this.setState({
        isRoomView: true,
        selectedSetup: setup,
        selected3DViewRoom: room,
      });
      MeetingSpaceAction.updateLayout(updatedRoom, updatedSetup, 'view');
    }
  }

  handle3DAlertClose(){
    this.setState({ show3DPopup: false });
  }

  handleRemove(file) {
    const { myDropzone } = this.state;
    myDropzone.removeFile(file);
  }

  removeUploadedFile(file) {
    const { selectedEventId } = this.state;
    this.setState({ selectedFile: file });
    RFPManagementAction.removeUploadedFile(selectedEventId, file.id);
  }

  handleDropdown(status, currentEvent) {
    const { hotelEventInfo, updateEvents } = this.state;
      if(hotelEventInfo.selectedEventInfo.id === currentEvent.id){
        hotelEventInfo.selectedEventInfo.status = status;
        RFPManagementAction.updateEvent(hotelEventInfo.selectedEventInfo, false);
      }
      updateEvents['status'] = status.id;
    this.setState({ hotelEventInfo, updateEvents });
  }

  onEditEvent(type) {
    const { eventEditFields } = this.state;
    eventEditFields.push(type);
    this.setState({ eventEditFields });
  }

  onEditAP(setup, type) {
    const { acitivityProgramEditFields } = this.state;
    if (acitivityProgramEditFields.hasOwnProperty(setup.setup_id)) {
      const editItems = acitivityProgramEditFields[`${setup.setup_id}`];
      editItems.push(type);
      acitivityProgramEditFields[`${setup.setup_id}`] = editItems;
    } else {
      acitivityProgramEditFields[`${setup.setup_id}`] = [type];
    }
    this.setState({ acitivityProgramEditFields });
  }

  handleDateEvent(e, picker, type) {
    const { updateEvents } = this.state;
    let { startDate } = picker;
    if (type === 'start') {
      updateEvents['start'] = moment(startDate).format('YYYY-MM-DD HH:mm');
    } else {
      startDate = `${moment(startDate).format('YYYY-MM-DD')} 23:59`;
      updateEvents['end'] = moment(startDate).format('YYYY-MM-DD HH:mm');
    }
    this.setState({ updateEvents });
  }

  handleEventTypes(e) {
    const { updateEvents } = this.state;
    const updatedType = parseInt(e.target.value, 10);
    updateEvents['type'] = updatedType;
    this.setState({
      updateEvents,
      selectedEventType: updatedType,
    });
  }

  onEventChange(type, e) {
    const { updateEvents } = this.state;
    updateEvents[type] = e.target.value;
    updateEvents[type] = type === 'attendees' ? parseInt(e.target.value, 10) : e.target.value;
    this.setState({ updateEvents });
  }

  displayNotificationMsg(msg) {
    RFPManagementAction.notificationMsg(msg, 'add');
    setTimeout(() => {
      RFPManagementAction.notificationMsg(msg, 'remove');
    }, 5000);
  }

  closeAlert(msg) {
    RFPManagementAction.notificationMsg(msg, 'remove');
  }

  handleUpdate() {
    const {
      updateEvents, eventEditFields, acitivityProgramEditFields, hotelEventInfo, guestRoomEditFields, contactEditFields,
      myDropzone, editServices, cloneSetup,
    } = this.state;
    const { selectedRooms, guestInfo, contact, save3D } = hotelEventInfo;
    const updateEventFields = ['id', 'name', 'start', 'end', 'attendees', 'status', 'hotel', 'type'];
    const guestEdit = this.isEditguestInfo(guestInfo);
    // const updateAPs = selectedRooms.filter(room => {
    //   const setupRoom = room.setup.filter(setup => {
    //     if (setup && (setup.edit || setup.create)) {
    //       return setup;
    //     }
    //   });
    //   if (setupRoom.length) {
    //     return room;
    //   }
    // });
    // const startDate = moment(`${setup.event_date_starts} ${setup.event_time_starts}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
    // const endDate = moment(`${setup.event_date_ends} ${setup.event_time_ends}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
    const isValid = this.editRfpValidation(eventEditFields, updateEvents,acitivityProgramEditFields, selectedRooms, guestRoomEditFields, 
                                guestInfo, contactEditFields, contact);
    this.setState({ validationStatus : isValid });
    if (isValid) {
      Object.keys(updateEvents).filter(info => {
        if (!updateEventFields.includes(info)) {
          delete updateEvents[info];
        }
      });

      if (Object.keys(acitivityProgramEditFields).length || editServices || cloneSetup || save3D) {
        const eventEdit = this.isEditSelectedRooms(selectedRooms);
        if (eventEdit || cloneSetup || save3D) {
          this.setState({
            acitivityProgramEditFields: {},
            editServices: false,
            cloneSetup: false,
          });
          RFPManagementAction.notificationMsg('Saving...', 'add');        
          RFPManagementAction.updateAPsInfo(selectedRooms, res => {
            RFPManagementAction.notificationMsg('Saving...', 'remove');
          });
        }
      }
      // if (Object.keys(guestRoomEditFields).length) {
        if (guestEdit) {
          this.setState({guestRoomEditFields: {}})
          RFPManagementAction.notificationMsg('Saving...', 'add');
          RFPManagementAction.updateGuestsInfo(guestInfo, res => {
            RFPManagementAction.notificationMsg('Saving...', 'remove');
          });
        }
      // }
      if (eventEditFields.length) {
        RFPManagementAction.notificationMsg('Saving...', 'add');
        RFPManagementAction.updateEventInfo(updateEvents, res => {
          RFPManagementAction.notificationMsg('Saving...', 'remove');
        });
      }
      if (contactEditFields.length) {
        RFPManagementAction.notificationMsg('Saving...', 'add');
        RFPManagementAction.updateContactInfo(contact, res => {
          RFPManagementAction.notificationMsg('Saving...', 'remove');
        });
      }
      if (myDropzone) {
        myDropzone.on('queuecomplete', () => {
          myDropzone.removeAllFiles(true);
        });
        myDropzone.on('complete', (file) => {
          myDropzone.removeFile(file);
          RFPManagementAction.uploadingFile(JSON.parse(file.xhr.responseText));
          myDropzone.processQueue();
        });
        myDropzone.on('error', (file, message, xhr) => {
          console.error('ERROR', file, JSON.stringify(message), xhr);
        });
        myDropzone.processQueue();
      }
      this.setState({
        eventEditFields: [],
        contactEditFields: [],
      });
    }
  }
  isEditSelectedRooms (selectedRooms) {
    let edit = false;
    selectedRooms.map(each=>{
      each.setup.map(eachSetup =>{
        if (eachSetup.hasOwnProperty('edit') || eachSetup.hasOwnProperty('create')) {
          edit = true;
        } 
      })
    })
    return edit;
  } 
  isEditguestInfo(guestInfos) {
    let isGuestEdit = false;
    guestInfos.map(guestInfo => {
      if (guestInfo.edit || guestInfo.create) {
        isGuestEdit = true;
      }
    })
    return isGuestEdit;
  }

  editRfpValidation (eventEditFields, updateEvents, acitivityProgramEditFields, selectedRooms, guestRoomEditFields, guestInfo, contactEditFields, contact) {
    let validation = true;
    if (eventEditFields.includes('attendees')) {
      if (isNaN(parseInt(updateEvents['attendees']))) {
        this.displayNotificationMsg('Please enter valid attendees');
        validation = false;
      }
    }
    if (Object.values(acitivityProgramEditFields).length >= 1) {
      Object.keys(acitivityProgramEditFields).map((acitivityProgramEditField,meetingRoom) => {
        acitivityProgramEditFields[acitivityProgramEditField].map((field) => {
          selectedRooms.map(room => {
            const currentSetUp = room.setup.filter(eachSetup => eachSetup.setup_id === parseInt(acitivityProgramEditField))[0];
            if (currentSetUp) {
              if (field === 'attendees' && ((!currentSetUp['chairs_count'])|| (isNaN(parseInt(currentSetUp['chairs_count']))))) {
                this.displayNotificationMsg('Please enter valid attendees');
                validation = false;
              }
            }
          })
            
        })
      });
    }
    if (Object.keys(guestRoomEditFields).length >= 1) {
      Object.keys(guestRoomEditFields).map((guestRoomEditField,guestRoom) => {
        guestRoomEditFields[guestRoomEditField].map((field) => {
          const currentGuestRoom = guestInfo.filter((each,i) => each.tempID === parseInt(guestRoomEditField))[0]
          if (currentGuestRoom['start'] === '') {
            this.displayNotificationMsg('Please enter valid date');
            validation = false;
          } 
          if (currentGuestRoom['end'] === '') {
            this.displayNotificationMsg('Please enter valid date');
            validation = false;
          }
          if (isNaN(parseInt(currentGuestRoom['single_rooms']))) {
            this.displayNotificationMsg('Please enter valid single rooms');
            validation = false;
          }
          if (isNaN(parseInt(currentGuestRoom['double_rooms']))) {
            this.displayNotificationMsg('Please enter valid double rooms');
            validation = false;
          }
        });
      });
    }
    if (contactEditFields.length >= 1) {
      contactEditFields.map(field =>{
        if (field === "name" && (contact[field] === '' || contact[field] === null)) {
          this.displayNotificationMsg('Please enter valid name');
          validation = false;
        } 
        if (field === "mail" && (!contact['email'].match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/))) {
          this.displayNotificationMsg('Please enter valid e-mail');
          validation = false;
        } 
        if (field === "phone" && (!/^[\d\.\-]+$/.test(contact[field]))) {
          this.displayNotificationMsg('Please enter valid phone');
          validation = false;
        } 
      })
    }
    
    const guestDetails = guestInfo.map(guest => {
      if (guest.create) {
        if (guest.start === '') {
          this.displayNotificationMsg('Please enter valid date');
          validation = false;
        }
        if (guest.end === '') {
          this.displayNotificationMsg('Please enter valid date');
          validation = false;
        } 
        if (isNaN(parseInt(guest.single_rooms))) {
          this.displayNotificationMsg('Please enter valid single rooms');
          validation = false;
        }
        if (isNaN(parseInt(guest.double_rooms))) {
          this.displayNotificationMsg('Please enter valid double rooms');
          validation = false;
        }
      }
    });
    return validation;
  }
  handleActivityName(roomId, setup, type, e) {
    const activityName = e.target.value;
    const params = {
      roomId,
      setupId: setup.setup_id,
      id: setup.id,
      name: activityName,
      edit: true,
      type,
    };
    if (setup.create) {
      delete params.edit;
      params['create'] = true;
    }
    RFPManagementAction.updateActivityName(params);
  }

  handleDateAP(roomId, setup, e, picker, type) {
    let { startDate } = picker;
    startDate = moment(startDate).format('DD/MM/YYYY');
    const params = {
      roomId,
      setupId: setup.setup_id,
      id: setup.id,
      name: startDate,
      edit: true,
      type,
    };
    if (setup.create) {
      delete params.edit;
      params['create'] = true;
    }
    RFPManagementAction.updateActivityName(params);
  }

  handleTimeChange(roomId, setup, type, e) {
    const datetime = moment(e).format('HH:mm');
    const params = {
      roomId,
      setupId: setup.setup_id,
      id: setup.id,
      name: datetime,
      edit: true,
      type,
    };
    if (setup.create) {
      delete params.edit;
      params['create'] = true;
    }
    RFPManagementAction.updateActivityName(params);
  }

  handleInputs(room, setup, e) {
    const { hotelEventInfo } = this.state;
    const { allHotelMeetingRooms } = hotelEventInfo;
    const roomId = room.id;
    const setupId = setup.setup_id;

    const selectedMeetingRoom = allHotelMeetingRooms.filter(rooms => rooms.id === roomId)[0];
    let value = null;
    const setupCount = selectedMeetingRoom.room_setups.map((roomSetup,i) => {
      if (roomSetup.type.name.toLowerCase() === setup.shape.toLowerCase()) {
        value = roomSetup.capacity;
      }
    })   
    if (e.target.value > value) {
      const msg = 'Maximum number of attendees count is ' + `${value}`;
      this.displayNotificationMsg(msg);
    } else {
      value = parseInt(e.target.value, 10);
      const params = {
        roomId,
        setupId,
        id: setup.id,
        edit: true,
        name: value,
        type: 'chairs_count',
      };
      if (setup.create) {
        delete params.edit;
        params['create'] = true;
      }
      RFPManagementAction.updateActivityName(params);
    }
  }

  handleServices(room, setup, service, type) {
    const roomId = room.id;
    const setupId = setup.setup_id;
    const params = {
      roomId,
      setupId,
      id: setup.id,
      edit: true,
      name: service.id,
      type
    };
    RFPManagementAction.updateActivityName(params);
  }

  handleShareInfo(type) {
    this.setState({ isSharePopup: type });
  }

  onEditGuestRoom(guestId, type) {
    const { guestRoomEditFields } = this.state;
    if (guestRoomEditFields.hasOwnProperty(guestId)) {
      const editItems = guestRoomEditFields[`${guestId}`];
      editItems.push(type);
      guestRoomEditFields[`${guestId}`] = editItems;
    } else {
      guestRoomEditFields[`${guestId}`] = [type];
    }
    this.setState({ guestRoomEditFields });
  }

  handleChange(guestId, e) {
    const { hotelEventInfo } = this.state;
    const { guestInfo } = hotelEventInfo;
    const { name, value } = e.target;
    guestId = parseInt(guestId, 10);
    const updateGuest = guestInfo.map((guest) => {
      const eachGuest = guest;
      if (guest.tempID === guestId) {
        eachGuest[name] = parseInt(value, 10);
        eachGuest['edit'] = true;
        return eachGuest;
      }
      return eachGuest;
    });
    MeetingSpaceAction.savePeriod(guestInfo);
  }

  handleDate(id, item, e, picker) {
    const { hotelEventInfo } = this.state;
    const { guestInfo } = hotelEventInfo;
    const startDate = moment(picker).format('YYYY-MM-DD HH:mm');
    const updateGuest = guestInfo.map((guest) => {
      const eachGuest = guest;
      if (guest.tempID === id) {
        eachGuest[item] = startDate;
        eachGuest['edit'] = true;
        return eachGuest;
      }
      return eachGuest;
    });
    RFPManagementAction.savePeriod(updateGuest);
  }

  deleteGuest(id) {
    RFPManagementAction.deletePeriod(id);
  }

  onEditContact(type) {
    const { contactEditFields } = this.state;
    contactEditFields.push(type);
    this.setState({ contactEditFields });
  }

  handleContactChange(type, e) {
    // const contactRequiredFields = ['id', 'mail', 'phone', 'contact_type', 'event'];
    const { hotelEventInfo } = this.state;
    const { contact } = hotelEventInfo;
    contact[type] = e.target.value;
    RFPManagementAction.saveContact(contact);
  }

  handleCheck(type) {
    this.onEditContact('contact_type');
    const { hotelEventInfo } = this.state;
    const { contact } = hotelEventInfo;
    const isPersonal = type === 'personal';
    contact.contact_type = isPersonal ? 'personal' : 'business';
    RFPManagementAction.saveContact(contact);
  }

  handleCountryChange(e) {
    this.onEditContact('country');
    const { hotelEventInfo } = this.state;
    const { contact } = hotelEventInfo;
    contact.country = e;
    RFPManagementAction.saveContact(contact);
  }

  handleSendMail() {
    const { hotelInfo, hotelEventInfo } = this.state;
    const {
      selectedEventHotelInfo, selectedEventInfo, selectedHotelChain, contact,
    } = hotelEventInfo;
    const emailList = $('#email-id').val();
    const link = $('#link-id').val();
    const emails = emailList.split(',');
    let isFormValid = true;
    emails.map((email)=>{
      email = email.trim();
      if(!email.match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/)){
        const msg = 'Please Enter The Valid Mail';
        this.displayNotificationMsg(msg);
        // this.handleShareInfo(true);
        isFormValid = false;
        return null;
      }
    });

    if(!isFormValid) {
      return false;
    }

    // hide share popup before generating pdf
    this.handleShareInfo(false);

    let html_content = '<div class = "share-url">';
    html_content += '<p> Hi there,</p>';
    html_content += '<p>'+contact.name+' wants you to see this 3D Diagramming for the event '+selectedEventInfo.name+' at '+selectedEventHotelInfo.name+':</p>';
    html_content += '<p>'+link+'</p>';
    html_content += '<p>We hope you enjoy this amazing event!</p>';
    html_content += '<p>Sincerely,<br>' +selectedEventHotelInfo.name + '</p>';
    html_content += `<p>PS. You can now create your own hyperrealistic 3D setups for any event at our hotel. <a href="${window.location.origin}/#/property/${selectedEventHotelInfo.id}">Try it now here!</a></p>`;
    html_content += '</div>';
    const email_template = [
      {
        "sender":selectedHotelChain.sender_email,
        "recipients":emails,
        "subject":""+contact.name+" wants you to see this event in 3D at "+selectedEventHotelInfo.name+"",
        "body":{
          "plain":"",
          "html": html_content
        }
      },
    ];
    const email = {
      "data": {
        "messages": JSON.stringify(email_template),
        "attachment": "",
      },
      "response": null
    };
    RFPManagementAction.notificationMsg('Sending...', 'add');
    this.setExpander(true);
    const container = document.getElementsByClassName('content-right')[0];
    this.setState({ hide3DView: true });
    Helpers.outputAsPDF(container, 'RFP.pdf', (blob) => {
      this.setExpander(false);
      this.setState({ hide3DView: false });
      email.data.attachment = new File([blob], 'RFP.pdf');
      this.sendEmail(email);
    });
  }

  setExpander(isExpand) {
    this.setState({ isScreenShot:isExpand});
  }

  copyToClipboard(){
    const copyText = $("#link-id");
    copyText.select();
    document.execCommand("copy");
  }

  sendEmail(email) {
    var url = AuthStore.getApiHost()+"/api/users/send";
    // const { setLoader, setReadWriteMode, setSuccessMessage } = this.props;
    var formData  = new FormData();
    for(var name in email.data) {
      formData.append(name, email.data[name]);
    }
    AuthStore.getHeaders(null, (headers) => {
      fetch(url, {
        mode: 'cors',
        method: 'POST',
        headers: headers,
        body: formData
      }).then(function (response) {
        email.response = response;
        if (response.ok) {
          RFPManagementAction.notificationMsg('Sending...', 'remove');
        }
      });
    });
  }

  handleClone(room, setup, type) {
    this.setState({cloneSetup:true});
    const newSetup = JSON.parse(JSON.stringify(setup));
    const { hotelEventInfo } = this.state;
    const clonedBy = hotelEventInfo.selectedRooms.filter(layoutRoom => layoutRoom.id === room.id)[0];
    const newSetupId = clonedBy.setup[clonedBy.setup.length - 1].setup_id + 1;
    newSetup['setup_id'] = newSetupId;
    newSetup['addon'] = type === 'add';
    newSetup['enable3D'] = false;
    newSetup['create'] = true;
    RFPManagementAction.duplicateLayout(room, newSetup);
  }

  
  handleNewPeriod() {
    this.setState({validationStatus:true});
      const { hotelEventInfo, selectedEventId } = this.state;
      const { guestInfo } = hotelEventInfo;

      // const newInfo = JSON.parse(JSON.stringify(guestInfo));
      let newGuestInfoID = 1;
      if (guestInfo.length){
        newGuestInfoID = guestInfo[guestInfo.length - 1].tempID + 1;
      }
      const newInfo = {
        'tempID': newGuestInfoID,
        'create': true,
        'start': '',
        'end': '',
        'single_rooms': '',
        'double_rooms': '',
        'event': selectedEventId,
        'isNew': true,
      }
      RFPManagementAction.duplicateGuestInfo(newInfo);
  }

  deleteAP(room, setup) {
    if (setup.create) {
      RFPManagementAction.removeLayout(room, setup);
    } else {
      RFPManagementAction.deleteAP(room, setup);
    }
  }

  selectMeetingRoom(roomId, setup, type, e) {
    const { hotelEventInfo, hotelInfo } = this.state;
    const { setUpImages } = hotelInfo;
    const { hotelMeetingRooms } = hotelEventInfo;
    const filteredRoom = hotelMeetingRooms.filter(hotelRoom => hotelRoom.id === parseInt(e.target.value, 10));
    // Changed the format to show the available setups in the presetup popup (viewLayout)
    const setupUpdate = [];
    if (filteredRoom.length && filteredRoom[0].room_setups.length) {
      filteredRoom[0].room_setups.map((room_setup) => {
        const updateFurniture = () => {
          const furnitures = {};
          if (room_setup.furnitures.length >= 1) {
            room_setup.furnitures.map((furniture => {
              if (furniture.item_type === 'ChairItem') {
                furnitures.chair = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
              if (furniture.item_type === 'TableItem') {
                furnitures.table = {
                  id: furniture.id,
                  type: 'gltf',
                  model_path: furniture.model_3d,
                };
              }
            }));
            return furnitures;
          }
          return furnitures;
        };
        let setupNameUpdated = room_setup.type.name.toLowerCase();
        setupNameUpdated = setupNameUpdated === 'u-shape' ? 'ushape' : setupNameUpdated;
        const img = setUpImages.hasOwnProperty(setupNameUpdated) ? setUpImages[setupNameUpdated] : setUpImages['theatre'];
        setupUpdate.push({
          id: room_setup.type.id,
          shape: room_setup.type.name,
          type: setupNameUpdated,
          count: room_setup.capacity,
          img,
          furnitures: updateFurniture(),
          config: room_setup.config,
        });
      });
    }
    const checkAvalSetup = setupUpdate.map(layoutSetup => {
      if (setup.type.toLowerCase() === layoutSetup.type.toLowerCase()) {
        if (layoutSetup.count > 0) {
          if (setup.chairs_count > layoutSetup.count) {
            const meetingRoomIdVal = parseInt(e.target.value, 10);
            this.setState({
              layoutSetup,
              roomId,
              meetingRoomIdVal,
              setup,
              showMaximumAlert: true,
              selectedMeetingRoom: filteredRoom[0],
            });
          } else {
            const params = {
              roomId,
              setupId: setup.setup_id,
              id: setup.id,
              name: parseInt(e.target.value, 10),
              edit: true,
              type,
            };
            if (setup.create) {
              delete params.edit;
              params['create'] = true;
            }
            RFPManagementAction.updateActivityName(params);
          }
        } else {
          const params = {
            roomId,
            setupId: setup.setup_id,
            id: setup.id,
            name: setup.meetingRoomId ? setup.meetingRoomId : setup.meeting_room.id,
            edit: true,
            type,
          };
          if (setup.create) {
            delete params.edit;
            params['create'] = true;
          }
          RFPManagementAction.updateActivityName(params);
          this.displayNotificationMsg(`${setup.shape} is not available for ${filteredRoom[0].name} room`);
        }
      }
    });
  }

  closeAlertMsg() {
    this.setState({ showMaximumAlert: false });
  }

  changeCount() {
    const {
      roomId, layoutSetup, meetingRoomIdVal, setup,
    } = this.state;
    const params = {
      roomId,
      setupId: setup.setup_id,
      id: setup.id,
      name: meetingRoomIdVal,
      edit: true,
      type: 'meetingRoomId',
    };
    if (layoutSetup.create) {
      delete params.edit;
      params['create'] = true;
    }
    RFPManagementAction.updateActivityName(params);
    const countParams = {
      roomId,
      setupId: setup.setup_id,
      id: setup.id,
      edit: true,
      name: layoutSetup.count,
      type: 'chairs_count',
    };
    if (layoutSetup.create) {
      delete params.edit;
      params['create'] = true;
    }
    this.onEditAP(setup, 'attendees');
    this.setState({ showMaximumAlert: false });
    RFPManagementAction.updateActivityName(countParams);
  }

  deleteGuestInfo(guest){
    if (!guest.id) {
      RFPManagementAction.removeGuestInfolayout(guest);
    } else {
      RFPManagementAction.deletePeriod(guest.id);
    }
  }

  render() {
    const {
      hotelEventInfo, showEventHistoryPopup, viewMap, showMapModal, isRoomView, show3DPopup, hotelInfo, selectedSetup,
      selected3DViewRoom, isLoggedIn, isRedirect, userInfo, isMobileSidebar, eventEditFields, updateEvents,
      selectedEventType, acitivityProgramEditFields, isSharePopup, guestRoomEditFields, contactEditFields, hide3DView,
      validationStatus, showMaximumAlert, layoutSetup, selectedMeetingRoom, isScreenShot,
    } = this.state;
    const link = window.location.href;
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    const {
      selectedRooms, contact, guestInfo, setupTypes, selectedEventInfo, selectedEventHotelInfo, hotelMeetingRooms,
      attachedFiles, meetingRoomInfo, historyInfo, eventStatus, userInfoOfLatestHistory, eventTypes, notificationMsg,
    } = hotelEventInfo;
    if (Object.keys(historyInfo).length >= 1) {
      historyInfo.sort(function(a, b){
        var keyA = new Date(a.date),
            keyB = new Date(b.date);
        // Compare the 2 dates
        if(keyA < keyB) return 1;
        if(keyA > keyB) return -1;
        return 0;
      });
    }
    const { setUpImages } = hotelInfo;
    // const { data } = userInfo.user;
    // let property = data.profile.hotel_chain.url;
    // const apiHost = AuthStore.getApiHost();
    // property = property.replace(`${apiHost}/api/hotel_chain/`, '');
    const property = parseInt(selectedEventHotelInfo.id, 10);
    const nameValid = (name) => {
      if ( name === '' || name === null) {
          return true;
      }
      return false;
    }

    const emailValid = (email) => {
      if ( (!email.match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/)) ) {
        return true;
      }
      return false;
    }

    const phoneValid = (phone) => {
      if (!/^[\d\.\-]+$/.test(phone)) {
        return true;
      }
      return false;
    }
    // let uuid;
    // if (selectedEventId) {
    //   const selectedEvent = hotelEventInfo.eventInfo.results.filter(event => event.id === selectedEventId)[0];
    //   uuid = selectedEvent.uuid;
    // }
    const modelAlertMessage = 'This meeting room is not yet available for 3D diagramming.';
    const changeSelectedRoom = (room, setup) => {
      if (setup.meetingRoomId) {
        const meetingRoomName = hotelMeetingRooms.filter(val => val.id === setup.meetingRoomId)[0].name;
        return meetingRoomName;
      } else {
        return setup.meeting_room.name;
      }
    };
    return (
      <Grid className="home-container rfp-management-container primary-color">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <Row className="content-right">
          <Row className="header-container">
            {this.pageHeader(true, notificationMsg)}
          </Row>
          { typeof(selectedEventInfo) !== 'undefined' && Object.keys(selectedEventInfo).length !== 0 && Object.keys(selectedEventHotelInfo).length !== 0 &&
        (
          <div>
          <Row>
            <Button
              className="back-button primary-color"
              onClick={this.context.router.history.goBack}
            >
              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/back-arrow.svg" width="25px"/>
              <span className="hidden-xs">&nbsp;&nbsp;Back</span>
            </Button>
          </Row>
          <Row className="row-title">
            <Col xs={8} sm={8}>
              <div className="column-header primary-color" >
                <p className="hidden-xs"><strong>{"RFP - Event \""+selectedEventInfo.name+"\""}</strong></p>
                <p className="mobile-iblock title-rfp"><strong>{selectedEventInfo.name}</strong></p>
              </div>
            </Col>
            <Col className="visible-xs visible-sm tmargin20" xs={4} sm={4}>
              <div className="i-block fright" onClick={() => this.showPopup(true)}>
                <span><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/history.svg" alt="chat icon" /></span>
                <span className="lpadding10">History</span>
              </div>
            </Col>
          </Row>
          <Row className="guest-history-details padding10">
            <Col lg={2} md={4} className="mob-col">
              <div className="row1 primary-border-right">
                <span className="gray-text destination">Destination</span>
                <span className="primary-color text-bold text-font14 destination">{selectedEventInfo.hotel.city}</span>
              </div>
              <div className="row1 primary-border-right hidden-md hidden-lg ipad-pro-enable">
                <span className="gray-text">Check-in</span>
                <span
                  className={`pointer ${eventEditFields.includes('start') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('start')}
                >
                  <span className="primary-color text-bold text-font14">
                    {
                      updateEvents.start ? 
                      moment(updateEvents.start, 'YYYY-MM-DD').format('DD/MM/YYYY') :
                      moment(selectedEventInfo.start, 'YYYY-MM-DD').format('DD/MM/YYYY')
                    }
                  </span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('start') && (
                    <DateRangePicker
                      singleDatePicker
                      containerStyles={{ backgroundColor: '#ffffff' }}
                      onApply={(e, picker) => this.handleDateEvent(e, picker, 'start')}
                    >
                      <Button className="pick-date primary-color">{moment(updateEvents.start, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Button>
                    </DateRangePicker>
                  )
                }
              </div>
              <div className="primary-border-right">
                <span className="gray-text">Attendees</span>
                <span
                  className={`pointer i-block ${eventEditFields.includes('attendees') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('attendees')}
                >
                  <span className="primary-color text-bold text-font14">{updateEvents.attendees ? updateEvents.attendees : selectedEventInfo.attendees }</span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('attendees') && (
                    <input
                      className="form-control i-block input-edit"
                      type="text"
                      style={isNaN(updateEvents.attendees) && (!validationStatus) ? {border: '1px solid red'}: {border: '0'}}
                      onChange={e => this.onEventChange('attendees', e)}
                      value={updateEvents.attendees >= 0 ? updateEvents.attendees : ''}
                    />
                  )
                }
              </div>
            </Col>
            <Col lg={3} md={4} className="hidden-xs hidden-sm ipad-pro-disable">
              <div className="row1 primary-border-right">
                <span className="gray-text">Check-in</span>
                <span
                  className={`pointer i-block ${eventEditFields.includes('start') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('start')}
                >
                  <span className="primary-color text-bold text-font14">
                    {
                      updateEvents.start ? 
                      moment(updateEvents.start, 'YYYY-MM-DD').format('DD/MM/YYYY') :
                      moment(selectedEventInfo.start, 'YYYY-MM-DD').format('DD/MM/YYYY')
                    }
                  </span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('start') && (
                    <DateRangePicker
                      singleDatePicker
                      containerStyles={{ backgroundColor: '#ffffff' }}
                      onApply={(e, picker) => this.handleDateEvent(e, picker, 'start')}
                    >
                      <Button className="pick-date primary-color">{moment(updateEvents.start, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Button>
                    </DateRangePicker>
                  )
                }
              </div>
              <div>
                <span className="gray-text">Type of event</span>
                <span
                  className={`pointer i-block ${eventEditFields.includes('type') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('type')}
                >
                  <span className="primary-color text-bold text-font14">
                    {
                      updateEvents.type ? eventTypes.results.filter(eType => eType.id === updateEvents.type)[0].name : selectedEventInfo.type.name
                    }
                  </span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('type') && (
                    <select
                      className="event-name-dropdown align-event-type-dropdown text-font16 dropdown-event-type"
                      onChange={this.handleEventTypes}
                      value={selectedEventType ? selectedEventType : selectedEventInfo.type.id}
                    >
                    <option disabled hidden>not selected</option>
                    {
                      Object.keys(eventTypes).length !== 0 && eventTypes.results.map(type => (
                        <option
                          value={type.id}
                          id={type.id}
                        >
                          {type.name}
                        </option>
                      ))
                    }
                   </select>
                  )
                }
              </div>
            </Col>
            <Col lg={3} md={4} className="mob-col">
              <div className="row1 primary-border-right hidden-lg hidden-md ipad-pro-enable">
                <span className="gray-text">Type of event</span>
                <span
                  className={`pointer ${eventEditFields.includes('type') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('type')}
                >
                  <span className="primary-color text-bold text-font14">
                    {
                      updateEvents.type ? eventTypes.results.filter(eType => eType.id === updateEvents.type)[0].name : selectedEventInfo.type.name
                    }
                  </span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('type') && (
                    <select
                      className="event-name-dropdown align-event-type-dropdown text-font16 dropdown-event-type"
                      onChange={this.handleEventTypes}
                      value={selectedEventType ? selectedEventType : selectedEventInfo.type.id}
                    >
                    <option disabled hidden>not selected</option>
                    {
                      Object.keys(eventTypes).length !== 0 && eventTypes.results.map(type => (
                        <option
                          value={type.id}
                          id={type.id}
                        >
                          {type.name}
                        </option>
                      ))
                    }
                   </select>
                  )
                }
              </div>
              <div className="row1">
                <span className="gray-text">Check-out</span>
                <span
                  className={`pointer i-block ${eventEditFields.includes('end') ? 'hide' : ''}`}
                  onClick={() => this.onEditEvent('end')}
                >
                  <span className="primary-color text-bold text-font14">
                    {
                      updateEvents.end ? 
                      moment(updateEvents.end, 'YYYY-MM-DD').format('DD/MM/YYYY') :
                      moment(selectedEventInfo.end, 'YYYY-MM-DD').format('DD/MM/YYYY')
                    }
                  </span>
                  <span className="glyphicon glyphicon-pencil" />
                </span>
                {
                  eventEditFields.includes('end') && (
                    <DateRangePicker
                      singleDatePicker
                      containerStyles={{ backgroundColor: '#ffffff' }}
                      onApply={(e, picker) => this.handleDateEvent(e, picker, 'end')}
                    >
                      <Button className="pick-date primary-color">{moment(updateEvents.end, 'YYYY-MM-DD').format('DD/MM/YYYY')}</Button>
                    </DateRangePicker>
                  )
                }
              </div>
            </Col>
            <Col lg={4} md={4} className="tpadding10 hidden-xs hidden-sm">
              <div className="i-block primary-border-color chat-icon lmargin20 primary-color col-md-9 history-icon fright" onClick={() => this.showPopup(true)}>
                <span className="i-block rpadding10 visible-pointers">View event history</span>
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/history.svg" alt="chat icon" />
              </div>
            </Col>
          </Row>
          <Row className="hotel-info-wrapper primary-color">
            <Col lg={5} md={5} className="primary-color text-font14 primary-border-right">
              <p className="text-bold bpadding5">{"Hotel "+selectedEventHotelInfo.name}</p>
              <p className="hotel-text hidden-xs">{selectedEventHotelInfo.address}</p>
              <p className="hotel-text hidden-xs">{selectedEventHotelInfo.phone+' | '+selectedEventHotelInfo.email_address}</p>
            </Col>
            <div className="i-block primary-border-color loc-mob chat-icon primary-color col-md-7 location-icon tmargin20 fright hidden-md hidden-lg"
              onClick={() => this.viewInMap()}
              >
                <span className="i-block rpadding10 hidden-xs hidden-sm">View location</span>
                <span className="i-block mobile-iblock lmargin10"><strong>Location</strong></span>
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/red-location.svg" alt="localization" />
              </div>

            <Col lg={2} md={3} xs={5} className="tpadding10 primary-border-right room-info-class">
              <img src="dist/img/meeting_space/icons/superficie-gray-bg.png" alt="meeting icon" />
              <span className="gray-text meeting-room hidden-xs">Meeting rooms</span>
              <span className="gray-text meeting-room mobile-iblock">Event rooms :</span>
              <p className="primary-color capacity-room text-bold">{selectedEventHotelInfo.hasOwnProperty('meeting_rooms') ? selectedEventHotelInfo.meeting_rooms.length : ''+' rooms'}</p>
            </Col>
            <Col lg={5} md={4} xs={7} className="capacity-info-class">
              <Col md={12}  className="tpadding10">
                <img className="opacity5 people-icon" src="dist/img/meeting_space/icons/people-black-bg.png" alt="people icon" />
                <span className="gray-text meeting-room">Capacity <span className="mobile-iblock">:</span></span>
                <p className="primary-color capacity-room text-bold">{selectedEventHotelInfo.capacity+' guests'}</p>
              </Col>
              <div className="i-block primary-border-color hidden-xs hidden-sm chat-icon primary-color col-md-7 location-icon tmargin20 fright"
              onClick={() => this.viewInMap()}
              >
                <span className="i-block rpadding10">View location</span>
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/red-location.svg" alt="localization" />
              </div>
            </Col>
          </Row>
          <Row className="status-container hidden-xs">
            <Col lg={4} md={5} sm={5}>
              <span className="primary-color">Status:</span>
              <button type="button" className={`select-button lmargin25 text-dropdown dropdown-item-1 dropdown-toggle status-${selectedEventInfo.status.id}`} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {selectedEventInfo.status.event_status}&nbsp;&nbsp;<img class="select-icon" src="dist/img/event_list/drop-arrow-white.png" width="20px"/>
            </button>
            <div className="dropdown-menu">
            { Object.keys(eventStatus).length !== 0 && eventStatus.results.map((status)=>(
              status.name !== selectedEventInfo.status.name && status.id !== 1 ?
              (<div className="dropdown-item-1 ">
                <Button className={`button status-${status.id}`} onClick={() => this.handleDropdown(status, selectedEventInfo)}>{status.event_status}</Button>
              </div>) : ''
              ))
            }
            </div>
            </Col>
            <Col lg={8} md={7} sm={7} className="primary-color">
              <span>By:</span>
              <div className="user-details">
                <div className="user-initial-name">{historyInfo.length>=1 && historyInfo[0].user.first_name.charAt(0).toUpperCase()+historyInfo[0].user.last_name.charAt(0).toUpperCase()}</div>
                <span className="header-text-color-2 lpadding10"><strong>{Object.keys(userInfoOfLatestHistory).length >= 1 && userInfoOfLatestHistory.profile.roleId}</strong></span>
                <span className="lpadding10">{historyInfo.length>=1 && historyInfo[0].user.first_name+' '+historyInfo[0].user.last_name+', on '+moment(historyInfo[0].date, 'YYYY-MM-DD').format('DD/MM/YYYY')+'.'}</span>
              </div>
            </Col>
          </Row>

          <Row className="rfp-label">
             <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper hidden-xs">
                <div className="img-class">
                  <img src="dist/img/meeting-icon.png" alt="Guest Room" />
                </div>
                <div className="meeting-info-title">
                  Meeting Rooms Info
                </div>
                <div className="hyphen-class">
                  <hr />
                </div>
              </Col>
          </Row>
            <div className="meeting-room-list-container">
              <Row className="selected-meeting-room-list primary-color">
                <Col className="">
                  <Table className="text-center" responsive>
                    <thead>
                      <tr className="hidden-xs hidden-sm">
                        {
                          this.eventHeader.map(header => (
                            <td key={header}>
                              <span>{header}</span>
                            </td>
                          ))
                        }
                      </tr>
                      
                    </thead>
                    {
                      selectedRooms.length >= 1
                        ? (
                          <tbody>
                            {
                              selectedRooms.map((room, j) => (
                                room.setup.map((setup, i) => ([
                                  <tr
                                    width="100%"
                                    // style={setup.hasOwnProperty('enable3D') && setup.enable3D ? primaryBg : {}}
                                    key={`mobile${room.id} ${setup.setup_id}`}
                                    className={`${(i !== room.setup.length - 1
                                      || (setup.hasOwnProperty('addon') && setup.addon))
                                      ? 'no-box-shadow visible-xs visible-sm' : 'selected-room-list visible-xs visible-sm'}
                                      ${setup.hasOwnProperty('enable3D') && setup.enable3D ? 'primary-background-color' : 'white-background'}
                                      `}
                                  >
                                   <td colSpan="7">
                                    {
                                      // i === 0 ? 
                                      (
                                          <div className="prelative text-left">
                                            {/*<img src="dist/img/reactangle-radius.png" alt="Border" className="border-simulator" />*/}
                                            <ReactSVG
                                              wrapper="div"
                                              src="dist/img/reactangle-radius.svg"
                                              wrapper="span"
                                              alt="Border"
                                              className={`svg-wrapper border-simulator ${i === 0 ? '' : 'hide'}`}
                                            />
                                            <span
                                              className={`meeting-room-name i-block pointer ${setup.hasOwnProperty('enable3D')
                                                && setup.enable3D ? 'white-text' : ''} ${acitivityProgramEditFields[setup.setup_id]
                                                && acitivityProgramEditFields[setup.setup_id].includes('meeting_room') ? 'hide' : ''}`}
                                              onClick={() => this.onEditAP(setup, 'meeting_room')}
                                            >
                                              {changeSelectedRoom(room, setup)}
                                              <span className="glyphicon glyphicon-pencil" />
                                            </span>
                                            {
                                              acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('meeting_room') && (
                                                <select
                                                  className="room-name-dropdown event-name-dropdown text-font16 dropdown-event-type"
                                                  onChange={e => this.selectMeetingRoom(room.id, setup, 'meetingRoomId', e)}
                                                  value={setup.meetingRoomId ? setup.meetingRoomId : setup.meeting_room.id}
                                                >
                                                {
                                                  hotelMeetingRooms.length && hotelMeetingRooms.map(type => (
                                                    <option
                                                      value={type.id}
                                                      id={type.id}
                                                      selected={setup.meetingRoomId ? (type.id === setup.meetingRoomId) : (type.id === setup.meeting_room.id)}
                                                    >
                                                      {type.name}
                                                    </option>
                                                  ))
                                                }
                                                {
                                                  !hotelMeetingRooms.length && (
                                                    <option
                                                      value={setup.meeting_room.name}
                                                      selected
                                                    >
                                                      {setup.meeting_room.name}
                                                    </option>
                                                  )
                                                }
                                               </select>
                                            )
                                          }
                                          </div>
                                        )
                                      // : <div />
                                    }
                                    <div className="event-name">
                                      {
                                        <Button className="visible-pointers"
                                         onClick={()=>{this.handleSetupInfo(room, setup, 'services')}}
                                        >
                                          {
                                            setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                              <img
                                                className={`${setup.hasOwnProperty('addon') && setup.addon
                                                  ? '' : 'transform'}`}
                                                src="dist/img/meeting_space/icons/arrow-white.png"
                                                alt="arrow icon"
                                              />
                                            ) : (
                                              <ReactSVG
                                                wrapper="span"
                                                className={`svg-wrapper activity-toggle ${setup.hasOwnProperty('addon') && setup.addon
                                                  ? '' : 'transform'}`}
                                                src="dist/img/meeting_space/icons/arrow-icon.svg"
                                                alt="arrow icon"
                                              />
                                            )
                                          }
                                        </Button>   
                                      }
                                      <span
                                        className={`pointer ${acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('name') ? 'hide' : ''}`}
                                        onClick={() => this.onEditAP(setup, 'name')}
                                      >
                                        <span
                                          className={`text-bold ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {setup.activity_name || ''}
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </span>
                                      {
                                        acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('name') && (
                                          <input
                                            type="text"
                                            className="event name"
                                            placeholder="Activity name"
                                            onChange={e => this.handleActivityName(room.id, setup, 'activity_name', e)}
                                            value={setup.activity_name || ''}
                                            // onBlur={() => this.handleActivityName(room.id, setup.setup_id)}
                                          />
                                        )
                                      }
                                      
                                      <Button className="visible-pointers fright" 
                                      onClick={() => this.handleSetupInfo(room, setup, 'meetingRoomInfo')}
                                      style={setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                          ? {'margin-left': '0px'} : {'margin-left': '10px'}}
                                      >
                                      {
                                        setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                          <img
                                            className={`${setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                              ? '' : 'transform'}`}
                                            src="dist/img/meeting_space/icons/arrow-white.png"
                                            alt="arrow icon"
                                          />
                                        ) : (
                                            <ReactSVG wrapper="div" className={`svg-wrapper activity-toggle ${setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                          ? '' : 'transform'}`} src="dist/img/meeting_space/icons/arrow-down.svg" alt="arrow icon" />
                                          )
                                      }
                                      </Button>
                                    </div>
                                    <hr className="activity-separator" />
                                    
                                    <div className= {`${setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo ? 'hide' : ''}`}>
                                      <div
                                        className={`pointer ${acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('start') ? 'hide' : 'text-left align-date-edit'}`}
                                        onClick={() => this.onEditAP(setup, 'start')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_date_starts} `}
                                        </span>
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_time_starts} `}
                                        </span>
                                        <span
                                          className={`i-block ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          h.
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </div>
                                      {
                                        acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('start') && (
                                          <div className="date-time-field">
                                            <DateRangePicker
                                              singleDatePicker
                                              containerStyles={(setup.event_date_starts === '') ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                              : { backgroundColor: '#ffffff'}}
                                              minDate={moment()}
                                              onApply={(e, picker) => this.handleDateAP(
                                                room.id, setup, e, picker, 'event_date_starts',
                                              )}
                                            >
                                              <Button
                                                className='primary-color'
                                                // style={{ color: hotelInfo.data.primary_color }}
                                              >
                                                {
                                                  setup.hasOwnProperty('event_date_starts') && setup.event_date_starts ? (
                                                    <span>
                                                      {`${setup.event_date_starts} `}
                                                    </span>
                                                  ) : '- - / - - / - -'
                                                }
                                              </Button>
                                            </DateRangePicker>
                                            <Datetime
                                              dateFormat={false}
                                              inputProps={{readonly:'readonly'}}
                                              timeFormat="HH:mm"
                                              className={setup.event_time_starts === '' ? 'red-border' : ''}
                                              value={setup.event_time_starts ? setup.event_time_starts : '- - : - -'}
                                              onChange={e => this.handleTimeChange(room.id, setup, 'event_time_starts', e)}
                                            />
                                            <span
                                              className={`i-block hour-class ${setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-text' : 'primary_color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { color: '#fcfcfc' } : { color: hotelInfo.data.primary_color }}
                                            >
                                              h.
                                            </span>
                                          </div>
                                        )
                                      }
                                      <div
                                        className={`pointer ${acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('end') ? 'hide' : 'text-left align-date-edit'}`}
                                        onClick={() => this.onEditAP(setup, 'end')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_date_ends} `}
                                        </span>
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_time_ends} `}
                                        </span>
                                        <span
                                          className={`i-block ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          h.
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </div>
                                      {
                                        acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('end') && (
                                          <div className="date-time-field">
                                            <DateRangePicker
                                              singleDatePicker
                                              inputProps={{style:{border: '2px solid red'}}}
                                              containerStyles={setup.event_date_ends === '' ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                              : { backgroundColor: '#ffffff'}}
                                              minDate={moment()}
                                              onApply={(e, picker) => this.handleDateAP(
                                                room.id, setup, e, picker, 'event_date_ends',
                                              )}
                                            >
                                              <Button
                                                className='primary-color'
                                                // style={{ color: hotelInfo.data.primary_color }}
                                              >
                                                {
                                                  setup.hasOwnProperty('event_date_ends') && setup.event_date_ends ? (
                                                    <span>
                                                      {`${setup.event_date_ends} `}
                                                    </span>
                                                  ) : '- - / - - / - -'
                                                }
                                              </Button>
                                            </DateRangePicker>
                                            <Datetime
                                              dateFormat={false}
                                              timeFormat="HH:mm"
                                              className={setup.event_time_starts === '' ? 'red-border' : ''}
                                              inputProps={{readonly:'readonly'}}
                                              value={setup.event_time_ends ? setup.event_time_ends : '- - : - -'}
                                              onChange={e => this.handleTimeChange(room.id, setup, 'event_time_ends', e)}
                                            />
                                            <span
                                              className={`i-block hour-class ${setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-text' : 'primary_color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { color: '#fcfcfc' } : { color: hotelInfo.data.primary_color }}
                                            >
                                              h.
                                            </span>
                                          </div>
                                        )
                                      }
                                      <div
                                        className={`pointer ${acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('attendees') ? 'hide' : 'text-left align-date-edit'}`}
                                        onClick={() => this.onEditAP(setup, 'attendees')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {setup.chairs_count || ''}
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </div>
                                      {
                                        acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('attendees') && (
                                          <div className="attendee-count">
                                            <input
                                              type="text"
                                              id="attendees"
                                              className="event count form-control"
                                              style={((!setup.chairs_count) || isNaN(setup.chairs_count)) && (!validationStatus) ? {border: '2px solid red'}: {border: '0'}}
                                              placeholder="––"
                                              onChange={e => this.handleInputs(room, setup, e)}
                                              value={setup.chairs_count >= 0 ? setup.chairs_count : ''}
                                            />
                                          </div>
                                        )
                                      }
                                      
                                      <div className="clearfix" />
                                      <div className="fleft lmargin20 tmargin10">
                                        <Button
                                          className={`share-btn-rfp ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-border' : ''}`}
                                          onClick={() => this.handleShareInfo(true)}
                                        >
                                          <ReactSVG
                                            wrapper="span"
                                            className={`svg-wrapper ${setup.hasOwnProperty('enable3D')
                                              && setup.enable3D ? 'white-fill' : ''}`}
                                            src="dist/img/meeting_space/icons/share-icon@3x.svg" 
                                            alt="share"
                                          />
                                        </Button>
                                        <Button
                                          className={`clone-btn ${setup.hasOwnProperty('enable3D') && setup.enable3D
                                            ? 'white-border' : 'primary-border-color'}`}
                                          onClick={() => this.handleClone(room, setup, 'clone')}
                                          // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                          //   ? { borderColor: '#fcfcfc' } : { borderColor: hotelInfo.data.primary_color }}
                                        >
                                          {
                                            setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                              <img src="dist/img/meeting_space/icons/clone-white@3x.png" alt="clone" />
                                            ) : (
                                              /*<img src="dist/img/meeting_space/icons/clone@3x.png" alt="clone" />*/
                                             <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/clone@3x.svg" alt="clone" />
                                            )
                                          }
                                        </Button>
                                      </div>
                                      <div className="fleft lmargin10 tmargin10 bmargin10mob ">
                                        {
                                          room.available_model ? (
                                            
                                            <Button
                                              className={`edit-btn visible-pointers ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-border' : 'primary-background-color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { border: '1px solid #fcfcfc' }
                                              //   : { backgroundColor: hotelInfo.data.primary_color }}
                                              onClick={() => this.handle3Dpage(room, setup)}
                                            >
                                              <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                            </Button>
                                            
                                          ) : (
                                            <Button
                                              className={`edit-btn visible-pointers ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-border' : 'primary-background-color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { border: '1px solid #fcfcfc' }
                                              //   : { backgroundColor: hotelInfo.data.primary_color }}
                                              onClick={() => this.handle3Dpage(room, setup)}
                                            >
                                              <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                            </Button>
                                          )
                                        }
                                      </div>
                                      <div className="fleft lmargin10 tmargin10">
                                        <Button
                                          className="delete-btn primary-border-left"
                                          onClick={() => this.deleteAP(room, setup)}
                                        >
                                          {
                                            setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                              <img src="dist/img/meeting_space/icons/delete-icon-white@3x.png" alt="delete" />
                                            ) : (
                                              /*<img src="dist/img/meeting_space/icons/delete-red@3x.png" alt="delete" />*/
                                              <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                                            )
                                          }
                                        </Button>
                                      </div>
                                    </div>
                                   </td>
                                  </tr>,
                                  <tr
                                    // style={setup.hasOwnProperty('enable3D') && setup.enable3D ? primaryBg : {}}
                                    key={`${room.id} ${setup.setup_id}`}
                                    className={`${(i !== room.setup.length - 1
                                      || (setup.hasOwnProperty('addon') && setup.addon))
                                      ? 'no-box-shadow hidden-xs hidden-sm' : 'selected-room-list hidden-sm hidden-xs'}
                                      ${setup.hasOwnProperty('enable3D') && setup.enable3D ? 'primary-background-color' : 'white-background'}
                                      `}
                                  >
                                    {
                                      // i === 0
                                      //   ? (
                                        (
                                          <td className="prelative selected-event-name">
                                            <ReactSVG
                                              wrapper="div"
                                              src="dist/img/reactangle-radius.svg"
                                              wrapper="span"
                                              alt="Border"
                                              className={`svg-wrapper border-simulator ${i === 0 ? '' : 'hide'} ${(acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('start')) || (acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('end')) ? 'align-border' : ''}`}
                                            />
                                            <span
                                              className={`meeting-room-name i-block pointer ${setup.hasOwnProperty('enable3D')
                                                && setup.enable3D ? 'white-text' : ''} ${acitivityProgramEditFields[setup.setup_id]
                                                && acitivityProgramEditFields[setup.setup_id].includes('meeting_room') ? 'hide' : ''}`}
                                              onClick={() => this.onEditAP(setup, 'meeting_room')}
                                            >
                                              {changeSelectedRoom(room, setup)}
                                              <span className="glyphicon glyphicon-pencil" />
                                            </span>
                                            {
                                              acitivityProgramEditFields[`${setup.setup_id}`] && acitivityProgramEditFields[`${setup.setup_id}`].includes('meeting_room') && (
                                                <select
                                                  className="room-name-dropdown text-font16 dropdown-event-type "
                                                  onChange={e => this.selectMeetingRoom(room.id, setup, 'meetingRoomId', e)}
                                                  value={setup.meetingRoomId ? setup.meetingRoomId : setup.meeting_room.id}
                                                >
                                                {
                                                  hotelMeetingRooms.length && hotelMeetingRooms.map(type => (
                                                    <option
                                                      value={type.id}
                                                      id={type.id}
                                                      selected={setup.meetingRoomId ? (type.id === setup.meetingRoomId) : (type.id === setup.meeting_room.id)}
                                                    >
                                                      {type.name}
                                                    </option>
                                                  ))
                                                }
                                                {
                                                  !hotelMeetingRooms.length && (
                                                    <option
                                                      value={setup.meeting_room.name}
                                                      selected
                                                    >
                                                      {setup.meeting_room.name}
                                                    </option>
                                                  )
                                                }
                                               </select>
                                            )
                                          }
                                          </td>
                                        )
                                        // ) : <td />
                                    }
                                    
                                    <td className="event-name">
                                      {
                                        <Button className="visible-pointers"
                                        onClick={()=>{this.handleSetupInfo(room, setup, 'services')}}
                                        >
                                          {
                                            setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                              <img
                                                className={`${setup.hasOwnProperty('addon') && setup.addon
                                                  ? '' : 'transform'}`}
                                                src="dist/img/meeting_space/icons/arrow-white.png"
                                                alt="arrow icon"
                                              />
                                            ) : (
                                              <ReactSVG
                                                wrapper="span"
                                                className={`svg-wrapper ${setup.hasOwnProperty('addon') && setup.addon
                                                  ? '' : 'transform'}`}
                                                src="dist/img/meeting_space/icons/arrow-icon.svg"
                                                alt="arrow icon"
                                              />
                                            )
                                          }
                                        </Button>
                                      }
                                      <span
                                        className={`pointer ${acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('name') ? 'hide' : ''}`}
                                        onClick={() => this.onEditAP(setup, 'name')}
                                      >
                                        <span
                                          className={`text-bold ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {setup.activity_name || ''}
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </span>
                                      
                                      {
                                        acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('name') && (
                                          <input
                                            type="text"
                                            className="event name"
                                            placeholder="Activity name"
                                            onChange={e => this.handleActivityName(room.id, setup, 'activity_name', e)}
                                            value={setup.activity_name || ''}
                                            // onBlur={() => this.handleActivityName(room.id, setup.setup_id)}
                                          />
                                        )
                                      }
                                      
                                    </td>
                                    <td className="date-time-field primary-color">
                                      <span
                                        className={`pointer ${acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('start') ? 'hide' : ''}`}
                                        onClick={() => this.onEditAP(setup, 'start')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_date_starts} `}
                                        </span>
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_time_starts} `}
                                        </span>
                                        <span
                                          className={`i-block ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          h.
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </span>
                                      {
                                        acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('start') && (
                                          <div className="date-time-field">
                                            <DateRangePicker
                                              singleDatePicker
                                              containerStyles={setup.event_date_starts === '' ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                              : { backgroundColor: '#ffffff'}}
                                              minDate={moment()}
                                              onApply={(e, picker) => this.handleDateAP(
                                                room.id, setup, e, picker, 'event_date_starts',
                                              )}
                                            >
                                              <Button
                                                className="primary-color align-date-edit"
                                                // style={{ color: hotelInfo.data.primary_color }}
                                              >
                                                {
                                                  setup.hasOwnProperty('event_date_starts') && setup.event_date_starts ? (
                                                    <span>
                                                      {`${setup.event_date_starts} `}
                                                    </span>
                                                  ) : '- - / - - / - -'
                                                }
                                              </Button>
                                            </DateRangePicker>
                                            <Datetime
                                              dateFormat={false}
                                              inputProps={{readonly:'readonly'}}
                                              timeFormat="HH:mm"
                                              className={setup.event_time_starts === '' ? 'red-border' : ''}
                                              value={setup.event_time_starts ? setup.event_time_starts : '- - : - -'}
                                              onChange={e => this.handleTimeChange(room.id, setup, 'event_time_starts', e)}
                                            />
                                            <span
                                              className={`i-block hour-class ${setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-text' : 'primary_color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { color: '#fcfcfc' } : { color: hotelInfo.data.primary_color }}
                                            >
                                              h.
                                            </span>
                                          </div>
                                        )
                                      }
                                    </td>
                                    <td className="date-time-field primary-color">
                                      <span
                                        className={`pointer ${acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('end') ? 'hide' : ''}`}
                                        onClick={() => this.onEditAP(setup, 'end')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_date_ends} `}
                                        </span>
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {`${setup.event_time_ends} `}
                                        </span>
                                        <span
                                          className={`i-block ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          h.
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </span>
                                      {
                                        acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('end') && (
                                          <div className="date-time-field">
                                            <DateRangePicker
                                              singleDatePicker
                                              inputProps={{style:{border: '2px solid red'}}}
                                              containerStyles={(setup.event_date_ends === '') ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                              : { backgroundColor: '#ffffff'}}
                                              minDate={moment()}
                                              onApply={(e, picker) => this.handleDateAP(
                                                room.id, setup, e, picker, 'event_date_ends',
                                              )}
                                            >
                                              <Button
                                                className="primary-color align-date-edit"
                                                // style={{ color: hotelInfo.data.primary_color }}
                                              >
                                                {
                                                  setup.hasOwnProperty('event_date_ends') && setup.event_date_ends ? (
                                                    <span>
                                                      {`${setup.event_date_ends} `}
                                                    </span>
                                                  ) : '- - / - - / - -'
                                                }
                                              </Button>
                                            </DateRangePicker>
                                            <Datetime
                                              dateFormat={false}
                                              timeFormat="HH:mm"
                                              className={setup.event_time_starts === '' ? 'red-border' : ''}
                                              inputProps={{readonly:'readonly'}}
                                              value={setup.event_time_ends ? setup.event_time_ends : '- - : - -'}
                                              onChange={e => this.handleTimeChange(room.id, setup, 'event_time_ends', e)}
                                            />
                                            <span
                                              className={`i-block hour-class ${setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'white-text' : 'primary_color'}`}
                                              // style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                              //   ? { color: '#fcfcfc' } : { color: hotelInfo.data.primary_color }}
                                            >
                                              h.
                                            </span>
                                          </div>
                                        )
                                      }
                                    </td>
                                    <td>
                                      <span
                                        className={`pointer ${acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('attendees') ? 'hide' : ''}`}
                                        onClick={() => this.onEditAP(setup, 'attendees')}
                                      >
                                        <span
                                          className={`${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-text' : 'primary-color'}`}
                                        >
                                          {setup.chairs_count || ''}
                                        </span>
                                        <span className={`glyphicon glyphicon-pencil ${ setup.hasOwnProperty('enable3D') && setup.enable3D
                                                ? 'color-white' : 'primary-color'}`} />
                                      </span>
                                      {
                                        acitivityProgramEditFields[setup.setup_id] && acitivityProgramEditFields[setup.setup_id].includes('attendees') && (
                                          <div className="attendee-count">
                                            <input
                                              type="text"
                                              id="attendees"
                                              className="event count form-control"
                                              style={((!setup.chairs_count) || isNaN(setup.chairs_count)) && (!validationStatus) ? {border: '2px solid red'}: {border: '0'}}
                                              placeholder="––"
                                              onChange={e => this.handleInputs(room, setup, e)}
                                              value={setup.chairs_count >= 0 ? setup.chairs_count : ''}
                                            />
                                          </div>
                                        )
                                      }
                                    </td>
                                    <td className="room-setup-dropdown">
                                      <Button
                                        className={`share-btn-rfp ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-border' : ''}`}
                                        onClick={() => this.handleShareInfo(true)}
                                      >
                                        <ReactSVG
                                          wrapper="span"
                                          className={`svg-wrapper ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-fill' : ''}`}
                                          src="dist/img/meeting_space/icons/share-icon@3x.svg" 
                                          alt="share"
                                        />
                                      </Button>
                                    </td>
                                    <td>
                                      <Button
                                        className={`clone-btn ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-border' : ''}`}
                                        onClick={() => this.handleClone(room, setup, 'clone')}
                                      >
                                        {
                                          setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                            <img src="dist/img/meeting_space/icons/clone-white@3x.png" alt="clone" />
                                          ) : (
                                            <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/clone@3x.svg" alt="clone" />
                                          )
                                        }
                                      </Button>
                                    </td>
                                    <td>
                                      {
                                        <Button
                                          className={`edit-btn primary-background-color ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-border' : ''}`}
                                          onClick={() => this.handle3Dpage(room, setup)}
                                        >
                                          <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                        </Button>
                                      }
                                    </td>
                                    <td>
                                      <Button
                                        className={`delete-btn ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-border' : 'primary-border-left'}`}
                                        onClick={() => this.deleteAP(room, setup)}
                                      >
                                        <ReactSVG
                                          wrapper="span"
                                          className={`svg-wrapper ${setup.hasOwnProperty('enable3D')
                                            && setup.enable3D ? 'white-fill' : ''}`}
                                          src="dist/img/meeting_space/icons/delete-red@3x.svg"
                                          alt="delete"
                                        />
                                      </Button>
                                    </td>
                                  </tr>,
                                  <React.Fragment>
                                    { ((setup.hasOwnProperty('addon') && setup.addon) || isScreenShot) ? 
                                      ([
                                        <tr className={'no-box-shadow'}>
                                          <td className="apply-bg prelative">
                                            <div className="service-col text-left addon-header ">
                                              <span>
                                                Services
                                              </span>
                                            </div>
                                          </td>
                                          
                                          <td colSpan="7" className="service-col">
                                            <tr className="service-container">
                                              {
                                               
                                                selectedEventHotelInfo.meeting_rooms.filter((meetingRoom)=> meetingRoom.id == room.id )[0].services.map((service)=> 
                                                  <div
                                                    className="services"
                                                    onClick={() => this.handleServices(room, setup, service, 'service')}
                                                  >
                                                    {service.type.name}
                                                    {
                                                (setup.hasOwnProperty('services_ids')
                                                  && setup.services_ids.indexOf(service.id) > -1 && (
                                                    <ReactSVG
                                                      wrapper="div"
                                                      className="svg-wrapper checkmark-img-selected pointer primary-background-color"
                                                      src="dist/img/meeting_space/icons/checkmark.svg"
                                                      alt="checkmark"
                                                    />)
                                                )
                                                    }
                                                  </div>
                                                )
                                                
                                              } 
                                              {
                                                selectedEventHotelInfo.meeting_rooms.filter((meetingRoom)=> meetingRoom.id == room.id )[0].services < 1 && (
                                                <div className="services-unavailable">
                                                 No services available
                                                </div>
                                                )
                                              }
                                            </tr>
                                          </td>
                                        </tr>,
                                      ]) : ''
                                    }
                                  {
                                  //   <React.Fragment>
                                  //   { setup.hasOwnProperty('addon') && setup.addon ?
                                  //     ([
                                  //       <tr className={'no-box-shadow'}>
                                  //         <td className="apply-bg" />
                                  //         <td className="apply-bg prelative">
                                  //           <div className="service-col text-left addon-header ">
                                  //             <span>
                                  //               Foods and beverages
                                  //             </span>
                                  //           </div>
                                  //         </td>
                                  //         <td colSpan="7" className="service-col">
                                  //           <tr className="service-container">
                                  //             {
                                  //               foodBeverages.map(food => (
                                  //                 <div
                                  //                   className="services"
                                  //                 >
                                  //                   {food}
                                  //                 </div>
                                  //               ))
                                  //             }
                                  //           </tr>
                                  //         </td>
                                  //       </tr>,
                                  //     ]) : ''
                                  //   }
                                  // </React.Fragment>,
                                  }
                                  </React.Fragment>,
                                  <React.Fragment>
                                    {setup.hasOwnProperty('addon') && setup.addon? (
                                      [
                                        <tr className={'no-box-shadow'}>
                                          <td className="apply-bg prelative">
                                            <div className="service-col text-left addon-header ">
                                              <span>
                                                Pre-Set up
                                              </span>
                                            </div>
                                          </td>
                                          <td colSpan="7" className="service-col">
                                            <tr className="service-container">
                                            {
                                              setupTypes.map((setup_type, k) => (
                                                <div
                                                  className={`setup-button-responsive setup-btn-container text-center ${setup_type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase() === setup.type.replace(/[^a-zA-Z]+/g, '').toLowerCase() ? 'no-btn-opacity' : ''}`}
                                                  style={k === 0 ? { paddingLeft: '0px' } : {}}
                                                >
                                                  <ReactSVG wrapper="div" className="svg-wrapper setup-image" src={setUpImages.hasOwnProperty(setup_type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase()) ? setUpImages[setup_type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase()] : ''} alt="setup icon" />
                                                  <p className="text-center tmargin10">{setup_type.name}</p>
                                                  <div>
                                                    <Button>
                                                      {meetingRoomInfo.length >=1
                                                      && meetingRoomInfo.filter((each)=> each.setUpId == setup.id) && meetingRoomInfo.filter((each)=> each.setUpId == setup.id).length >= 1 && meetingRoomInfo.filter((each)=> each.setUpId == setup.id)[0].room_setups.filter((setup)=> (setup.type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase()) === (setup_type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase())).length >=1 
                                                      && meetingRoomInfo.filter((each)=> each.setUpId == setup.id) && meetingRoomInfo.filter((each)=> each.setUpId == setup.id).length >= 1 && meetingRoomInfo.filter((each)=> each.setUpId == setup.id)[0].room_setups.filter((setup)=> (setup.type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase()) === (setup_type.name.replace(/[^a-zA-Z]+/g, '').toLowerCase()))[0].capacity 
                                                      }
                                                      <img
                                                        className={`person-image ${setup_type.capacity === 'not available' ? 'hide' : ''}`}
                                                        src={`dist/img/meeting_space/icons/person.png`}
                                                        alt="person icon"
                                                      />
                                                    </Button>
                                                  </div>
                                                </div>
                                              ))
                                            }
                                            </tr>
                                          </td>
                                        </tr>,
                                      ]) : ''
                                  }
                                  </React.Fragment>,
                                  <React.Fragment>
                                    {
                                      i === room.setup.length - 1
                                        && <tr className="i-block tmargin10" key={`${room.id} ${room.room_name}`} />
                                    }
                                  </React.Fragment>,
                                ]))
                              ))
                            }
                          </tbody>)
                        : (
                          <tbody className="no-meeting-room">
                            <tr>
                              <td colSpan="9" className="mobile-nomeeting">No Meeting rooms selected yet</td>
                            </tr>
                          </tbody>
                        )
                    }
                  </Table>
                </Col>
              </Row>
            </div>
              {
                isRoomView && (
                  <HotelPage
                    isOnlyView
                    hotelProperty={property}
                    key={`${selected3DViewRoom.id}${selectedSetup.id}${selectedSetup.setup_id}`}
                    hide3DView={hide3DView}
                  />
                )
              }
            <div
              className={`submit-container prelative primary-color`}
            >
              <div className="overlay" />
              <div className={`room-info-sections`}>
                <Row className={`rfp-label`}>
                  <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
                    <div className="img-class">
                      <img src="dist/img/guest-room.png" alt="Guest Room" />
                    </div>
                    <div className="meeting-info-title">
                      Guests Rooms Info
                    </div>
                    <div className="hyphen-class">
                      <hr />
                    </div>
                  </Col>
                </Row>
                <div className="box-rfp">
                  {
                    guestInfo.length >= 1 && (
                      <div>
                        {
                          guestInfo.map(guest => ([
                            <Row className="guest-room-section border-right bpadding20" key={guest.id}>
                              <Col xs={12} sm={12} md={3} lg={3} className="no-padding prelative">
                                <span className="meeting-info-label align-label">Arrival</span>
                                <span
                                  className={`pointer ${guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('start') ? 'hide' : ''}`}
                                  onClick={() => this.onEditGuestRoom(guest.tempID, 'start')}
                                >
                                 {
                                  guest.hasOwnProperty('start') && guest.start ? (
                                    <div className="i-block">
                                     <span className="meeting-info-value pointer">
                                       {moment(guest.start).format('ddd, MMM DD YYYY')}
                                     </span>
                                     <span className="glyphicon glyphicon-pencil" />
                                    </div>
                                  ):(
                                    <DateRangePicker
                                      singleDatePicker
                                      containerClass="arrival-input"
                                      minDate={moment()}
                                      onApply={(e, picker) => this.handleDate(guest.tempID, 'start', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                                    >
                                     <input 
                                      className="form-control" 
                                      style={(guest.start === '' && (!validationStatus)) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}
                                      type="button"/>
                                    </DateRangePicker>
                                  )
                                 }
                                  
                                </span>
                                {
                                  guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('start') && (
                                    <DateRangePicker
                                      singleDatePicker
                                      containerClass="arrival-input"
                                      minDate={moment()}
                                      onApply={(e, picker) => this.handleDate(guest.tempID, 'start', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                                    >
                                      <Button
                                        className="primary-color white-background"
                                        style={(guest.start === '' && (!validationStatus)) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}
                                      >
                                        {
                                          guest.hasOwnProperty('start') && guest.start ? (
                                            <span>
                                              {`${moment(guest.start).format('ddd, MMM DD YYYY')}`}
                                            </span>
                                          ) : '- - / - - / - -'
                                        }
                                      </Button>
                                    </DateRangePicker>
                                  )
                                }
                                <Button
                                  className="delete-btn fright visible-xs"
                                  onClick={() => this.deleteGuestInfo(guest)}
                                >
                                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                                </Button>
                              </Col>
                              <Col xs={12} sm={12} md={4} lg={4} className="no-padding">
                                <span className="meeting-info-label align-label">Departure</span>
                                <span
                                  className={`pointer ${guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('end') ? 'hide' : ''}`}
                                  onClick={() => this.onEditGuestRoom(guest.tempID, 'end')}
                                >
                                 {
                                   guest.hasOwnProperty('end') && guest.end ? (
                                    <div className="i-block">
                                     <span className="meeting-info-value pointer">
                                        {moment(guest.end).format('ddd, MMM DD YYYY')}
                                     </span>
                                     <span className="glyphicon glyphicon-pencil" />
                                    </div>
                                   ) : (
                                    <DateRangePicker
                                      singleDatePicker
                                      containerClass="departure-input"
                                      minDate={moment()}
                                      onApply={(e, picker) => this.handleDate(guest.tempID, 'end', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                                    >
                                     <input 
                                      className="form-control"
                                      style={(guest.start === '' && (!validationStatus)) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}
                                      type="button"/>
                                    </DateRangePicker>
                                   )
                                 }
                                  
                                </span>
                                {
                                  guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('end') && (
                                    <DateRangePicker
                                      singleDatePicker
                                      containerClass="departure-input"
                                      minDate={moment()}
                                      onApply={(e, picker) => this.handleDate(guest.tempID, 'end', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                                    >
                                      <Button
                                        className="primary-color white-background"
                                        style={(guest.start === '' && (!validationStatus)) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}
                                      >
                                        {
                                          guest.hasOwnProperty('end') && guest.end ? (
                                            <span>
                                              {`${moment(guest.end).format('ddd, MMM DD YYYY')} `}
                                            </span>
                                          ) : '- - / - - / - -'
                                        }
                                      </Button>
                                    </DateRangePicker>
                                  )
                                }
                              </Col>
                              <Col xs={12} sm={12} md={2} lg={2} className="no-padding">
                                <span className="meeting-info-label align-label">Single rooms</span>
                                <span
                                  className={`pointer ${guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('single_rooms') ? 'hide' : ''}`}
                                  onClick={() => this.onEditGuestRoom(guest.tempID, 'single_rooms')}
                                >
                                    {
                                      guest.single_rooms >= 0 && !guest.create ? 
                                      (
                                        <span
                                          className="meeting-info-value"
                                        > 
                                         <span>{guest.single_rooms}
                                         </span>
                                         <span className="glyphicon glyphicon-pencil" />
                                        </span>
                                      ): 
                                      (
                                        <input
                                          type="text"
                                          className="form-control i-block single-room-input"
                                          style={(guest.single_rooms === '' || isNaN(guest.single_rooms)) && (!validationStatus) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}  
                                          value={guest.single_rooms >= 0 ? guest.single_rooms : ''}
                                          onChange={e => this.handleChange(guest.tempID, e)}
                                          name="single_rooms"
                                        />
                                      )
                                    }
                                </span>
                                {
                                  guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('single_rooms') && (
                                    <input
                                      type="text"
                                      className="form-control i-block single-room-input"
                                      style={(guest.single_rooms === '' || isNaN(guest.single_rooms)) && (!validationStatus) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}  
                                      value={guest.single_rooms >= 0 ? guest.single_rooms : ''}
                                      onChange={e => this.handleChange(guest.tempID, e)}
                                      name="single_rooms"
                                    />
                                  )
                                }
                                
                              </Col>
                              <Col xs={12} sm={12} md={3} lg={3} className="no-padding">
                                <span className="meeting-info-label">Double rooms</span>
                                <Button
                                  className="delete-btn fright hidden-xs"
                                  onClick={() => this.deleteGuestInfo(guest)}
                                >
                                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                                </Button>
                                <span
                                  className={`pointer ${guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('double_rooms') ? 'hide' : ''}`}
                                  onClick={() => this.onEditGuestRoom(guest.tempID, 'double_rooms')}
                                >
                                    {
                                      guest.double_rooms >= 0 && !guest.create ? (
                                        <span
                                          className="meeting-info-value"
                                        >
                                         <span>{guest.double_rooms}
                                         </span>
                                         <span className="glyphicon glyphicon-pencil" />
                                        </span>
                                      ) : 
                                      (
                                        <input
                                          type="text"
                                          className="form-control i-block double-rooms-input"
                                          style={(guest.double_rooms === '' || isNaN(guest.double_rooms)) && (!validationStatus) ? {border: '2px solid red'} : {border: '1px solid #00000040'}}  
                                          value={guest.double_rooms >= 0 ? guest.double_rooms : ''}
                                          id={guest.id}
                                          onChange={e => this.handleChange(guest.tempID, e)}
                                          name="double_rooms"
                                        />
                                      )
                                    }
                                  </span>
                                
                                {
                                  guestRoomEditFields[guest.tempID] && guestRoomEditFields[guest.tempID].includes('double_rooms') && (
                                    <input
                                      type="text"
                                      className="form-control i-block double-rooms-input"
                                      style={(guest.double_rooms === '' || isNaN(guest.double_rooms)) && (!validationStatus) ? {border: '2px solid red'} : {border: '1px solid #00000040'}}  
                                      value={guest.double_rooms >= 0 ? guest.double_rooms : ''}
                                      id={guest.id}
                                      onChange={e => this.handleChange(guest.tempID, e)}
                                      name="double_rooms"
                                    />
                                  )
                                }
                              </Col>
                            </Row>,
                          ]))
                        }
                      </div>
                    )
                  }
                  {
                    guestInfo.length <= 0 && (
                      <div className="text-center tmargin20 bpadding20">No Guest rooms requested</div>
                    )
                  }
                  <Row>
                    <Col xs={12} sm={12} md={12} lg={12} className="lpadding0">
                      <div className="add-items-wrapper">
                        <div className="add-items lpadding0">
                          <div
                            className="i-block new-meeting-room"
                            onClick={() => this.handleNewPeriod()}
                          >
                            <img
                              className="add-image primary-background-color"
                              src="dist/img/meeting_space/icons/plus.png"
                              alt="plus"
                            />
                            <span>
                              Add New Period
                            </span>
                          </div>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className={`room-info-sections tmargin20`} id="contact-info-section">
                <Row className={`rfp-label`}>
                  <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
                    <div className="img-class">
                      <img src="dist/img/contact-icon.png" alt="Contact Info" />
                    </div>
                    <div className={`meeting-info-title rfp-label`}>
                      Contact Info
                    </div>
                    <div className="hyphen-class">
                      <hr />
                    </div>
                  </Col>
                </Row>
                <div className="box-rfp">
                  <Row className="contact-info-row">
                    <Col md={6} lg={6} xs={12} className="required-contact-info">
                      <span className="contact-sub-title block">Contact details</span>
                      <div className="i-block col-md-12 col-xs-12">
                        <span className="meeting-info-label">Name</span>
                        <span
                          className={`pointer ${contactEditFields.includes('name') ? 'hide' : ''}`}
                          onClick={() => this.onEditContact('name')}
                        >
                          <span className={`align-email contact-input ${contact.name ? '' : 'hide'}`}>{contact.name}</span>
                          <span className="glyphicon glyphicon-pencil" />
                        </span>
                        {
                          contactEditFields.includes('name') && (
                             <input
                              type="text"
                              className={`form-control i-block contact-input`}
                              style={nameValid(contact.name) && (!validationStatus) ? {border: '1px solid red'}: {border: '0'}}
                              onChange={e => this.handleContactChange('name', e)}
                              value={contact.name}
                            />
                          )
                        }
                        <br />
                        <span className="meeting-info-label">Email</span>
                        <span
                          className={`pointer ${contactEditFields.includes('mail') ? 'hide' : ''}`}
                          onClick={() => this.onEditContact('mail')}
                        >
                          <span className={`align-email tpadding10 contact-input ${contact.email ? '' : 'hide'}`}>{contact.email}</span>
                          <span className="glyphicon glyphicon-pencil" />
                        </span>
                        {
                          contactEditFields.includes('mail') && (
                            <input
                              type="text"
                              className={`form-control i-block contact-input`}
                              style={emailValid(contact.email) && (!validationStatus) ? {border: '1px solid red'}: {border: '0'}}
                              onChange={e => this.handleContactChange('email', e)}
                              value={contact.email}
                            />
                          )
                        }
                        <br />
                        <span className="meeting-info-label">Phone</span>
                        <span
                          className={`pointer ${contactEditFields.includes('phone') ? 'hide' : ''}`}
                          onClick={() => this.onEditContact('phone')}
                        >
                          <span className={`align-email contact-input ${contact.phone ? '' : 'hide'}`}>{contact.phone}</span>
                          <span className="glyphicon glyphicon-pencil" />
                        </span>
                        {
                          contactEditFields.includes('phone') && (
                             <input
                              type="text"
                              className={`form-control i-block contact-input`}
                              style={phoneValid(contact.phone) && (!validationStatus) ? {border: '1px solid red'}: {border: '0'}}
                              onChange={e => this.handleContactChange('phone', e)}
                              value={contact.phone}
                            />
                          )
                        }
                      </div>
                    </Col>
                    <Col md={6} lg={6} className="fix-top-rfp-contact-box">
                      <span className="contact-sub-title block tmargin20">Address</span>
                      <br />
                      <input
                        type="radio"
                        className="rmargin20"
                        id="business"
                        onChange={() => this.handleCheck('business')}
                        checked={contact.contact_type === 'business'}
                      />
                      <label htmlFor="business" className="i-block width50 tmargin20 business">Business</label>
                      <input
                        type="radio"
                        id="personal"
                        className="rmargin20"
                        onChange={() => this.handleCheck('personal')}
                        checked={contact.contact_type === 'personal'}
                      />
                      <label htmlFor="personal" className="i-block  tmargin20">Personal</label>
                      <br />
                      <span className="meeting-info-label">Company Name</span>
                      <span
                        className={`pointer ${contactEditFields.includes('company_name') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('company_name')}
                      >
                        <span className={`align-address contact-input ${contact.company_name ? '' : 'hide'}`}>{contact.company_name}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      {
                        contactEditFields.includes('company_name') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('company_name', e)}
                            name="company_name"
                            value={contact.company_name}
                          />
                        )
                      }
                      <br />
                      <span className="meeting-info-label">Country / Region</span>
                      <CountryDropdown
                        className="form-control i-block contact-input contact-country-dropdown"
                        defaultOptionLabel="Not selected"
                        onChange={e => this.handleCountryChange(e)}
                        value={contact.country}
                        name="country"
                      />
                      <br />
                      <span className="meeting-info-label">Address</span>
                      <span
                        className={`pointer ${contactEditFields.includes('address') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('address')}
                      >
                        <span className={`align-address tpadding10 contact-input ${contact.address ? '' : 'hide'}`}>{contact.address}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      {
                        contactEditFields.includes('address') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('address', e)}
                            name="address"
                            value={contact.address}
                          />
                        )
                      }
                      <br />
                      <span>
                      <span className="meeting-info-label">Address line 2</span>
                      <span
                        className={`pointer ${contactEditFields.includes('address_line_2') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('address_line_2')}
                      >
                        <span className={`align-address contact-input ${contact.address_line_2 ? '' : 'hide'}`}>{contact.address_line_2}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      </span>
                      {
                        contactEditFields.includes('address_line_2') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('address_line_2', e)}
                            name="address_line_2"
                            value={contact.address_line_2}
                          />
                        )
                      }
                      <br className="visible-xs ipad-pro-enable" />
                      <span>
                      <span className="meeting-info-label">City</span>
                      <span
                        className={`pointer ${contactEditFields.includes('city') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('city')}
                      >
                        <span className={`align-address contact-input ${contact.city ? '' : 'hide'}`}>{contact.city}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      </span>
                      {
                        contactEditFields.includes('city') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('city', e)}
                            name="city"
                            value={contact.city}
                          />
                        )
                      }
                      <br />
                      <span className="meeting-info-label">State / Province</span>
                      <span
                        className={`pointer ${contactEditFields.includes('state') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('state')}
                      >
                        <span className={`align-address contact-input ${contact.state ? '' : 'hide'}`}>{contact.state}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      {
                        contactEditFields.includes('state') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('state', e)}
                            name="state"
                            value={contact.state}
                          />
                        )
                      }
                      <br />
                      <span className="meeting-info-label">Zip / Postal code</span>
                      <span
                        className={`pointer ${contactEditFields.includes('postal_code') ? 'hide' : ''}`}
                        onClick={() => this.onEditContact('postal_code')}
                      >
                        <span className={`align-address contact-input ${contact.postal_code ? '' : 'hide'}`}>{contact.postal_code}</span>
                        <span className="glyphicon glyphicon-pencil" />
                      </span>
                      {
                        contactEditFields.includes('postal_code') && (
                          <input
                            type="text"
                            className="form-control i-block contact-input"
                            onChange={e => this.handleContactChange('postal_code', e)}
                            name="postal_code"
                            value={contact.postal_code}
                          />
                        )
                      }
                    </Col>
                  </Row>
                  <Row>
                    <Col
                      md={12}
                      lg={12}
                      xs={12}
                      sm={12}
                      className={`fix-center-rfp-additional-info`}
                    >
                      <div className="additional-info-div tmargin20">
                        <span 
                          className="pointer"
                          onClick={() => this.onEditContact('additional_information')}
                        >
                          <span className="event-summ-label text-bold i-block">
                            Additional Information
                          </span>
                          <span className="glyphicon glyphicon-pencil" />
                        </span>
                        <div
                          className={contactEditFields.includes('additional_information') ? 'hide' : ''}
                        >
                          {
                            contact.additional_information ? (
                              <div
                                className="additional-info-val"
                              >
                                {contact.additional_information}
                              </div>
                            ) : (
                              <div
                                className="no-info"
                              >
                                No additional information is available.
                              </div>
                            )
                          }
                        </div>
                        {
                          contactEditFields.includes('additional_information') && (
                            <textarea
                              rows="10"
                              maxLength="500"
                              value={contact.additional_information}
                              onChange={e => this.handleContactChange('additional_information', e)}
                              placeholder="Include any additional request here. (500 character limit)"
                              name="additional_information"
                            />
                          )
                        }
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div
                className={`room-info-sections tmargin20`}
                id="add-document-section"
              >
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
                    <div className="img-class">
                      <img src="dist/img/add-document-icon.png" alt="Document Info" />
                    </div>
                    <div className="meeting-info-title">
                      Uploaded documents
                    </div>
                    <div className="hyphen-class">
                      <hr />
                    </div>
                  </Col>
                </Row>

                <div className="box-rfp">
                  <Row className="contact-info-row">
                    <span className="contact-sub-title block tmargin20">My documents</span>
                  {
                    attachedFiles.length >= 1 && (
                    <Col md={12} lg={12} xs={12} className="required-contact-info">
                      {
                      attachedFiles.map((doc, i) => (
                      <div className="event-planner-documents i-block">
                        <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/doc.svg" alt="doc icon" />
                        <span className="lpadding10">
                        <a
                          href={doc.file}
                          target="_blank"
                          noopener
                          noreferrer
                        >
                          {doc.file.split('/')[doc.file.split('/').length-1]}
                        </a>
                        </span>
                        <span>
                          <Button
                            className="delete-btn fright hidden-xs"
                            onClick={() => this.removeUploadedFile(doc)}
                          >
                          <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                          </Button>
                        </span>
                      </div>
                      ))
                    }
                    </Col>
                    ) 
                  }
                    <Col md={12} lg={12} xs={12} className="fix-top-rfp-contact-box doc-box">
                      <br />
                      {
                        // acceptedFiles.length >= 1 && (
                        //   acceptedFiles.map(file => (
                        //     <div className="event-planner-documents">
                        //       <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/doc.svg" alt="doc icon" />
                        //       <span className="lpadding10">{file.name}</span>
                        //       <span>
                        //         <Button
                        //           onClick={() => this.handleRemove(file)}
                        //           className="delete-btn fright hidden-xs"
                        //         >
                        //           <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                        //         </Button>
                        //       </span>
                        //     </div>
                        //   ))
                        // )
                      }
                    </Col>
                    <Row className="add-doc-row box-rfp bpadding20">
                      <Col md={12} lg={12} xs={12} id="upload-doc-section" className="text-center tpadding20">
                        <div id="my-document-upload-zone" />
                        <div id="file-upload-template">
                          <div className="dz-preview dz-file-preview">
                            <div className="dz-details">
                              <img src="" data-dz-thumbnail />
                              <span data-dz-name className="file-dz-name" />
                              (
                              <span data-dz-size />
                              )
                              <span data-dz-remove className="dz-remove pointer">✘</span>
                              <span className="dz-error-message" data-dz-errormessage />
                            </div>
                            <div className="dz-progress">
                              <span className="dz-upload" data-dz-uploadprogress />
                            </div>
                          </div>
                        </div>
                        <div className="drag-upload-file block">
                          <ReactSVG wrapper="div" className="svg-wrapper text-center" src="dist/img/upload-icon.svg" alt="Upload Icon" />
                          <span className="block tmargin20 font16 text-center">
                            Drag and drop files here
                            <br />
                            or click upload
                          </span>
                        </div>
                      </Col>
                    </Row>
                  </Row>
                </div>
              </div>
            </div>

            <Row className="primary-color">
              <div
                className="i-block save-btn pointer"
                onClick={this.handleUpdate}
              >
                <span className="save-text">Save</span>
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/rfp_management/save.svg" alt="doc icon" />
              </div>
            </Row>
          </div>  )}
        </Row> 
        {
          (!selectedEventInfo || (Object.keys(selectedEventInfo).length < 1) || (Object.keys(selectedEventHotelInfo).length < 1) ) && 
          <Row>
            <div className="loading">
              <span>
                Loading...
              </span>
              </div>
          </Row>
        }
        {
          viewMap && (
            <MapBox
              handleMapClose={this.handleMapClose}
              showMapModal={showMapModal}
              hotelInfo={selectedEventHotelInfo}
            />
          )
        }   
        {
          showEventHistoryPopup
          &&(
            <EventHistoryPopup
               handleClose={this.handleClose}
               historyInfo={historyInfo}
               selectedEventInfo={selectedEventInfo}
               eventStatus = {eventStatus}
              />
          )
        }
        {
          isSharePopup && (
            <div className="share-room-details visible-pointers primary-color">
              <Button
                className="close-button fright visible-pointers"
                onClick={() => this.handleShareInfo(false)}
              >
                &#x2715;
              </Button>
              <div className="share-info-header">
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/3d-icon.svg" alt="3D" />
                <h1>
                  Event: {selectedEventInfo.name}
                </h1>
              </div>
              <div className="share-info-email">
                <span className="share-info-label">To:</span>
                <input
                  id="email-id"
                  type="text"
                  placeholder="Email"
                  className="form-control email-field primary-color"
                />
              </div>
              <div className="share-info-link">
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/link-icon.svg" alt="Link" />
                <input
                  id="link-id"
                  type="text"
                  placeholder="https://fonts.google.com"
                  className="form-control link-field primary-color"
                  value={link}
                />
                <span className="share-info-note">
                  <span className="share-info-label rmargin10">Anyone</span>
                  with the link can
                  <span className="share-info-label rmargin10 lmargin10">view</span>
                  this 3D setup.
                </span>
                <Button 
                id = "copy-link"
                className="copy-btn primary-color" 
                onClick = {()=>{this.copyToClipboard()}}
                >
                  Copy link
                </Button>
              </div>
              <Button 
              className="send-btn primary-color" 
              onClick={()=>{this.handleSendMail()}}
              >
                Send
              </Button>
            </div>
          )
        }
        {
          show3DPopup
            && (
              <LightBox
                handleClose={this.handle3DAlertClose}
                handleSubmit={this.handle3DAlertClose}
                message={modelAlertMessage}
                header="3D diagramming"
                successText="Ok"
                showModal={show3DPopup}
              />
            )
        } 
        {
          showMaximumAlert
            && (
              <LightBox
                handleClose={this.closeAlertMsg}
                handleSubmit={this.changeCount}
                message={`Maximum limit for ${layoutSetup.type} setup for ${selectedMeetingRoom.name} room
                  is ${layoutSetup.count}. Are you sure you want to change the attendees count?`}
                header="Maximum Alert"
                successText="Change"
                failureText="Skip"
                showModal={showMaximumAlert}
              />
            )
        } 
      </Grid>
    )
  }
}

RFPManagement.contextTypes = {
  router: PropTypes.object,
};

export default RFPManagement;

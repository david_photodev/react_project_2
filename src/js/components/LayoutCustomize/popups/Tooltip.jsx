import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import ReactSVG from 'react-svg';

const tooltipContent = [
  {
    id: 'finishing',
    name: 'Linen Colors',
    icon: 'colour',
  },
  {
    id: 'copy',
    name: 'Duplicate',
    icon: 'clone',
  },
  {
    id: 'delete',
    name: 'Delete',
    icon: 'delete-furniture',
  },
];

class Tooltip extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleFurnitureAction = this.handleFurnitureAction.bind(this);
    this.handleRotateStop = this.handleRotateStop.bind(this);
    this.handleRotateStart = this.handleRotateStart.bind(this);
  }

  handleClick() {
    const { tooltipPosition, handleTooltipPosition, viewer } = this.props;
    viewer.unselectItem();
    handleTooltipPosition(false, tooltipPosition);
  }

  handleFurnitureAction(id) {
    const { viewer, handleFurnitureAction, saveRoom } = this.props;
    if (id === 'delete') {
      viewer.deleteSelectedItem();
      saveRoom();
    } else if (id === 'copy') {
      viewer.cloneSelectedItem();
      saveRoom();
    }
    handleFurnitureAction(id);
  }

  handleRotateStart(id, dir, e) {
    if (id === 'rotate' && e.button !== 2) {
      const { viewer } = this.props;
      if (dir === 'clockwise') {
        viewer.startItemRotation(1);
      } else {
        viewer.startItemRotation(-1);
      }
    }
  }

  handleRotateStop(id) {
    if (id === 'rotate') {
      const { viewer, saveRoom } = this.props;
      viewer.stopItemRotation();
      saveRoom();
    }
  }

  render() {
    const {
      tooltipPosition, isTooltip, show2DMiniViewer, isButtonDisable,
    } = this.props;
    const {
      left, top, arrowLeft, arrowTop, screenWidth,
    } = tooltipPosition;
    const arrowStyle = {
      left: arrowLeft,
      top: screenWidth<=480 ? arrowTop - 6 : arrowTop,
      right: tooltipPosition.arrowRight || 'auto',
    };
    let tooltipStyle = {
      left : left,
      top,
      right: (tooltipPosition.arrowRight - 10 || 'auto'),
    };
    if (screenWidth<=480 && tooltipStyle.right === 'auto'){
      if (arrowLeft < 0){
        tooltipStyle['margin-left'] = 30;
      }else if(left > 300){
        tooltipStyle['margin-left'] = -(left - 80);
      }else if(arrowLeft < 100){
        tooltipStyle['margin-left'] = -20;
      }else{
        tooltipStyle['margin-left'] = -(arrowLeft - 100);
      }
    }
    const closeIcon = show2DMiniViewer ? 'close-3d-icon primary-background' : 'close-2d-icon';
    return (
      <div className="furniture-tooltip-container">
        <div className="tooltip-arrow" style={arrowStyle} />
        {
          isTooltip
            && (
              <div
                id="tooltip-content"
                className={`tooltip-container ${isButtonDisable ? 'left-margin' : 'minwidth'}`}
                style={tooltipStyle}
              >
                <div className="tooltip-wrapper">
                  <ul className="nav">
                    <div
                      className={`i-block btn btn-default ${isButtonDisable ? 'hide' : ''}`}
                    >
                      <div
                        className="tooltip-info-rotate primary-border-right"
                        style={{ borderRight: '1px solid', paddingRight: '20px' }}
                      >
                        <Button
                          onClick={() => this.handleFurnitureAction('rotate')}
                          onMouseDown={e => this.handleRotateStart('rotate', 'counterclockwise', e)}
                          onMouseUp={() => this.handleRotateStop('rotate')}
                          onTouchStart={e => this.handleRotateStart('rotate', 'counterclockwise', e)}
                          onTouchEnd={() => this.handleRotateStop('rotate')}
                        >
                          <ReactSVG wrapper="span" src="dist/img/rotate.svg" alt="rotate" />
                        </Button>
                        <Button
                          onClick={() => this.handleFurnitureAction('rotate')}
                          onMouseDown={e => this.handleRotateStart('rotate', 'clockwise', e)}
                          onMouseUp={() => this.handleRotateStop('rotate')}
                          onTouchStart={e => this.handleRotateStart('rotate', 'clockwise', e)}
                          onTouchEnd={() => this.handleRotateStop('rotate')}
                        >
                          <ReactSVG wrapper="span" className="clockwise" src="dist/img/rotate.svg" alt="rotate" />
                        </Button>
                        <li className="mob-view primary-color">Rotate</li>
                      </div>
                    </div>
                    {
                      tooltipContent.map(({ id, name, icon }) => (
                        <Button
                          className={`i-block ${(id === 'finishing' || id === 'copy') && isButtonDisable
                            ? 'hide' : ''}`}
                          key={id}
                          onClick={() => this.handleFurnitureAction(id)}
                          // onMouseDown={e => this.handleRotateStart(id, e)}
                          // onMouseUp={() => this.handleRotateStop(id)}
                        >
                          <div
                            className="tooltip-info primary-border-right"
                            style={id !== 'delete'
                              ? { borderRight: '1px solid' } : { borderRight: 'transparent' }}
                          >
                            <ReactSVG wrapper="span" src={`dist/img/${icon}.svg`} alt={name} />
                            <li className="mob-view primary-color">{name}</li>
                          </div>
                        </Button>
                      ))
                   }
                  </ul>
                </div>
                <div className="close-button">
                  <Button className={closeIcon} onClick={this.handleClick}>&#x2715;</Button>
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

Tooltip.propTypes = {
  viewer: PropTypes.object.isRequired,
  handleFurnitureAction: PropTypes.func.isRequired,
  tooltipPosition: PropTypes.object.isRequired,
  handleTooltipPosition: PropTypes.func.isRequired,
  saveRoom: PropTypes.func.isRequired,
  isTooltip: PropTypes.bool.isRequired,
  show2DMiniViewer: PropTypes.bool.isRequired,
  isButtonDisable: PropTypes.bool.isRequired,
};

export default Tooltip;

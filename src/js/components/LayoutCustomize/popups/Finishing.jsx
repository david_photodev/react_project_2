import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Button,
} from 'react-bootstrap';

const availableColors = [
  {
    id: '001',
    name: '001',
    color: '#464a4c',
  },
  {
    id: '002',
    name: '002',
    color: '#6b5140',
  },
  {
    id: '003',
    name: '003',
    color: '#e3e3e3',
  },
  {
    id: '004',
    name: '004',
    color: '#eeeeee',
  },
];
const availableFinishings = [
  {
    id: '001',
    name: '001',
    img: 'dist/img/textures/grey-brown_wood.jpg',
  },
  {
    id: '002',
    name: '002',
    img: 'dist/img/textures/oak_wood.jpg',
  },
  {
    id: '003',
    name: '003',
    img: 'dist/img/textures/ps_7.jpg',
  },
  {
    id: '004',
    name: '004',
    img: 'dist/img/textures/walnut-marin.jpg',
  },
];
class Finishing extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.setTexture = this.setTexture.bind(this);
    this.setTextureColor = this.setTextureColor.bind(this);
  }

  setTexture(id) {
    const { viewer, saveRoom } = this.props;
    const data = availableFinishings.filter(item => item.id === id);
    viewer.setItemTexture(data[0], () => saveRoom());
  }

  setTextureColor(id) {
    const { viewer, saveRoom } = this.props;
    const data = availableColors.filter(item => item.id === id);
    viewer.setItemColor(data[0].color, () => saveRoom());
  }

  handleClick() {
    const { handleFurnitureAction } = this.props;
    handleFurnitureAction('');
  }

  render() {
    // const { isFinishingPopup } = this.props;
    return (
      <div className="texture-info-container">
        {
          // isFinishingPopup
            // && (
            <div className="texture-info-wrapper">
              <Row>
                <Button className="close-button" onClick={this.handleClick}>&#x2715;</Button>
              </Row>
              <Row className="texture-header available-texture">
                <Col md={6} sm={12} xs={12} className="about">
                  <p className="text-border">Chair cloth</p>
                </Col>
                <Col md={6} xs={12} sm={12} className="texture-image visible-xs visible-sm texture-info">
                  {
                    availableColors.map(({ id, name, color }) => (
                      <div key={id} className="available-colors-container">
                        <div className="chair-cloth-container">
                          <Button
                            className="available-colors"
                            style={{ backgroundColor: color }}
                            onClick={() => this.setTextureColor(id)}
                          />
                        </div>
                        <p>{name}</p>
                      </div>
                    ))
                  }
                </Col>
                <Col md={6} xs={12} sm={12} className="about">
                  <p className="text-border">Table cloth</p>
                </Col>
                <Col md={6} xs={12} sm={12} className="texture-info-msg visible-xs visible-sm texture-info">
                  {
                    availableFinishings.map(({ id, name, img }) => (
                      <div key={id} className="available-finishing-container">
                        <Button className="available-finishing" onClick={() => this.setTexture(id)}>
                          <img src={`${img}`} alt="color" />
                        </Button>
                        <p>{name}</p>
                      </div>
                    ))
                  }
                </Col>
              </Row>
              <Row className="texture-info">
                <Col md={6} className="texture-image hidden-xs hidden-sm">
                  {
                    availableColors.map(({ id, name, color }) => (
                      <div key={id} className="available-colors-container">
                        <div className="chair-cloth-container">
                          <Button
                            className="available-colors"
                            style={{ backgroundColor: color }}
                            onClick={() => this.setTextureColor(id)}
                          />
                        </div>
                        <p>{name}</p>
                      </div>
                    ))
                  }
                </Col>
                <Col md={6} className="texture-info-msg hidden-xs hidden-sm">
                  {
                    availableFinishings.map(({ id, name, img }) => (
                      <div key={id} className="available-finishing-container">
                        <Button className="available-finishing" onClick={() => this.setTexture(id)}>
                          <img src={`${img}`} alt="color" />
                        </Button>
                        <p>{name}</p>
                      </div>
                    ))
                  }
                </Col>
              </Row>
            </div>
            // )
        }
      </div>
    );
  }
}


Finishing.propTypes = {
  viewer: PropTypes.object.isRequired,
  isFinishingPopup: PropTypes.bool.isRequired,
  handleFurnitureAction: PropTypes.func.isRequired,
  saveRoom: PropTypes.func.isRequired,
};

export default Finishing;

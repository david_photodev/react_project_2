import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Button,
} from 'react-bootstrap';
import CustomizeLayoutAction from '../../actions/CustomizeLayoutAction';
import CustomizeLayoutStore from '../../stores/CustomizeLayoutStore';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import Finishing from './popups/Finishing';
import LightBox from '../Reusables/LightBox';
import ReactSVG from 'react-svg';

let viewer = null;
const models = {
  chair: {
    id: 'chair_1',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Chair/chair_005/chair_005.gltf',
  },
  table: {
    id: 'table_1',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Table/table_003/table_003.gltf',
  },
};

const roundModels = {
  chair: {
    id: 'chair_2',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Chair/chair_005/chair_005.gltf',
  },
  table: {
    id: 'table_2',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Table/table_004/table_004.gltf',
  },
};

class LayoutCustomize extends Component {
  constructor(props) {
    super(props);
    // const { hotelInfo, property } = this.props;
    // const { layout, defaultChairsPerTable, viewRoomSetup } = hotelInfo;
    // const { context, property, hotelInfo } = props;
    // const { defaultChairsPerTable, layout } = hotelInfo;
    // const { layout, defaultChairsPerTable, eventUuId } = hotelInfo;
    // console.log('this.context', this.context);
    // const { location } = context.router.route;
    // const { pathname } = location;
    // let eventUuId;
    // let isListRedirect = false;
    // if (!viewRoomSetup && pathname.includes('view')) {
    //   const values = ['3d', '2d', 'dollhouse'];
    //   const availableVal = values.find(element => pathname.includes(element));
    //   if (availableVal) {
    //     eventUuId = pathname.split(`${availableVal}/`)[1].split('/view')[0];
    //     console.log('eventUuId', eventUuId);
    //   }
    //   isListRedirect = true;
    // }


    this.interval = 0;
    this.state = {
      isFurniturePopup: false,
      addedItems: [],
      furniture: CustomizeLayoutStore.getAll(),
      isTooltip: false,
      tooltipPosition: {},
      viewerState: viewer,
      isModelLoaded: false,
      hideDrag: false,
      showModal: false,
      selectedCategoryId: null,
    };
    this.furnitureTypes = ['chairs', 'tables', 'multimedia', 'others'];
    this.onDragStart = this.onDragStart.bind(this);
    this.filterBy = this.filterBy.bind(this);
    this.searchBy = this.searchBy.bind(this);
    this.onCustomizeLayoutStoreChange = this.onCustomizeLayoutStoreChange.bind(this);
    this.handleLayoutSelection = this.handleLayoutSelection.bind(this);
    this.submitLayout = this.submitLayout.bind(this);
    this.handleInputs = this.handleInputs.bind(this);
    // this.handleEmptyLayout = this.handleEmptyLayout.bind(this);
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleFurniturePage = this.handleFurniturePage.bind(this);
  }

  componentDidMount() {
    const container = document.getElementById('scene-container');
    const progress = document.getElementById('scene-load-progress');
    const overlayHeight = $('#scene-container').height();
    $(`<style>.loader-3d-img-overlay{height: ${overlayHeight}px;}</style>`).appendTo('head');
    const {
      setViewer, handleModelLoading, saveRoom, readOnlyMode, hotelInfo, setLoader, handleFurnitureCount, handle3DPopup,
      resetDroppedItems,
    } = this.props;
    resetDroppedItems();
    const { scenes, config3D } = hotelInfo;
    viewer = hotelInfo.viewer;
    if (config3D && !config3D.available_model) {
      handle3DPopup(true);
      return false;
    }
    const { isModelLoaded } = this.state;
    if (scenes.length < 1) {
      const msg = 'Model is not available';
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
      }, 1);
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
      }, 5000);
      return false;
    }
    setLoader(true);
    setTimeout(() => {
      MeetingSpaceAction.storeSceneData(scenes);
    }, 1);
    // ModelLoader.load(scenes[0].model3d, (m, scene, error) => {
    //   if (error) {
    //     console.error('Could not load model', error);
    //     return;
    //   }
    //   console.log('Model loaded', m, scene);
    //   if (m.id) {
    if (container && progress) {
      if (viewer) {
        MeetingSpaceAction.setViewer(null);
        viewer.dispose();
        setViewer(null);
      }
      viewer = new SceneViewer(container, progress);
      const modelsFurniture = this.loadModels();
      const params = {
        model3d: scenes[0].model3d,
        model2d: scenes[0].model2d,
        editing: readOnlyMode ? false : true,
        viewMode: 'DH',
      };
      Object.assign(params, modelsFurniture);
      viewer.init(params);
      viewer.addEventListener('ready', () => this.handleRoomReady());
      viewer.addEventListener('itemselected', event => this.setTooltip(event));
      viewer.addEventListener('itemdragmove', event => this.setTooltip(event));
      viewer.addEventListener('itemdragend', event => saveRoom(event));
      viewer.addEventListener('itemadded', event => saveRoom(event));
      viewer.addEventListener('itemupdated', event => saveRoom(event));
      viewer.addEventListener('itemunselected', event => this.setTooltip(event));
      viewer.addEventListener('zoom', event => this.setZoom(event.value));
      viewer.addEventListener('itemcountchanged', event => handleFurnitureCount(event));
      this.setState({ viewerState: viewer });
      setViewer(viewer);
      setTimeout(() => {
        MeetingSpaceAction.setViewer(viewer);
      }, 1);
      handleModelLoading(isModelLoaded);
    }
    document.getElementById('scene-load-progress').style.display = 'none';
      // }
    // }, (m, prog) => {
    //   setLoader(true);
    //   document.getElementById('scene-load-progress').style.display = 'block';
    //   document.getElementById('scene-load-progress').setAttribute('value', prog);
    // }
    // );
    const { layout } = hotelInfo;
    this.setState({ selectedLayout: layout });
    // } else {
      
    // }
    const { property } = this.props;
    CustomizeLayoutAction.getFurnitures(property);
    CustomizeLayoutAction.getFurnitureCategories(property);
    CustomizeLayoutStore.addStoreListener(this.onCustomizeLayoutStoreChange);
    // CustomizeLayoutAction.getData();
  }

  static getDerivedStateFromProps(props, state) {
    const { hotelInfo } = props;
    const { layout, defaultChairsPerTable } = hotelInfo;
    if (!state.isConfigurationPopup) {
      const peopleCount = layout.chairs_count >= 0 ? layout.chairs_count : layout.count;
      let columnsPerRow = null;
      const tablesCount = layout.tables_count >= 0 ? layout.tables_count : (layout.config &&
        layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable >= 0)
        ? layout.config.defaultChairsPerTable : ((layout.type.toLowerCase() === 'theatre' ||
          layout.type.toLowerCase() === 'cocktail')
          ? '-' : Math.ceil(peopleCount / defaultChairsPerTable[layout.type.toLowerCase()]));
      const chairsPerTable = layout.chairs_per_table || ((layout.config &&
              layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable >= 0) ?
            layout.config.defaultChairsPerTable : ((layout.type.toLowerCase() === 'theatre' ||
              layout.type.toLowerCase() === 'cocktail') ? '-' : defaultChairsPerTable[layout.type.toLowerCase()]));
      const maxColumns = layout.config && layout.config.hasOwnProperty('maxColumns') ? layout.config.maxColumns : 3;
      columnsPerRow = (layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'classroom')
        && layout.config && layout.config.hasOwnProperty('columns') ? layout.config.columns : 1;
      return {
        selectedLayout: layout,
        currentLayout: layout,
        peopleCount,
        tablesCount,
        chairsPerTable,
        maxColumns,
        columnsPerRow,
      };
    }
    return null;
  }

  componentWillUnmount() {
    CustomizeLayoutStore.removeStoreListener(this.onCustomizeLayoutStoreChange);
  }

  onCustomizeLayoutStoreChange() {
    const furniture = CustomizeLayoutStore.getAll();
    this.setState({ furniture });
  }

  onDragStart(id, e) {
    const { addedItems, furniture } = this.state;
    const { filteredData } = furniture;
    let draggingItem = addedItems.filter(addedItem => addedItem.id === id);
    if (draggingItem.length !== 1) {
      draggingItem = filteredData.filter(item => item.id === id);
      draggingItem[0]['count'] = 1;
    }
    draggingItem = JSON.stringify(draggingItem[0]);
    e.dataTransfer.setData('data', draggingItem);
  }

  setTooltip(event) {
    let { isTooltip, tooltipPosition } = this.state;
    const { handleTooltipPosition, handleDisableButtons, saveRoom } = this.props;
    if (event.type === 'itemselected') {
      if (event.data.itemType === 'RulerItem') {
      // disable rotation and finishing buttons
        handleDisableButtons('disable');
      } else {
        handleDisableButtons('enable');
      }
      isTooltip = true;
      tooltipPosition = {
        left: ((event.data.bbox.min.x + event.data.bbox.max.x) / 2) - 45,
        top: event.data.bbox.max.y - 74,
      };
      tooltipPosition['arrowLeft'] = tooltipPosition.left + 35;
      tooltipPosition['arrowTop'] = tooltipPosition.top + 62;
      this.setState({ isTooltip, tooltipPosition });
    } else if (event.type === 'itemdragmove') {
      isTooltip = false;
      tooltipPosition = {
        left: (event.data.bbox.min.x + event.data.bbox.max.x) / 2 - 45,
        top: event.data.bbox.max.y - 74,
      };
      tooltipPosition['arrowLeft'] = tooltipPosition.left + 35;
      tooltipPosition['arrowTop'] = tooltipPosition.top + 62;
      this.setState({ isTooltip, tooltipPosition });
    }
    // else if (event.type === 'itemdragend') {
    //   isTooltip = false;
    //   tooltipPosition = {
    //     left: (event.data.bbox.min.x + event.data.bbox.max.x) / 2 - 45,
    //     top: event.data.bbox.max.y - 74,
    //   };
    //   tooltipPosition['arrowLeft'] = tooltipPosition.left + 35;
    //   tooltipPosition['arrowTop'] = tooltipPosition.top + 62;
    //   this.setState({ isTooltip, tooltipPosition });
    //   saveRoom(event);
    // }
    else {
      isTooltip = false;
      this.setState({ isTooltip });
    }
    handleTooltipPosition(isTooltip, tooltipPosition);
    const element = document.getElementById('tooltip-content');
    const container3DWidth = $('#room_setup_3d').width();
    tooltipPosition['screenWidth'] = container3DWidth;
    if (element) {
      const elementRect = element.getClientRects()[0];
      if (elementRect.right > container3DWidth) {
        const arrowRight = (container3DWidth - tooltipPosition.left) - 35;
        tooltipPosition['left'] = 'auto';
        tooltipPosition['right'] = 20;
        tooltipPosition['arrowRight'] = arrowRight > 30 ? arrowRight : 30;
        tooltipPosition['arrowLeft'] = 'auto';
      } else if (elementRect.left < 0) {
        tooltipPosition['left'] = 0;
        tooltipPosition['arrowLeft'] = 35;
      }
      this.setState({ isTooltip, tooltipPosition });
      handleTooltipPosition(isTooltip, tooltipPosition);
    }
  }

  setZoom(value) {
    const { setZoom } = this.props;
    setZoom(value);
  }

  handleFurniturePage(type) {
    const { tooltipPosition } = this.state;
    const { handleTooltipPosition } = this.props;
    if (type === 'open') {
      handleTooltipPosition(false, tooltipPosition);
      setTimeout(() => this.setState({ hideDrag: true }), 5000);
    } else {
      this.setState({ hideDrag: false });
      const target = document.getElementById('room_setup_3d');
      if (target !== null) {
        target.scrollIntoView({ block: 'start', behavior: 'smooth' });
      }
      CustomizeLayoutAction.filterData(null, '');
      this.setState({ selectedCategoryId: null });
    }
    this.setState(prevState => ({
      isFurniturePopup: !prevState.isFurniturePopup,
    }));
  }

  handleConfigurationPage(type) {
    const { hotelInfo } = this.props;
    const { layout } = hotelInfo;
    const { chairs_count, tables_count, chairs_per_table } = layout;
    if (type === 'close') {
      this.setState({
        peopleCount: chairs_count,
        tablesCount: tables_count,
        chairsPerTable: chairs_per_table,
      });
    }
    this.setState(prevState => ({
      isConfigurationPopup: !prevState.isConfigurationPopup,
    }));
  }

  filterBy(id) {
    // let selectedFurniture;
    // if (item === 'chairs') {
    //   selectedFurniture = 'ChairItem';
    // } else if (item === 'tables') {
    //   selectedFurniture = 'TableItem';
    // } else {
    //   selectedFurniture = item;
    // }
    const { selectedCategoryId, furniture } = this.state;
    const { searchVal } = furniture;
    let filterId = id;
    // if (selectedCategoryId === id) {
    //   filterId = null;
    // }
    CustomizeLayoutAction.filterData(filterId, searchVal);
    this.setState({ selectedCategoryId: filterId });
  }

  searchBy(e) {
    const { selectedCategoryId } = this.state;
    CustomizeLayoutAction.filterData(selectedCategoryId, e.target.value);
  }

  handleFurnitureCount(id, type) {
    const { addedItems, furniture } = this.state;
    const { filteredData } = furniture;
    const selectedItem = addedItems.filter(addedItem => addedItem.id === id);
    if (selectedItem.length === 1) {
      const updatedCount = selectedItem[0];
      if (type === 'increase') {
        updatedCount['count'] += 1;
      } else if (type === 'decrease' && (updatedCount['count'] && updatedCount['count'] >= 1)) {
        updatedCount['count'] -= 1;
      }
      addedItems.forEach((addedItem, i) => {
        if (addedItem.id === selectedItem.id) {
          addedItems[i] = updatedCount;
        }
      });
    } else if (type === 'increase') {
      filteredData.forEach((item) => {
        const furnitureCount = item;
        if (item.id === id) {
          furnitureCount['count'] = 0;
          furnitureCount['count'] += 1;
          addedItems.push(furnitureCount);
        }
      });
    }
    this.setState({ addedItems });
  }

  loadModels() {
    const {
      hotelInfo, setStorageName, handleModelLoading, setLoader, isOnlyView,
    } = this.props;
    const { selectedRooms, defaultChairsPerTable, viewLayout } = hotelInfo;
    let { storageName } = this.props;
    let setup;
    const sceneLayout = {};
    let furnitureData = null;
    const updatedRoomLayout = selectedRooms.filter((selectedRoom) => {
      setup = selectedRoom.setup.filter(config => config.hasOwnProperty('enable3D') && config.enable3D);
      if (setup.length < 1) {
        return false;
      }
      return selectedRoom;
    });
    if (!isOnlyView) {
      if (updatedRoomLayout.length >= 1) {
        setup = updatedRoomLayout[0].setup.filter(config => config.hasOwnProperty('enable3D') && config.enable3D);
        sceneLayout['type'] = setup[0].type;
        Object.assign(sceneLayout, setup[0].config);
        if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
          sceneLayout['columns'] = setup[0].columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
        }
        if (setup[0].hasOwnProperty('model_3d')) {
          furnitureData = Object.keys(setup[0].model_3d.data).length >= 1 ? setup[0].model_3d.data : null;
        }
        storageName = `SceneData ${updatedRoomLayout[0].id}${setup[0].setup_id} ${setup[0].type}`;
        MeetingSpaceAction.updateLayout(updatedRoomLayout[0], setup[0]);
      } else {
        sceneLayout['type'] = selectedRooms[0].setup[0].type;
        Object.assign(sceneLayout, selectedRooms[0].setup[0].config);
        if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
          sceneLayout['columns'] = selectedRooms[0].setup[0].columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
        }
        if (selectedRooms[0].setup[0].hasOwnProperty('model_3d')) {
          furnitureData = Object.keys(selectedRooms[0].setup[0].model_3d.data).length >= 1 ? selectedRooms[0].setup[0].model_3d.data : null;
        }
        storageName = `SceneData ${selectedRooms[0].id}${selectedRooms[0].setup[0].setup_id} ${selectedRooms[0].setup[0].type}`;
        MeetingSpaceAction.updateLayout(selectedRooms[0], selectedRooms[0].setup[0]);
      }
    } else {
      if (hotelInfo.layout.hasOwnProperty('model_3d')) {
        furnitureData = Object.keys(hotelInfo.layout.model_3d.data).length >= 1 ? hotelInfo.layout.model_3d.data : null;
      }
      sceneLayout['type'] = hotelInfo.layout.type;
      storageName = `SceneData ${hotelInfo.layout.roomId}${hotelInfo.layout.setup_id} ${hotelInfo.layout.type}`;
      Object.assign(sceneLayout, hotelInfo.layout.config);
      if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
        sceneLayout['columns'] = hotelInfo.layout.columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
      }
    }
    
    const { layout } = hotelInfo;
    const peopleCount = layout.chairs_count || layout.count;
    const tablesCount = (layout.config && layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable) || (layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'cocktail')
      ? 0 : Math.ceil(peopleCount / defaultChairsPerTable[layout.type.toLowerCase()]);
    const chairsPerTable = (layout.config && layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable) || (layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'cocktail')
      ? 0 : defaultChairsPerTable[layout.type.toLowerCase()];
    setStorageName(storageName);
    let data = DataProvider.getData(storageName);
    data = data || furnitureData;
    if (data) {
      return { json: data };
    } else if (layout.type.toLowerCase() === 'custom') {
      // viewer.clearFurnitures();
      // this.handleModel();
    } else {
      let layoutModel = {};
      if (sceneLayout.type.toLowerCase() === 'theatre') {
        layoutModel['chair'] = models['chair'];
      } else {
        layoutModel = models;
      }
      let modelsType = (sceneLayout.type.toLowerCase() === 'banquet' || sceneLayout.type.toLowerCase() === 'round'
        || sceneLayout.type.toLowerCase() === 'cabaret' || sceneLayout.type.toLowerCase() === 'cocktail')
        ? roundModels : layoutModel;
      modelsType = Object.keys(layout.furnitures).length >= 1 ? layout.furnitures : modelsType;
      return { models: modelsType };
      // console.log('sceneLayout1', sceneLayout, parseInt(peopleCount, 10), parseInt(layout.tables_count, 10),
      //   parseInt(layout.chairs_per_table, 10), modelsType);
    }
  }

  handleRoomReady() {
    const {
      hotelInfo, setStorageName, handleModelLoading, setLoader, isOnlyView,
    } = this.props;
    const { selectedRooms, defaultChairsPerTable, viewLayout } = hotelInfo;
    let { storageName } = this.props;
    let setup;
    const sceneLayout = {};
    let furnitureData = null;
    const updatedRoomLayout = selectedRooms.filter((selectedRoom) => {
      setup = selectedRoom.setup.filter(config => config.hasOwnProperty('enable3D') && config.enable3D);
      if (setup.length < 1) {
        return false;
      }
      return selectedRoom;
    });
    if (!isOnlyView) {
      if (updatedRoomLayout.length >= 1) {
        setup = updatedRoomLayout[0].setup.filter(config => config.hasOwnProperty('enable3D') && config.enable3D);
        sceneLayout['type'] = setup[0].type;
        Object.assign(sceneLayout, setup[0].config);
        if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
          sceneLayout['columns'] = setup[0].columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
        }
        if (setup[0].hasOwnProperty('model_3d')) {
          furnitureData = Object.keys(setup[0].model_3d.data).length >= 1 ? setup[0].model_3d.data : null;
        }
        storageName = `SceneData ${updatedRoomLayout[0].id}${setup[0].setup_id} ${setup[0].type}`;
        MeetingSpaceAction.updateLayout(updatedRoomLayout[0], setup[0]);
      } else {
        sceneLayout['type'] = selectedRooms[0].setup[0].type;
        Object.assign(sceneLayout, selectedRooms[0].setup[0].config);
        if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
          sceneLayout['columns'] = selectedRooms[0].setup[0].columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
        }
        if (selectedRooms[0].setup[0].hasOwnProperty('model_3d')) {
          furnitureData = Object.keys(selectedRooms[0].setup[0].model_3d.data).length >= 1 ? selectedRooms[0].setup[0].model_3d.data : null;
        }
        storageName = `SceneData ${selectedRooms[0].id}${selectedRooms[0].setup[0].setup_id} ${selectedRooms[0].setup[0].type}`;
        MeetingSpaceAction.updateLayout(selectedRooms[0], selectedRooms[0].setup[0]);
      }
    } else {
      if (hotelInfo.layout.hasOwnProperty('model_3d')) {
        furnitureData = Object.keys(hotelInfo.layout.model_3d.data).length >= 1 ? hotelInfo.layout.model_3d.data : null;
      }
      sceneLayout['type'] = hotelInfo.layout.type;
      Object.assign(sceneLayout, hotelInfo.layout.config);
      storageName = `SceneData ${hotelInfo.layout.roomId}${hotelInfo.layout.setup_id} ${hotelInfo.layout.type}`;
      if (sceneLayout.type === 'theatre' || sceneLayout.type === 'classroom') {
        sceneLayout['columns'] = hotelInfo.layout.columns_per_row || (sceneLayout.hasOwnProperty('columns') ? sceneLayout.columns : 1);
      }
    }
    
    const { layout } = hotelInfo;
    const peopleCount = layout.chairs_count || layout.count;
    const tablesCount = (layout.config && layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable) || (layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'cocktail')
      ? 0 : Math.ceil(peopleCount / defaultChairsPerTable[layout.type.toLowerCase()]);
    const chairsPerTable = (layout.config && layout.config.hasOwnProperty('defaultChairsPerTable') && layout.config.defaultChairsPerTable) || (layout.type.toLowerCase() === 'theatre' || layout.type.toLowerCase() === 'cocktail')
      ? 0 : defaultChairsPerTable[layout.type.toLowerCase()];
    setStorageName(storageName);
    let data = DataProvider.getData(storageName);
    data = data || furnitureData;
    if (data) {
      setLoader(true);
      viewer.fromJSON(data, () => this.handleModel());
    } else if (layout.type.toLowerCase() === 'custom') {
      viewer.clearFurnitures();
      this.handleModel();
    } else {
      let layoutModel = {};
      if (sceneLayout.type.toLowerCase() === 'theatre') {
        layoutModel['chair'] = models['chair'];
      } else {
        layoutModel = models;
      }
      let modelsType = (sceneLayout.type.toLowerCase() === 'banquet' || sceneLayout.type.toLowerCase() === 'round'
        || sceneLayout.type.toLowerCase() === 'cabaret' || sceneLayout.type.toLowerCase() === 'cocktail')
        ? roundModels : layoutModel;
      modelsType = Object.keys(layout.furnitures).length >= 1 ? layout.furnitures : modelsType;
      console.log('sceneLayout1', sceneLayout, parseInt(peopleCount, 10), parseInt(layout.tables_count, 10),
        parseInt(layout.chairs_per_table, 10), modelsType);
      MeetingSpaceAction.selectedLayout(layout);
      viewer.loadLayout(sceneLayout, parseInt(peopleCount, 10), parseInt(layout.tables_count, 10),
        parseInt(layout.chairs_per_table, 10), modelsType, () => this.handleModel());
    }
    this.setState({
      peopleCount,
      tablesCount,
      chairsPerTable,
      selectedLayout: layout,
    });
  }

  handleModel() {
    const { handleModelLoading, setLoader, readOnlyMode } = this.props;
    this.setState({ isModelLoaded: true });
    handleModelLoading(true);
    setLoader(false);
    if (readOnlyMode) {
      viewer.play();
    }
  }

  getCount(selectedLayout, columnsPerRow, callback) {
    const { peopleCount, chairsPerTable, tablesCount } = this.state;
    const options = {
      type: selectedLayout.type,
    };
    Object.assign(options, selectedLayout.config);
    options.columns = columnsPerRow;
    let layoutModel = {};
    if (selectedLayout.type.toLowerCase() === 'theatre') {
      layoutModel['chair'] = models['chair'];
    } else {
      layoutModel = models;
    }
    let modelsType = (selectedLayout.type.toLowerCase() === 'banquet' || selectedLayout.type.toLowerCase() === 'round'
      || selectedLayout.type.toLowerCase() === 'cabaret' || selectedLayout.type === 'cocktail') ? roundModels : layoutModel;
    modelsType = Object.keys(selectedLayout.furnitures).length >= 1 ? selectedLayout.furnitures : modelsType;
    viewer.getFutureCount(options, parseInt(selectedLayout.count, 10), parseInt(peopleCount, 10),
      parseInt(tablesCount, 10), parseInt(chairsPerTable, 10), modelsType, true, (res) => {
        callback(res);
      });
  }

  handleLayoutSelection(layoutSetup) {
    if (layoutSetup.count > 0 || layoutSetup.type.toLowerCase() === 'custom') {
      const { hotelInfo } = this.props;
      const { layout, defaultChairsPerTable } = hotelInfo;
      const { peopleCount } = this.state;
      let { maxColumns, columnsPerRow } = this.state;
      const newSetup = JSON.parse(JSON.stringify(layout));
      newSetup['id'] = layoutSetup['id'];
      newSetup['type'] = layoutSetup['type'];
      newSetup['shape'] = layoutSetup['shape'];
      newSetup['count'] = layoutSetup['count'];
      newSetup['config'] = layoutSetup.config ? layoutSetup.config : {};
      newSetup['furnitures'] = layoutSetup.furnitures ? layoutSetup['furnitures'] : {};
      newSetup['chairs_count'] = layout.type.toLowerCase() === 'custom' ? layoutSetup['count'] : peopleCount;
      newSetup['droppedItems'] = [];
      if ((newSetup['chairs_count'] > newSetup['count']) && (newSetup.type.toLowerCase() !== 'custom')) {
        // const msg = `Maximum limit for ${newSetup.shape} setup is ${newSetup.count}. Please change the chair count`;
        // MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
        // setTimeout(() => {
        //   MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
        // }, 5000);
        this.setState({ showModal: true });
        // return;
      }
      // newSetup.count = newSetup.count ? newSetup.count : 0;
      const chairsPerTable = (newSetup.config.hasOwnProperty('defaultChairsPerTable') &&
        newSetup.config.defaultChairsPerTable >= 0) ? newSetup.config.defaultChairsPerTable
        : (newSetup.type.toLowerCase() === 'theatre' || newSetup.type.toLowerCase() === 'cocktail')
        ? 0 : defaultChairsPerTable[newSetup.type.toLowerCase()];
      // const tablesCount = (newSetup.config.hasOwnProperty('defaultChairsPerTable') &&
      //   newSetup.config.defaultChairsPerTable >= 0) ? newSetup.config.defaultChairsPerTable
      //   : (newSetup.type.toLowerCase() === 'theatre' || newSetup.type.toLowerCase() === 'cocktail')
      //   ? 0 : Math.ceil(newSetup.chairs_count / defaultChairsPerTable[newSetup.type.toLowerCase()]);
      const tablesCount = newSetup.type.toLowerCase() === 'theatre' || newSetup.type.toLowerCase() === 'cocktail' ? '-' : Math.ceil(parseInt(newSetup['chairs_count'], 10) / parseInt(chairsPerTable, 10));
      maxColumns = newSetup.config && newSetup.config.hasOwnProperty('maxColumns') ? newSetup.config.maxColumns : maxColumns;
      columnsPerRow = (newSetup.type.toLowerCase() === 'theatre' || newSetup.type.toLowerCase() === 'classroom')
        && newSetup.config && newSetup.config.hasOwnProperty('columns') ? newSetup.config.columns : columnsPerRow;
      this.setState(prevState => ({
        prevLayout: prevState.selectedLayout,
        selectedLayout: newSetup,
        peopleCount: newSetup['chairs_count'],
        tablesCount: newSetup.type.toLowerCase() === 'custom' ? newSetup['chairs_count'] : tablesCount,
        chairsPerTable: newSetup.type.toLowerCase() === 'custom' ? newSetup['chairs_per_table'] : chairsPerTable,
        maxColumns,
        columnsPerRow,
      }));
    }
  }

  displayNotification (msg) {
    MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
    setTimeout(() => {
      MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
    }, 5000);
  }

  handleInputs(type, action, e) {
    const { selectedLayout } = this.state;
    let {
      peopleCount, tablesCount, chairsPerTable, maxColumns, columnsPerRow,
    } = this.state;
    tablesCount = tablesCount === '-' ? 0 : tablesCount;
    chairsPerTable = chairsPerTable === '-' ? 0 : chairsPerTable;
    const { hotelInfo } = this.props;
    const { viewLayout, defaultChairsPerTable } = hotelInfo;
    const maxCount = selectedLayout.config && selectedLayout.config.hasOwnProperty('defaultChairsPerTable') &&
      selectedLayout.config.defaultChairsPerTable >= 0 ? selectedLayout.config.defaultChairsPerTable
      : defaultChairsPerTable[selectedLayout.type.toLowerCase()];
    let params = {};
    let updateTableCount;
    params = {
      roomId: viewLayout.id,
      setupId: selectedLayout.setup_id,
      type,
    };
    if (type === 'people') {
      if (action === 'increase') {
        if (e.type === 'mousedown') {
          const setPeopleCountIncrease = () => {
            if(selectedLayout.type.toLowerCase() === 'custom'){
              peopleCount+=1;
              this.setState({ peopleCount, tablesCount, chairsPerTable });
            }
            else if (peopleCount < selectedLayout.count) {
              ++peopleCount;
            } 
            else if (selectedLayout.type.toLowerCase() !== 'theatre' && selectedLayout.type.toLowerCase() !== 'classroom'){
              const msg = `Maximum limit for ${selectedLayout.shape} setup is ${selectedLayout.count}`;
              MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
              setTimeout(() => {
                MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
              }, 5000);
            }
            if(selectedLayout.type.toLowerCase() !== 'custom'){
              updateTableCount = Math.ceil(peopleCount / chairsPerTable);
              if (selectedLayout.type.toLowerCase() === 'ushape') {
                if (tablesCount <= 3) {
                  updateTableCount = tablesCount;
                }
              }
              if (selectedLayout.type.toLowerCase() === 'boardroom') {
                if (tablesCount <= 4) {
                  updateTableCount = tablesCount;
                }
              }
              tablesCount = updateTableCount;
              if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail') {
                tablesCount = 0;
                chairsPerTable = 0;
              }
              this.setState({ peopleCount, tablesCount, chairsPerTable });
              // Get the count according to the columns per row
              if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'classroom') {
                this.getCount(selectedLayout, columnsPerRow, res => {
                  if (peopleCount > res.maxChairs) {
                    this.displayNotification('Maximum chairs count reached for the set columns per row!');
                  } else if (peopleCount >= selectedLayout.count) {
                    this.displayNotification(`Maximum limit for ${selectedLayout.shape} setup is ${selectedLayout.count}`);
                  }
                  this.setState({
                    peopleCount: res.chairs,
                    tablesCount: res.tables,
                    columnsPerRow,
                  });
                });
              }
            }
          };
          this.interval = setTimeout(() => {
            setPeopleCountIncrease();
          }, 100);
          this.interval = setInterval(() => {
            setPeopleCountIncrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      } else if (action === 'decrease') {
        if (e.type === 'mousedown') {
          const setPeopleCountDecrease = () => {
            if (peopleCount >= 1) {
              --peopleCount;
              
              if(selectedLayout.type.toLowerCase() === 'custom'){
              this.setState({ peopleCount, tablesCount, chairsPerTable });
              }
            }
            if(selectedLayout.type.toLowerCase() !== 'custom'){
              updateTableCount = Math.ceil(peopleCount / chairsPerTable);
              if (selectedLayout.type.toLowerCase() === 'ushape') {
                if (tablesCount <= 3) {
                  updateTableCount = tablesCount;
                }
              }
              if (selectedLayout.type.toLowerCase() === 'boardroom') {
                if (tablesCount <= 4) {
                  updateTableCount = tablesCount;
                }
              }
              tablesCount = updateTableCount;
              if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail') {
                tablesCount = 0;
                chairsPerTable = 0;
              }
              this.setState({ peopleCount, tablesCount, chairsPerTable });
              // Get the count according to the columns per row
              if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'classroom') {
                this.getCount(selectedLayout, columnsPerRow, res => {
                  this.setState({
                    peopleCount: res.chairs,
                    tablesCount: res.tables,
                    columnsPerRow,
                  });
                });
              }
            }
          };
          this.interval = setTimeout(() => {
            setPeopleCountDecrease();
          }, 100);
          this.interval = setInterval(() => {
            setPeopleCountDecrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      }
      params['peopleCount'] = peopleCount;
    } else if (type === 'table') {
      if (action === 'increase') {
        if (e.type === 'mousedown') {
          const setTableCountIncrease = () => {
            if(selectedLayout.type.toLowerCase() === 'custom'){
              tablesCount++;
              this.setState({ peopleCount, tablesCount, chairsPerTable });
            }
            else{
              updateTableCount = tablesCount;
              tablesCount++;
              peopleCount = Math.ceil(chairsPerTable * tablesCount);
              if (peopleCount > selectedLayout.count) {
                tablesCount = updateTableCount;
                const msg = `Maximum limit for ${selectedLayout.shape} setup is ${selectedLayout.count}`;
                MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
                setTimeout(() => {
                  MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
                }, 5000);
              } else {
                this.setState({ peopleCount, tablesCount, chairsPerTable });
              }
            }
            // Get the count according to the columns per row
            if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'classroom') {
              this.getCount(selectedLayout, columnsPerRow, res => {
                this.setState({
                  peopleCount: res.chairs,
                  tablesCount: res.tables,
                  columnsPerRow,
                });
              });
            }
          };
          this.interval = setTimeout(() => {
            setTableCountIncrease();
          }, 100);
          this.interval = setInterval(() => {
            setTableCountIncrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      } else if (action === 'decrease') {
        if (e.type === 'mousedown') {
          const setTableCountDecrease = () => {
            if(selectedLayout.type.toLowerCase() !== 'custom'){
              if (tablesCount >= 1) {
                if ((selectedLayout.type.toLowerCase() === 'ushape' && tablesCount <= 3)
                || (selectedLayout.type.toLowerCase() === 'boardroom' && tablesCount <= 4)) {
                  updateTableCount = tablesCount;
                } else {
                  tablesCount -= 1;
                  updateTableCount = tablesCount;
                }
                tablesCount = updateTableCount;
                peopleCount = Math.ceil(chairsPerTable * tablesCount);
                this.setState({ peopleCount, tablesCount, chairsPerTable });
              }
            }else{
              if(tablesCount >= 1) {
              tablesCount -= 1;
              this.setState({ peopleCount, tablesCount, chairsPerTable });
              }
            }
            // Get the count according to the columns per row
            if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'classroom') {
              this.getCount(selectedLayout, columnsPerRow, res => {
                this.setState({
                  peopleCount: res.chairs,
                  tablesCount: res.tables,
                  columnsPerRow,
                });
              });
            }
          };
          this.interval = setTimeout(() => {
            setTableCountDecrease();
          }, 100);
          this.interval = setInterval(() => {
            setTableCountDecrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      }
      params['tablesCount'] = tablesCount;
    } else if (type === 'per_table') {
      if (e.type === 'mousedown') {
        const setChairsPerTable = () => {
          if (action === 'increase' && (selectedLayout.type.toLowerCase() === 'custom' ? true : chairsPerTable < maxCount)) {
            chairsPerTable += 1;
            if(selectedLayout.type.toLowerCase() === 'custom'){
              this.setState({ peopleCount, tablesCount, chairsPerTable });
            }
          } else if (action === 'decrease' && chairsPerTable >= 1) {
            chairsPerTable -= 1;
            if(selectedLayout.type.toLowerCase() === 'custom'){
              this.setState({ peopleCount, tablesCount, chairsPerTable });
            }
          }
          if(selectedLayout.type.toLowerCase() !== 'custom'){
            peopleCount = Math.ceil(tablesCount * chairsPerTable);
            if (peopleCount > selectedLayout.count) {
              const msg = `Maximum limit for ${selectedLayout.shape} setup is ${selectedLayout.count}`;
              MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
              setTimeout(() => {
                MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
              }, 5000);
            } else {
              this.setState({ peopleCount, tablesCount, chairsPerTable });
            }
          }
          // Get the count according to the columns per row
          if (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'classroom') {
            this.getCount(selectedLayout, columnsPerRow, res => {
              this.setState({
                peopleCount: res.chairs,
                tablesCount: res.tables,
                columnsPerRow,
              });
            });
          }
        };
        this.interval = setTimeout(() => {
          setChairsPerTable();
        }, 100);
        this.interval = setInterval(() => {
          setChairsPerTable();
        }, 120);
      } else if (e.type === 'mouseup') {
        clearInterval(this.interval);
      }
      params['chairsPerTable'] = chairsPerTable;
    } else if (type === 'columns_per_row') {
      if (action === 'increase') {
        if (e.type === 'mousedown') {
          // columnsPerRow = selectedLayout.config && selectedLayout.config.hasOwnProperty('columns') ? selectedLayout.config.columns : 1;
          const setColumnsPerRowIncrease = () => {
            if (columnsPerRow < maxColumns) {
              columnsPerRow += 1;
              this.getCount(selectedLayout, columnsPerRow, res => {
                this.setState({
                  peopleCount: res.chairs,
                  tablesCount: res.tables,
                  columnsPerRow,
                });
              });
            } else {
              const msg = `Maximum columns per row for ${selectedLayout.shape} setup is ${maxColumns}`;
              MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
              setTimeout(() => {
                MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
              }, 5000);
            }
          };
          this.interval = setTimeout(() => {
            setColumnsPerRowIncrease();
          }, 100);
          this.interval = setInterval(() => {
            setColumnsPerRowIncrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      } else if (action === 'decrease') {
        if (e.type === 'mousedown') {
          // columnsPerRow = selectedLayout.config && selectedLayout.config.hasOwnProperty('columns') ? selectedLayout.config.columns : 1;
          const setColumnsPerRowDecrease = () => {
            if (columnsPerRow > 1) {
              columnsPerRow -= 1;
              this.getCount(selectedLayout, columnsPerRow, res => {
                this.setState({
                  peopleCount: res.chairs,
                  tablesCount: res.tables,
                  columnsPerRow,
                });
              });
            } else {
              const msg = `Minimum columns per row for ${selectedLayout.shape} setup is 1`;
              MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
              setTimeout(() => {
                MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
              }, 5000);
            }
          };
          this.interval = setTimeout(() => {
            setColumnsPerRowDecrease();
          }, 100);
          this.interval = setInterval(() => {
            setColumnsPerRowDecrease();
          }, 120);
        } else if (e.type === 'mouseup') {
          clearInterval(this.interval);
        }
      }
      params['columnsPerRow'] = columnsPerRow;
    }
    this.setState({
      chairsPerTable, peopleCount, tablesCount, columnsPerRow
    });
    // MeetingSpaceAction.updateLayoutInputs(params);
  }

  submitLayout() {
    const { selectedLayout } = this.state;
    let { peopleCount, tablesCount, chairsPerTable, columnsPerRow } =  this.state;
    tablesCount = tablesCount === '-' ? 0 : tablesCount;
    chairsPerTable = chairsPerTable === '-' ? 0 : chairsPerTable;
    const {
      hotelInfo, handleModelLoading, setLoader,
    } = this.props;
    const { viewLayout, viewer } = hotelInfo;
    const layout = {};
    layout['type'] = selectedLayout.type;
    Object.assign(layout, selectedLayout.config);
    if (layout.type === 'theatre' || layout.type === 'classroom') {
      layout['columns'] = columnsPerRow || (layout.hasOwnProperty('columns') ? layout.columns : 1);
    }
    const msg = `${viewLayout.room_name} was changed to ${selectedLayout.shape} setup`;
    if (peopleCount > selectedLayout.count && selectedLayout.type !== 'custom') {
      this.setState({ showModal: true });
      return;
    }
    MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
    setTimeout(() => {
      MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
    }, 5000);
    selectedLayout['enable3D'] = true;
    selectedLayout['chairs_count'] = peopleCount;
    selectedLayout['tables_count'] = tablesCount;
    selectedLayout['chairs_per_table'] = chairsPerTable;
    selectedLayout['columns_per_row'] = columnsPerRow;
    this.setState({ selectedLayout });
    const chairsCount = peopleCount || 0;
    const params = {
      roomId: viewLayout.id,
      setupId: selectedLayout.setup_id,
      peopleCount: chairsCount,
      tablesCount,
      chairsPerTable,
      columnsPerRow,
    };
    MeetingSpaceAction.updateLayoutInputs(params);
    MeetingSpaceAction.submitLayout(selectedLayout, viewLayout.id);
    this.handleConfigurationPage('close');
    if (layout.type.toLowerCase() === 'custom') {
      viewer.clearFurnitures();
      this.setState({ isModelLoaded: true });
      handleModelLoading(true);
    } else {
      let layoutModel = {};
      if (layout.type.toLowerCase() === 'theatre') {
        layoutModel['chair'] = models['chair'];
      } else {
        layoutModel = models;
      }
      let modelsType = (layout.type.toLowerCase() === 'banquet' || layout.type.toLowerCase() === 'round'
        || layout.type.toLowerCase() === 'cabaret' || layout.type === 'cocktail') ? roundModels : layoutModel;
      modelsType = Object.keys(selectedLayout.furnitures).length >= 1 ? selectedLayout.furnitures : modelsType;
      console.log('SceneLayout2', layout, parseInt(chairsCount, 10), parseInt(tablesCount, 10),
        parseInt(chairsPerTable, 10), modelsType);
      setLoader(true);
      MeetingSpaceAction.selectedLayout(layout);
      viewer.loadLayout(layout, parseInt(chairsCount, 10), parseInt(tablesCount, 10), parseInt(chairsPerTable, 10),
        modelsType, () => this.handleModel());
    }
    // const target = document.getElementById('room_setup_3d');
    // if (target !== null) {
    //   target.scrollIntoView({ block: 'start', behavior: 'smooth' });
    // }
  }

  // handleEmptyLayout() {
  //   const { selectedLayout } = this.state;
  //   const newSetup = JSON.parse(JSON.stringify(selectedLayout));
  //   newSetup['id'] = Math.random();
  //   newSetup['type'] = 'custom';
  //   newSetup['shape'] = 'Custom';
  //   this.setState(prevState => ({
  //     prevLayout: prevState.selectedLayout,
  //     selectedLayout: newSetup,
  //   }));
  // }

  handleKeyup() {
    console.log('keyup');
  }

  handleSubmit() {
    const { hotelInfo } = this.props;
    const { defaultChairsPerTable, viewLayout } = hotelInfo;
    const { selectedLayout } = this.state;
    const peopleCount = selectedLayout.count;
    selectedLayout['chairs_count'] = selectedLayout.count;
    // const tablesCount = selectedLayout.config && selectedLayout.config.hasOwnProperty('defaultChairsPerTable') &&
    //   selectedLayout.config.defaultChairsPerTable >= 0 ? selectedLayout.config.defaultChairsPerTable
    //   : (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail')
    //   ? '-' : Math.ceil(peopleCount / defaultChairsPerTable[selectedLayout.type.toLowerCase()]);
    const chairsPerTable = selectedLayout.config && selectedLayout.config.hasOwnProperty('defaultChairsPerTable') &&
      selectedLayout.config.defaultChairsPerTable >= 0 ? selectedLayout.config.defaultChairsPerTable
      : (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail')
      ? '-' : defaultChairsPerTable[selectedLayout.type.toLowerCase()];
    const tablesCount = selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail' ? '-' : Math.ceil(parseInt(peopleCount, 10) / parseInt(chairsPerTable, 10));
    this.setState({
      chairsPerTable, peopleCount, tablesCount, showModal: false,
    }, () => this.submitLayout());
    // const params = {
    //   roomId: viewLayout.id,
    //   setupId: selectedLayout.setup_id,
    //   type: 'people',
    //   peopleCount,
    // };
    // MeetingSpaceAction.updateLayoutInputs(params);
  }

  handleClose() {
    const { prevLayout } = this.state;
    this.setState({
      selectedLayout: prevLayout,
      showModal: false,
    });
  }

  render() {
    const {
      isFurniturePopup, isConfigurationPopup, addedItems, furniture, viewerState, isModelLoaded, hideDrag,
      selectedLayout, peopleCount, showModal, selectedCategoryId, columnsPerRow,
    } = this.state;
    const {
      droppedItems, show2DMiniViewer, isFinishingPopup, handleFurnitureAction, isTooltip, saveRoom, context,
      hotelInfo, readOnlyMode, isFullScreenBtn, property, isDownload, userPrimaryColor,
    } = this.props;
    const { viewLayout, layout } = hotelInfo;
    const { filteredData, searchVal, categories } = furniture;
    let { tablesCount, chairsPerTable } = this.state;
    tablesCount = (Number.isNaN(tablesCount) || (selectedLayout.type.toLowerCase() === 'cocktail' || selectedLayout.type.toLowerCase() === 'theatre')) ? '-' : tablesCount;
    chairsPerTable = (Number.isNaN(chairsPerTable) || (selectedLayout.type.toLowerCase() === 'cocktail' || selectedLayout.type.toLowerCase() === 'theatre'))   ? '-' : chairsPerTable;
    const styleObj = {
      border: '1px solid',
      borderRadius: '10px',
      color: userPrimaryColor,
      borderColor: userPrimaryColor,
    };
    const primaryColor = {
      color: userPrimaryColor,
    };
    const isNoPointers = (selectedLayout.type.toLowerCase() === 'theatre' || selectedLayout.type.toLowerCase() === 'cocktail');
    if (!showModal && document.getElementsByClassName("layout-popup").length >= 1) {
      document.getElementsByClassName("layout-popup")[0].remove();
    }
    return (
      <div
        className={`layout-customization-container ${isFullScreenBtn ? 'hidden-xs hidden-sm' : ''}`}
      >
        {
          isFurniturePopup && (
            <Row>
              <img id="drag-image" src="dist/img/drag.png" alt="drag icon" className={hideDrag ? 'hide' : ''} />
              <Col md={6} lg={5} sm={8} xs={11} className="choose-furniture">
                <Row>
                  <Button className="primary-color close-button" onClick={() => this.handleFurniturePage('close')}>&#x2715;</Button>
                </Row>
                <Row className="select-furniture">
                  <Col lg={12} md={12} className="select-furniture-text">
                    Select your furniture.
                    <br />
                    Drag and drop it into the scene.
                  </Col>
                  <Col lg={12} md={12}>
                    <i className="fa fa-search" />
                    <input className="find-furniture primary-border" type="text" placeholder="Find furniture" value={searchVal} onChange={this.searchBy} />
                  </Col>
                </Row>
                <Row className="properties-button">
                  {
                    categories.map(({ id, name }) => (
                      <Button
                        key={id}
                        className={`primary-color ${selectedCategoryId === id ? 'primary-background highlight-background' : ''}`}
                        onClick={() => { this.filterBy(id) }}
                      >
                        {name}
                      </Button>
                    ))
                  }
                  <Button
                    key={'All'}
                    className={`primary-color ${selectedCategoryId === null ? 'primary-background highlight-background' : ''}`}
                    onClick={() => { this.filterBy(null) }}
                  >
                    {'All'}
                  </Button>
                </Row>
                {
                  layout.droppedItems && layout.droppedItems.length >= 1
                    ? (
                      <Row className="added-furnitures-container">
                        Already added on to your room
                        <div className="added-furnitures">
                          {
                            layout.droppedItems.map(({
                              id, name, type, thumbnail_path,
                            }) => (
                              <img
                                key={`model_${id}`}
                                src={thumbnail_path}
                                alt={`${type}${name}`}
                                width="83px"
                                height="87px"
                                // onDragStart={(e) => { this.onDragStart(id, e); }}
                                // draggable="true"
                              />
                            ))
                          }
                        </div>
                      </Row>
                    ) : ''
                }
                <Row>
                  <div className="properties-container">
                    {
                      filteredData.map(({
                        id, name, type, thumbnail_path,
                      }) => (
                        <Col lg={6} md={6} sm={6} key={`model_${id}`} className="property-details">
                          <img
                            src={thumbnail_path}
                            alt={`${type}${name}`}
                            width="114px"
                            height="137px"
                            onDragStart={(e) => { this.onDragStart(id, e); }}
                            draggable="true"
                          />
                          <p>{name}</p>
                          <div className="primary-border add_properties" style={styleObj}>
                            <span>Add units</span>
                            <Button
                              className="plus primary-border-right"
                              onClick={() => { this.handleFurnitureCount(id, 'increase'); }}
                            >
                              <i className="fa fa-plus primary-color" />
                            </Button>
                            <Button
                              onClick={() => { this.handleFurnitureCount(id, 'decrease'); }}
                            >
                              <i className="fa fa-minus primary-color" />
                            </Button>
                          </div>
                          {
                            addedItems.length >= 1
                              ? addedItems.map(item => (
                                (item.id === id && item.count > 0)
                                  && <div className="primary-background added-count" key={id}><span>{item.count}</span></div>
                              )) : ''
                          }
                        </Col>
                      ))
                    }
                    {
                      filteredData.length < 1 && (
                        <div className="empty-furniture-msg">No furniture found.</div>
                      )
                    }
                  </div>
                </Row>
              </Col>
            </Row>
          )
        }
        {
          isConfigurationPopup && (
            <Row>
              <Col md={4} lg={4} sm={8} xs={8} className="choose-configuration" style={primaryColor}>
                <Row>
                  <Button
                    style={primaryColor}
                    className="close-button fright"
                    onClick={() => this.handleConfigurationPage('close')}
                  >
                    &#x2715;
                  </Button>
                </Row>
                <Row>
                  <Col xs={12} sm={12} md={12} lg={12} className="select-configure-title" style={primaryColor}>
                    Select the configuration
                    <br />
                    that best fits your event.
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} sm={12} className={`tmargin30 ${selectedLayout.type.toLowerCase() === 'custom' ? 'col-lg-12 col-md-12' : 'border-right col-lg-8 col-md-8'}`}>
                    <Row>
                      {
                        viewLayout.setup.map(layoutSetup => (
                          layoutSetup.type.toLowerCase() !== 'custom' ?
                          (<Col md={3} lg={3} sm={6} className="tmargin10" key={layoutSetup.id}>
                            <div className="config-div text-center">
                              <Button
                                className={`layout-btn ${layoutSetup.count <= 0 ? 'opacity5' : ''}`}
                                style={selectedLayout && (selectedLayout.id === layoutSetup.id)
                                  ? styleObj : primaryColor}
                                onClick={() => this.handleLayoutSelection(layoutSetup)}
                                disabled={!(layoutSetup.count > 0)}
                              >
                                <ReactSVG wrapper="span" className="svg-wrapper layout-img" src={layoutSetup.img} alt={layoutSetup.shape} />
                                <span className="block config-title tmargin20">{layoutSetup.shape}</span>
                                <hr />
                                <span className="block">
                                  <span className="maximum-persons rmargin10">{layoutSetup.count >= 0 ? layoutSetup.count : '-'}</span>
                                  <img src="dist/img/person.png" alt="Person" />
                                </span>
                              </Button>
                            </div>
                          </Col>) : 
                          (<Col
                            sm={5}
                            md={3}
                            lg={3}
                            style={selectedLayout.type.toLowerCase() === 'custom'
                              ? styleObj : {}}
                            className="tmargin10 create-own-config pointer"
                            onClick={() => this.handleLayoutSelection(layoutSetup)}
                          >
                            <img
                              className="add-image primary-background"
                              style={{ backgroundColor: userPrimaryColor }}
                              src="dist/img/meeting_space/icons/plus.png"
                              alt="plus"
                            />
                            <span className="block tmargin10 create-own-btn">Create your own</span>
                          </Col>)
                        ))
                      }
                    </Row>
                  </Col>
                  {
                    selectedLayout.type.toLowerCase() !== 'custom' &&
                   (<Col xs={12} sm={12} md={4} lg={4} className="tmargin30 config-operation">
                    <div>
                      <span className="config-label">
                        Type of setup
                      </span>
                      <Button className="config-setup-btn btn" style={primaryColor}>
                        {selectedLayout.shape}
                      </Button>
                    </div>
                    <hr />
                    <div>
                      <span className="config-label">
                        Chairs needed
                      </span>
                      <div className="config-chairs-btn btn" style={primaryColor}>
                        <span
                          className="fa fa-minus"
                          onMouseDown={e => this.handleInputs('people', 'decrease', e)}
                          onMouseUp={e => this.handleInputs('people', 'decrease', e)}
                          onKeyUp={this.handleKeyup}
                        />
                        <span
                          className="fa fa-plus"
                          onMouseDown={e => this.handleInputs('people', 'increase', e)}
                          onMouseUp={e => this.handleInputs('people', 'increase', e)}
                          onKeyUp={this.handleKeyup}
                        />
                      </div>
                      <span className="chair-count rmargin20">{peopleCount}</span>
                    </div>
                    <div className="tmargin50 bmargin30">
                      <span className="config-label">
                        Tables needed
                      </span>
                      <div className="config-table-btn btn" style={primaryColor}>
                        <span
                          className={`fa fa-minus ${isNoPointers ? 'no-pointers' : ''}`}
                          onMouseDown={e => this.handleInputs('table', 'decrease', e)}
                          onMouseUp={e => this.handleInputs('table', 'decrease', e)}
                          onKeyUp={this.handleKeyup}
                        />
                        <span
                          className={`fa fa-plus ${isNoPointers ? 'no-pointers' : ''}`}
                          onMouseDown={e => this.handleInputs('table', 'increase', e)}
                          onMouseUp={e => this.handleInputs('table', 'increase', e)}
                          onKeyUp={this.handleKeyup}
                        />
                      </div>
                      <span className="table-count rmargin20">{tablesCount}</span>
                    </div>
                    <hr />
                    <div>
                      <span className="config-label">
                        Chairs per table
                      </span>
                      <div className="config-chairs-btn btn" style={primaryColor}>
                        <span
                          className={`fa fa-minus ${isNoPointers ? 'no-pointers' : ''}`}
                          onMouseDown={e => this.handleInputs('per_table', 'decrease', e)}
                          onMouseUp={e => this.handleInputs('per_table', 'decrease', e)}
                          onKeyUp={this.handleKeyup}
                        />
                        <span
                          className={`fa fa-plus ${isNoPointers ? 'no-pointers' : ''}`}
                          onMouseDown={e => this.handleInputs('per_table', 'increase', e)}
                          onMouseUp={e => this.handleInputs('per_table', 'increase', e)}
                          onKeyUp={this.handleKeyup}
                        />
                      </div>
                      <span className="chair-count rmargin20">{chairsPerTable}</span>
                    </div>
                    {
                      (selectedLayout.type === 'theatre' || selectedLayout.type === 'classroom') && (
                        <div className="tmargin50 bmargin30">
                          <span className="config-label">
                            Columns per row
                          </span>
                          <div className="config-chairs-btn btn" style={primaryColor}>
                            <span
                              className="fa fa-minus"
                              onMouseDown={e => this.handleInputs('columns_per_row', 'decrease', e)}
                              onMouseUp={e => this.handleInputs('columns_per_row', 'decrease', e)}
                              onKeyUp={this.handleKeyup}
                            />
                            <span
                              className="fa fa-plus"
                              onMouseDown={e => this.handleInputs('columns_per_row', 'increase', e)}
                              onMouseUp={e => this.handleInputs('columns_per_row', 'increase', e)}
                              onKeyUp={this.handleKeyup}
                            />
                          </div>
                          <span className="chair-count rmargin20">
                            {
                              columnsPerRow || (selectedLayout.config && selectedLayout.config.hasOwnProperty('columns') ? selectedLayout.config.columns : 1) 
                            }
                          </span>
                        </div>
                      ) 
                    }
                    
                    <div className="clearfix" />
                    <Button className="done-configuration primary-color primary-border" onClick={this.submitLayout} style={primaryColor}>
                      Done
                    </Button>
                  </Col>)
                }
                { 
                  selectedLayout.type.toLowerCase() === 'custom' && (
                    <Button className="done-configuration primary-color primary-border" onClick={this.submitLayout} style={primaryColor}>
                      Done
                    </Button>
                  )
                }
                </Row>
              </Col>
            </Row>
          )
        }
        {
          !isFurniturePopup && isModelLoaded && !readOnlyMode && (
            <div>
              <Row>
                <Col sm={5} md={3} className="choose-furniture-button align-search-button hidden-xs">
                  <span
                    className={show2DMiniViewer ? 'white' : 'primary-color'}
                    onClick={() => this.handleFurniturePage('open')}
                    onKeyUp={this.handleKeyup}
                  >
                    Choose Furniture
                  </span>
                  <i className={`fa fa-search align-search-icon ${show2DMiniViewer ? 'search-icon-color' : 'primary-color'}`} />
                  <Button onClick={() => this.handleFurniturePage('open')} />
                </Col>
              </Row>
            </div>
          )
        }
        {
          isFinishingPopup && isTooltip
          && (
          <Finishing
            viewer={viewerState}
            handleFurnitureAction={handleFurnitureAction}
            isFinishingPopup={isFinishingPopup}
            saveRoom={saveRoom}
          />)
        }
        {
          !isFurniturePopup && isModelLoaded && !readOnlyMode && (
            <div className="pre-set-button">
              <Button className="primary-color" style={primaryColor} onClick={() => this.handleConfigurationPage('open')}>
                Pre-set up
                <span className="fa fa-angle-up" />
              </Button>
            </div>
          )
        }
        {
          // Confirmation popup for the full screen mode and normal mode
          showModal && (
            <div role="dialog">
              <div className="modal-backdrop in layout-popup" />
              <div
                className="modal alert-confirmation-popup fade in"
                tabIndex="-1"
                role="dialog"
                style={{ display: 'block' }}
              >
                <div className="modal-dialog" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h4 className="modal-title">Maximum alert</h4>
                      <span
                        className="close-icon"
                        onClick={this.handleClose}
                        onKeyUp={this.handleKeyup}
                      >
                        &#x2715;
                      </span>
                    </div>
                    <div className="modal-body">
                      <p>
                        {`Maximum limit for ${selectedLayout.shape} setup is ${selectedLayout.count}. Are you sure you
                        want to change the layout?`}
                      </p>
                      <div className="modal-action-btns">
                        <Button className="skip" onClick={this.handleClose}>Skip</Button>
                        <Button className="change" onClick={this.handleSubmit}>Change</Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        }
      </div>
    );
  }
}


LayoutCustomize.propTypes = {
  setViewer: PropTypes.func.isRequired,
  handleTooltipPosition: PropTypes.func.isRequired,
  handleFurnitureAction: PropTypes.func.isRequired,
  setZoom: PropTypes.func.isRequired,
  saveRoom: PropTypes.func.isRequired,
  handleModelLoading: PropTypes.func.isRequired,
  handleDisableButtons: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
  setStorageName: PropTypes.func.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  resetDroppedItems: PropTypes.func.isRequired,
  handleFurnitureCount: PropTypes.func.isRequired,
  show2DMiniViewer: PropTypes.bool.isRequired,
  isFinishingPopup: PropTypes.bool.isRequired,
  readOnlyMode: PropTypes.bool.isRequired,
  isFullScreenBtn: PropTypes.bool.isRequired,
  isTooltip: PropTypes.bool.isRequired,
  droppedItems: PropTypes.array.isRequired,
  hotelInfo: PropTypes.object.isRequired,
  storageName: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
};

LayoutCustomize.contextTypes = {
  router: PropTypes.object,
};

export default LayoutCustomize;

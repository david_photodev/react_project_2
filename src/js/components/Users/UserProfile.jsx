import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import UserAction from '../../actions/UserAction';
import UserStore from '../../stores/UserStore';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/Users/UserProfile.scss';

class UserProfile extends CommonLayout {
  constructor(props) {
    super(props);
    const userInfo = UserStore.getAll();
    this.state = {
      userInfo,
      isLoggedIn: false,
      selectedUser: userInfo.selectedUser ? userInfo.selectedUser : {},
      setLoader: userInfo.selectedUser ? false : true,
      isSpaziousIdEdit: false,
      isFirstNameEdit: false,
      isLastNameEdit: false,
      isEmailEdit: false,
      isPhoneEdit: false,
      notificationType: '',
      valueOfEditSpaziousId: '',
      valueOfEditPhone: '',
      valueOfEditFirstName: '',
      valueOfEditLastName: '',
      userId: '',
    };
    this.onUserStoreChange = this.onUserStoreChange.bind(this);
    this.updateHotel = this.updateHotel.bind(this);
    this.enableEditingField = this.enableEditingField.bind(this);
    this.updateFields = this.updateFields.bind(this);
    this.emailValidation = this.emailValidation.bind(this);
    this.makeEditable = this.makeEditable.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
  }

  componentWillMount() {
    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    let userId;
    const context = window.location.href;
    if (context.indexOf('/users/profile/') > -1) {
      userId = context.split('/users/profile/')[1];
      this.setState({ userId : userId });
      UserAction.readUser(userId, (res) => {
        this.setState({ selectedUser: res, setLoader: false });
      });
    }
    UserStore.addStoreListener(this.onUserStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnMount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  componentDidUpdate() {
    const userIdChange = window.location.href.split('/users/profile/')[1];
    const { userId } = this.state;
    if (userIdChange !== userId) {
      this.componentWillMount();
    }
  }

  onUserStoreChange() {
    const userInfo = UserStore.getAll();
    if (userInfo.selectedUser && userInfo.selectedUser.length >= 1) {
      this.setState({ setLoader: false, selectedUser: userInfo.selectedUser });
    }
    this.setState({ userInfo });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  makeEditable(e) {
    this.setState({ isFirstNameEdit: e === 'isFirstNameEdit' ,isLastNameEdit: !(e === 'isFirstNameEdit') });
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ isFirstNameEdit: false ,isLastNameEdit: false });
    }
  }

  enableEditingField(fieldType, isEnable, nameType, onblur) {
    const { userInfo } = this.state;
    const { selectedUser } = userInfo;
    if (fieldType === 'isSpaziousIdEdit') {
      this.setState({ isSpaziousIdEdit: isEnable, valueOfEditSpaziousId: selectedUser.email});
    } else if (fieldType === 'isNameEdit') {
      if (isEnable) {
        this.setState({
          isFirstNameEdit: isEnable,
          isLastNameEdit: isEnable,
          valueOfEditFirstName: selectedUser.first_name,
          valueOfEditLastName: selectedUser.last_name,
        });
      }
    } else if (fieldType === 'isEmailEdit') {
      this.setState({
        isEmailEdit: isEnable,
        valueOfEditSpaziousId: selectedUser.email,
      });
    } else {
      this.setState({
        isPhoneEdit: isEnable,
        valueOfEditPhone: selectedUser.profile.phone,
      });
    }
  }

  emailValidation(email) {
    if ((email === '') || (!email.match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/))) {
      return false;
    }
    return true;
  }

  phoneValidation(phone) {
    if ((phone === '') || (!/^[\d\.\-]+$/.test(phone))) {
      return false;
    }
    return true;
  }

  updateProfileApi(fieldType, updateInfo, profileId) {
    const { userInfo } = this.state;
    const { selectedUser } = userInfo;
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({
        displayNotification: true,
        notificationMsg: ['Updating...'],
        notificationType: 'update',
      });
      UserAction.updateUserProfile(profileId, updateInfo, (res)=>{
        if (!res) {
          this.displayErrorMsg('Could not update');
        } else {
          this.setState({
            displayNotification: false,
            notificationMsg: [],
            notificationType: null,
          });
          if (fieldType === 'isSpaziousIdEdit' || fieldType === 'isEmailEdit') {
            selectedUser.email = res.email;
            this.setState({ valueOfEditSpaziousId: res.email, selectedUser });
          }
          if (fieldType === 'isPhoneEdit') {
            selectedUser.profile.phone = res.profile.phone;
            this.setState({ valueOfEditPhone: res.profile.phone, selectedUser });
          }
          if (fieldType === 'first_name') {
            selectedUser.first_name = res.first_name;
            this.setState({ valueOfEditFirstName: res.first_name, selectedUser });
          }
          if (fieldType === 'last_name') {
            selectedUser.last_name = res.last_name;
            this.setState({ valueOfEditLastName: res.last_name, selectedUser });
          }
        }
      });
    }, 500);
  }

  displayErrorMsg(msg) {
    setTimeout(() => {
      this.setState({
        displayNotification: true,
        notificationMsg: [msg],
      });
    }, 2000);
    setTimeout(() => {
      this.setState({
        displayNotification: false,
        notificationMsg: [],
      });
    }, 5000);
  }

  updateFields(e, profileId, fieldType) {
    const updateInfo = {};
    updateInfo['profile'] = {};
    if (fieldType === 'isSpaziousIdEdit' || fieldType === 'isEmailEdit') {
      this.setState({ valueOfEditSpaziousId: e.target.value });
      const isEmailValid = this.emailValidation(e.target.value);
      if (isEmailValid) {
        updateInfo['email'] = e.target.value;
        this.updateProfileApi(fieldType, updateInfo, profileId);
      } else {
        this.displayErrorMsg('Please enter valid email');
      }
    } else if (fieldType === 'isPhoneEdit') {
      this.setState({ valueOfEditPhone: e.target.value });
      const isPhoneValid = this.phoneValidation(e.target.value);
      if (isPhoneValid) {
        updateInfo.profile['phone'] = e.target.value;
        this.updateProfileApi(fieldType, updateInfo, profileId);
      } else {
        this.displayErrorMsg('Please enter valid phone');
      }
    } else if (fieldType === 'first_name') {
      this.setState({ valueOfEditFirstName: e.target.value });
      updateInfo['first_name'] = e.target.value;
      this.updateProfileApi(fieldType, updateInfo, profileId);
    } else if (fieldType === 'last_name') {
      this.setState({ valueOfEditLastName: e.target.value });
      updateInfo['last_name'] = e.target.value;
      this.updateProfileApi(fieldType, updateInfo, profileId);
    }
  }

  updateHotel(e) {
    const { userInfo } = this.state;
    const { selectedUser } = userInfo;
    const selectedHotelId = e.target.value;
    this.setState({
      displayNotification: true,
      notificationMsg: ['Updating...'],
      notificationType: 'update',
    });
    UserAction.selectedHotel(selectedHotelId, selectedUser, (res) => {
      if (res) {
        this.setState({
          displayNotification: false,
          notificationMsg: [],
          notificationType: null,
        });
      }
    });
  }

  render() {
    const {
      isLoggedIn, isRedirect, isMobileSidebar, setLoader, selectedUser, displayNotification, notificationMsg,
      isSpaziousIdEdit, isFirstNameEdit, isLastNameEdit, isEmailEdit, isPhoneEdit, valueOfEditSpaziousId,
      valueOfEditPhone, notificationType, valueOfEditFirstName, valueOfEditLastName,
    } = this.state;
    // const { selectedUser } = userInfo;
    const userInitial = (user) => {
      const { first_name, last_name, username } = user;
      const firstInitial = first_name ? first_name[0] : '';
      const lastInitial = last_name ? last_name[0] : '';
      let userInitialName = `${firstInitial}${lastInitial}`;
      const usernameInitial = `${username[0]}${username[1]}`;
      userInitialName = userInitialName || usernameInitial;
      userInitialName = userInitialName.toUpperCase();

      return userInitialName;
    };
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <Grid className="primary-color user-profile-container layout-header">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <Row className="content-right">
          {this.pageHeader(displayNotification, notificationMsg, null, notificationType)}
          {
            selectedUser && Object.keys(selectedUser).length >= 1 && (
              <div>
                <Row className="header-user-info tpadding20">
                  <Col>
                    <div className="fleft">
                      <span className="user-image-big i-block">{userInitial(selectedUser)}</span>
                    </div>
                    <div className="fleft lmargin20">
                      <div className="text-font-28"><strong>{`${selectedUser.first_name} ${selectedUser.last_name}`}</strong></div>
                      <div className="text-font16 primary-color">{selectedUser.profile.role}</div>
                    </div>
                  </Col>
                  <Col className="primary-border-left" lg={8} md={8} />
                  
                </Row>
                <Row className="main-content padding20">
                  <div className="text-font16 lmargin15">
                    Spazious ID
                  </div>
                  { !isSpaziousIdEdit ? (
                    <div
                      className="i-block text-font16 tmargin10 lmargin15"
                      onClick={() => { this.enableEditingField('isSpaziousIdEdit', true); }}
                    >
                      <strong>{selectedUser.email || '-'}</strong>
                      <span className="lmargin10 glyphicon glyphicon-pencil" />
                    </div>
                  ) : (
                    <Col className="lpadding0" lg={3} md={3} sm={5} xs={6}>
                      <input
                        type="text"
                        placeholder="Spazious ID"
                        className="edit-input-field lmargin15 tmargin10"
                        value={valueOfEditSpaziousId}
                        onChange={(e) => { this.updateFields(e, selectedUser.id, 'isSpaziousIdEdit'); }}
                        autoFocus="true"
                        onBlur={() => { this.enableEditingField('isSpaziousIdEdit', false); }}
                      />
                    </Col>
                  )}
                </Row>
                <Row className="main-content content-details padding20 bpadding100">
                  <Col lg={6} md={6} sm={12} xs={12}>
                    <div className="text-font16 padding-heading primary-border-bottom">
                      <strong>Contact details</strong>
                    </div>
                    <Row className="tpadding20">
                      <Col className="text-align-mobile" lg={4} md={4} sm={4} xs={4}>
                        <span className="text-font16">Name</span>
                      </Col>
                      <Col lg={8} md={8} sm={8} xs={8}>
                        { (!isFirstNameEdit) && (!isLastNameEdit) ? (
                          <Row lg={12} md={12} sm={12} xs={12} onClick={() => { this.enableEditingField('isNameEdit', true); }}>
                            <Col lg={10} md={10} sm={10} xs={8}>
                              <span className="text-font-18 word-wrap"><strong>{(selectedUser.first_name || selectedUser.last_name) ? `${selectedUser.first_name} ${selectedUser.last_name}` : '-'}</strong></span>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={4}>
                              <span className="lmargin10 glyphicon glyphicon-pencil" />
                            </Col>
                          </Row>
                        ) : (
                          <div
                            id="name"
                            ref={this.setWrapperRef}
                          >
                            <Col className="rpadding0" type="first_name" lg={12} md={12} sm={12} xs={12}>
                              <input
                                type="text"
                                placeholder="First name"
                                className="edit-input-field"
                                value={valueOfEditFirstName}
                                onChange={(e) => { this.updateFields(e, selectedUser.id, 'first_name'); }}
                                autoFocus="true"
                                onFocus={() => { this.makeEditable('isFirstNameEdit'); }}
                              />
                            </Col>
                            <Col className="rpadding0" type="last_name" lg={12} md={12} sm={12} xs={12}>
                              <input
                                type="text"
                                placeholder="Last name"
                                className="edit-input-field"
                                value={valueOfEditLastName}
                                onChange={(e) => { this.updateFields(e, selectedUser.id, 'last_name'); }}
                                onFocus={() => { this.makeEditable('isLastNameEdit'); }}
                              />
                            </Col>
                          </div>
                        )}
                      </Col>
                    </Row>
                    <Row className="tpadding20">
                      <Col className="text-align-mobile" lg={4} md={4} sm={4} xs={4}>
                        <span className="text-font16">E-mail</span>
                      </Col>
                      <Col lg={8} md={8} sm={8} xs={8}>
                        { !isEmailEdit ? (
                          <Row lg={12} md={12} sm={12} xs={12} onClick={() => { this.enableEditingField('isEmailEdit', true); }}>
                            <Col
                              lg={10}
                              md={10}
                              sm={10}
                              xs={8}
                            >
                              <span className="text-font-18 word-wrap"><strong>{selectedUser.email || '-'}</strong></span>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={4}>
                              <span className="lmargin10 glyphicon glyphicon-pencil" />
                            </Col>
                          </Row>
                        ) : (
                          <Row>
                            <Col className="rpadding0" lg={12} md={12} sm={12} xs={12}>
                              <input
                                type="text"
                                placeholder="E-mail"
                                className="edit-input-field"
                                value={valueOfEditSpaziousId}
                                onChange={(e) => { this.updateFields(e, selectedUser.id, 'isEmailEdit'); }}
                                autoFocus="true"
                                onBlur={() => { this.enableEditingField('isEmailEdit', false); }}
                              />
                            </Col>
                          </Row>
                        )}
                      </Col>
                    </Row>
                    <Row className="tpadding20">
                      <Col className="text-align-mobile" lg={4} md={4} sm={4} xs={4}>
                        <span className="text-font16">Phone</span>
                      </Col>
                      <Col lg={8} md={8} sm={8} xs={8}>
                        { !isPhoneEdit ? (
                          <Row onClick={() => { this.enableEditingField('isPhoneEdit', true); }}>
                            <Col lg={10} md={10} sm={10} xs={8}>
                              <span className="text-font-18 word-wrap"><strong>{selectedUser.profile.phone || '-'}</strong></span>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={4}>
                              <span className="lmargin10 glyphicon glyphicon-pencil" />
                            </Col>
                          </Row>
                        ) : (
                          <Row>
                            <Col className="rpadding0" lg={12} md={12} sm={12} xs={12}>
                              <input
                                type="text"
                                placeholder="Phone"
                                className="edit-input-field"
                                value={valueOfEditPhone}
                                onChange={(e) => { this.updateFields(e, selectedUser.id, 'isPhoneEdit'); }}
                                autoFocus="true"
                                onBlur={() => {this.enableEditingField('isPhoneEdit', false); }}
                              />
                            </Col>
                          </Row>
                        )}
                      </Col>
                    </Row>
                  </Col>
                  <Col className="align-column" lg={6} md={6} sm={12} xs={12}>
                    <div className="text-font16 padding-heading primary-border-bottom"><strong>Hotel</strong>
                    </div>
                    <div className="dropdown-background">
                      <select
                        className="hotel-dropdown options-font-color"
                        onChange={this.updateHotel}
                      >
                        <option disabled hidden selected={selectedUser.hotel.length < 1}>not selected</option>
                        {
                          selectedUser.availableHotels.map(hotel => (
                            <option value={hotel.id} selected={selectedUser.hotel.length >= 1 && selectedUser.hotel[0].id === hotel.id}>{hotel.name}</option>
                          ))
                        }
                      </select>
                    </div>
                  </Col>
                </Row>
                <Row className="tpadding20 bpadding20">
                  <Col>
                    <Button
                      className="fright button-back primary-color primary-border-color-light rmargin10"
                      onClick={this.context.router.history.goBack}
                    >
                      Back
                    </Button>
                  </Col>
                </Row>
                <div className="site-footer visible-xs">
                  <div className="logo-section">
                    <p className="rmargin10">Powered by</p>
                    <img src="dist/img/white-logo.png" alt="White Logo" />
                  </div>
                  <div className="footer-menu-section">
                    <span>Hotel</span>
                    <span>Meeting Rooms</span>
                  </div>
                </div>
              </div>
            )
          }
          {
            Object.keys(selectedUser).length < 1 && setLoader && (
              <div className="empty-user-profile">Loading...</div>
            )
          }
        </Row>
      </Grid>
    );
  }
}

UserProfile.contextTypes = {
  router: PropTypes.object,
};

export default UserProfile;

import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import UserAction from '../../actions/UserAction';
import UserStore from '../../stores/UserStore';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/Users/UsersList.scss';

class UsersList extends CommonLayout {
  constructor(props) {
    super(props);
    const userInfo = UserStore.getAll();
    this.state = {
      userInfo,
      isLoggedIn: false,
      setLoader: false,
      username: false,
      role: false,
      hotel_chain: false,
      hotel: false,
      showDropdown: false,
    };
    this.onUserStoreChange = this.onUserStoreChange.bind(this);
    this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.handleSortingDropdown = this.handleSortingDropdown.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.createUser = this.createUser.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.userRole = this.userRole.bind(this);
  }

  componentWillMount() {
    UserStore.addStoreListener(this.onUserStoreChange);
    AuthStore.addStoreListener(this.onAuthStoreChange);
    const { userInfo } = this.state;
    if (userInfo.userList.length < 1) {
      this.setState({ setLoader: true });
      UserAction.getUserList((res) => {
        if (res) {
          this.setState({ setLoader: false });
        }
      });
    }

    window.scrollTo(0, 0);
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
  }

  onUserStoreChange() {
    const userInfo = UserStore.getAll();
    this.setState({ userInfo });
  }

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  handleSearch(e) {
    UserAction.searchBy(e.target.value);
  }

  handleSortingDropdown(e) {
    const key = e.target.value;
    this.sortBy(key);
  }

  handleDropdown(e, username) {
    e.preventDefault();
    const { userInfo } = this.state;
    const selectedEvent = userInfo.userList.map((item) => {
      const selectedItem = item;
      if (item.username !== username) {
        selectedItem['showDropdown'] = false;
        return selectedItem;
      }
      selectedItem['showDropdown'] = !selectedItem['showDropdown'];
      return selectedItem;
    });
    this.setState({ userInfo });
  }

  sortBy(key) {
    this.setState(prevState => ({ [key]: !prevState[key] }), () => {
      UserAction.sortBy(key, this.state[key]);
    });
  }

  deleteUser(user, e) {
    e.preventDefault();
    UserAction.deleteUser(user);
  }

  createUser(user) {
    UserAction.selectedUser(user);
  }

  userInitialName(user) {
    let { first_name, last_name, username } = user;
    first_name = first_name ? first_name[0] : '';
    last_name = last_name ? last_name[0] : '';
    let userInitialName = `${first_name}${last_name}`;
    const usernameInitial = `${username[0]}${username[1]}`;
    userInitialName = userInitialName || usernameInitial;
    userInitialName = userInitialName.toUpperCase();

    if (userInitialName) {
      return (
        <div className="user-name">{userInitialName}</div>
      );
    }
  }

  userRole(user) {
    let userRole = null;
    if (user.profile.role == 'Admin') {
      userRole = 'SA';
    } else {
      const firstInitialRole = user.profile.role[0];
      const lastInitialRole = user.profile.role.split(' ')[1] ? user.profile.role.split(' ')[1].charAt(0) : '';
      userRole = `${firstInitialRole}${lastInitialRole}`;
      userRole = userRole.toUpperCase();
    }
    if (userRole) {
      return (
        <span>{userRole}</span>
      );
    }
  }
  // componentWillUnmount() {
  //   UserStore.removeStoreListener(this.onUserStoreChange);
  // }

  render() {
    const {
      isRedirect, isLoggedIn, userInfo, setLoader, role, username, hotel_chain, hotel, isMobileSidebar, showDropdown,
    } = this.state;
    const { userList, searchVal } = userInfo;
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <Grid className="primary-color user-list-container layout-header">
        {this.sideBar()}
        {
          isMobileSidebar && (
            this.mobileSideBar()
          )
        }
        <Row className="content-right">
          {this.pageHeader()}

          <Row className="row-title">
            <Col lg={9} md={8} sm={7} xs={5}>
              <div className="column-header primary-color">
                <p className="tmargin10 heading"><strong>Users</strong></p>
              </div>
            </Col>
            <Col className="primary-border-color tmargin10 border-search-bar" lg={3} md={4} sm={5} xs={7}>
              <Col lg={9} md={9} sm={9} xs={9}>
                <div className="fleft">
                  <input
                    className="find-text text-font-20"
                    type="text"
                    placeholder="Find User"
                    value={searchVal}
                    onChange={this.handleSearch}
                  />
                </div>
              </Col>
              <Col className="fleft tmargin5" lg={3} md={3} sm={3} xs={3}>
                <span className="fright"><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" width="25px" /></span>
              </Col>

            </Col>
          </Row>
          <Row>
            <Col className="fright sort-by visible-xs" xs={7}>
              <div className="sort-by-option lmargin10 fleft">Sort By:</div>
              <select
                className="fleft sorting-dropdown text-bold"
                id="text-small"
                onChange={(e) => { this.handleSortingDropdown(e); }}
              >
                <optgroup label="Sort by:">
                  <option value="role">User role</option>
                  <option value="username">User name</option>
                  <option value="role">Brand</option>
                  <option value="username">Hotel</option>
                </optgroup>
              </select>
            </Col>
          </Row>
          <Row className="main-content">
            <Row className="header-title hidden-xs">
              <Col
                className="text-font-12 rpadding0"
                lg={1}
                md={1}
                sm={1}
                onClick={() => this.sortBy('role')}
              >
                <div className="fleft heading-width i-block">
                  <strong>User role&nbsp;&nbsp;</strong>
                </div>
                <div className="fleft i-block">
                  <ReactSVG
                    wrapper="span"
                    className={`svg-wrapper ${role ? '' : 'transform'}`}
                    src="dist/img/meeting_space/icons/arrow-down.svg"
                  />
                </div>
              </Col>
              <Col
                className="text-font-12"
                lg={2}
                md={2}
                sm={2}
                onClick={() => this.sortBy('username')}
              >
                <div className="fleft heading-width i-block">
                  <strong>User name&nbsp;&nbsp;</strong>
                </div>
                <div className="fleft i-block">
                  <ReactSVG
                    wrapper="span"
                    className={`svg-wrapper ${username ? '' : 'transform'}`}
                    src="dist/img/meeting_space/icons/arrow-down.svg"
                  />
                </div>
              </Col>
              <Col
                className="text-font-12"
                lg={2}
                md={2}
                sm={2}
                onClick={() => this.sortBy('hotel_chain')}
              >
                <span className="lmargin5"><strong>Brand&nbsp;&nbsp;</strong></span>
                <ReactSVG
                  wrapper="span"
                  className={`svg-wrapper ${hotel_chain ? '' : 'transform'}`}
                  src="dist/img/meeting_space/icons/arrow-down.svg"
                />
              </Col>
              <Col
                className="text-font-12"
                lg={2}
                md={2}
                sm={2}
                onClick={() => this.sortBy('hotel')}
              >
                <strong>Hotel&nbsp;&nbsp;</strong>
                <ReactSVG
                  wrapper="span"
                  className={`svg-wrapper ${hotel ? '' : 'transform'}`}
                  src="dist/img/meeting_space/icons/arrow-down.svg"
                />
              </Col>
              <Col
                className="text-font-12"
                lg={2}
                md={2}
                sm={2}
              >
                <strong>Phone&nbsp;&nbsp;</strong>
              </Col>
              <Col
                className="text-font-12"
                lg={3}
                md={3}
                sm={3}
              >
                <strong>E-mail&nbsp;&nbsp;</strong>
              </Col>
            </Row>

            {
              userList.length >= 1 && (
                <div>
                  {
                    userList.map(user => (
                      <Link to={`/property/users/profile/${user.id}`}>
                        <Row className="list-event" onClick={() => this.createUser(user)}>
                          <div className="display-contents">
                            <Col lg={1} md={1} sm={1} xs={3} className="lpadding0">
                              <div className="fleft i-block">
                                <span>
                                  {this.userInitialName(user)}
                                </span>
                              </div>
                              <div className="fleft align-content tmargin10 i-block">
                                <span className="header-text-color-2">
                                  <strong
                                    className={`${user.profile.role === 'Admin' ? 'super-admin-color' : 'manager-color'}`}
                                  >
                                    {this.userRole(user)}
                                  </strong>
                                </span>
                              </div>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={7} className="lpadding0 text-font-14 pointer">
                              <div className="fleft word-wrap tmargin10 dropdown-arrow username">{user.username || '-'}</div>
                              <div className="fright dropdown-arrow tmargin10 visible-xs">
                                <ReactSVG
                                  wrapper="span"
                                  className={`svg-wrapper fright desktop-view-dropdown-icon ${user.showDropdown ? 'arrow-toggle' : ''}`}
                                  src="dist/img/meeting_space/icons/arrow-down.svg"
                                  alt="arrow down"
                                  onClick={e => this.handleDropdown(e, user.username)}
                                />
                              </div>
                            </Col>
                            <Col lg={2} md={2} sm={2} className="text-font-14 tmargin10 hidden-xs">
                              {user.profile.hotel_chain.name || '-'}
                            </Col>
                            <Col lg={2} md={2} sm={2} className="text-font-14 tmargin10 hidden-xs">
                              {user.hotel.length >= 1 ? user.hotel[0].name : '-'}
                            </Col>
                            <Col lg={2} md={2} sm={2} className="text-font-14 tmargin10 hidden-xs">
                              {user.profile.phone || '-'}
                            </Col>
                            <Col lg={3} md={3} sm={3} className="text-font-14 tmargin5 rpadding0">
                              <Row>
                                <Col className="email word-wrap hidden-xs" lg={10} md={8} sm={8}>
                                  <span>{user.email || '-'}</span>
                                </Col>
                                <Col
                                  className="delete-button fright pointer"
                                  lg={2}
                                  md={4}
                                  xs={2}
                                  sm={4}
                                  onClick={e => this.deleteUser(user, e)}
                                >
                                  <ReactSVG
                                    wrapper="span"
                                    className="svg-wrapper primary-border-left fright"
                                    src="dist/img/meeting_space/icons/delete-red@3x.svg"
                                    alt="delete"
                                  />
                                </Col>
                              </Row>
                            </Col>
                            <Row className={`row-details-mobile-view bmargin10 ${user.showDropdown ? 'show' : 'hide'}`}>
                              <div className="padding10 primary-border-bottom-light">
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font14"><strong>Brand</strong></span>
                                </div>
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font16">{user.profile.hotel_chain.name || '-'}</span>
                                </div>
                              </div>
                              <div className="padding10 primary-border-bottom-light">
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font14"><strong>Hotel</strong></span>
                                </div>
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font16">
                                    NH Kookwinkel Oldenhof
                                  </span>
                                </div>
                              </div>
                              <div className="padding10 primary-border-bottom-light">
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font14"><strong>Phone</strong></span>
                                </div>
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font16">{user.profile.phone || '-'}</span>
                                </div>
                              </div>
                              <div className="padding10">
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font14"><strong>E-mail</strong></span>
                                </div>
                                <div className="tpadding10 lpadding10">
                                  <span className="text-font16">{user.email || '-'}</span>
                                </div>
                              </div>
                            </Row>
                          </div>
                        </Row>
                      </Link>
                    ))
                  }
                </div>
              )
            }
            {
              userList.length < 1 && !setLoader && (
                (
                  <Row>
                    <div className="empty-user-msg">
                      <span>No Users Found!</span>
                    </div>
                  </Row>
                )
              )
            }
            {
              setLoader && (
                (
                  <Row>
                    <div className="empty-user-msg">
                      <span>Loading....</span>
                    </div>
                  </Row>
                )
              )
            }
          </Row>
        </Row>
        <div className="site-footer visible-xs">
          <div className="logo-section">
            <p className="rmargin10">Powered by</p>
            <img src="dist/img/white-logo.png" alt="White Logo" />
          </div>
          <div className="footer-menu-section">
            <span>Hotel</span>
            <span>Meeting Rooms</span>
          </div>
        </div>
      </Grid>
    );
  }
}

UsersList.contextTypes = {
  router: PropTypes.object,
};

export default UsersList;

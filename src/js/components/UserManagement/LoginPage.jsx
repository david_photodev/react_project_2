import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import '../../../css/HomePage/LoginPage.scss';
import AuthStore from '../../stores/AuthStore';

class LoginPage extends Component {
  constructor(props){
  	super(props);
  	this.state = {
       username : "",
       password : "",
       redirect : false,
       invalidData: false,
       errorDescription: '',
  	}
  	this.handleUsername=this.handleUsername.bind(this);
  	this.handlePassword=this.handlePassword.bind(this);
  	this.handleLogin=this.handleLogin.bind(this);
  }

  componentWillMount() {
  	$('link#font-link').remove();
  	const backOfficeFontLink = $('link#font-link-backoffice');
    if (!backOfficeFontLink.length) {
	  	$('link#font-link-backoffice').remove();
	  	const mainTypography = 'Montserrat';
	    const linkCss = `https://fonts.googleapis.com/css?family=${mainTypography}:400,700`;
	    const $css = $(`<link id="font-link-backoffice" href=${linkCss} rel='stylesheet'/>`);
	    $css.appendTo('head');
	    $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
	  }
  	const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    window.scrollTo(0, 0);
  }
 
  handleUsername(e){
    const user = e.target.value;
    this.setState({ username : user, invalidData: false });
  }

  handlePassword(e){
    const user_password = e.target.value;
    this.setState({ password : user_password, invalidData: false });
  }

  handleLogin(){
    let data = { "username" : this.state.username, "password" : this.state.password };
    AuthStore.login(data, (success, res) => {
    	if (success) {
          AuthStore.getUser((complete, user) => {
          	console.log('response', user);
          	this.setState({ redirect: true, invalidData: false });
          });
    	} else {
    		this.setState({
    			invalidData: true,
    			errorDescription: res.error_description,
    		});
    	}
    });
  }
  render() {
  	const {
  		username, password, redirect, invalidData, errorDescription, isLoggedIn,
  	} = this.state;
  	if (redirect || isLoggedIn) {
  		return <Redirect to="/property/event/list" />;
  	}
  	return(
  		<Grid className="login-page-container layout-content">
  		  <Row className="mask">
  		    <div className="header-logo-top lmargin45 hidden-xs hidden-sm">
	            <Col className="lmargin45 text-color-button">
	              <p>Welcome to</p>
	            </Col>
	            <Col className="lmargin20">
	              <ReactSVG 
	                wrapper="span" 
	                className="svg-wrapper white-logo-spazious" 
	                src="dist/img/logo.svg"  
	                alt="White logo"/>
	            </Col>
            </div>
  		    <div className="column-header margin-header visible-xs visible-sm">
	            <Col className="lmargin30 text-color-button">
	              <p>Welcome to</p>
	            </Col>
	            <Col>
	              <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
	            </Col>
            </div>
  		  </Row>
		  	<Row className="mask-down">
          <div className="remove-skew lmargin60 hidden-xs hidden-sm">
         		<div className="tmargin50">
            </div>
          </div>
  		  </Row>
  		  <Row className="float-left">
  		    <div className="mask-right-no-corner-radius bottom-element">
  		    </div>
  		    <div className="mask-right top-element">
  		    </div>
  		    <div className="main-content tmargin20 hidden-xs hidden-sm">
  		        <Col>
	  		      <span className="primary-text-color font-size-32">Login</span>
		  	    </Col>
		   	    <Col className="tmargin50"> 
		  		  <input 
		  		  className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
		  		  type="text" 
		  		  placeholder="Enter your email" 
		  		  value={username}
		  		  onChange={this.handleUsername}
		  		  />
		  		</Col>
		  		<Col className="tmargin20"> 
		  		  <input 
		  		  className={`input-field-style password-field ${invalidData ? 'invalid-input' : ''}`}
		  		  type="password" 
		  		  placeholder="Enter your password" 
		  		  value={password}
		  		  onChange={this.handlePassword}/>
		  		</Col>
		  		<Col className="style-width tmargin20 lmargin10">
		  		  <input
	                  type="checkbox"/>
		  		  <label className="primary-text-color font-size-14 vertical-align">&nbsp;&nbsp;Remember me
		  		  </label>
		  		  <div className="fright rmargin10">
		  		    <Link to="/property/reset_password">
		  		      <Button className="color-blue font-size-14 button-forget-password">Forgot your password?</Button>
		  		    </Link>
		  		  </div>
		  		</Col>
		  		{
		  			invalidData && (
		  				<p className="error-description">{errorDescription}</p>
		  			)
		  		}
		  		<Col className="style-width tmargin10">
		  			
		  		  	  <Button className="style-width button-style text-color-button" onClick={() => this.handleLogin()}>Login
		  		  	    <img src="dist/img/arrow-right.png" className="fright button-image-style" width="20px"/>
		  		  	  </Button>
		  			
		  		</Col>
		  		<div className="footer-text tmargin50">
                   <div className="fright">
                     <span className="color-blue">
                     <a href="mailto: hello@spazious.com">
                     Contact us </a></span> if having problems logging in.
                   </div>
                </div>
  		    </div>
  		  </Row>
  		  <Row className="visible-xs visible-sm">
  		    <div className="main-content visible-xs visible-sm">
  		        <Col>
	  		      <span className="primary-text-color lmargin10 font-size-32">Login</span>
		  	    </Col>
		   	    <Col className="tmargin50"> 
		  		  <input
		  		  	className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
		  		  	type="text"
		  		  	placeholder="Enter your email"
		  		  	value={username}
		  		  	onChange={this.handleUsername}
		  		  />
		  		</Col>
		  		<Col className="tmargin20"> 
		  		  <input
		  		  	className={`input-field-style password-field ${invalidData ? 'invalid-input' : ''}`}
		  		  	type="password"
		  		  	placeholder="Enter your password"
		  		  	value={password}
		  		  	onChange={this.handlePassword}
		  		  />
		  		</Col>
		  		<Col className="style-width tmargin20 lmargin10">
		  		  <input
	                  type="checkbox"
	                  className="tmargin20"/>
		  		  <label className="text-color-button font-size-14 vertical-align">&nbsp;&nbsp;Remember me
		  		  </label>
		  		  <div className="fright">
		  		    <Link to="/property/reset_password">
		  		      <Button className="color-blue font-size-14 button-forget-password">Forgot your password?</Button>
		  		    </Link>
		  		  </div>
		  		</Col>
		  		{
		  			invalidData && (
		  				<p className="error-description tmargin50">{errorDescription}</p>
		  			)
		  		}
		  		<Col className={`style-width ${invalidData ? 'tmargin0' : 'tmargin50'}`}>
	  		    <Button 
	  		    	className="style-width tmargin20 button-style text-color-button"
	  		    	onClick={() => this.handleLogin()}
	  		    >
		  		    Login
		  		    <img src="dist/img/arrow-right.png" className="fright button-image-style" width="20px"/>
	  		    </Button>
		  		</Col>
		  		<div className="align-content-bottom text-align-center tmargin50">
		  		  <p><span className="color-blue">
		  		  <a href="mailto: hello@spazious.com">Contact us</a> 
		  		  </span> if having problems logging in.</p>
		  		</div>
  		    </div>
  		  </Row>
  		</Grid>
  		);
  	}
  }

export default LoginPage;

import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import ReactSVG from 'react-svg';
import '../../../css/HomePage/LoginPage.scss';
import AuthStore from '../../stores/AuthStore';

class LoginWithNewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmPassword: '',
      invalidData: false,
      updatedPassword: false,
      errorDescription: '',
    };
    this.handleNewPassword = this.handleNewPassword.bind(this);
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
    this.changePassword = this.changePassword.bind(this);
  }

  componentWillMount() {
    $('link#font-link').remove();
    const backOfficeFontLink = $('link#font-link-backoffice');
    if (!backOfficeFontLink.length) {
      $('link#font-link-backoffice').remove();
      const mainTypography = 'Montserrat';
      const linkCss = `https://fonts.googleapis.com/css?family=${mainTypography}:400,700`;
      const $css = $(`<link id="font-link-backoffice" href=${linkCss} rel='stylesheet'/>`);
      $css.appendTo('head');
      $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
    }
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    window.scrollTo(0, 0);
  }

  handleNewPassword(e) {
    const newPassword = e.target.value;
    this.setState({
      newPassword,
      invalidData: false,
    });
  }

  handleConfirmPassword(e) {
    const confirmPassword = e.target.value;
    this.setState({
      confirmPassword,
      invalidData: false,
    });
  }

  changePassword() {
    const { newPassword, confirmPassword } = this.state;
    const { router } = this.context;
    if (!newPassword || !confirmPassword) {
      this.setState({
        invalidData: true,
        errorDescription: 'Please enter the password.',
      });
    } else if (newPassword === confirmPassword) {
      const location = router.history.location.search;
      const token = location.substring(1).split('=')[1];
      const params = {
        token,
        password: newPassword,
      };
      AuthStore.resetPassword(params, (success, res) => {
        if (success) {
          this.setState({
            updatedPassword: true,
            invalidData: false,
          });
        } else {
          this.setState({
            updatedPassword: false,
            invalidData: true,
            errorDescription: 'Invalid Token.',
          });
        }
      });
    } else {
      this.setState({
        invalidData: true,
        errorDescription: 'Password Mismatch.',
      });
    }
  }

  render() {
    const {
      newPassword, confirmPassword, invalidData, updatedPassword, errorDescription, isLoggedIn,
    } = this.state;
    if (updatedPassword) {
      return <Redirect to="/property/login" />;
    }
    if (isLoggedIn) {
      return <Redirect to="/property/event/list" />;
    }
  	return (
  		<Grid className="login-page-container">
  		  <Row className="mask">
          <div className="header-logo-top lmargin45 hidden-xs hidden-sm">
              <Col className="lmargin45 text-color-button">
                <p>Welcome to</p>
              </Col>
              <Col className="lmargin20">
                <ReactSVG
                  wrapper="span"
                  className="svg-wrapper white-logo-spazious"
                  src="dist/img/logo.svg"
                  alt="White logo"
                />
              </Col>
          </div>
          <div className="column-header margin-header visible-xs visible-sm">
              <Col className="lmargin30 text-color-button">
                <p>Welcome to</p>
              </Col>
              <Col>
                <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
              </Col>
          </div>
  		  </Row>
        <Row className="mask-down">
          <div className="remove-skew lmargin75">
          </div>
        </Row>
  		  <Row className="float-left">
  		    <div className="mask-right-no-corner-radius bottom-element">
  		    </div>
  		    <div className="mask-right top-element">
  		    </div>
  		    <div className="main-content tmargin20 hidden-xs hidden-sm">
  		      <Col>
	  		      <span className="primary-text-color font-size-32">Hello!</span>
		  	    </Col>
		   	    <Col className="tmargin50"> 
		  		    <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="password"
                value={newPassword}
                placeholder="Enter a new password"
                onChange={this.handleNewPassword}
              />
		  		  </Col>
		  		  <Col className="tmargin20"> 
		  		    <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="password"
                value={confirmPassword}
                placeholder="Confirm your new password"
                onChange={this.handleConfirmPassword}
              />
		  		  </Col>
		  		  <Col className="style-width tmargin20 lmargin10">
		  		    <input
	                  type="checkbox"/>
		  		    <label className="primary-text-color font-size-14">&nbsp;&nbsp;Remember me
		  		    </label>
		  		    <div className="fright rmargin10">
		  		      <Link to="/property/reset_password">
		  		        <p className="color-blue font-size-14 pointer">Forgot your password?</p>
		  		      </Link>
		  		    </div>
		  		  </Col>
            {
              invalidData && (
                <p className="error-description">{errorDescription}</p>
              )
            }
		  		  <Col className="style-width tmargin10">
		  		      <Button
                  className="style-width button-style text-color-button"
                  onClick={this.changePassword}
                >
                  Login
                  <img src="dist/img/arrow-right.png" className="fright button-image-style" width="20px"/>
                </Button>
		  		  </Col>
            <div className="footer-text tmargin50">
              <div className="fright">
                <span className="color-blue">
                <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.
              </div>
            </div>
  		    </div>
  		  </Row>
        <Row>
          <div className="main-content visible-xs visible-sm">
            <Col>
              <span className="primary-text-color lmargin10 font-size-32">Hello!</span>
            </Col>
            <Col className="tmargin50">
              <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="password"
                value={newPassword}
                placeholder="Enter a new password"
                onChange={this.handleNewPassword}
              />
            </Col>
            <Col className="tmargin20">
              <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="password"
                value={confirmPassword}
                placeholder="Confirm your new password"
                onChange={this.handleConfirmPassword}
              />
            </Col>
            <Col className="style-width tmargin20 lmargin10">
              <input
                type="checkbox"
              />
              <label className="text-color-button font-size-14">
                &nbsp;&nbsp;Remember me
              </label>
              <div className="fright rmargin10">
                <Link to="/property/reset_password">
                  <p className="color-blue font-size-14 pointer">Forgot your password?</p>
                </Link>
              </div>
            </Col>
            {
              invalidData && (
                <p className="error-description tmargin50">{errorDescription}</p>
              )
            }
            <Col className={`style-width ${invalidData ? 'tmargin0' : 'tmargin50'}`}>
              <Button
                className="style-width tmargin20 button-style text-color-button"
                onClick={this.changePassword}
              >
                Login
                <img src="dist/img/arrow-right.png" className="fright button-image-style" width="20px" />
              </Button>
            </Col>
            <div className="align-content-bottom text-align-center tmargin50">
              <p><span className="color-blue">
              <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.</p>
            </div>
          </div>
        </Row>
  		</Grid>
  		);
  	}
  }


LoginWithNewPassword.contextTypes = {
  router: PropTypes.object,
};


export default LoginWithNewPassword;

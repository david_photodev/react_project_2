import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import '../../../css/HomePage/LoginPage.scss';
import AuthStore from '../../stores/AuthStore';

class ResetPasswordSent extends Component {
  componentWillMount() {
    $('link#font-link').remove();
    const backOfficeFontLink = $('link#font-link-backoffice');
    if (!backOfficeFontLink.length) {
      $('link#font-link-backoffice').remove();
      const mainTypography = 'Montserrat';
      const linkCss = `https://fonts.googleapis.com/css?family=${mainTypography}:400,700`;
      const $css = $(`<link id="font-link-backoffice" href=${linkCss} rel='stylesheet'/>`);
      $css.appendTo('head');
      $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
    }
    const isLoggedIn = AuthStore.isLoggedIn();
    // if (isLoggedIn) {
    //   this.setState({ isLoggedIn });
    // }
    window.scrollTo(0, 0);
  }

  render(){
    // const { isLoggedIn } = this.state;
    // if (isLoggedIn) {
    //   return <Redirect to="/property/event/list" />;
    // }
  	return(
  		<Grid className="login-page-container reset-password-sent-container">
  		  <Row className="mask">
          <div className="header-logo-top lmargin45 hidden-xs hidden-sm">
              <Col className="lmargin45 text-color-button">
                <p>Welcome to</p>
              </Col>
              <Col className="lmargin20">
                <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
      
              </Col>
          </div>
          <div className="column-header margin-header visible-xs visible-sm">
             <Col className="lmargin30 text-color-button">
                <p>Welcome to</p>
             </Col>
             <Col>
                <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
             </Col>
          </div>
  		  </Row>
  		  <Row className="mask-down">
  		  </Row>
  		  <Row className="float-left">
  		    <div className="mask-right-no-corner-radius bottom-element">
          </div>
          <div className="mask-right top-element">
          </div>
          <div className="main-content tmargin20 hidden-xs hidden-sm">
            <Col className="lmargin10">
              <span className="primary-text-color font-size-14"><img src="dist/img/login/logo-email-sent.png" width="45px"/>&nbsp;&nbsp;&nbsp;Email sent</span>
            </Col>
            <Col className="style-width tmargin10 lmargin10">
              <div className="primary-text-color font-size-32">Please check your</div>
              <div className="primary-text-color font-size-32">inbox and follow</div>
              <div className="primary-text-color font-size-32">instructions.</div>
              <div className="tmargin20">
                <div className="display-inline">
                 <span className="primary-text-color font-size-14">Don't forget to check in spam folder</span>
                </div>
                <div className="fright">
                <Link to="/property/login">
                 <Button className="text-color-button font-size-14 button-send-email">I got it !</Button>
                </Link>
                </div>
              </div>
            </Col>
            <div className="footer-text tmargin50">
              <div className="fright">
                <span className="color-blue">
                <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.
              </div>
            </div>
          </div>
  		  </Row>
        <Row>
          <div className="main-content visible-xs visible-sm">
            <Col className="lmargin10 content-align-bottom">
              <span className="primary-text-color font-size-14"><img src="dist/img/login/logo-email-sent.png" width="45px"/>&nbsp;&nbsp;&nbsp;Email sent</span>
            </Col>
            <Col className="style-width tmargin50 lmargin10 margin-content">
              <div className="primary-text-color font-size-32">Please check your inbox</div>
              <div className="primary-text-color font-size-32">and follow instructions.</div>
              <div className="tmargin20">
                <Link to="/property/login">
                 <Button className="text-color-button font-size-14 button-got-it">I got it !</Button>
                </Link>
              </div>
              <div className="tmargin20 text-align-center">
                <span className="tmargin20 primary-text-color font-size-14">Don't forget to check in spam folder</span>
              </div>
            </Col>
            <div className="align-content-bottom text-align-center tmargin50">
              <p><span className="color-blue">
              <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.</p>
            </div>
          </div>
        </Row>
  		</Grid> 
  	);
  }
}
export default ResetPasswordSent;
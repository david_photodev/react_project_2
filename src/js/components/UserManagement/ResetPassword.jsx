import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import ReactSVG from 'react-svg';
import '../../../css/HomePage/LoginPage.scss';
import AuthStore from '../../stores/AuthStore';

class ResetPassword extends Component {
  constructor(props){
    super(props);
    this.state = {
      userEmail: '',
      successEmail: false,
      invalidData: false,
      errorMessage: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.sendEmail = this.sendEmail.bind(this);
  }

  componentWillMount() {
    $('link#font-link').remove();
    const backOfficeFontLink = $('link#font-link-backoffice');
    if (!backOfficeFontLink.length) {
      $('link#font-link-backoffice').remove();
      const mainTypography = 'Montserrat';
      const linkCss = `https://fonts.googleapis.com/css?family=${mainTypography}:400,700`;
      const $css = $(`<link id="font-link-backoffice" href=${linkCss} rel='stylesheet'/>`);
      $css.appendTo('head');
      $(`<style>body {font-family: ${mainTypography}}</style>`).appendTo('head');
    }
    const isLoggedIn = AuthStore.isLoggedIn();
    if (isLoggedIn) {
      this.setState({ isLoggedIn });
    }
    window.scrollTo(0, 0);
  }

  handleChange(e) {
    const userEmail = e.target.value;
    this.setState({
      userEmail,
      invalidData: false,
    });
  }

  sendEmail() {
    const { userEmail } = this.state;
    const params = {
      email: userEmail,
    };
    AuthStore.forgotPassword(params, (success, res) => {
      if (success) {
        this.setState({
          successEmail: true,
          invalidData: false,
        });
      } else {
        this.setState({
          successEmail: false,
          invalidData: true,
          errorMessage: res.email,
        });
      }
    });
  }

  render() {
    const { successEmail, invalidData, userEmail, isLoggedIn, errorMessage } = this.state;
    if (successEmail) {
      return <Redirect to="/property/reset_password_sent" />;
    }
    if (isLoggedIn) {
      return <Redirect to="/property/reset_password_sent" />;
    }
  	return(
  		<Grid className="login-page-container reset-password-container">
  		  <Row className="mask">
          <div className="header-logo-top lmargin45 hidden-xs hidden-sm">
              <Col className="lmargin45 text-color-button">
                <p>Welcome to</p>
              </Col>
              <Col className="lmargin20">
                <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
      
              </Col>
          </div>
          <div className="column-header margin-header visible-xs visible-sm">
            <Col className="lmargin30 text-color-button">
                <p>Welcome to</p>
              </Col>
              <Col>
                <ReactSVG 
                  wrapper="span" 
                  className="svg-wrapper white-logo-spazious" 
                  src="dist/img/logo.svg"  
                  alt="White logo"/>
              </Col>
          </div>
  		  </Row>
  		  <Row className="mask-down">
  		  </Row>
  		  <Row className="float-left">
  		    <div className="mask-right-no-corner-radius bottom-element">
  		    </div>
  		    <div className="mask-right top-element">
  		    </div>
  		    <div className="main-content tmargin20 hidden-xs hidden-sm">
	  		    <Col className="lmargin10">
		  		    <span className="primary-text-color font-size-32">Reset Password</span>
		  	    </Col>
		  	    <Col className="style-width tmargin10 lmargin10">
		  	      <span className="primary-text-color font-size-14">Enter the email you used when signed up</span>
		  	    </Col>
		  	    <Col className="tmargin20"> 
		  		    <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="text"
                value={userEmail}
                placeholder="Enter your email"
                onChange={this.handleChange}
              />
  		  		</Col>
            {
              invalidData && (
                <p className="error-description">{errorMessage}</p>
              )
            }
  		  		<Col className="style-width tmargin20">
              <Link to="/property/login">
  		  		    <Button className="primary-text-color button-back-to-signin"><img className="rmargin10" src="dist/img/login/arrow-left-blue.png" width="5px"/> Go back to <strong>sign in</strong></Button>
  		  		  </Link>
              <div className="fright">
		  		      <Button
                  className="text-color-button font-size-14 button-send-email"
                  onClick={this.sendEmail}
                >
                  email me
                  <img src="dist/img/arrow-right.png" className="fright align-button-image" />
                </Button>
  		  		  </div>
  		  		</Col>
            <div className="footer-text tmargin50">
                <div className="fright">
                  <span className="color-blue">
                  <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.
                </div>
            </div>
  		    </div>
  		  </Row>
        <Row className="visible-xs visible-sm">
          <div className="main-content tmargin20 visible-xs visible-sm">
            <Col className="lmargin10">
              <span className="primary-text-color font-size-32">Reset Password</span>
            </Col>
            <Col className="style-width tmargin10 lmargin10">
              <p className="primary-text-color font-size-14">Enter the email you used when <span className="arrow-visible">signed up.</span></p>
              <p className="primary-text-color font-size-14 text-wrap">signed up.</p>
            </Col>
            <Col className="tmargin20">
              <input
                className={`input-field-style ${invalidData ? 'invalid-input' : ''}`}
                type="text"
                value={userEmail}
                placeholder="Enter your email"
                onChange={this.handleChange}
              />
            </Col>
            {
              invalidData && (
                <p className="error-description tmargin150">{errorMessage}</p>
              )
            }
            <Col className={`style-width ${invalidData ? 'tmargin0' : 'tmargin150'}`}>
              <Link to="/property/login">
                <Button className="primary-text-color button-back-to-signin">
                  <img className="rmargin10 arrow-visible" src="dist/img/login/arrow-left-blue.png" width="5px" />
                  Go back to <strong>sign in</strong>
                </Button>
              </Link>
              <div className="fright">
                <Button
                  className="text-color-button font-size-14 button-send-email"
                  onClick={this.sendEmail}
                >
                  email me
                  <img src="dist/img/arrow-right.png" className="fright align-button-image" width="20px" />
                </Button>
              </div>
            </Col>
            <div className="align-content-bottom text-align-center tmargin50">
              <p><span className="color-blue">
              <a href="mailto: hello@spazious.com">Contact us</a></span> if having problems logging in.</p>
            </div>
          </div>
        </Row>
  		</Grid> 
  	);
  }
}
export default ResetPassword;
function downloadAsPDF(element, fileName, callback) {
  convertToPDF(element, (pdf) => {
    pdf.save(fileName);
    if (callback) {
      callback();
    }
  });
}

function outputAsPDF(element, fileName, callback) {
  convertToPDF(element, (pdf) => {
    const blob = pdf.output('blob', { filename: fileName });
    if (callback) {
      callback(blob);
    }
  });
}

function convertToPDF(element, callback) {
  // timeout to allow for hiding virtual keyboard.
  // Also scroll to top of the page
  // Otherwise it results in window resize during canvas conversion leading to css distortions
  window.scrollTo(0,1);
  setTimeout(function(){
    computedStyleToInlineStyle(element);

    html2canvas(element, { useCORS: true, windowWidth: 1632, scale: 1 })
      .then((canvas) => {
        const imgData = canvas.toDataURL('image/jpeg');
        const { width, height } = canvas;
        const pdf = new jsPDF({
          orientation: (width > height ? 'landscape' : 'portrait'),
          unit: 'pt',
          format: [width, height],
        });
        pdf.addImage(imgData, 'PNG', 0, 0, width, height);

        callback(pdf);
      });
  }, 1000);
}

function computedStyleToInlineStyle(container) {
  var mappings = [
    {
      'selector': 'svg .fill-primary',
      'attrs': ['fill']
    },
    {
      'selector': 'svg .stroke-primary',
      'attrs': ['stroke']
    }
  ];
  container = container || document;

  mappings.forEach(function(mapping){
    var nodes = container.querySelectorAll(mapping.selector);
    for(var i=0; i<nodes.length; i++) {
      mapping.attrs.forEach(function(attr) {
        if(nodes[i].hasAttribute(attr)) {
          var computedStyle = getComputedStyle(nodes[i]);
          var computedVal = computedStyle.getPropertyValue(attr);
          if(computedVal.length) {
            nodes[i].setAttribute(attr, computedVal);
          }
        }
      });
    }
  });
}

function adjustForScrollBar(selector) {
  var elements = document.querySelectorAll(selector);
  for (var i=0; i<elements.length; i++) {
    var sWidth = getScrollbarWidth(elements[i]);
    if (sWidth > 0) {
      elements[i].style['margin-right'] = "-"+sWidth+"px";
    }
  }
}

function getScrollbarWidth(element) {
  return element.offsetWidth-element.clientWidth;
}


//to support old browsers you need to shim Array.forEach

Array.prototype.forEach= [].forEach || function(fun, scope){
    var T= this, L= T.length, i= 0;
    if(typeof fun== 'function'){
        while(i< L){
            if(i in T){
                fun.call(scope, T[i], i, T);
            }
            ++i;
        }
    }
    return T;
}

Number.prototype.nth= function(){
    if(this> 3 && this<21) return this+'th';
    var suffix= this%10;
    switch(suffix){
        case 1:return this+'st';
        case 2:return this+'nd';
        case 3:return this+'rd';
        default:return this+'th';
    }
}


// Convert the number words to number

function makeFloors() {
  var nums= [
      ['ground', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
      'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'],
      ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred']
  ],
  floor = {
      first:1, second:2, third:3, fifth:5, eighth:8, ninth:9, twelfth:12
  };
  nums[0].forEach(function(itm, i){
      floor[itm]= i;
      if(i>3)floor[itm+'th']= i;
  });
  nums[1].forEach(function(itm, i){
      i = 10 * i;
      if(itm) floor[itm]= i;
      floor[itm+'th']= i;
      if(i<100) floor[itm.slice(0, -2)+'tieth']= i;
  });
  return floor;
}

function getFloor(s) {
    const floor = makeFloors();
    if(parseInt(s)) return parseInt(s);
    s= s.toLowerCase().replace(/(\W+|and|floor)/g, '').replace(/y(\S)/, 'y $1');
    var F= 0, nums, ax, s2;
    var ax= s.indexOf('hundred');
    if(ax!= -1){
        s= s.split(/hundred/);
        s2= s[0]? floor[s[0]]:1;
        F= 100*s2;
        s= s[1];
    }
    if(floor[s]) F+= floor[s];
    else{
        s= s.split(/ +/);
        if(floor[s[0]]) F+= floor[s[0]];
        if(floor[s[1]]) F+= floor[s[1]];
    }
    return F;
}

// Compare two objects

function isEquivalent(a, b) {
  const replaceItems = Object.keys(a).filter(info => a[info] === 'None' || a[info] === null);
  replaceItems.forEach((element) => {
    delete a[element];
  });
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);
  // if (aProps.length !== bProps.length) {
  //   return false;
  // }
  for (let i = 0; i < aProps.length; i++) {
    const propName = aProps[i];
    if (a[propName] !== b[propName]) {
      return false;
    }
  }
  return true;
}

export default {
  downloadAsPDF,
  outputAsPDF,
  adjustForScrollBar,
  getFloor,
  isEquivalent
};

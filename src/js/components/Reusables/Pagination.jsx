/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions,
react/jsx-no-bind,react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selectedPage || 1,
    };
  }
  // check selected page is same as state.selected
  // when receive props
  componentWillReceiveProps(newProps) {
    if (typeof newProps.selectedPage === 'number' &&
            newProps.selectedPage !== this.state.selected) {
      this.setState({ selected: newProps.selectedPage });
    }
  }

  previous() {
    if (this.state.selected > 1) {
      this.props.handlePageClick(this.state.selected - 1);
      this.setState(state => ({
        selected: state.selected - 1,
      }));
    }
  }
  next() {
    if (this.state.selected < this.props.numOfPages) {
      this.props.handlePageClick(this.state.selected + 1);
      this.setState(state => ({
        selected: state.selected + 1,
      }));
    }
  }
  changePage(pageIndex) {
    this.props.handlePageClick(pageIndex);
    this.setState({ selected: pageIndex });
  }
  pagesCreator() {
    const pages = [];
    const that = this;
    const { selected } = this.state;
    const { padding, selectedTabMargin } = this.props;
    const numOfPages = Math.ceil(this.props.numOfPages);
    let lastIsDisplayed = true;
    for (let i = 1; i <= numOfPages; i++) {
      if (i < padding ||
                 i >= numOfPages - padding ||
                (i <= selected + selectedTabMargin && i >= selected - selectedTabMargin)) {
        pages.push((
          <li
            key={i}
            onClick={that.changePage.bind(that, i)}
            className={`${i === selected ? 'active primary-background-color' : ''} page`}
          >{i}
          </li>));
        lastIsDisplayed = true;
      } else if (lastIsDisplayed) {
        pages.push(<li className="skipped-page" key={i}>...</li>);
        lastIsDisplayed = false;
      }
    }
    return pages;
  }
  render() {
    const { selected } = this.state;
    const numOfPages = parseInt(this.props.numOfPages, 10);
    return (
      <ul id="pagination">
        <li
          className={selected === 1 ? 'disabled' : 'previous'}
          onClick={this.previous.bind(this)}
        >
          <i className="fa fa-angle-double-left" />
        </li>
        {this.pagesCreator()}
        <li
          className={(selected === numOfPages) ? 'disabled' : 'next'}
          onClick={this.next.bind(this)}
        >
          <i className="fa fa-angle-double-right" />
        </li>
      </ul>
    );
  }
}
Pagination.propTypes = {
  numOfPages: PropTypes.number.isRequired,
  handlePageClick: PropTypes.func.isRequired,
  selectedPage: PropTypes.number,
  padding: PropTypes.number,
  selectedTabMargin: PropTypes.number,
};
Pagination.defaultProps = {
  padding: 3,
  selectedTabMargin: 1,
};
export default Pagination;

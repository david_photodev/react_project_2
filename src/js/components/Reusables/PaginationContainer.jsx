/* eslint-disable react/prop-types,class-methods-use-this,react/jsx-no-bind */
import React from 'react';
import Pagination from './Pagination';

class PaginationContainer extends React.Component {
  /**
     * Will change the table offset value
     * @param  {[numnrt]} page [description]
     */
  constructor(props) {
    super(props);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  handlePageClick(page) {
    if (this.props.selectedPage !== page) {
      this.props.paginationCB(page);
    }
  }

  render() {
    const { count } = this.props;
    const numOfPages = Math.ceil(count / 20);
    const selected = 1;
    return (
      <div id="pagination-container" className="container-fluid">
        <div className="row">
          <div className="text-center">
            <Pagination
              numOfPages={numOfPages}
              handlePageClick={this.handlePageClick}
              selectedPage={this.props.selectedPage}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default PaginationContainer;

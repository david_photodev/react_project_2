import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

class LightBox extends Component {
  constructor(props) {
    super(props);
    this.handleKeyup = this.handleKeyup.bind(this);
  }

  handleKeyup() {
    console.log('Key up');
  }

  render() {
    const {
      header, showModal, handleClose, handleSubmit, message, successText, failureText, inputField, handleChange,
      placeholder,
    } = this.props;
    return (
      <Modal show={showModal} className="alert-confirmation-popup">
        <Modal.Header>
          <Modal.Title>{header}</Modal.Title>
          <span
            className="close-icon"
            onClick={handleClose}
            onKeyUp={this.handleKeyup}
          >
            &#x2715;
          </span>
        </Modal.Header>
        <Modal.Body>
          <p className={`${inputField ? 'no-border' : ''}`}>
            {message}
          </p>
          {
            inputField && (
              <div className="input-field-container">
                <input
                  className="input-field primary-color-light primary-border-color-light"
                  type="text"
                  placeholder={placeholder}
                  onChange={handleChange}
                />
              </div>
            )
          }
          <div className="modal-action-btns">
            {
              failureText && <Button className="skip" onClick={handleClose}>{failureText}</Button>
            }
            {
              successText && <Button className={failureText ? "change":"change ok-right"} onClick={handleSubmit}>{successText}</Button>
            }
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

LightBox.propTypes = {
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  successText: PropTypes.string.isRequired,
  failureText: PropTypes.string.isRequired,
  showModal: PropTypes.bool.isRequired,
};

export default LightBox;

import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Alert,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import CommonLayout from '../CommonLayout/CommonLayout';
import AuthStore from '../../stores/AuthStore';
import '../../../css/Notifications/Notifications.scss';
import NotificationAction from '../../actions/NotificationAction';
import NotificationStore from '../../stores/NotificationStore';
import RFPManagementAction from "../../actions/RFPManagementAction";

class Notifications extends CommonLayout{
    constructor(props){
    	super(props);
    	this.state = {
    		notificationInfo: NotificationStore.getAll(),
           isLoggedIn : false,
           unReadNotification: 0,
           setLoader: false,
    	};
    	this.onNotificationStoreChange = this.onNotificationStoreChange.bind(this);
    	this.onAuthStoreChange = this.onAuthStoreChange.bind(this);
    	this.updateUnreadToRead = this.updateUnreadToRead.bind(this);    
    	this.handleSearch = this.handleSearch.bind(this);	
    }
    componentWillMount(){
    	window.scrollTo(0, 0);
    	const isLoggedIn = AuthStore.isLoggedIn();
    	if (isLoggedIn) {
    		this.setState({ isLoggedIn });
            NotificationAction.clearStoreInfo();
            let { unReadNotification, notificationInfo } = this.state;
	    	const userInfo = AuthStore.getAll();
				const {data} = userInfo.user;
				this.setState({data})
		    const { unReadNotificationCount, notificationList, searchVal } = notificationInfo;
		    NotificationAction.searchBy('');
		    if (notificationList.length === 0) {
		    	this.setState({ setLoader: true });
	      	NotificationAction.getNotificationInfos(data.id, (res)=> {
	      		this.setState({ setLoader: false });
	      	});
		    }
    	}
	    NotificationStore.addStoreListener(this.onNotificationStoreChange);
	    AuthStore.addStoreListener(this.onAuthStoreChange);
    }

    onNotificationStoreChange(action) {
	    console.log("onNotificationStoreChange", arguments);
	    const notificationInfo = NotificationStore.getAll();
	    this.setState({ notificationInfo });
		}

  onAuthStoreChange() {
    const isLoggedIn = AuthStore.isLoggedIn();
    this.setState({ isLoggedIn });
  }

	updateUnreadToRead(updateNotification) {
		const { router } = this.context;
		if (updateNotification.context.hasOwnProperty('event_uuid')) {
			router.history.push(`/property/event/view/${updateNotification.context.event_uuid}`);
		} else {
            router.history.push('/property/event/list');
		}
		if (updateNotification.status === 1) {
			const { notificationInfo } = this.state;
			const { data } = this.state;
			NotificationAction.updateNotificationStatus(data.id, updateNotification.id, (res)=> {
				if (notificationInfo.notificationList.filter(each=> each.id === res.id).length >= 1){
					notificationInfo.notificationList.filter(each=> each.id === res.id)[0]["status"]=2;
				}

				if (notificationInfo.allNotification.filter(each=> each.id === res.id).length >= 1) {
					notificationInfo.allNotification.filter(each=> each.id === res.id)[0]["status"]=2;
				}
				
				if (notificationInfo.unReadNotificationCount) {
					notificationInfo.unReadNotificationCount = notificationInfo.unReadNotificationCount - 1; 
				}
				this.setState(notificationInfo);
			});
		}
	}

	handleSearch(e) {
    	NotificationAction.searchBy(e.target.value);
	}

	render(){
		const { isLoggedIn, isRedirect, isMobileSidebar, data, unReadNotification, notificationInfo, setLoader} = this.state;
    if (isRedirect || !isLoggedIn) {
      return <Redirect to="/" />;
    }
		const { notificationList, searchVal, unReadNotificationCount } = notificationInfo;
		const notificationResults = notificationList;
     //    const { first_name, last_name, profile } = data;
	    // const firstInitial =  first_name ? first_name[0] : '';
	    // const lastInitial = last_name ? last_name[0] : '';
	    // let userInitial = `${firstInitial}${lastInitial}` ;
     //    userInitial = userInitial.toUpperCase();
     	const anonymousStatus = (notification) => {
     		if (!(notification.context.creator.hasOwnProperty('role') && notification.context.creator.hasOwnProperty('id'))) {
	     		return true;
	     	}
	     	else if (notification.context.creator.hasOwnProperty('is_anonymous')) {
	     		return notification.context.creator.is_anonymous;
	     	} else if (notification.context.hasOwnProperty('is_anonymous')) {
				return notification.context.is_anonymous;
	     	} 
     	}

     	const userInitial = (notification) => {
     		if (notification.context.creator.hasOwnProperty('role') && notification.context.creator.hasOwnProperty('id') && notification.context.creator.hasOwnProperty('name')) {
     			return true;
     		} else {
     			return false;
     		}
     	}

	    if (notificationResults && notificationResults.length >= 1) {
	      notificationResults.sort(function(a, b){
	        var keyA = new Date(a.date),
	            keyB = new Date(b.date);
	        // Compare the 2 dates
	        if(keyA < keyB) return 1;
	        if(keyA > keyB) return -1;
	        return 0;
	      });
	    }
		return(
			<Grid className="primary-color notifications-container layout-header">
			  {this.sideBar( null, false, unReadNotificationCount)} 
			  {
                 isMobileSidebar && (
                   this.mobileSideBar()
                 )
              }
		   	  <Row className="content-right">
		   	    <Row>
	  		      {this.pageHeader()}
	              <Row>
	               <Col className="tmargin20" lg={12} md={12} sm={12} xs={12}>
	                 <Col className="primary-border-color fright border-search-bar" lg={3} md={4} sm={5} xs={8}>
		              <Col className="lpadding0" sm={9} xs={9}>
		                <div>
		                  <input
		                  className="find-text text-font14"
		                  type="text"
		                  placeholder="Find Notification"
		                  value={searchVal}
		                  onChange={this.handleSearch}
		                  />
		                </div>
		              </Col>
		              <Col className="fleft search-icon" lg={3} md={3} sm={3} xs={3}>
		                <span className="fright"><ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/event_list/search.svg" width="25px"/></span>
		              </Col>
		             </Col>
                   </Col>
	              </Row>
	            </Row>
	            <Row className="row-title">
	              <Col>
	                 <div className="column-header primary-color" >
	                     <p className="header-title"><strong>Notifications</strong></p>
	                 </div>
	              </Col>
	            </Row>
	            <Row className="main-content">
	            { notificationResults && notificationResults.length >=1 &&
					notificationResults.map(notificationResult =>(
		          <Row className="list-event">
		            <Col>
		               <span className={notificationResult.status===1 ? 'oval-copy' : 'read-notification'}></span>
		            </Col>
		            <Col 
		            	className={`${notificationResult.status === 2 ? 'display-contents-viewed' : 'display-contents'}`} 
		            	onClick={()=>{this.updateUnreadToRead(notificationResult)}}
		            >
		              <Col className="column-profile" lg={2} md={2} sm={2} xs={4}>
		              <div className={`user-name ${anonymousStatus(notificationResult) ? 'align-image' : 'align-text'}`}>	             
		                {
		                  anonymousStatus(notificationResult) ? (
		                  	<i className="fa user-icon-size fa-user" aria-hidden="true"></i>
		                  ) : (userInitial(notificationResult) && notificationResult.context.creator.name.split(' ').length === 1 ? notificationResult.context.creator.name.split(' ')[0].charAt(0).toUpperCase()+notificationResult.context.creator.name.split(' ')[0].charAt(1).toUpperCase() 
		                	: notificationResult.context.creator.name.split(' ')[0].charAt(0).toUpperCase()+notificationResult.context.creator.name.split(' ')[1].charAt(0).toUpperCase())
		                }
		              </div>
                        {notificationResult.context.creator.hasOwnProperty('role') && <span className="user-role"><strong>{notificationResult.context.creator.role === 'Admin' ? "SA" : (notificationResult.context.creator.role.split(' ').length === 1 ? notificationResult.context.creator.role.split(' ')[0].charAt(0).toUpperCase() 
                        	: notificationResult.context.creator.role.split(' ')[0].charAt(0).toUpperCase()+notificationResult.context.creator.role.split(' ')[1].charAt(0).toUpperCase())}</strong></span>}
                      </Col>
	                  <Col className="column-detail" lg={8} md={8} sm={10} xs={8}>
	                      <div className="primary-color visible-xs visible-sm">{"On "+moment(notificationResult.created_at, 'YYYY-MM-DD').format('MMM. DD, YYYY')}</div> 
	                      <div className="primary-color align-event text-font-14">{notificationResult.message}</div>
		                  <div className="room-details tmargin5"><strong>{notificationResult.context.event_name+' / '+notificationResult.context.event_type}</strong></div>
		                  <div className="primary-color tmargin5 text-font-12 hidden-xs hidden-sm">
		                    <span>{moment(notificationResult.context.start_date, 'YYYY-MM-DD').format('MMMM DD, YYYY')+'    |'}</span>
		                    <span className="lmargin10">{`${moment(notificationResult.context.start_date, 'YYYY-MM-DD HH:mm').format('HH:mm')} to ${moment(notificationResult.context.end_date, 'YYYY-MM-DD HH:mm').format('HH:mm')}h.   |`}</span>
		                    <span className="lmargin10">{notificationResult.context.attendees+' attendees'} </span>
		                  </div>
		               </Col>
		               <Col className="column-date fright primary-border-left hidden-xs hidden-sm" lg={2} md={2}>
		                  <div className="tmargin20">
	                        <span className="primary-color lmargin10">{"On "+moment(notificationResult.created_at, 'YYYY-MM-DD').format('MMM. DD, YYYY')}</span>
    	                  </div>
		               </Col>
		            </Col>
		          </Row>
		        )) 
		        } 		          
		          </Row>
	          	{ setLoader && <Row>
		          	<div className="empty-noti-msg">
	                  <img src="dist/img/loading.png" alt="White Logo" width="75"/>
	                </div>
	          	</Row>
	          }
	          { ((!notificationResults)|| (notificationResults.length < 1)) && (!setLoader) &&
	          	<Row>
		          	<div className="empty-noti-msg">
	                  <span>
	                    No new notifications available
	                  </span>
	                </div>
	          	</Row>
	          }
	            </Row>
	            <div className="site-footer visible-xs">
		            <div className="logo-section">
		                <p className="rmargin10">Powered by</p>
		                <img src="dist/img/white-logo.png" alt="White Logo" />
		            </div>
		            <div className="footer-menu-section">
		                <span>Hotel</span>
		                <span>Meeting Rooms</span>
		            </div>
		        </div>
			</Grid>
		);
	}
}

Notifications.contextTypes = {
  router: PropTypes.object,
};

export default Notifications;
import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import SetupDropdown from './SetupDropdown';
import RoomGallery from './RoomGallery';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import ReactSVG from 'react-svg';

class MeetingRoom extends Component {
  constructor(props) {
    super(props);
    const { hotelInfo } = this.props;
    const { meetingRoomInfo } = hotelInfo;
    this.state = {
      meetingRoomInfo,
    };
    this.capacityArray = ['0-20', '21-50', '51-100', '101-200', 'More than 201'];
    this.handleClick = this.handleClick.bind(this);
    this.handleSetupConfig = this.handleSetupConfig.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.showFullImage = this.showFullImage.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleOwnConfig = this.handleOwnConfig.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.meetingRoomInfo !== props.meetingRoomInfo) {
      return {
        meetingRoomInfo: props.hotelInfo.meetingRoomInfo,
      };
    }
    return null;
  }

  componentDidMount() {
    const { hotelInfo, userPrimaryColor } = this.props;
    // $('.primary-color').css('color', userPrimaryColor);
    // $('.primary-color::after').css('border-color', userPrimaryColor);
    // $('.primary-color::before').css('border-color', userPrimaryColor);
    // // $('.site-footer').css('background-image', `linear-gradient(0deg, ${userPrimaryColor} 45%,
    // //   rgb(185,146,146) 145%)`);
    // $('hr.meeting-room-line').css('border-top', `1px solid ${userPrimaryColor}`);
  }

  handleClick(id) {
    const { meetingRoomInfo } = this.state;
    const selectedRoom = meetingRoomInfo.map((item) => {
      const selectedItem = item;
      if (item.id !== id) {
        selectedItem['showSetup'] = false;
        return selectedItem;
      }
      selectedItem['showSetup'] = !selectedItem['showSetup'];
      return selectedItem;
    });
    this.setState({ meetingRoomInfo: selectedRoom });
  }

  handleSetupConfig(room, setup) {
    const match = this.props;
    const roomId = room.id;
    const setupId = setup.id;
    const setupCount = setup.count;
    MeetingSpaceAction.updateAddons(room, setup, 'meetingRoomInfo');
    const { meetingRoomInfo } = this.state;
    let msg = '';
    const selectedSetup = meetingRoomInfo.filter(roomSetupConfigs => roomSetupConfigs.id === roomId);

    if (setupCount > 0){
      const selectedRoom = selectedSetup[0].setup.map((item) => {
        const selectedConfig = item;
        if (item.id !== setupId) {
          // selectedConfig['active'] = false;
          return selectedConfig;
        }
        selectedConfig['active'] = !selectedConfig['active'];
        selectedConfig['activity_name'] = '';
        selectedConfig['event_date_starts'] = '';
        selectedConfig['event_date_ends'] = '';
        selectedConfig['event_hour'] = '';
        selectedConfig['attendees_count'] = null;
        if (selectedConfig['active']) {
          msg = `${selectedSetup[0].room_name} added to RFP`;
          MeetingSpaceAction.notificationMsg(msg, 'add');
        }
        setTimeout(() => {
          msg = `${selectedSetup[0].room_name} added to RFP`;
          MeetingSpaceAction.notificationMsg(msg, 'remove');
        }, 5000);
        return selectedConfig;
      });
      MeetingSpaceAction.updateRoomSetup(roomId, setupId, selectedRoom);
      MeetingSpaceAction.updateLayout(room, setup, 'view');

      MeetingSpaceAction.storeSceneData(room.scenes);
      MeetingSpaceAction.showSetupView(room);

      this.context.router.history.push(`${match.url}/dollhouse`);


    }
  }

  showFullImage(item, type) {
    const { showFullImage } = this.props;
    showFullImage(item, type);
  }

  handleKeyUp(e) {
    console.log('keyup', e);
  }

  handleClickOutside() {
    const { meetingRoomInfo } = this.state;
    const selectedRoom = meetingRoomInfo.map((item) => {
      const selectedItem = item;
      selectedItem['showSetup'] = false;
      return selectedItem;
    });
    this.setState({ meetingRoomInfo: selectedRoom });
  }

  handleOwnConfig(room) {
    const { handle3DPopup, match } = this.props;
    if (!room.available_model) {
      handle3DPopup(true);
      // return false;
    } else {
      MeetingSpaceAction.storeSceneData(room.scenes);
      const customSetup = room.setup.filter(setup => setup.type.toLowerCase() === 'custom');
      MeetingSpaceAction.updateLayout(room, customSetup[0]);
      MeetingSpaceAction.createOwnSetup();
      this.context.router.history.push(`${match.url}/dollhouse`);
      this.showFullImage(0, 'close');
    }
    // if (toPageNavigate !== 'room_setup') {
    //   handleTabClick('room_setup');
    // }
  }

  render() {
    const {
      meetingRoomInfo,
    } = this.state;
    const {
      hotelInfo, isFullImage, handleDropdown, typeOfSetup, typeOfCapacity, handleToggle, sqmMode, match, isGetAllMeetingRooms, userPrimaryColor,
    } = this.props;
    const { available_setup, data } = hotelInfo;
    const checkmarkStyle = {
      backgroundColor: userPrimaryColor,
    };
    // const { primary_color } = userPrimaryColor;
    const circleSliderStyle = { background: `linear-gradient(159deg, ${userPrimaryColor}80, ${userPrimaryColor})` };
    return (
      <Grid className="primary-color prelative">
        <div className="overlay" />
          <div className="text-right bmargin15 meter-feet-toggle">
            <span className={sqmMode ? "i-block font-bold rmargin20" : "i-block rmargin20"}>SQM</span>
            <label className="switch meter-toggle">
              <input type="checkbox" 
              checked = {!sqmMode}
              onChange={(e) => handleToggle(e)}
              />
              <span className="slider-no-background round">
                <span className="circle" style={circleSliderStyle} />
              </span>
            </label>
            <span className={sqmMode ? "i-block lmargin20" : "i-block font-bold lmargin20"}>SQFT</span>
          </div>
        <div className="filter-container fleft">
          <select
            className="filter-rooms type-setup"
            onChange={e => handleDropdown('setup', e)}
            value={typeOfSetup === 'setup' ? 'Type of Setup' : typeOfSetup}
          >
            <option key="setup" value="Type of Setup" disabled hidden>Type of Setup</option>
            <option
              key="all"
              value="all"
            >
              All
            </option>
            {
              available_setup.map(list => (
                <option
                  key={list.type}
                  value={list.type}
                >
                  {list.shape}
                </option>
              ))
            }
          </select>
          <select
            className="filter-rooms capacity-select"
            onChange={e => handleDropdown('capacity', e)}
            value={typeOfCapacity === 'capacity' ? 'Capacity' : typeOfCapacity}
          >
            <option key="capacity" value="Capacity" disabled hidden>Capacity</option>
            <option
              key="all"
              value="all"
            >
              All
            </option>
            {
              this.capacityArray.map(list => (
                <option
                  key={list}
                  value={list}
                >
                  {list}
                </option>
              ))
            }
          </select>
        </div>
        <Row className="meeting-room-container">
          {
            meetingRoomInfo && meetingRoomInfo.length >= 1
              ? meetingRoomInfo.map(item => (
                <Col lg={3} md={4} sm={6} className="meeting-room" key={item.id}>
                  <div className="room-info-container" style={item.room_selected ? {} : {}}>
                    <img
                      onClick={() => this.showFullImage(item, 'open')}
                      onKeyUp={this.handleKeyUp}
                      style={item.room_selected ? { borderTop: '13px solid' } : {}}
                      className="room-img pointer"
                      src={item.room_image}
                      alt="room_image"
                    />
                    {
                      item.room_selected
                        && (
                          <ReactSVG
                            wrapper="div"

                            // onClick={this.showFullImage}
                            onKeyUp={this.handleKeyUp}
                            style={checkmarkStyle}
                            className="svg-wrapper checkmark-img-selected pointer"
                            src="dist/img/meeting_space/icons/checkmark.svg"
                            alt="checkmark"
                          />
                        )
                    }
                    <div className="room-details">
                      <div className="i-block padding10 fleft width25">
                        {/*<img src="dist/img/meeting_space/icons/eye-red-bg.png" alt="eye icon" />*/}
                        <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/eye-red-bg.svg" alt="eye icon" />
                      </div>
                      <div className="i-block width75">
                        <p className="tmargin20 text-bold black-text meeting-room-info-name">{item.room_name}</p>
                        <div className="i-block value-area rpadding10">
                          <img src="dist/img/meeting_space/icons/superficie-gray-bg.png" alt="meeting icon" />
                          <span className="black-text opacity5 lpadding10">{sqmMode? item.room_space : (parseFloat(item.room_space)*10.764).toFixed(2)+' sq.ft.'}</span>
                        </div>
                        <div className={`i-block ${sqmMode ? 'max-sqm-value' : 'max-sqft-value'}`}>
                          <img
                            className="opacity5 people-icon"
                            src="dist/img/meeting_space/icons/people-black-bg.png"
                            alt="people icon"
                          />
                          <span className="black-text opacity5 lpadding10">{item.room_capacity}</span>
                        </div>
                      </div>
                    </div>
                    <div
                      className={`room-setup-button-container bmargin20 ${item.showSetup ? 'open-setup' : 'close-setup'}`}
                    >
                      <Button className="primary-color" onClick={() => this.handleClick(item.id)}>
                        <span className="fleft i-block">Select room set up</span>
                        {/*<img*/}
                          {/*className="fright arrow"*/}
                          {/*src="dist/img/meeting_space/icons/arrow-icon.png"*/}
                          {/*alt="arrow icon"*/}
                        {/*/>*/}
                        <ReactSVG wrapper="div" className="svg-wrapper fright arrow" src="dist/img/meeting_space/icons/arrow-icon.svg" alt="arrow icon" />
                      </Button>
                      {
                        item.showSetup
                        && (
                          <SetupDropdown
                            item={item}
                            hotelInfo={hotelInfo}
                            handleClickOutside={this.handleClickOutside}
                            handleSetupConfig={this.handleSetupConfig}
                            handleOwnConfig={this.handleOwnConfig}
                            match={match}
                            userPrimaryColor={this.props.userPrimaryColor}
                          />
                        )
                      }
                    </div>
                  </div>
                </Col>
              ))
              : (
                <div className={`padding100 text-center ${meetingRoomInfo.length < 1 && isGetAllMeetingRooms? 'block' : 'hide'}`}>
                  Meeting rooms are not available for this filter. Please change the filter to see more meeting rooms.
                </div>
              )
          }
          {
            isFullImage
            && (
              <RoomGallery
                hotelInfo={hotelInfo}
                handleOwnConfig={this.handleOwnConfig}
                showFullImage={this.showFullImage}
                match={match}
                userPrimaryColor={this.props.userPrimaryColor}
              />
            )
          }
        </Row>
        {
          !isGetAllMeetingRooms && (
            <Row>
              <div className="padding100 text-center">Meeting rooms are loading. Please wait...</div>
            </Row>
          )
        }
        {
          meetingRoomInfo && meetingRoomInfo.length >= 1 && (
            <hr className="meeting-room-line primary-border-top" />
          )
        }
      </Grid>
    );
  }
}

MeetingRoom.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  showFullImage: PropTypes.func.isRequired,
  handleTabClick: PropTypes.func.isRequired,
  handleDropdown: PropTypes.func.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  isFullImage: PropTypes.bool.isRequired,
  typeOfSetup: PropTypes.string.isRequired,
  typeOfCapacity: PropTypes.string.isRequired,
  toPageNavigate: PropTypes.string.isRequired,
  match: PropTypes.string.isRequired,
  sqmMode: PropTypes.bool.isRequired,
  isGetAllMeetingRooms: PropTypes.bool.isRequired,
};

MeetingRoom.contextTypes = {
  router: PropTypes.object,
};

export default MeetingRoom;

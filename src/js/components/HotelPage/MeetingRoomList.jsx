import React, { Component } from 'react';
import {
  Grid, Row, Col, Button, Table,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import Datetime from 'react-datetime';
import ReactSVG from 'react-svg';
import PropTypes from 'prop-types';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import LightBox from '../Reusables/LightBox';
import Helpers from '../Reusables/Helpers';
import AuthStore from "../../stores/AuthStore";
// import customHistory from '../customHistory';

const models = {
  chair: {
    id: 'chair_1',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Chair/chair_005/chair_005.gltf',
  },
  table: {
    id: 'table_1',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Table/table_003/table_003.gltff',
  },
};

const roundModels = {
  chair: {
    id: 'chair_2',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Chair/chair_005/chair_005.gltf',
  },
  table: {
    id: 'table_2',
    type: 'gltf',
    model_path: 'models/3D_PACK_FURNITURE/Table/table_004/table_004.gltf',
  },
};

// const services = ['Screen', 'TV', 'Printer', 'PA System'];
const foodBeverages = ['Breakfast', 'Drinks', 'Coffee', 'Picnic'];
class MeetingRoomList extends Component {
  constructor(props) {
    super(props);
    this.eventHeader = [
      'Selected meeting rooms', 'Event type', 'Event Date Starts', 'Event Date Ends', 'Attendees', 'Room Setup',
      'Duplicate', 'Edit', '',
    ];
    this.eventHeaderView = [
      'Selected meeting rooms', 'Event type', 'Event Date Starts', 'Event Date Ends', 'Attendees', 'Room Setup', 'Edit',
    ]
    this.activityHeader = [
      'Meeting space name', 'Activity name', 'Event Date Starts', 'Event Date Ends', 'Attendees', 'Room Setup',
      'New Time Period', 'Edit', '',
    ];
    this.activityHeaderView = [
      'Meeting space name', 'Activity name', 'Event Date Starts', 'Event Date Ends', 'Attendees', 'Room Setup', 'Edit',
    ];
    this.state = {
      // isSharePopup: true,
      showModal: false,
      storageName: '',
      isTooltip: false,
      tooltipPosition: {},
      setModeType : '',
      link : '',
      shareEmail: false,
    };
    this.timer = 0;
    this.interval = 0;
    this.edit3DSetup = this.edit3DSetup.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClone = this.handleClone.bind(this);
    this.handleCollapse = this.handleCollapse.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleKeyup = this.handleKeyup.bind(this);
    this.newMeetingRoom = this.newMeetingRoom.bind(this);
    this.handleShareInfo = this.handleShareInfo.bind(this);
    this.handleEvents = this.handleEvents.bind(this);
    this.handleActivityInputs = this.handleActivityInputs.bind(this);
    this.handleInputs = this.handleInputs.bind(this);
    this.handleDateEvent = this.handleDateEvent.bind(this);
    this.handleActivityName = this.handleActivityName.bind(this);
    this.handleShowModel = this.handleShowModel.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.setZoom = this.setZoom.bind(this);
    this.setTooltip = this.setTooltip.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleSendMail = this.handleSendMail.bind(this);
  }

  componentWillMount() {
    const { hotelInfo, context, setMode } = this.props;
    // $(`<style>.daterangepicker .drp-calendar .calendar-table table thead{color:
    //   ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.daterangepicker .drp-calendar .calendar-table thead th.month{color:
    //   ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.daterangepicker .drp-calendar .calendar-table table tbody td.active{background-color:
    //   ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.rdt input{color: ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.event-name input::placeholder{color: ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.site-footer{background-image: linear-gradient(0deg, ${hotelInfo.data.primary_color} 45%,
    //   rgb(185,146,146) 145%)}</style>`).appendTo('head');
    // $(`<style>.modal-content{color: ${hotelInfo.data.primary_color}}</style>`).appendTo('head');
    // $(`<style>.modal-content .modal-body .modal-action-btns button{color: ${hotelInfo.data.primary_color};
    //   border-color: ${hotelInfo.data.primary_color}</style>`).appendTo('head');
    // $(`<style>.modal-content .modal-body .modal-action-btns .change{color: #fcfcfc;
    //   background-color: ${hotelInfo.data.primary_color}</style>`).appendTo('head');
    if (context.indexOf('/rfp/view/') > -1) {
      const uuId = context.split('/rfp/view/')[1];
      // eventId = parseInt(eventId, 10);
      MeetingSpaceAction.getEventID(uuId, (eventId)=>{
        MeetingSpaceAction.getContactInformations(eventId);
        MeetingSpaceAction.getAttachmentFile(eventId);
        MeetingSpaceAction.getActivityInformations(eventId);
      });
      setMode('viewOnly');
      this.setState({
        setModeType: 'viewOnly',
        shareEmail: true,
      });
    }
  }

  componentDidMount() {
    const { hotelInfo, userPrimaryColor } = this.props;
    // $('.primary-color').css('color', userPrimaryColor);
    // $('.primary-color::after').css('border-color', userPrimaryColor);
    // $('.primary-color::before').css('border-color', userPrimaryColor);
    // const target = document.getElementById('room_setup_3d');
    // if (target !== null) {
    //   target.scrollIntoView({ block: 'start', behavior: 'smooth' });
    // }
  }

  setZoom(value) {
    const { setZoom } = this.props;
    setZoom(value);
  }

  setTooltip(event) {
    let { isTooltip, tooltipPosition } = this.state;
    const { handleTooltipPosition, handleDisableButtons } = this.props;
    if (event.type === 'itemselected') {
      if (event.data.itemType === 'RulerItem') {
      // disable rotation and finishing buttons
        handleDisableButtons('disable');
      } else {
        handleDisableButtons('enable');
      }
      isTooltip = true;
      tooltipPosition = {
        left: ((event.data.bbox.min.x + event.data.bbox.max.x) / 2) - 45,
        top: event.data.bbox.max.y - 74,
      };
      tooltipPosition['arrowLeft'] = tooltipPosition.left + 35;
      tooltipPosition['arrowTop'] = tooltipPosition.top + 62;
      this.setState({ isTooltip, tooltipPosition });
    } else if (event.type === 'itemdragmove') {
      // isTooltip = true;
      tooltipPosition = {
        left: (event.data.bbox.min.x + event.data.bbox.max.x) / 2 - 45,
        top: event.data.bbox.max.y - 74,
      };
      tooltipPosition['arrowLeft'] = tooltipPosition.left + 35;
      tooltipPosition['arrowTop'] = tooltipPosition.top + 62;
      this.setState({ isTooltip, tooltipPosition });
    } else {
      isTooltip = false;
      this.setState({ isTooltip });
    }
    handleTooltipPosition(isTooltip, tooltipPosition);
    const element = document.getElementById('tooltip-content');
    const container3DWidth = $('#room_setup_3d').width();
    if (element) {
      const elementRect = element.getClientRects()[0];
      if (elementRect.right > container3DWidth) {
        const arrowRight = (container3DWidth - tooltipPosition.left) - 35;
        tooltipPosition['left'] = 'auto';
        tooltipPosition['right'] = 20;
        tooltipPosition['arrowRight'] = arrowRight > 30 ? arrowRight : 30;
        tooltipPosition['arrowLeft'] = 'auto';
      } else if (elementRect.left < 0) {
        tooltipPosition['left'] = 0;
        tooltipPosition['arrowLeft'] = 35;
      }
      this.setState({ isTooltip, tooltipPosition });
      handleTooltipPosition(isTooltip, tooltipPosition);
    }
  }

  edit3DSetup(room, setup) {
    MeetingSpaceAction.storeSceneData(room.scenes);
    const {
      handleTabClick, hotelInfo, setStorageName, handleLayout, setViewer, property, setLoader, setModeType,
      readOnlyMode, handle3DPopup, toPageNavigate, saveRoom, setMode, set3dViewmode, handleFurnitureCount,
      resetDroppedItems,
    } = this.props;
    const { router } = this.context;
    const { match } = router.route;
    resetDroppedItems();
    // const { eventId } = this.state;
    const { viewLayout, selectedLayout, eventId, eventUuId } = hotelInfo;
    const show3dModel = room.available_model;
    if (!show3dModel) {
      handle3DPopup(true);
      return false;
    }
    const { scenes } = room;

    if (scenes.length < 1) {
      const msg = 'Model is not available';
      MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
      }, 5000);
      return false;
    }

    if (eventUuId) {
      this.context.router.history.push(`${match.url}/dollhouse/${eventUuId}/view`);
    } else {
      this.context.router.history.push(`${match.url}/dollhouse`);
    }
    // let alreadyInRoomSetup = (toPageNavigate === 'room_setup');
    // if ( ! alreadyInRoomSetup) {
    //   handleTabClick('room_setup');
    // }

    if (viewLayout.hasOwnProperty('id') && selectedLayout.hasOwnProperty('id')) {
      if (viewLayout.id === room.id && selectedLayout.setup_id === setup.setup_id
        && selectedLayout.chairs_count === setup.chairs_count && selectedLayout.type === setup.type
      ) {
        return false;
      }
    }

    handleLayout(false);
    setLoader(true);

    // handleTabClick('room_setup');
    // handleLayout(room);

    // if(alreadyInRoomSetup) {
    let { viewer } = hotelInfo;
    MeetingSpaceAction.updateLayout(room, setup);
    const storageName = `SceneData ${room.id}${setup.setup_id} ${setup.type}`;
    setStorageName(storageName);
    this.setState({ storageName });
    const container = document.getElementById('scene-container');
    const progress = document.getElementById('scene-load-progress');
    // ModelLoader.load(scenes[0].model3d, (m, scene, error) => {
    //   if (error) {
    //     console.error('Could not load model', error);
    //     return;
    //   }
      // console.log('Model loaded', m, scene);
      // if (m.id) {
        if (container && progress) {
          if (viewer) {
            viewer.dispose();
            setViewer(null);
            MeetingSpaceAction.setViewer(null);
          }
          viewer = new SceneViewer(container, progress);
          const modelsFurniture = this.loadModels();
          if (setModeType === 'viewOnly') {
            // customHistory.push(`#/property/1/3d/${eventId}/view`);
            MeetingSpaceAction.savedFurnitureData(setup.model_3d.data);
            const params = {
              model3d: setup.model_3d.model3d,
              model2d: setup.model_3d.model2d,
              editing: false,
              viewMode: 'DH',
            };
            Object.assign(params, modelsFurniture);
            viewer.init(params);
          } else if (readOnlyMode) {
            const params = {
              model3d: scenes[0].model3d,
              model2d: scenes[0].model2d,
              editing: false,
              viewMode: 'DH',
            };
            Object.assign(params, modelsFurniture);
            viewer.init(params);
          } else {
            const params = {
              model3d: scenes[0].model3d,
              model2d: scenes[0].model2d,
              viewMode: 'DH',
            };
            Object.assign(params, modelsFurniture);
            viewer.init(params);
          }
          setViewer(viewer);
          MeetingSpaceAction.setViewer(viewer);
          viewer.addEventListener('ready', () => this.handleRoomReady());
          viewer.addEventListener('itemselected', event => this.setTooltip(event));
          viewer.addEventListener('itemdragend', event => saveRoom(event));
          viewer.addEventListener('itemadded', event => saveRoom(event));
          viewer.addEventListener('itemupdated', event => saveRoom(event));
          viewer.addEventListener('itemunselected', event => this.setTooltip(event));
          viewer.addEventListener('itemcountchanged', event => handleFurnitureCount(event));
          viewer.addEventListener('zoom', event => this.setZoom(event.value));
          // }
        }
        // setLoader(false);
        document.getElementById('scene-load-progress').style.display = 'none';
      // }
    // }, (m, prog) => {
    //   setLoader(true);
    //   document.getElementById('scene-load-progress').style.display = 'block';
    //   document.getElementById('scene-load-progress').setAttribute('value', prog);
    // });
  }

  loadModels() {
    const {
      hotelInfo, handleModelLoading, setLoader,
    } = this.props;
    const { layout } = hotelInfo;
    const { viewer } = hotelInfo;
    const { storageName } = this.state;
    let data = DataProvider.getData(storageName);
    if (layout.hasOwnProperty('furnitureData')) {
      data = data || layout.furnitureData;
    }
    if (data) {
      return { json: data };
    } else if (layout.type.toLowerCase() === 'custom') {

    } else if (viewer) {
      const peopleCount = layout.chairs_count >= 0 ? layout.chairs_count : layout.count;
      let layoutModel = {};
      const sceneLayout = {
        type: layout.type,
      };
      Object.assign(sceneLayout, layout.config);
      if (sceneLayout.type.toLowerCase() === 'theatre') {
        layoutModel['chair'] = models['chair'];
      } else {
        layoutModel = models;
      }
      let modelsType = (sceneLayout.type.toLowerCase() === 'banquet' || sceneLayout.type.toLowerCase() === 'round'
        || sceneLayout.type.toLowerCase() === 'cabaret' || sceneLayout.type.toLowerCase() === 'cocktail')
        ? roundModels : layoutModel;
      modelsType = Object.keys(layout.furnitures).length >= 1 ? layout.furnitures : modelsType;
      return { models: modelsType };
    }
  }

  handleRoomReady() {
    const {
      hotelInfo, handleModelLoading, setLoader,
    } = this.props;
    const { layout } = hotelInfo;
    const { viewer } = hotelInfo;
    const { storageName } = this.state;
    let data = DataProvider.getData(storageName);
    if (layout.hasOwnProperty('furnitureData')) {
      data = data || layout.furnitureData;
    }
    if (data) {
      setLoader(true);
      viewer.fromJSON(data, () => this.handleModel());
    } else if (layout.type.toLowerCase() === 'custom') {
      viewer.clearFurnitures();
      setLoader(false);
      handleModelLoading(true);
    } else if (viewer) {
      const peopleCount = layout.chairs_count >= 0 ? layout.chairs_count : layout.count;
      let layoutModel = {};
      const sceneLayout = {
        type: layout.type,
      };
      Object.assign(sceneLayout, layout.config);
      if (sceneLayout.type.toLowerCase() === 'theatre') {
        layoutModel['chair'] = models['chair'];
      } else {
        layoutModel = models;
      }
      let modelsType = (sceneLayout.type.toLowerCase() === 'banquet' || sceneLayout.type.toLowerCase() === 'round'
        || sceneLayout.type.toLowerCase() === 'cabaret' || sceneLayout.type.toLowerCase() === 'cocktail')
        ? roundModels : layoutModel;
      modelsType = Object.keys(layout.furnitures).length >= 1 ? layout.furnitures : modelsType;
      console.log('scene layout', sceneLayout, parseInt(peopleCount, 10), parseInt(layout.tables_count, 10),
        parseInt(layout.chairs_per_table, 10), modelsType);
      MeetingSpaceAction.selectedLayout(layout);
      viewer.loadLayout(sceneLayout, peopleCount, layout.tables_count, layout.chairs_per_table, modelsType,
        () => this.handleModel());
    }
  }

  handleModel() {
    const { hotelInfo, readOnlyMode } = this.props;
    const { viewer } = hotelInfo;
    const { handleModelLoading, setLoader } = this.props;
    handleModelLoading(true);
    setLoader(false);
    if (readOnlyMode) {
      viewer.play();
    }
  }

  handleShareInfo(bool) {
    const { showSharePopup, property } = this.props;
    showSharePopup(bool);
    const { hotelInfo } = this.props;
    const { eventUuId } = hotelInfo;
    if(bool){
      const link = `${window.location.origin}/#/property/${property}/rfp/view/${eventUuId}`;
      this.setState({
        link: link,
      });
    }
  }

  handleDelete(room, setup) {
    const { hotelInfo, toPageNavigate } = this.props;
    const { router } = this.context;
    const { match } = router.route;
    const { floorPlan, viewLayout, selectedRooms } = hotelInfo;
    if (viewLayout.id === room.id && toPageNavigate === 'room_setup') {
      if (selectedRooms.length >= 1) {
        const updateMeetingRoomList = selectedRooms.filter((sroom) => {
          const listedRoom = sroom;
          if (sroom.id !== room.id) {
            return listedRoom;
          }
          listedRoom['setup'] = listedRoom.setup.filter(layout => layout.setup_id !== setup.setup_id);
          if (listedRoom['setup'].length < 1) {
            return false;
          }
          return listedRoom;
        });
        if (updateMeetingRoomList.length >= 1) {
          this.edit3DSetup(updateMeetingRoomList[0], updateMeetingRoomList[0].setup[0]);
        } else if (selectedRooms.length === 1 && setup.enable3D) {
          router.history.push(`${match.url}/list`);
        }
      }
    }
    this.setState({ showModal: false });
    const storageName = `SceneData ${room.id}${setup.setup_id} ${setup.type}`;
    localStorage.removeItem(storageName);
    if (floorPlan) {
      floorPlan.setRoomSelected(room.id, false);
    }
    MeetingSpaceAction.removeLayout(room, setup);
  }

  handleClone(room, setup, type) {
    const newSetup = JSON.parse(JSON.stringify(setup));
    const { hotelInfo, isSubmitRfp } = this.props;
    isSubmitRfp(false);
    const clonedBy = hotelInfo.selectedRooms.filter(layoutRoom => layoutRoom.id === room.id)[0];
    const storageName = `SceneData ${room.id}${setup.setup_id} ${setup.type}`;
    const getData = DataProvider.getData(storageName);
    const newSetupId = clonedBy.setup[clonedBy.setup.length - 1].setup_id + 1;
    newSetup['setup_id'] = newSetupId;
    newSetup['addon'] = type === 'add';
    newSetup['enable3D'] = false;
    // newSetup['event_date_starts'] = '';
    // newSetup['event_date_ends'] = '';
    MeetingSpaceAction.duplicateLayout(room, newSetup);
    const newClonedStorageName = `SceneData ${room.id}${newSetupId} ${setup.type}`;
    if (getData) {
      DataProvider.storeData(newClonedStorageName, getData);
    }
  }

  handleCollapse(room, setup, type) {
    MeetingSpaceAction.updateAddons(room, setup, type);
  }

  handleClick(room, setup, addon, type) {
    if (type === 'service'){
      addon = addon.id;
    }
    const params = {
      room, setup, addon, type,
    };
    MeetingSpaceAction.updateFoodAndServices(params);
  }

  handleKeyup() {
    console.log('keyup');
  }

  newMeetingRoom() {
    const { handleTabClick, isSubmitRfp} = this.props;
    isSubmitRfp(false);
    handleTabClick('meeting_spaces');
  }

  handleInputs(room, setup, e) {
    const { hotelInfo } = this.props;
    const { layout } = hotelInfo;
    const roomId = room.id;
    const setupId = setup.setup_id;
    if ((setup.type.toLowerCase() == 'custom') || (e.target.value <= setup.count)){
      this.setState({ peopleCount: e.target.value, prevLayout: JSON.parse(JSON.stringify(layout))});
      const params = {
        roomId,
        setupId,
        peopleCount: parseInt(e.target.value, 10),
        type: 'people',
      };
      MeetingSpaceAction.updateLayoutInputs(params);
    } else if (setup.type.toLowerCase() !== 'custom'){
      const msg = `Maximum limit for ${setup.shape} setup is ${setup.count}`;
      MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
      setTimeout(() => {
      MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
      }, 5000);
    }
  }

  handleActivityInputs(e) {
    this.setState({ activityName: e.target.value });
  }

  handleDateEvent(roomId, setupId, e, picker, type) {
    // const date = picker.startDate;
    let { startDate } = picker;
    startDate = moment(startDate).format('DD/MM/YYYY');
    // this.setState({
    //   dateRange: true,
    //   startDate,
    // });
    const params = {
      roomId,
      setupId,
      name: startDate,
      type,
    };
    MeetingSpaceAction.updateActivityName(params);
  }

  handleEvents() {
    clearTimeout(this.timer);
    clearTimeout(this.interval);
    const { peopleCount } = this.state;
    this.timer = setTimeout(() => {
      const msg = `Number of attendees has been updated to ${peopleCount}`;
      if (peopleCount) {
        MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
        this.interval = setTimeout(() => {
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
        }, 5000);
      }
    }, 1000);
  }

  handleActivityName(roomId, setupId, e) {
    const activityName = e.target.value;
    const params = {
      roomId,
      setupId,
      name: activityName,
      type: 'activity_name',
    };
    MeetingSpaceAction.updateActivityName(params);
  }

  handleShowModel(room, setup) {
    this.setState({
      showModal: true,
      selectedRoom: room,
      selectedSetup: setup,
    });
  }

  handleClose() {
    this.setState({
      showModal: false,
    });
  }

  handleDropdown(room, setup, isRoomSetupPage, e) {
    // const { hotelInfo } = this.props;
    // const { available_setup } = hotelInfo;
    const list = parseInt(e.target.value, 10);
    const filteredSetup = room.allLayouts.filter(setup => setup.id === list);
    const roomId = room.id;
    const setupId = setup.setup_id;

    if((filteredSetup[0].count < setup.chairs_count) && (filteredSetup[0].type.toLowerCase() !== 'custom')){
      const msg = `Maximum limit for ${filteredSetup[0].shape} setup is ${filteredSetup[0].count}`;
      MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
      setTimeout(() => {
      MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
      }, 5000);
      const params = {
        roomId,
        setupId,
        peopleCount: filteredSetup[0].count,
        type: 'people',
      };
      MeetingSpaceAction.updateLayoutInputs(params); 
    }
    MeetingSpaceAction.handleRoomSetupList(room, setup.setup_id, filteredSetup[0]);

    const msg = `Room Set Up has been changed to ${filteredSetup[0].shape}`;
    if (isRoomSetupPage) {
       filteredSetup[0].chairs_count = JSON.parse(JSON.stringify(setup)).chairs_count;
       this.edit3DSetup(room, filteredSetup[0]);
    }

    MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
    setTimeout(() => {
      MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
    }, 5000);
  }

  handleSendMail(){
    const { hotelInfo, property, setLoader } = this.props;
    const emailList = $('#email-id').val();
    const link = $('#link-id').val();
    const emails = emailList.split(',');
    let isFormValid = true;
    emails.map((email)=>{
      email = email.trim();
      if(!email.match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/)){
        const msg = 'Please Enter The Valid Mail';
        MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
        setTimeout(() => {
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
        }, 5000);
        // this.handleShareInfo(true);
        isFormValid = false;
        return null;
      }
    });

    if( ! isFormValid) {
      return false;
    }

    // hide share popup before generating pdf
    this.handleShareInfo(false);

    let html_content = '<div class = "share-url">';
    html_content += '<p> Hi there,</p>';
    html_content += '<p>'+hotelInfo.contactDetails.name+' wants you to see this 3D Diagramming for the event '+ hotelInfo.contactDetails.name_of_event+' at '+hotelInfo.data.hotel_name+':</p>';
    html_content += '<p>'+link+'</p>';
    html_content += '<p>We hope you enjoy this amazing event!</p>';
    html_content += '<p>Sincerely,<br>' + hotelInfo.data.hotel_name + '</p>';
    html_content += '<p>PS. You can now create your own hyperrealistic 3D setups for any event at our hotel.<a href="'+window.location.origin+'/#/property/'+property+'/"> Try it now here!</a></p>';
    html_content += '</div>';
    const email_template = [
      {
        "sender":hotelInfo.data.sender_email,
        "recipients":emails,
        "subject":""+hotelInfo.contactDetails.name+" wants you to see this event in 3D at "+hotelInfo.data.hotel_name+"",
        "body":{
          "plain":"",
          "html": html_content
        }
      },
    ];
    const email = {
      "data": {
        "messages": JSON.stringify(email_template),
        "attachment": "",
      },
      "response": null
    };
    this.props.setExpander(true);
    const container = document.getElementsByClassName('layout-header-tab-container')[0];
    Helpers.outputAsPDF(container, 'RFP.pdf', (blob) => {
      email.data.attachment = new File([blob], 'RFP.pdf');
      setLoader(true);
      this.sendEmail(email);
    });
  }

  copyToClipboard(){
    const copyText = $("#link-id");
    copyText.select();
    document.execCommand("copy");
  }

  sendEmail(email) {
    var url = AuthStore.getApiHost()+"/api/users/send";
    const { setLoader, setReadWriteMode, setSuccessMessage } = this.props;
    var formData  = new FormData();
    for(var name in email.data) {
      formData.append(name, email.data[name]);
    }
    AuthStore.getHeaders(null, (headers) => {
      fetch(url, {
        mode: 'cors',
        method: 'POST',
        headers: headers,
        body: formData
      }).then(function (response) {
        email.response = response;
        if (response.ok) {
          setReadWriteMode(true);
          setSuccessMessage(true);
        }
        setLoader(false);
      });
    });
  }
  handleTimeChange(roomId, setupId, type, e) {
    const datetime = moment(e).format('HH:mm');
    const params = {
      roomId,
      setupId,
      name: datetime,
      type,
    };
    MeetingSpaceAction.updateActivityName(params);
  }
  handleKeyPress(room, setup, event){
    if (event.which === 13){
      //user Entered the "Enter" Key
      const storageName = `SceneData ${room.id}${setup.setup_id} ${setup.type}`;
      const { hotelInfo } = this.props;
      const { prevLayout } = this.state;
      let { layout } = hotelInfo;
      layout = JSON.parse(JSON.stringify(layout));
      if (prevLayout.chairs_count !== setup.chairs_count) {
        localStorage.removeItem(storageName);
      }
      this.edit3DSetup(room, setup);
      this.handleEvents();
    }
  }

  render() {
    const {
      showModal, selectedRoom, selectedSetup, setModeType, link, shareEmail,
    } = this.state;
    const {
      hotelInfo, toPageNavigate, readOnlyMode, isDownload, handleDownloadPDF, mailSuccess, setMode, isSharePopup,
      isClickedSubmitRfp, property, enable3DbtnList, userPrimaryColor,
    } = this.props;

    const { router } = this.context;
    const { match } = router.route;
    const {
      selectedRooms, contactDetails, eventId, meetingRoomInfo, eventUuId,
    } = hotelInfo;
    let { activityHeader, eventHeader } = this;
    const { activityHeaderView, eventHeaderView } = this;
    activityHeader = readOnlyMode ? activityHeaderView : activityHeader;
    eventHeader = readOnlyMode ? eventHeaderView : eventHeader;
    const primaryBg = {
      backgroundColor: userPrimaryColor,
    };
    const primaryColor = {
      color: userPrimaryColor,
    };
    const message = 'Are you sure you want to discard this meeting room from your basket?';
    const styleObj = { 'padding': '25px 0px' };
    const timeStyle = { 'background': 'white'};
    return (
      <Grid className={`primary-color meeting-room-list-container
        ${toPageNavigate === 'submit_rfp' ? 'bmargin20' : ''} ${readOnlyMode ? 'no-pointers' : ''}`}
      >
        {
          readOnlyMode && shareEmail ? (
            <div className="rfp-name">
              <div
                className="text-bold text-font25 i-block"
                lg={8}
                md={7}
              >
                {`RFP for Event "${hotelInfo.contactDetails.name_of_event}"`}
              </div>
              <Col className="share-downld-btns i-block">
                <Button
                  className={`share-btn i-block ${readOnlyMode ? 'visible-pointers' : ''}`}
                  style={{ borderColor: userPrimaryColor }}
                  onClick={() => this.handleShareInfo(true)}
                >
                  {/*<img src="dist/img/meeting_space/icons/share-icon@3x.png" alt="share" />*/}
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/share-icon@3x.svg" alt="share" />
                </Button>
              </Col>
            </div>
          ) : ''
        }
        {
          readOnlyMode && !shareEmail && mailSuccess && (
            <Row className="rfp-confirmation-msg" id="rfp-confirmation-msg-container">
              <Col lg={8} md={7}>
                <div className="text-bold">
                  <p>{`RFP for Event "${contactDetails.name_of_event}" has been sent!`}</p>
                  <p>Check your inbox for email confirmation. The hotel will be in touch soon.</p>
                </div>
              </Col>
              <Col lg={4} md={5} className="share-downld-btns">
                <Button
                  className={`share-btn i-block ${readOnlyMode ? 'visible-pointers' : ''}`}
                  style={{ borderColor: userPrimaryColor }}
                  onClick={() => this.handleShareInfo(true)}
                >
                  {/*<img src="dist/img/meeting_space/icons/share-icon@3x.png" alt="share" />*/}
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/share-icon@3x.svg" alt="share" />
                </Button>
                <div
                  className={`downld-btn i-block lpadding15 pointer ${readOnlyMode ? 'visible-pointers' : ''}`}
                  onClick={() => handleDownloadPDF('download')}
                  onKeyUp={this.handleKeyup}
                >
                  <span className="rpadding10">
                    Download PDF
                  </span>
                  {/*<img src="dist/img/pdf-icon.png" alt="PDF Icon" />*/}
                  <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/pdf-icon.svg" alt="PDF Icon" />
                </div>
              </Col>
            </Row>
          )
        }
        {
          (toPageNavigate === 'submit_rfp' || readOnlyMode)
            && (
              <Row className="rfp-label">
                <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
                  <div className="img-class">
                    <img src="dist/img/meeting-icon.png" alt="Guest Room" />
                  </div>
                  <div className="meeting-info-title">
                    Meeting Rooms Info
                  </div>
                  <div className="hyphen-class">
                    <hr />
                  </div>
                </Col>
              </Row>
            )
        }
        <Row className="selected-meeting-room-list selected-meeting-room-list-loggedIn">
          <Col className="">
            <Table className="text-center" responsive>
              <thead>
                <tr className="hidden-xs hidden-sm">
                  {
                    toPageNavigate === 'submit_rfp'
                      ? activityHeader.map(header => (
                        <td key={header}>
                          <span>{header}</span>
                        </td>
                      )) : eventHeader.map(header => (
                        <td key={header}>
                          <span>{header}</span>
                        </td>
                      ))
                  }
                </tr>
                <tr className="visible-xs visible-sm">
                  <td>
                    <span className="fleft">Selected meeting rooms</span>
                    <span className="fright">
                      <span className="i-block">Sort by</span>
                      <button className="btn btn-default i-block bold event-type-select">
                        Event type
                        <span className="fa fa-angle-down lmargin10"></span>
                      </button>
                    </span>
                  </td>
                </tr>
              </thead>
              {
                selectedRooms.length >= 1
                  ? (
                    <tbody>
                      {
                        selectedRooms.map(room => (
                          room.setup.map((setup, i) => ([
                            <tr
                              width="100%"
                              style={setup.hasOwnProperty('enable3D') && setup.enable3D ? primaryBg : {}}
                              key={`mobile${room.id} ${setup.setup_id}`}
                              className={`white-background ${(i !== room.setup.length - 1
                                || (setup.hasOwnProperty('addon') && setup.addon))
                                ? 'no-box-shadow visible-xs visible-sm' : 'selected-room-list visible-xs visible-sm'}`}
                            >
                              {
                                i === 0
                                  ? (
                                    <div className="prelative">
                                      {/*<img src="dist/img/reactangle-radius.png" alt="Border" className="border-simulator" />*/}
                                      <ReactSVG
                                        wrapper="div"
                                        src="dist/img/reactangle-radius.svg"
                                        wrapper="span"
                                        alt="Border"
                                        className="svg-wrapper border-simulator"
                                      />
                                      <span
                                        className={`meeting-room-name i-block ${setup.hasOwnProperty('enable3D')
                                          && setup.enable3D ? 'white-text' : ''}`}
                                      >
                                        {room.room_name}
                                      </span>
                                    </div>
                                  ) : <div />
                              }
                              <div className="event-name">
                                {
                                  (toPageNavigate === 'submit_rfp' || readOnlyMode)
                                    && (
                                      <Button className="visible-pointers" onClick={() => this.handleCollapse(room, setup, 'services')}>
                                        {
                                          setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                            <img
                                              className={`${setup.hasOwnProperty('addon') && setup.addon
                                                ? '' : 'transform'}`}
                                              src="dist/img/meeting_space/icons/arrow-white.png"
                                              alt="arrow icon"
                                            />
                                          ) : (
                                            <ReactSVG
                                              wrapper="span"
                                              className={`svg-wrapper activity-toggle ${setup.hasOwnProperty('addon') && setup.addon
                                                ? '' : 'transform'}`}
                                              src="dist/img/meeting_space/icons/arrow-icon.svg"
                                              alt="arrow icon"
                                            />
                                          )
                                        }
                                      </Button>
                                    )
                                }
                                <input
                                  type="text"
                                  className="event name"
                                  placeholder="Activity name"
                                  onChange={e => this.handleActivityName(room.id, setup.setup_id, e)}
                                  value={setup.activity_name || ''}
                                  // onBlur={() => this.handleActivityName(room.id, setup.setup_id)}
                                />
                                <Button className="visible-pointers fright" 
                                onClick={() => this.handleCollapse(room, setup, 'meetingRoomInfo')}
                                style={setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                    ? {'margin-left': '0px'} : {'margin-left': '10px'}}
                                >
                                {
                                  setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                    <img
                                      className={`${setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                        ? '' : 'transform'}`}
                                      src="dist/img/meeting_space/icons/arrow-white.png"
                                      alt="arrow icon"
                                    />
                                  ) : (
                                      <ReactSVG wrapper="div" className={`svg-wrapper activity-toggle ${setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo
                                    ? '' : 'transform'}`} src="dist/img/meeting_space/icons/arrow-down.svg" alt="arrow icon" />
                                    )
                                }
                                </Button>
                              </div>
                              <hr className="activity-separator" />
                              <div className="clearfix" />
                              <div className= {setup.hasOwnProperty('meetingRoomInfo') && setup.meetingRoomInfo ? 'hide' : ''}>
                                <div className="date-time-field align-event-date">
                                  <DateRangePicker
                                    singleDatePicker
                                    containerStyles={((setup.event_date_starts=== '') && isClickedSubmitRfp) ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                    : { backgroundColor: '#ffffff'}}
                                    minDate={moment()}
                                    onApply={(e, picker) => this.handleDateEvent(
                                      room.id, setup.setup_id, e, picker, 'event_date_starts',
                                    )}
                                  >
                                    <Button
                                      style={{ color: userPrimaryColor }}
                                    >
                                      {
                                        setup.hasOwnProperty('event_date_starts') && setup.event_date_starts ? (
                                          <span>
                                            {`${setup.event_date_starts} `}
                                          </span>
                                        ) : '- - / - - / - -'
                                      }
                                    </Button>
                                  </DateRangePicker>
                                  <Datetime
                                    dateFormat={false}
                                    inputProps={{readonly:'readonly', style:timeStyle}}
                                    timeFormat="HH:mm"
                                    className={setup.event_time_starts === '' && isClickedSubmitRfp ? 'red-border' : ''}
                                    value={setup.event_time_starts ? setup.event_time_starts : '- - : - -'}
                                    onChange={e => this.handleTimeChange(room.id, setup.setup_id, 'event_time_starts', e)}
                                  />
                                  <span
                                    className="i-block hour-class"
                                    style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                      ? { color: '#fcfcfc' } : { color: userPrimaryColor }}
                                  >
                                    h.
                                  </span>
                                </div>
                                <div className="date-time-field align-event-date">
                                  <DateRangePicker
                                    singleDatePicker
                                    inputProps={{style:{border: '2px solid red'}}}
                                    containerStyles={((setup.event_date_ends=== '') && isClickedSubmitRfp) ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                    : { backgroundColor: '#ffffff'}}
                                    minDate={moment()}
                                    onApply={(e, picker) => this.handleDateEvent(
                                      room.id, setup.setup_id, e, picker, 'event_date_ends',
                                    )}
                                  >
                                    <Button
                                      style={{ color: userPrimaryColor }}
                                    >
                                      {
                                        setup.hasOwnProperty('event_date_ends') && setup.event_date_ends ? (
                                          <span>
                                            {`${setup.event_date_ends} `}
                                          </span>
                                        ) : '- - / - - / - -'
                                      }
                                    </Button>
                                  </DateRangePicker>
                                  <Datetime
                                    dateFormat={false}
                                    timeFormat="HH:mm"
                                    className={setup.event_time_starts === '' && isClickedSubmitRfp ? 'red-border' : ''}
                                    inputProps={{readonly:'readonly', style:timeStyle}}
                                    value={setup.event_time_ends ? setup.event_time_ends : '- - : - -'}
                                    onChange={e => this.handleTimeChange(room.id, setup.setup_id, 'event_time_ends', e)}
                                  />
                                  <span
                                    className="i-block hour-class"
                                    style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                      ? { color: '#fcfcfc' } : { color: userPrimaryColor }}
                                  >
                                    h.
                                  </span>
                                </div>
                                <div className="attendee-count">
                                  <input
                                    type="text"
                                    id="attendees"
                                    className="event count form-control"
                                    style={(Number.isNaN(setup.chairs_count) && isClickedSubmitRfp) ? {border: '2px solid red'}: {border: '0'}}
                                    placeholder="––"
                                    onKeyPress={e => {toPageNavigate === 'room_setup' ? this.handleKeyPress(room,setup,e) : ''}}
                                    onChange={e => this.handleInputs(room, setup, e)}
                                    onKeyUp={toPageNavigate === 'room_setup' ? '' : this.handleEvents}
                                    value={setup.chairs_count >= 0 ? setup.chairs_count : ''}
                                  />
                                </div>
                                <div className="room-setup">
                                  <select
                                    style={{ borderColor: userPrimaryColor }}
                                    onChange={e => {toPageNavigate === 'room_setup'? this.handleDropdown(room, setup, true, e) : this.handleDropdown(room, setup, false, e)}}
                                  >
                                    {
                                      room.allLayouts.map(list => {
                                        if (list.count > 0 || list.type.toLowerCase() === 'custom') {
                                          return (
                                            <option
                                              key={list.id}
                                              value={list.id}
                                              selected={list.shape === setup.shape}
                                            >
                                              {list.shape}
                                            </option>
                                          );
                                        }
                                      })
                                    }
                                  </select>
                                </div>
                                <div className="clearfix" />
                                <div className={`fleft lmargin20 tmargin10 ${readOnlyMode ? 'hide' : ''}`}>
                                  <Button
                                    className="clone-btn"
                                    onClick={() => this.handleClone(room, setup, 'clone')}
                                    style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                      ? { borderColor: '#fcfcfc' } : { borderColor: userPrimaryColor }}
                                  >
                                    {
                                      setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                        <img src="dist/img/meeting_space/icons/clone-white@3x.png" alt="clone" />
                                      ) : (
                                        /*<img src="dist/img/meeting_space/icons/clone@3x.png" alt="clone" />*/
                                       <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/clone@3x.svg" alt="clone" />
                                      )
                                    }
                                  </Button>
                                </div>
                                <div className="fleft lmargin10 tmargin10 bmargin10mob ">
                                  {
                                    room.available_model ? (
                                      <Link to={eventUuId ? `${match.url}/dollhouse/${eventUuId}/view` : `${match.url}/dollhouse`}>
                                        <Button
                                          className={`edit-btn ${readOnlyMode ? 'visible-pointers' : ''}`}
                                          style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                            ? { border: '1px solid #fcfcfc' }
                                            : { backgroundColor: userPrimaryColor }}
                                          onClick={() => this.edit3DSetup(room, setup)}
                                          disabled={!enable3DbtnList}
                                        >
                                        {
                                          !readOnlyMode ? (
                                            <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                          ) : (
                                            <span>CLICK & EXPLORE 3D SET UP</span>
                                          )
                                        }
                                        </Button>
                                      </Link>
                                    ) : (
                                      <Button
                                        className={`edit-btn ${readOnlyMode ? 'visible-pointers' : ''}`}
                                        style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                          ? { border: '1px solid #fcfcfc' }
                                          : { backgroundColor: userPrimaryColor }}
                                        onClick={() => this.edit3DSetup(room, setup)}
                                        disabled={!enable3DbtnList}
                                      >
                                      {
                                        readOnlyMode ? (
                                          <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                        ) : (
                                          <span>CLICK & EXPLORE 3D SET UP</span>
                                        )
                                      }
                                      </Button>
                                    )
                                  }
                                </div>
                                <div className={`fleft lmargin10 tmargin10 ${readOnlyMode ? 'hide' : ''}`}>
                                  <Button
                                    className="delete-btn primary-border-left"
                                    onClick={() => this.handleShowModel(room, setup)}
                                  >
                                    {
                                      setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                        <img src="dist/img/meeting_space/icons/delete-icon-white@3x.png" alt="delete" />
                                      ) : (
                                        /*<img src="dist/img/meeting_space/icons/delete-red@3x.png" alt="delete" />*/
                                        <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                                      )
                                    }
                                  </Button>
                                </div>
                              </div>
                            </tr>,
                            <tr
                              style={setup.hasOwnProperty('enable3D') && setup.enable3D ? primaryBg : {}}
                              key={`${room.id} ${setup.setup_id}`}
                              className={`white-background ${(i !== room.setup.length - 1
                                || (setup.hasOwnProperty('addon') && setup.addon))
                                ? 'no-box-shadow hidden-xs hidden-sm' : 'selected-room-list hidden-xs hidden-sm'}`}
                            >
                              {
                                i === 0
                                  ? (
                                    <td className="prelative">
                                      {/*<img src="dist/img/reactangle-radius.png" alt="Border" className="border-simulator" />*/}
                                      <ReactSVG
                                        wrapper="div"
                                        src="dist/img/reactangle-radius.svg"
                                        wrapper="span"
                                        alt="Border"
                                        className="svg-wrapper border-simulator"
                                      />
                                      <span
                                        className={`meeting-room-name align-meeting-room-name i-block ${setup.hasOwnProperty('enable3D')
                                          && setup.enable3D ? 'white-text' : ''}`}
                                      >
                                        {room.room_name}
                                      </span>
                                    </td>
                                  ) : <td />
                              }

                              <td className={`event-name align-event-name ${readOnlyMode ? 'align-rfp-container-event-name' : ''}`}>
                                {
                                  (toPageNavigate === 'submit_rfp' || readOnlyMode)
                                    && (
                                      <Button className="visible-pointers rfp-specific" onClick={() => this.handleCollapse(room, setup, 'services')}>
                                        {
                                          setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                            <img
                                              className={`${setup.hasOwnProperty('addon') && setup.addon
                                                ? '' : 'transform'}`}
                                              src="dist/img/meeting_space/icons/arrow-white.png"
                                              alt="arrow icon"
                                            />
                                          ) : (
                                            <ReactSVG
                                              wrapper="span"
                                              className={`svg-wrapper ${setup.hasOwnProperty('addon') && setup.addon
                                                ? '' : 'transform'}`}
                                              src="dist/img/meeting_space/icons/arrow-icon.svg"
                                              alt="arrow icon"
                                            />
                                          )
                                        }
                                      </Button>
                                    )
                                }
                                <input
                                  type="text"
                                  className={`event name input-field-length ${readOnlyMode ? 'align-rfp-container-input-field-length' : ''}`}
                                  placeholder="Activity name"
                                  onChange={e => this.handleActivityName(room.id, setup.setup_id, e)}
                                  value={setup.activity_name || ''}
                                />
                              </td>
                              <td className="date-time-field align-event-date">
                                <DateRangePicker
                                  singleDatePicker
                                  containerStyles={((setup.event_date_starts=== '') && isClickedSubmitRfp) ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                  : { backgroundColor: '#ffffff'}}
                                  minDate={moment()}
                                  onApply={(e, picker) => this.handleDateEvent(
                                    room.id, setup.setup_id, e, picker, 'event_date_starts',
                                  )}
                                >
                                  <Button
                                    className="primary-color align-date"
                                  >
                                    {
                                      setup.hasOwnProperty('event_date_starts') && setup.event_date_starts ? (
                                        <span>
                                          {`${setup.event_date_starts} `}
                                        </span>
                                      ) : '- - / - - / - -'
                                    }
                                  </Button>
                                </DateRangePicker>
                                <Datetime
                                  dateFormat={false}
                                  timeFormat="HH:mm"
                                  className={setup.event_time_starts === '' && isClickedSubmitRfp ? 'red-border' : ''}
                                  inputProps={{readonly:'readonly', style:timeStyle}}
                                  value={setup.event_time_starts ? setup.event_time_starts : '- - : - -'}
                                  onChange={e => this.handleTimeChange(room.id, setup.setup_id, 'event_time_starts', e)}
                                />
                                <span
                                  className="i-block hour-class align-hour-class"
                                  style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                    ? { color: '#fcfcfc' } : { color: userPrimaryColor }}
                                >
                                  h.
                                </span>
                              </td>
                              <td className="date-time-field align-event-date">
                                <DateRangePicker
                                  singleDatePicker
                                  containerStyles={((setup.event_date_ends=== '') && isClickedSubmitRfp) ? { backgroundColor: '#ffffff', border: '2px solid red'} 
                                  : { backgroundColor: '#ffffff'}}
                                  minDate={moment()}
                                  onApply={(e, picker) => this.handleDateEvent(
                                    room.id, setup.setup_id, e, picker, 'event_date_ends',
                                  )}
                                >
                                  <Button
                                    className="primary-color align-date"
                                  >
                                    {
                                      setup.hasOwnProperty('event_date_ends') && setup.event_date_ends ? (
                                        <span>
                                          {`${setup.event_date_ends} `}
                                        </span>
                                      ) : '- - / - - / - -'
                                    }
                                  </Button>
                                </DateRangePicker>
                                <Datetime
                                  dateFormat={false}
                                  timeFormat="HH:mm"
                                  className={setup.event_time_starts === '' && isClickedSubmitRfp ? 'red-border' : ''}
                                  inputProps={{readonly:'readonly', style:timeStyle}}
                                  value={setup.event_time_ends ? setup.event_time_ends : '- - : - -'}
                                  onChange={e => this.handleTimeChange(room.id, setup.setup_id, 'event_time_ends', e)}
                                />
                                <span
                                  className="i-block hour-class align-hour-class"
                                  style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                    ? { color: '#fcfcfc' } : { color: userPrimaryColor }}
                                >
                                  h.
                                </span>
                              </td>
                              <td className="align-count">
                                <input
                                  type="text"
                                  className="event count form-control align-event-count"
                                  style={(Number.isNaN(setup.chairs_count) && isClickedSubmitRfp) ? {border: '2px solid red'}: {border: '0'}}
                                  placeholder="––"
                                  id="attendees"
                                  onKeyPress={e => {toPageNavigate === 'room_setup' ? this.handleKeyPress(room,setup,e) : ''}}
                                  onChange={e => this.handleInputs(room, setup, e)}
                                  onKeyUp={toPageNavigate === 'room_setup' ? '' : this.handleEvents}
                                  value={setup.chairs_count >= 0 ? setup.chairs_count : ''}
                                />
                              </td>
                              <td className="room-setup-dropdown">
                                <select
                                  className="primary-border"
                                  onChange={e => {toPageNavigate === 'room_setup'? this.handleDropdown(room, setup, true, e) : this.handleDropdown(room, setup, false, e)}}
                                >
                                  {
                                    room.allLayouts.map(list => {
                                      if (list.count > 0 || list.type.toLowerCase() === 'custom') {
                                        return (
                                          <option
                                            key={list.id}
                                            value={list.id}
                                            selected={list.shape === setup.shape}
                                          >
                                            {list.shape}
                                          </option>
                                        );
                                      }
                                    })
                                  }
                                </select>
                              </td>
                              <td className={`${readOnlyMode ? 'hide' : ''}`}>
                                <Button
                                  className="clone-btn"
                                  onClick={() => this.handleClone(room, setup, 'clone')}
                                  style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                    ? { borderColor: '#fcfcfc' } : { borderColor: userPrimaryColor }}
                                >
                                  {
                                    setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                      <img src="dist/img/meeting_space/icons/clone-white@3x.png" alt="clone" />
                                    ) : (
                                      /*<img src="dist/img/meeting_space/icons/clone@3x.png" alt="clone" />*/
                                      <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/clone@3x.svg" alt="clone" />
                                    )
                                  }
                                </Button>
                              </td>
                              <td>
                                {
                                  room.available_model ? (
                                    <Link to={eventUuId ? `${match.url}/dollhouse/${eventUuId}/view` : `${match.url}/dollhouse`}>
                                      <Button
                                        className={`edit-btn ${readOnlyMode ? 'visible-pointers  edit-btn-submit-rfp' : ''}`}
                                        style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                          ? { border: '1px solid #fcfcfc' }
                                          : { backgroundColor: userPrimaryColor }}
                                        disabled={!enable3DbtnList}
                                        onClick={() => this.edit3DSetup(room, setup)}
                                      >
                                      {
                                        !readOnlyMode ? (
                                          <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                        ) : (
                                          <span>CLICK & EXPLORE 3D SET UP</span>
                                        )
                                      }
                                      </Button>
                                    </Link>
                                  ) : (
                                    <Button
                                      className={`edit-btn ${readOnlyMode ? 'visible-pointers  edit-btn-submit-rfp' : ''}`}
                                      style={setup.hasOwnProperty('enable3D') && setup.enable3D
                                        ? { border: '1px solid #fcfcfc' }
                                        : { backgroundColor: userPrimaryColor }}
                                      disabled={!enable3DbtnList}
                                      onClick={() => this.edit3DSetup(room, setup)}
                                    >
                                      {
                                        !readOnlyMode ? (
                                          <ReactSVG wrapper="span" className="svg-wrapper icon-3d" src="dist/img/meeting_space/icons/3d-icon@3x.svg" alt="edit" />
                                        ) : (
                                          <span>CLICK & EXPLORE 3D SET UP</span>
                                        )
                                      }
                                    </Button>
                                  )
                                }
                              </td>
                              <td className={`${readOnlyMode ? 'hide' : ''}`}>
                                <Button
                                  className="delete-btn primary-border-left"
                                  onClick={() => this.handleShowModel(room, setup)}
                                >
                                  {
                                    setup.hasOwnProperty('enable3D') && setup.enable3D ? (
                                      <img src="dist/img/meeting_space/icons/delete-icon-white@3x.png" alt="delete" />
                                    ) : (
                                      /*<img src="dist/img/meeting_space/icons/delete-red@3x.png" alt="delete" />*/
                                      <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                                    )
                                  }
                                </Button>
                              </td>
                            </tr>,
                            <React.Fragment>
                              {
                              (toPageNavigate === 'submit_rfp' || readOnlyMode) && ((setup.hasOwnProperty('addon') && setup.addon) || isDownload)
                                && ([
                                  <tr className="visible-xs visible-sm" width="100%">
                                    <Row>
                                      <Col xs={6} sm={6}>
                                        <div className="service-title tmargin10">
                                          Services
                                        </div>
                                        <div className="service-container">
                                           { 
                                             meetingRoomInfo.filter((meetingRoom)=> room.id === meetingRoom.id)[0].room_services.map(service => (
                                              <div
                                                className="services"
                                                onClick={() => this.handleClick(room, setup, service, 'service')}
                                                onKeyUp={this.handleKeyup}
                                              >
                                                {service.name}
                                                {
                                                  (setModeType === 'viewOnly') ? (setup.hasOwnProperty('services_ids')
                                                && setup.services_ids.indexOf(service.id) > -1 && (
                                                  <ReactSVG
                                                    wrapper="div"
                                                    className="svg-wrapper checkmark-img-selected pointer primary-background"
                                                    src="dist/img/meeting_space/icons/checkmark.svg"
                                                    alt="checkmark"
                                                  />
                                                ))
                                                : (setup.hasOwnProperty('service')
                                                  && setup.service.indexOf(service.id) > -1 && (
                                                    <ReactSVG
                                                      wrapper="div"
                                                      className="svg-wrapper checkmark-img-selected pointer primary-background"
                                                      src="dist/img/meeting_space/icons/checkmark.svg"
                                                      alt="checkmark"
                                                    />

                                                  ))
                                                }
                                              </div>
                                            ))
                                          }
                                          {
                                            meetingRoomInfo.filter((meetingRoom)=> room.id === meetingRoom.id)[0].room_services.length < 1 && (
                                              <div className="services-unavailable">
                                                No services available
                                              </div>
                                            )
                                          }
                                        </div>
                                      </Col>
                                      {
                                      // <Col xs={6} sm={6}>
                                      //   <div className="service-title tmargin10">
                                      //     Foods / Drinks
                                      //   </div>
                                      //   <div className="service-container">
                                      //     {
                                      //       foodBeverages.map(food => (
                                      //         <div
                                      //           className="services"
                                      //           onClick={() => this.handleClick(room, setup, food, 'food_beverages')}
                                      //           onKeyUp={this.handleKeyup}
                                      //         >
                                      //           {food}
                                      //           {
                                      //             setup.hasOwnProperty('food_beverages')
                                      //             && setup.food_beverages.indexOf(food) > -1 && (
                                      //               <ReactSVG
                                      //                 wrapper="div"
                                      //                 className="svg-wrapper checkmark-img-selected pointer primary-background"
                                      //                 src="dist/img/meeting_space/icons/checkmark.svg"
                                      //                 alt="checkmark"
                                      //               />
                                      //             )
                                      //           }
                                      //         </div>
                                      //       ))
                                      //     }
                                      //   </div>
                                      // </Col>
                                      }
                                      <Col xs={12} sm={12}>
                                        <hr className="hor-line" />
                                        <div className="add-items-wrapper add-activity visible-xs visible-sm">
                                          <div
                                            className="add-items pointer"
                                            style={(readOnlyMode || isDownload) ? { display: 'none' } : { display: 'inline-block' }}
                                          >
                                            <div
                                              className="i-block add-activity-span"
                                              onClick={() => this.handleClone(room, setup, 'add')}
                                              onKeyUp={this.handleKeyup}
                                            >
                                              <img
                                                className="add-image primary-background"
                                                src="dist/img/meeting_space/icons/plus.png"
                                                alt="plus"
                                              />
                                              <span>Add activity</span>
                                            </div>
                                          </div>
                                        </div>
                                      </Col>
                                    </Row>
                                  </tr>,
                                  <tr className="no-box-shadow hidden-xs hidden-sm">
                                    <td />
                                    <td className="apply-bg prelative">
                                      <div className="service-col text-left addon-header ">
                                        <span>
                                          Services
                                        </span>
                                      </div>
                                    </td>
                                    <td colSpan="7" className="service-col">
                                      <tr className="service-container">
                                        {
                                           meetingRoomInfo.filter((meetingRoom)=> room.id === meetingRoom.id)[0].room_services.map(service => (
                                            <div
                                              className="services"
                                              onClick={() => this.handleClick(room, setup, service, 'service')}
                                              onKeyUp={this.handleKeyup}
                                            >
                                              {service.name}
                                              {(setModeType === 'viewOnly') ? (setup.hasOwnProperty('services_ids')
                                                && setup.services_ids.indexOf(service.id) > -1 && (
                                                  <ReactSVG
                                                    wrapper="div"
                                                    className="svg-wrapper checkmark-img-selected pointer primary-background"
                                                    src="dist/img/meeting_space/icons/checkmark.svg"
                                                    alt="checkmark"
                                                  />
                                                ))
                                                : (setup.hasOwnProperty('service')
                                                && setup.service.indexOf(service.id) > -1 && (
                                                  <ReactSVG
                                                    wrapper="div"
                                                    className="svg-wrapper checkmark-img-selected pointer primary-background"
                                                    src="dist/img/meeting_space/icons/checkmark.svg"
                                                    alt="checkmark"
                                                  />
                                                ))
                                              }
                                            </div>
                                          ))
                                        }
                                        {
                                          meetingRoomInfo.filter((meetingRoom)=> room.id === meetingRoom.id)[0].room_services.length < 1 && (
                                            <div className="services-unavailable">
                                             No services available
                                            </div>
                                          )
                                        }
                                      </tr>
                                    </td>
                                  {
                                    // <tr className="no-box-shadow hidden-xs hidden-sm">
                                    // <td />
                                    // <td className="apply-bg prelative">
                                    //   <div className="service-col text-left addon-header bblRadius">
                                    //     <span>
                                    //       Foods and beverages
                                    //     </span>
                                    //   </div>
                                    // </td>
                                    // <td colSpan="7" className="service-col">
                                    //   <tr className="service-container">
                                    //     {
                                    //       foodBeverages.map(food => (
                                    //         <div
                                    //           className="services"
                                    //           onClick={() => this.handleClick(room, setup, food, 'food_beverages')}
                                    //           onKeyUp={this.handleKeyup}
                                    //         >
                                    //           {food}
                                    //           {
                                    //             setup.hasOwnProperty('food_beverages')
                                    //             && setup.food_beverages.indexOf(food) > -1 && (
                                    //               <ReactSVG
                                    //                 wrapper="div"
                                    //                 className="svg-wrapper checkmark-img-selected pointer primary-background"
                                    //                 src="dist/img/meeting_space/icons/checkmark.svg"
                                    //                 alt="checkmark"
                                    //               />
                                    //             )
                                    //           }
                                    //         </div>
                                    //       ))
                                    //     }
                                    //   </tr>
                                    // </td>
                                    // </tr>,
                                  }
                                  </tr>,
                                  <tr className="add-items-wrapper add-activity hidden-xs hidden-sm">
                                    <td />
                                    <td colSpan="8">
                                      <div
                                        className="add-items pointer"
                                        style={(readOnlyMode || isDownload) ? { display: 'none' } : { display: 'inline-block' }}
                                      >
                                        <div
                                          className="i-block add-activity-span"
                                          onClick={() => this.handleClone(room, setup, 'add')}
                                          onKeyUp={this.handleKeyup}
                                        >
                                          <img
                                            className="add-image primary-background"
                                            src="dist/img/meeting_space/icons/plus.png"
                                            alt="plus"
                                          />
                                          <span>Add activity</span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>,
                                ])
                            }
                            </React.Fragment>,
                            <React.Fragment>
                              {
                                i === room.setup.length - 1
                                  && <tr className="i-block tmargin10" key={`${room.id} ${room.room_name}`} />
                              }
                            </React.Fragment>,
                          ]))
                        ))
                      }
                    </tbody>)
                  : (
                    <tbody className="no-meeting-room">
                      <tr>
                        <td colSpan="9">No Meeting rooms selected yet</td>
                      </tr>
                    </tbody>
                  )
              }
            </Table>
          </Col>
        </Row>
        {
          toPageNavigate === 'submit_rfp'
            && (
              <Row>
                <Col xs={12} sm={12} md={12} lg={12} className="lpadding0">
                  <div className="add-items-wrapper">
                    <div className="add-items lpadding0">
                      <Link to={`${match.url}/list`} className="primary-color">
                        <div
                          className="i-block new-meeting-room"
                          onClick={this.newMeetingRoom}
                          onKeyUp={this.handleKeyup}
                          style={(readOnlyMode || isDownload) ? { display: 'none' } : { display: 'inline-block' }}
                        >
                          <img
                            className="add-image primary-background"
                            style={{ backgroundColor: userPrimaryColor }}
                            src="dist/img/meeting_space/icons/plus.png"
                            alt="plus"
                          />
                          <span>Add new meeting room</span>
                        </div>
                      </Link>
                    </div>
                  </div>
                </Col>
              </Row>
            )
        }
        {
          isSharePopup && (
            <div className="share-room-details visible-pointers" style={primaryColor}>
              <Button className="close-button fright visible-pointers" onClick={() => this.handleShareInfo(false)}>
                &#x2715;
              </Button>
              <div className="share-info-header">
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/3d-icon.svg" alt="3D" />
                <h1>
                  Clinton Room
                  <div className="member-count">
                  2 members
                  </div>
                </h1>
              </div>
              <div className="share-info-email">
                <span className="share-info-label">To:</span>
                <input
                  id = "email-id"
                  type="text"
                  placeholder="Email"
                  className="form-control email-field"
                  style={primaryColor}
                />
              </div>
              <div className="share-info-link">
                <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/link-icon.svg" alt="Link" />
                <input
                  id="link-id"
                  type="text"
                  placeholder="https://fonts.google.com"
                  className="form-control link-field"
                  style={primaryColor}
                  value={link}
                />
                <span className="share-info-note">
                  <span className="share-info-label rmargin10">Anyone</span>
                  with the link can
                  <span className="share-info-label rmargin10 lmargin10">view</span>
                  this 3D setup.
                </span>
                <Button 
                id = "copy-link"
                className="copy-btn" 
                style={primaryColor}
                onClick = {()=>{this.copyToClipboard()}}
                >
                  Copy link
                </Button>
              </div>
              <Button 
              className="send-btn" 
              style={primaryColor}
              onClick={()=>{this.handleSendMail()}}
              >
                Send
              </Button>
            </div>
          )
        }
        {
          showModal
            && (
              <LightBox
                handleClose={this.handleClose}
                handleSubmit={() => this.handleDelete(selectedRoom, selectedSetup)}
                message={message}
                header="Discard space"
                successText="Discard"
                failureText="Cancel"
                showModal={showModal}
              />
            )
        }
      </Grid>
    );
  }
}

MeetingRoomList.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  handleTabClick: PropTypes.func.isRequired,
  setStorageName: PropTypes.func.isRequired,
  handleModelLoading: PropTypes.func.isRequired,
  setViewer: PropTypes.func.isRequired,
  handleLayout: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  handleDownloadPDF: PropTypes.func.isRequired,
  handleTooltipPosition: PropTypes.func.isRequired,
  setZoom: PropTypes.func.isRequired,
  saveRoom: PropTypes.func.isRequired,
  handleFurnitureCount: PropTypes.func.isRequired,
  handleDisableButtons: PropTypes.func.isRequired,
  setMode: PropTypes.func.isRequired,
  isSubmitRfp: PropTypes.func.isRequired,
  toPageNavigate: PropTypes.string.isRequired,
  context: PropTypes.string.isRequired,
  readOnlyMode: PropTypes.bool.isRequired,
  isSharePopup:PropTypes.bool.isRequired,
  isDownload: PropTypes.bool.isRequired,
  mailSuccess: PropTypes.bool.isRequired,
  isClickedSubmitRfp: PropTypes.bool.isRequired,
  enable3DbtnList: PropTypes.bool.isRequired,
};

MeetingRoomList.contextTypes = {
  router: PropTypes.object,
};

export default MeetingRoomList;

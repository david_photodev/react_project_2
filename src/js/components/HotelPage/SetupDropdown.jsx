import enhanceWithClickOutside from 'react-click-outside';
import React, { Component } from 'react';
import {
  Col, Button,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import ReactSVG from 'react-svg';
import MeetingSpaceStore from "../../stores/MeetingSpaceStore";
import AuthStore from "../../stores/AuthStore";
import RFPManagementStore from "../../stores/RFPManagementStore";

class SetupDropdown extends Component {
  constructor(props) {
    super(props);
    this.handleOwnConfig = this.handleOwnConfig.bind(this);
    this.handleKeyup = this.handleKeyup.bind(this);
  }

  handleClickOutside() {
    const { handleClickOutside } = this.props;
    handleClickOutside();
  }

  handleOwnConfig(item, setup) {
    const { handleOwnConfig } = this.props;
    MeetingSpaceAction.storeSceneData(item.scenes);
    MeetingSpaceAction.showSetupView(item);
    handleOwnConfig(item);
  }

  handleKeyup() {
    console.log('key up');
  }

  render() {
    const {
      item, handleSetupConfig, hotelInfo, match, userPrimaryColor
    } = this.props;
    const selecteLayoutStyle = {
      backgroundColor: userPrimaryColor,
      color: '#fcfcfc',
    };
    const { selectedRooms } = hotelInfo;
    const activeRoom = selectedRooms.filter(room => room.id === item.id);
    const isActive = activeRoom.length >= 1;
    return (
      <div className="room-setup-config-container">
        <div className="i-block">
          {
            item.setup.map((setup, i) => (
            setup.type.toLowerCase() !== 'custom' ?
              (<Col
                lg={6}
                md={6}
                sm={6}
                className={`i-block setup-image-container ${ setup.count > 0 ? '' : 'setup-image-container-disable'}`}
                key={setup.id}
                onClick={() => handleSetupConfig(item, setup)}
                //onClick={() => this.handleOwnConfig(item, setup)}
              >
                {/*<img className="setup-image" src={setup.img} alt="setup icon" />*/}
                <ReactSVG wrapper="div" className="svg-wrapper setup-image" src={setup.img} alt="setup icon" />
                <p className="text-center tmargin10">{setup.shape}</p>
                <div>
                  <Button
                    className={`${i % 2 === 0 ? 'button-left' : 'button-right'}`}
                    style={isActive && activeRoom[0] && activeRoom[0].setup
                      && activeRoom[0].setup.filter(layout => layout.type === setup.type).length >= 1
                      ? selecteLayoutStyle : {}}
                    disabled={!(setup.count > 0)}
                  >
                    {setup.count >= 0 ? setup.count : '-'}
                    <img
                      className="person-image"
                      src={`dist/img/meeting_space/icons/${isActive && activeRoom[0] && activeRoom[0].setup
                      && activeRoom[0].setup.filter(layout => layout.type === setup.type).length >= 1
                        ? 'white-person.png'
                        : 'person.png'}`}
                      alt="person icon"
                    />
                  </Button>
                </div>
              </Col>) : ''
            ))
          }
          {
          item.setup.map((setup, i) => (
          setup.type.toLowerCase() === 'custom' ?
          (<div
            className="block own-setup"
            onClick={() => this.handleOwnConfig(item)}
            onKeyUp={this.handleKeyup}
          >
            <img
              className="add-image"
              style={{ backgroundColor: userPrimaryColor }}
              src="dist/img/meeting_space/icons/plus.png"
              alt="plus"
            />
            <span className="block padding10">Create your own set up</span>
          </div>) : ''))
        }
        </div>
      </div>
    );
  }
}

SetupDropdown.propTypes = {
  handleSetupConfig: PropTypes.func.isRequired,
  handleClickOutside: PropTypes.func.isRequired,
  handleOwnConfig: PropTypes.func.isRequired,
  hotelInfo: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  property: PropTypes.string.isRequired,
};

export default enhanceWithClickOutside(SetupDropdown);

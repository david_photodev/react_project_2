import React, { Component } from 'react';
import {
  Grid, Row, Col, Button,
} from 'react-bootstrap';
import { Route, Link } from 'react-router-dom';
import ReactSVG from 'react-svg';

import PropTypes from 'prop-types';
import MeetingRoom from './MeetingRoom';
import CapacityChart from './CapacityChart';
import FloorPlans from './FloorPlans';
// import customHistory from '../customHistory';

// import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
// import MeetingSpaceStore from '../../stores/MeetingSpaceStore';

const subTabs = [
  {
      id: 'capacity_chart',
      name: 'Capacity Chart View',
      path: '/capacity-chart',
  },
  {
    id: 'meeting_rooms',
    name: 'Meeting Rooms',
    path: '/list',
  },
  {
      id: 'floor_plan',
      name: 'Floor Plan',
      path: '/floor-plan',
  },
];
class MeetingSpace extends Component {
  constructor(props) {
    super(props);
    const { hotelInfo } = this.props;
    const { floorPlanSetup, allMeetingRooms } = hotelInfo;
    this.state = {
      toSubPageNavigate: 'capacity_chart',
      floorPlanSetup,
      allMeetingRooms,
    };
    this.handleSubTabClick = this.handleSubTabClick.bind(this);
    this.setFloorPlanLoader = this.setFloorPlanLoader.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (state.floorPlanSetup !== props.hotelInfo.floorPlanSetup
        || state.allMeetingRooms !== props.hotelInfo.allMeetingRooms ) {
      return {
        floorPlanSetup: props.hotelInfo.floorPlanSetup,
        allMeetingRooms: props.hotelInfo.allMeetingRooms,
      };
    }
    return null;
  }

  componentDidMount() {
    const { hotelInfo, property, userPrimaryColor } = this.props;
    const { router } = this.context;
    const context = router.history.location;
    const { match } = router.route;
    if (context.pathname === `/property/${property}`) {
      router.history.push(`${match.url}/list`);

    } else {
      router.history.push(context.pathname);
    }
    // $('.primary-color').css('color', userPrimaryColor);
    // $('.primary-color::after').css('border-color', userPrimaryColor);
    // $('.primary-color::before').css('border-color', userPrimaryColor);
    // $('.site-footer').css('background-image', `linear-gradient(0deg, ${userPrimaryColor} 45%,
    //   rgb(185,146,146) 145%)`);
  }

  handleSubTabClick(id, path) {
    const { property } = this.props;
    const { router } = this.context;
    const { match } = router.route;
    this.context.router.history.push(`${match.url}${path}`);
    this.setState({
      toSubPageNavigate: id,
    });
  }

  setFloorPlanLoader(isEnable) {
    this.setState({ showFloorPlanLoader: isEnable });
  }

  render() {
    let { toSubPageNavigate, floorPlanSetup, showFloorPlanLoader } = this.state;
    const {
      hotelInfo, isFullImage, showFullImage, toPageNavigate, handleTabClick, handleDropdown, typeOfSetup,
      typeOfCapacity, handle3DPopup, property, handleToggle, sqmMode, setLoader, isGetAllMeetingRooms
    } = this.props;
    const { available_setup, currentViewRoom, allMeetingRooms } = hotelInfo;
    // const context = this.context.router.history.location;
    const { router } = this.context;
    const context = router.history.location;
    const { match } = router.route;
    const { data } = hotelInfo;
    const { primary_color } = data;
    const circleSliderStyle = { background: `linear-gradient(159deg, ${primary_color}80, ${primary_color})` };
    const hotelId = data.hasOwnProperty('hotelId') ? data['hotelId'] : 0;
    if (context.pathname === `${match.url}/list` || context.pathname === match.url) {
      toSubPageNavigate = 'meeting_rooms';
    } else if (context.pathname === `${match.url}/capacity-chart`) {
      toSubPageNavigate = 'capacity_chart';
    } else if (context.pathname === `${match.url}/floor-plan`) {
      toSubPageNavigate = 'floor_plan';
    }

    return (
      <Grid className="primary-color">
        <Row className="subtab-container hidden-xs">
          <div id="layout-sub-tab" className="tmargin50">
            <ul className={`nav nav-pills ${toSubPageNavigate}`}>
              {
                subTabs.map(({ id, name, path }) => (
                  <Col lg={4} md={4} sm={4} xs={4} className={`sub-tab ${id}`} key={id}
                    style = {hotelId === 1 && id === 'floor_plan' ? { cursor: 'not-allowed' } : {}}
                  >
                    <Link
                      to={`${match.url}${path}`}
                      style={hotelId === 1 && id === 'floor_plan' ? { pointerEvents: 'none' } : {}}
                    >
                      <Button
                        className="i-block primary-color"
                        onClick={() => this.handleSubTabClick(id, path)}
                        disabled={hotelId === 1 && id === 'floor_plan'}
                        style={hotelId === 1 && id === 'floor_plan' ? { pointerEvents: 'none' } : {}}
                      >
                        <li className="primary-color text-left">
                          {
                            toSubPageNavigate === id
                            && <ReactSVG wrapper="span" className="svg-wrapper hidden-sm" src="dist/img/meeting_space/icons/eye-icon.svg" alt="eye icon" />
                          }
                          <span className="i-block primary-color">{name}</span>
                          <hr />
                        </li>
                      </Button>
                    </Link>
                  </Col>
                ))
              }
            </ul>
          </div>
        </Row>
        {
          toSubPageNavigate === 'meeting_rooms'
            && (
              <Route
                path={`${match.path}/list`}
                render={() => (
                  <MeetingRoom
                    hotelInfo={hotelInfo}
                    isFullImage={isFullImage}
                    showFullImage={showFullImage}
                    toPageNavigate={toPageNavigate}
                    handleTabClick={handleTabClick}
                    handleDropdown={handleDropdown}
                    typeOfSetup={typeOfSetup}
                    typeOfCapacity={typeOfCapacity}
                    handle3DPopup={handle3DPopup}
                    handleToggle={handleToggle}
                    sqmMode={sqmMode}
                    match={match}
                    isGetAllMeetingRooms= {isGetAllMeetingRooms}
                    userPrimaryColor={this.props.userPrimaryColor}
                  />
                )}
              />
            )
        }
        {
          toSubPageNavigate === 'floor_plan' && (
            <Route path={`${match.path}/floor-plan`} render={() => 
              <div>
                <div className="floor-plans-container">
                  <Row className="hide">
                    <Col lg={12} md={12} sm={12} xs={12} className="text-right bmargin15">
                      <span className="i-block font-bold rmargin20">Meters</span>
                      <label className="switch meter-toggle">
                        <input type="checkbox" />
                        <span className="slider-no-background round">
                          <span className="circle" />
                        </span>
                      </label>
                      <span className="i-block lmargin20">Feet</span>
                    </Col>
                  </Row>
                  <Row className="prelative floor-plans-header">
                    {
                      // <div className="overlay" />
                    }
                    <Col lg={10} md={10} sm={12} xs={12} className="border-right bmargin20">
                    {
                      floorPlanSetup && floorPlanSetup.length >= 1
                        ? (
                            <Row>
                              {
                                floorPlanSetup.map((setup, i) => (
                                  setup.type.toLowerCase() !== 'custom' ?
                                  (<Col lg={2} md={2} sm={2} xs={2} className="text-center setup-image-container  setup-image-container-loggedIn">
                                    <ReactSVG wrapper="span" className="svg-wrapper setup-img" src={setup.img} alt="setup icon" />
                                    <p className="text-center tmargin15">{setup.shape}</p>
                                    <div>
                                      <Button
                                        className={`${i === 0 ? 'button-left' : (i === floorPlanSetup.length - 1
                                          ? 'button-right' : 'no-radius')}`}
                                      >
                                        {setup.hasOwnProperty('count') ? setup.count : '-'}
                                        <img
                                          className="person-image"
                                          src="dist/img/meeting_space/icons/person.png"
                                          alt="person icon"
                                        />
                                      </Button>
                                    </div>
                                  </Col>) : ''
                                ))
                              }
                            </Row>
                          ) : ''
                    }
                    {
                      !floorPlanSetup && (
                        <div className="padding100 text-center">Meeting rooms are loading. Please wait...</div>
                      )
                    }
                    </Col>
                    <Col lg={2} md={2} sm={5} xs={12}>
                      <div className="tmargin10 lpadding10">
                        <span className="i-block fleft">Room Dimens.</span>
                        <span className="i-block fright" style={{margin: '0px -36px 0px 0px'}}>{currentViewRoom ? currentViewRoom.dimensions : '-'}</span>
                        <div className="clearfix" />
                      </div>
                      <div className="tmargin10 lpadding10">
                        <span className="i-block fleft">Ceiling Height</span>
                        <span className="i-block fright" style={{margin: '0px -36px 0px 0px'}}>{currentViewRoom ? currentViewRoom.ceiling_height : '-'}</span>
                        <div className="clearfix" />
                      </div>
                      <div className="tmargin10 lpadding10">
                        <span className="i-block fleft">m²</span>
                        <span className="i-block fright" style={{margin: '0px -36px 0px 0px'}}>{currentViewRoom ? currentViewRoom.room_space : '-'}</span>
                        <div className="clearfix" />
                      </div>
                    </Col>
                  </Row>
                </div>
                {
                  allMeetingRooms && allMeetingRooms.length >= 1
                    ? (
                    <div className="floor-plan-setup-container">
                    <div>
                      {
                        showFloorPlanLoader && (
                          <div className="loader-img-overlay pabsolute">
                            <div className="loader-img">
                              <img src="dist/img/loader.gif" alt="loading" />
                            </div>
                          </div>
                        )
                      }
                    </div>
                      <div
                        id="floor-scene-container"
                      />
                      <FloorPlans
                        hotelInfo={hotelInfo}
                        match={match}
                        handle3DPopup={handle3DPopup}
                        setFloorPlanLoader={this.setFloorPlanLoader}
                        property={property}
                        userPrimaryColor={this.props.userPrimaryColor}
                      />
                    </div>
                  ) : (
                    <div className="padding100 text-center">Meeting rooms are loading. Please wait...</div>
                  )
                }
              </div>
            } />
          )
        }
        {
          toSubPageNavigate === 'capacity_chart' && (
            <Route
              exact
              path={`${match.path}/capacity-chart`}
              render={() => (
                <CapacityChart
                  hotelInfo={hotelInfo}
                  handleDropdown={handleDropdown}
                  typeOfSetup={typeOfSetup}
                  typeOfCapacity={typeOfCapacity}
                  toPageNavigate={toPageNavigate}
                  isFullImage={isFullImage}
                  showFullImage={showFullImage}
                  handleTabClick={handleTabClick}
                  handle3DPopup={handle3DPopup}
                  handleToggle={handleToggle}
                  sqmMode={sqmMode}
                  match={match}
                  userPrimaryColor={this.props.userPrimaryColor}
                />
              )}
            />
          )
        }
      </Grid>
    );
  }
}

MeetingSpace.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  isFullImage: PropTypes.bool.isRequired,
  showFullImage: PropTypes.func.isRequired,
  handleTabClick: PropTypes.func.isRequired,
  handleDropdown: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
  handle3DPopup: PropTypes.func.isRequired,
  typeOfSetup: PropTypes.string.isRequired,
  typeOfCapacity: PropTypes.string.isRequired,
  toPageNavigate: PropTypes.string.isRequired,
  context: PropTypes.string.isRequired,
  handleToggle: PropTypes.func.isRequired,
  sqmMode: PropTypes.bool.isRequired,
  isGetAllMeetingRooms: PropTypes.bool.isRequired,
};

MeetingSpace.contextTypes = {
  router: PropTypes.object,
};

export default MeetingSpace;

import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';

class CapacityColumn extends Component {
  constructor(props) {
    super(props);
    this.handleKeyup = this.handleKeyup.bind(this);
  }

  handleKeyup() {
    console.log('key up');
  }

  render() {
    const {
      item, handleSetupConfig, hotelInfo, userPrimaryColor,
    } = this.props;
    const styleObj = {
      borderColor: `${userPrimaryColor}24`,
    };
    const activeBtnStyle = {
      background: `${userPrimaryColor}99`,
      color: '#fcfcfc',
      fontSize: '16px',
    };
    const checkmarkStyle = {
      backgroundColor: userPrimaryColor,
    };
    const { selectedRooms } = hotelInfo;
    const activeRoom = selectedRooms.filter(room => room.id === item.id);
    const isActive = activeRoom.length >= 1;
    return (
      item.setup.map(setup => (
        setup.type.toLowerCase() !== 'custom' ?
        (<Col
          sm={1}
          md={1}
          lg={1}
          className="meeting-list-col lrpadding0 text-center"
          key={setup.id}
          style={styleObj}
        >
          <div
            className={`capacity-col pointer ${setup.count > 0 ? '' : 'capacity-col-disabled'}`}
            style={isActive && activeRoom[0] && activeRoom[0].setup
              && activeRoom[0].setup.filter(layout => layout.type === setup.type).length >= 1
              ? activeBtnStyle : {}}
            onClick={() => handleSetupConfig(item, setup)}
            onKeyUp={this.handleKeyUp}
          >
            {
              isActive && activeRoom[0] && activeRoom[0].setup
              && activeRoom[0].setup.filter(layout => layout.type === setup.type).length >= 1
                && (
                  <ReactSVG
                    wrapper="div"
                    className="svg-wrapper checkmark-img"
                    src="dist/img/meeting_space/icons/checkmark.svg"
                    alt="checkmark"
                  />
                )
            }
            <span>{setup.count >= 0 ? setup.count : '-'}</span>
            <img
              className="person-image hidden-sm"
              src={`dist/img/meeting_space/icons/${isActive && activeRoom[0] && activeRoom[0].setup
              && activeRoom[0].setup.filter(layout => layout.type === setup.type).length >= 1 ? 'white-person.png'
                : 'person.png'}`}
              alt="person icon"
            />
          </div>
        </Col>) : ''
      ))
    );
  }
}

CapacityColumn.propTypes = {
  handleSetupConfig: PropTypes.func.isRequired,
  hotelInfo: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
};

export default CapacityColumn;

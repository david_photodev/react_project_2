import React, { Component } from 'react';
import {
  Row, Col, Button,
} from 'react-bootstrap';
import { Carousel } from 'react-responsive-carousel';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ReactSVG from 'react-svg';
import VideoThumbnail from 'react-video-thumbnail';

class RoomGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRoomDetails: false,
      choosedItem: 0,
    };
  }

  getItemInfo(e,hotelInfo) {
    setTimeout(function(){ 
      const { image } = hotelInfo.config3D;
      const selectedItem = $('li.slide.selected')[0].firstChild.id
      if (image.length >=1) {
        image.map(otherVideo => {
          if ((otherVideo.id !== parseInt(selectedItem)) && (otherVideo.type.id === 13)) {
            var video = document.getElementById(otherVideo.id); 
            if (video) {
              video.pause();
            }
          }
        })
      }
    }, 500);
  }

  getThumbnailFile (thumbnailUrl, assetId) {
    $('li.thumb').find('#videoThumb-'+assetId)[0].src = thumbnailUrl;
    $('li.thumb').find('#videoThumb-'+assetId).after("<img src=dist/img/upload_assets/play.png class=play-icon alt=360 />");
  }

  show360View() {
    const virtual_elem = document.getElementById('video-frame');
    const container_elem = document.getElementById('matterport-id');
    const carousel_elem = document.getElementById('meeting-room-pictures');
    virtual_elem.classList.remove('hide');
    carousel_elem.classList.add('hide');
    container_elem.classList.remove('picture-view');
    container_elem.classList.add('full-view');
  }

  showPictureSection() {
    const virtual_elem = document.getElementById('video-frame');
    const container_elem = document.getElementById('matterport-id');
    const carousel_elem = document.getElementById('meeting-room-pictures');
    virtual_elem.classList.add('hide');
    carousel_elem.classList.remove('hide');
    container_elem.classList.add('picture-view');
    container_elem.classList.remove('full-view');
  }

  showRoomDetails() {
    this.setState(prevState => ({
      isRoomDetails: !prevState.isRoomDetails,
    }));
  }

  getPosition(e) {
    this.setState({ choosedItem:e })
  }

  handleKeyUp(e) {
    console.log(e);
  }

  render() {
    const {
      hotelInfo, handleOwnConfig, showFullImage, match, userPrimaryColor,
    } = this.props;
    const { isRoomDetails, choosedItem } = this.state;
    const { config3D, data } = hotelInfo;
    const primaryColor = {
      color: userPrimaryColor,
    };
    const borderColor = {
      borderColor: `${userPrimaryColor}61`,
    };
    const checkmarkStyle = {
      backgroundColor: userPrimaryColor,
    };
    // const
      // virtUrl = 'https://my.matterport.com/show/?m=iSNSkf7JSmX&play=1&ts=1&hl=2&brand=0&hr=0&mt=0&title=0&utm_source=4';
    return (
      <div className="show-full-image" id="matterport-id">
        <div id="video-frame" className={config3D.virtUrl === null ? 'hide' : ''}>
          <iframe title="virtual video" className="embed-responsive-item" src={config3D.virtUrl} allowFullScreen />
        </div>
        <div id="meeting-room-pictures" className={config3D.image.length >= 1 && config3D.virtUrl === null ? '' : 'hide'}>
          {
            <Carousel 
              className="meeting-room-carousel"
              onChange = {(e)=>this.getItemInfo(e, hotelInfo)}
              onClickThumb = {(e)=>this.getPosition(e)}
              selectedItem = {choosedItem}
              >
              {
                config3D.image.length >= 1 ? (
                  config3D.image.map(img => (
                    img.type.id === 13 ?
                    (<video id={img.id} class="hide-focus" controls src={img.file}>
                      <img id={'videoThumb-'+img.id} src="dist/img/video-loading.png" alt="360" />
                      <VideoThumbnail
                        videoUrl={img.file}
                        thumbnailHandler={(thumbnail) => {this.getThumbnailFile(thumbnail, img.id)}}
                      />
                    </video>) 
                    : (<img src={img.file} alt="360" />)
                  ))) : (
                    <img src={config3D.room_image} alt="360" />
                )
              }
            </Carousel>
          }
        </div>
        <Row className="user-action-section">
          <Col xs={1} sm={4} md={4} lg={4} className="text-right">
            <Button className="close-button fleft" onClick={() => showFullImage(0, 'close')}>
              <span className="close-icon">&#x2715;</span>
              <span className="lmargin10 font16 hidden-xs hidden-sm">
                Exit Viewer
              </span>
            </Button>
            <div className={config3D.virtUrl === null ? 'hide' : ''}>
              <Button className="full-view-btn hidden-xs" onClick={() => this.show360View()}>
                360
                <sup className="rmargin10">o</sup>
                View
                <img src="dist/img/360-icon.png" alt="360" />
              </Button>
              <div className="clearfix" />
            </div>
          </Col>
          <Col xs={11} sm={4} md={4} lg={4} className="text-center">
            <Button
              className="room-details-btn"
              onClick={() => this.showRoomDetails('open')}
              style={primaryColor}
            >
              {/*<img src="dist/img/room-details-icon.png" className="rmargin20" alt="Details" />*/}
              <ReactSVG wrapper="span" className="svg-wrapper rmargin20" src="dist/img/room-details-icon.svg" alt="Details" />
              Room Details
              <span className="fa fa-angle-down" />
            </Button>
          </Col>
          <Col sm={4} md={4} lg={4} className="text-left hidden-xs">
            <Button className={config3D.image.length >= 1 ? 'picture-view-btn' : 'hide'} onClick={() => this.showPictureSection()}>
              Picture & Video
              <img className="align-button-right" src="dist/img/picture-icon.png" alt="View" />
            </Button>
          </Col>
        </Row>
        <Row>
          <Button className={config3D.virtUrl !== null ? 'full-view-btn visible-xs' : 'hide'} onClick={() => this.show360View()}>
            360
            <sup className="rmargin10">o</sup>
            <span className="hidden-xs">
              View
            </span>
            <img className="align-image" src="dist/img/360-icon.png" alt="360" />
          </Button>
          <Button className="picture-view-btn visible-xs" onClick={() => this.showPictureSection()}>
            Picture & Video
            <img className="align-image" src="dist/img/picture-icon.png" alt="View" />
          </Button>
          <img
            src="dist/img/configure-3d.png"
            style={checkmarkStyle}
            className="config-3d pointer"
            alt="Configure 3D"
            onClick={() => handleOwnConfig(config3D)}
            onKeyUp={this.handleKeyUp}
          />
        </Row>
        {
          isRoomDetails
          && (
            <div className="room-details-section">
              <Button
                className="close-button fleft"
                onClick={() => this.showRoomDetails('close')}
                style={primaryColor}
              >
              &#x2715;
              </Button>
              <div className="sub-head text-font-20" style={primaryColor}>
                <strong>{config3D.room_name}</strong>
              </div>
              <Row className="content">
                <Col xs={7} sm={6} md={6} lg={6} className="lpadding0 tmargin20 room-details-column" style={borderColor}>
                  <Row>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          Size
                        </span>
                      </div>
                    </Col>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          {config3D.room_space}
                        </span>
                      </div>
                    </Col>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          Dimensions
                        </span>
                      </div>
                    </Col>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          {config3D.dimensions}
                        </span>
                      </div>
                    </Col>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          Ceiling Height
                        </span>
                      </div>
                    </Col>
                    <Col sm={6} xs={6} md={6} lg={6} className="tmargin10 lpadding0">
                      <div className="item-container">
                        <span>
                          {config3D.ceiling_height}
                        </span>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col xs={5} sm={6} md={6} lg={6}>
                  <div className="main-heading" style={primaryColor}>
                    Services
                  </div>
                  <div className="room-details-info">
                    {
                      config3D.services.map(services => (
                        <div className="tmargin10" style={primaryColor} >{services.type.name}
                       </div>
                      )
                    )}
                    {
                      config3D.services.length < 1 && (
                        <div className="room-details-info">
                           -                                            
                        </div>
                      )
                    }
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12} lg={12} className="lpadding0 tmargin20">
                 <Row>
                  {
                    config3D.setup.map((setup,i) => (
                      setup.type.toLowerCase() !== 'custom' ?
                      (<Col
                        className={`i-block ${ setup.count > 0 ? 'setup-image-container' : 'setup-image-container-disable'} ${ i==0 ? '' : 'lmargin20'} tmargin20 text-center`}
                        key={setup.id}
                        >
                        {/*<img className="setup-image" src={setup.img} alt="setup icon" />*/}
                        <ReactSVG wrapper="div" className="svg-wrapper setup-image" src={setup.img} alt="setup icon" width="10px" />
                        <p className="text-center tmargin20">{setup.shape}</p>
                        <div>
                          <span
                            className={`${i % 2 === 0 ? 'button-left' : 'button-right'}`}
                            // style={isActive && activeRoom[0] && activeRoom[0].setup
                            //   && activeRoom[0].config3D.setup.filter(layout => layout.type === setup.type).length >= 1
                            //   ? selecteLayoutStyle : {}}
                            disabled={!(setup.count > 0)}
                          >
                            {setup.count >= 0 ? setup.count : '-'}
                            <img
                              className="person-image lmargin10"
                              src="dist/img/meeting_space/icons/person.png"
                              alt="person icon"
                            />
                          </span>
                        </div>
                      </Col>) : ''
                    ))
                  }
                 </Row>
                </Col>
              </Row>
            </div>
          )
        }
      </div>
    );
  }
}

RoomGallery.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  handleOwnConfig: PropTypes.func.isRequired,
  showFullImage: PropTypes.func.isRequired,
  property: PropTypes.string.isRequired,
};

export default RoomGallery;

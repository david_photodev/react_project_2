import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import {
  Row, Col, Button,
} from 'react-bootstrap';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import MeetingSpaceAction from '../../actions/MeetingSpaceAction';
import customHistory from '../customHistory';
import Helpers from '../Reusables/Helpers';
import AuthStore from "../../stores/AuthStore";
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

class SubmitRFP extends Component {
  constructor(props) {
    super(props);
    const { hotelInfo } = this.props;
    this.state = {
      hotelInfo,
      isPersonalChecked: true,
      dragImage: true,
      requiredFields: [],
      deviceName: '',
      // uploadedFile: false,
    };
    this.handleNewPeriod = this.handleNewPeriod.bind(this);
    this.handleKeyup = this.handleKeyup.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleContactEdit = this.handleContactEdit.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleContactChange = this.handleContactChange.bind(this);
    this.handleSaveContact = this.handleSaveContact.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePrivacyPolicy = this.handlePrivacyPolicy.bind(this);
  }

  componentWillMount() {
    const { hotelInfo, setLoader } = this.props;
    MeetingSpaceAction.getAttachementTypes((res) => {
      if (res === false) {
        setLoader(false);
      }
      const attachmentTypes = res;
      const myDropzone = new Dropzone('div#file-upload-zone', {
        url: (files) => {
          const event_id = hotelInfo.eventId;
          return AuthStore.getApiHost()+`/api/events/${event_id}/attachments`;
        },
        paramName: 'file',
        params: (files, xhr, chunk) => {
          console.log('PARAMS:', files);
          let params = {};
          if (chunk) {
            params = Dropzone.prototype.defaultOptions.params();
          }

          let fileTypeId = 1;
          if (files.length) {
            fileTypeId = (attachmentTypes[files[0].type] || fileTypeId);
          }
          params['type'] = fileTypeId;
          return params;
        },
        acceptedFiles: Object.keys(attachmentTypes).join(','),
        accept: (file, done) => {
          this.setState({ dragImage: false });
          const msg = `Please upload only the following file types ${attachmentTypes.names.join(', ')}`;
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
          const uploadZone = $('#file-upload-zone');
          if (uploadZone) {
            const fileUpload = $('#file-upload-zone')[0].childNodes.length;
            const dragMessageTop = `${40 + (fileUpload * 30)}px`;
            const dropZoneHeight = `${224 + (fileUpload * 30)}px`;
            $(`<style>.submit-container #upload-doc-section .drag-upload-file
            {top: ${dragMessageTop};}</style>`).appendTo('head');
            $(`<style>.submit-container #file-upload-zone {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
          }
          console.log('ACCEPT: ', file);
          done();
        },
        uploadMultiple: false,
        parallelUploads: 1,
        previewTemplate: document.querySelector('#file-upload-template').innerHTML,
        thumbnailWidth: 28,
        thumbnailHeight: 28,
        thumbnailMethod: 'crop',
        autoProcessQueue: false, // upload all files at once when submit button is clicked
      });
      myDropzone.on('sending', (file, xhr, formData) => {
        // set authorization header
        const headers = AuthStore.getHeaders();
        for (let headerName in headers) {
          if(headers.hasOwnProperty(headerName)) {
            let headerValue = headers[headerName];
            if (headerValue) {
              xhr.setRequestHeader(headerName, headerValue);
            }
          }
        }
      });
      myDropzone.on('error', (file, message, xhr) => {
        if (xhr == null) myDropzone.removeFile(file); // Remove the unsupported files
        const msg = `Unsupported file format. Please upload only ${attachmentTypes.names.join(', ')}`;
        MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
        setTimeout(() => {
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
        }, 5000);
      });
      myDropzone.on('removedfile', () => {
        const uploadZone = $('#file-upload-zone');
        if (uploadZone) {
          const fileUpload = $('#file-upload-zone')[0].childNodes.length;
          const dragMessageTop = `${40 + (fileUpload * 30)}px`;
          const dropZoneHeight = `${224 + (fileUpload * 30)}px`;
          $(`<style>.submit-container #upload-doc-section .drag-upload-file
          {top: ${dragMessageTop};}</style>`).appendTo('head');
          $(`<style>.submit-container #file-upload-zone {min-height: ${dropZoneHeight};}</style>`).appendTo('head');
        }
        if (myDropzone.files.length <= 0) {
          // MeetingSpaceAction.addedFileCount(false);
          this.setState({ uploadedFile: false });
        }
      });
      myDropzone.on('addedfile', () => {
        this.setState({ uploadedFile: true });
        // MeetingSpaceAction.addedFileCount(true);
      });
      this.setState({ myDropzone });
    });
  }

  componentDidMount() {
    if (/iphone|ipad|iPod/i.test(navigator.userAgent)) {
      console.log('ios');
      $(`<style>.styled-checkbox:checked + label:after{top: 21px;}</style>`).appendTo('head');
      $(`<style>.styled-checkbox:checked + label:after{left: 4px;}</style>`).appendTo('head');
    }
  }

  handleNewPeriod() {
    const { isSubmitRfp } = this.props;
    isSubmitRfp(false);
    MeetingSpaceAction.handleNewPeriod();
  }

  handleKeyup() {
    console.log('keyup', e);
  }

  handleDelete(id) {
    MeetingSpaceAction.deletePeriod(id);
  }

  handleEdit(id, item) {
    MeetingSpaceAction.editPeriod(id, item, 'edit');
  }

  handleChange(e) {
    const { hotelInfo } = this.state;
    const { guestInfo } = hotelInfo;
    const { id, name, value } = e.target;
    const guestId = parseInt(id, 10);
    const updateGuest = guestInfo.map((guest) => {
      const eachGuest = guest;
      if (guest.id === guestId) {
        eachGuest[name] = parseInt(value, 10);
        return eachGuest;
      }
      return eachGuest;
    });
    this.setState({ guestInfo: updateGuest });
  }

  handleSaveData(id, item) {
    const { guestInfo } = this.state;
    MeetingSpaceAction.editPeriod(id, item, 'read');
    MeetingSpaceAction.savePeriod(guestInfo);
  }

  handleDate(id, item, e, picker) {
    const { hotelInfo } = this.state;
    const { guestInfo } = hotelInfo;
    // let { startDate } = picker;
    // startDate = moment(startDate).format('MMMM DD, YYYY');
    const updateGuest = guestInfo.map((guest) => {
      const eachGuest = guest;
      if (guest.id === id) {
        eachGuest[item] = picker;
        return eachGuest;
      }
      return eachGuest;
    });
    MeetingSpaceAction.editPeriod(id, item, 'read');
    MeetingSpaceAction.savePeriod(updateGuest);
    this.setState({ guestInfo: updateGuest });
  }

  handleContactEdit(item) {
    MeetingSpaceAction.editContact(item, 'edit');
  }

  handleCheck(type) {
    const { hotelInfo } = this.state;
    const { contactDetails } = hotelInfo;
    const isPersonal = type === 'personal';
    this.setState({ isPersonalChecked: isPersonal });
    contactDetails.contact_type = isPersonal ? 'personal' : 'business';
    MeetingSpaceAction.saveContact(contactDetails);
  }

  handleContactChange(e, contactField='') {
    const { hotelInfo, requiredFields } = this.state;
    const { contactDetails } = hotelInfo;
    let errorMsg = requiredFields.map((required) => {
      if (contactDetails[required] === '' || contactDetails[required] === null) {
        return required;
      }
      return '';
    });
    errorMsg = errorMsg.filter(err => err !== '');
    
    if (contactField === 'country') {
      contactDetails[contactField] = e;
      MeetingSpaceAction.editContact(contactField, 'read');
      MeetingSpaceAction.saveContact(contactDetails);
    } else {
      const { name, value } = e.target;
      contactDetails[name] = value;
    }

    this.setState({ contactDetails, requiredFields: errorMsg });
  }

  handleSaveContact(item) {
    const { contactDetails } = this.state;
    MeetingSpaceAction.editContact(item, 'read');
    MeetingSpaceAction.saveContact(contactDetails);
  }

  handlePrivacyPolicy(e) {
    this.setState({ isAcceptedConditions: e.target.checked });
  }

  sendEmail(email) {
    var url = AuthStore.getApiHost()+"/api/users/send";
    const { setLoader, setReadWriteMode, setSuccessMessage } = this.props;
    var formData  = new FormData();
    for(var name in email.data) {
      formData.append(name, email.data[name]);
    }
    AuthStore.getHeaders(null, (headers) => {
      fetch(url, {
        mode: 'cors',
        method: 'POST',
        headers: headers,
        body: formData
      }).then(function (response) {
        email.response = response;
        if (response.ok) {
          setReadWriteMode(true);
          setLoader(false);
          setSuccessMessage(true);
        }
      });
    });
  }

  handleSubmit() {
    const { isAcceptedConditions, myDropzone } = this.state;
    const {
      hotelInfo, setLoader, setExpander, property, isSubmitRfp,
    } = this.props;
    isSubmitRfp(true);
    const {
      contactDetails, selectedRooms, guestInfo, eventInfo,
    } = hotelInfo;
    let validateGuestInfo = true;
    const validateGuestInfoFields= ['double_rooms', 'single_rooms', 'start', 'end'];
    if(guestInfo.length>=1){
      guestInfo.map((guest_room)=>{
        validateGuestInfoFields.map((field)=>{
          if (guest_room[field] === '' || guest_room[field] === null || Number.isNaN(guest_room[field])){
              validateGuestInfo = false;
              return null;
          }
        })
      })
    }
    let validateActivityStatus = true; 
    const validateActivityFields= ['event_date_starts', 'event_date_ends', 'event_time_starts', 'event_time_ends', 'chairs_count', 'count'];
    if (selectedRooms.length >= 1) {
      selectedRooms.map((each_room)=>{each_room.setup.map((each_setup,i)=>{
        validateActivityFields.map((field)=>{
          if (each_setup[field] === '' || each_setup[field] === null || Number.isNaN(each_setup[field])){
              validateActivityStatus = false;
              return null;
          }
        })
      })})
    }  
    if (!validateActivityStatus){
          const msg = 'Please fill all the fields of meeting rooms info';
          MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
          setTimeout(() => {
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
          }, 5000);
    }

    if (!validateGuestInfo){
          const msg = 'Please fill all the fields of guests rooms info';
          MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
          setTimeout(() => {
          MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
          }, 5000);
    }

    const requiredFields = ['name', 'email', 'phone', 'name_of_event', 'attendees'];
    let emailFormatMsg = '';
    let phoneFormatMsg = '';

    let errorMsg = requiredFields.map((required) => {
      if (contactDetails[required] === '' || contactDetails[required] === null) {
        
        return required;
      }
      if ( (contactDetails['email'] !== '') && (!contactDetails['email'].match(/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/))) {
        
        emailFormatMsg = 'Please Enter The Valid Email'

        }
      if ( (contactDetails['phone'] !== '') && (!/^[\d\.\-]+$/.test(contactDetails['phone']))) {
        
        phoneFormatMsg = 'Please Enter The Valid Phone Number'

        }
      return '';
    });
    if (emailFormatMsg){
      MeetingSpaceAction.notificationMsg(emailFormatMsg, 'add', 'presetup');
      setTimeout(() => {
      MeetingSpaceAction.notificationMsg(emailFormatMsg, 'remove', 'presetup');
      }, 5000);
    }
    
    if (phoneFormatMsg){
      MeetingSpaceAction.notificationMsg(phoneFormatMsg, 'add', 'presetup');
      setTimeout(() => {
      MeetingSpaceAction.notificationMsg(phoneFormatMsg, 'remove', 'presetup');
      }, 5000);
    }

    errorMsg = errorMsg.filter(err => err !== '');
    this.setState({ requiredFields: errorMsg });
    if (errorMsg.length < 1) {
      MeetingSpaceAction.notificationMsg('Please fill the required fields', 'remove', 'presetup');
    } else if (errorMsg.length >= 1) {
      const msg = 'Please fill the required fields';
      MeetingSpaceAction.notificationMsg(msg, 'add', 'presetup');
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg(msg, 'remove', 'presetup');
      }, 5000);
    }
    if (selectedRooms.length < 1) {
      MeetingSpaceAction.notificationMsg('Please select atleast one room', 'add', 'presetup');
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg('Please select atleast one room', 'remove', 'presetup');
      }, 5000);
    }
    if (!isAcceptedConditions) {
      MeetingSpaceAction.notificationMsg('Please accept the terms and conditions', 'add', 'presetup');
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg('Please accept the terms and conditions', 'remove', 'presetup');
      }, 5000);
    }
    if (errorMsg.length < 1 && selectedRooms.length >= 1 && isAcceptedConditions && validateActivityStatus && validateGuestInfo && (!emailFormatMsg) && (!phoneFormatMsg)) {
      setExpander(true);
      const msg = 'Your request submitted successfully';
      MeetingSpaceAction.notificationMsg(msg, 'add', 'success');
      setTimeout(() => {
        MeetingSpaceAction.notificationMsg(msg, 'remove', 'success');
      }, 5000);
      setLoader(true);

      const startDateArray = [];
      const endDateArray = [];
      const attendeesList = [];
      selectedRooms.map((room) => {
        room.setup.map((setup) => {
          const startDate = moment(`${setup.event_date_starts} ${setup.event_time_starts}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
          const endDate = moment(`${setup.event_date_ends} ${setup.event_time_ends}`, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm');
          startDateArray.push(startDate);
          endDateArray.push(endDate);
          attendeesList.push(setup.chairs_count);
        });
      });

      this.setState({
        isNoGuestRequest: guestInfo.length <= 0,
      });

      startDateArray.sort((a, b) => {
        return new Date(b) - new Date(a);
      });

      endDateArray.sort((a, b) => {
        return new Date(b) - new Date(a);
      });

      attendeesList.sort((a, b) => {
        return a - b;
      });

      const event_info = {};
      event_info['name'] = contactDetails.name_of_event;
      event_info['start'] = eventInfo.start ? eventInfo.start : startDateArray[startDateArray.length - 1];
      event_info['end'] = eventInfo.end ? eventInfo.end : endDateArray[0];
      event_info['attendees'] = contactDetails.attendees;
      event_info['is_private'] = 'n';
      event_info['status'] = 6;
      event_info['hotel'] = eventInfo.hotel ? eventInfo.hotel : parseInt(property, 10);
      event_info['type'] = eventInfo.type ? eventInfo.type : 1;
      MeetingSpaceAction.createEvent(event_info,
        (res) => {
          if (res === false) {
            setLoader(false);
          }
          this.props.setUUId();
          customHistory.push(`/#/property/${property}/rfp/view/${res.uuid}`);
          const link = window.location.href;
          let html_user   = '<div class = "email-template-user">';
          html_user += '<p> Hi '+ contactDetails.name +',</p>';
          html_user += '<p> Congratulations! Your RFP '+ contactDetails.name_of_event +' to ' + hotelInfo.data.hotel_name + ' has been received. </p>';
          html_user += '<p> The hotel will be in touch soon to review your request.</p>';
          html_user += '<p> Please find attached your request.</p>';
          html_user += '<p>'+link+'</p>';
          html_user += '<p>Sincerely,<br>' + hotelInfo.data.hotel_chain_name + '</p>';
          html_user += '<p>---<br>Powered by Spazious™</P>';
          html_user += '</div>';
          let html_hotel = '<div class = "email-template-hotel">';
          html_hotel += '<p>Dear ' + hotelInfo.data.hotel_name + ',<br>There is a new RFP that requires your attention.</p>';
          html_hotel += '<p>RFP Name: ' + contactDetails.name_of_event + '';
          html_hotel += '<br>RFP Sender: ' + contactDetails.name + '';
          html_hotel += '<br>Total # of attendees: ' + contactDetails.attendees + '';
          html_hotel += '<br>RFP Sender’s email: ' + contactDetails.email + '';
          html_hotel += '<br>RFP Sender’s phone: ' + contactDetails.phone + '</p>';
          html_hotel += '<p>Please review the information attached and get back to the client with a proposal.</p>';
          html_hotel += '<p>'+link+'</p>';
          html_hotel += '<p>Thank you,<br>' + hotelInfo.data.hotel_chain_name + '</p>';
          html_hotel += '<p>---<br>Powered by Spazious™</P>';
          html_hotel += '</div>';

          const email_template = [
            {
              "sender":hotelInfo.data.sender_email,
              "recipients":[contactDetails.email],
              "subject":"Your RFP for event "+ contactDetails.name_of_event + " has been sent!",
              "body":{
                "plain":"Hi "+contactDetails.name+", your RFP has been sent, also you have attached the PDF with all the data, thanks.",
                "html": html_user
              }
            },
            {
              "sender":hotelInfo.data.sender_email,
              "recipients":hotelInfo.data.hotel_manager_email,
              "subject":"A new RFP is available!",
              "body":{
                "plain":"Hi hotel manager, you have new, also you have attached the PDF with all the data, thanks.",
                "html": html_hotel
              }
            }
          ];
          const email = {
            "data": {
              "messages": JSON.stringify(email_template),
              "attachment": "",
            },
            "response": null
          };

          const container = document.getElementsByClassName('layout-header-tab-container')[0];
          setLoader(false);
          Helpers.outputAsPDF(container, 'RFP.pdf', (blob) => {
            setLoader(true);
            email.data.attachment = new File([blob], 'RFP.pdf');
            if (myDropzone) {
              myDropzone.on('queuecomplete', () => {
                console.log('ALL FILES UPLOADED');
                myDropzone.disable(); // no more interactions after uploading RFP
              });
              myDropzone.on('complete', (file) => {
                console.log('ONE FILE UPLOADED', file);
                MeetingSpaceAction.storeEventId(res.id, res.uuid);
                MeetingSpaceAction.uploadedFile(JSON.parse(file.xhr.responseText));
                myDropzone.processQueue();
              });
              myDropzone.on('error', (file, message, xhr) => {
                setLoader(false);
                console.error('ERROR', file, JSON.stringify(message), xhr);
              });
              myDropzone.processQueue();
            }
            MeetingSpaceAction.activityProgram(res, hotelInfo, (bool) => {
              if (bool) {
                this.sendEmail(email);
              } else {
                setLoader(false);
              }
            });
          });
        });
    }
  }

  render() {
    const {
      hotelInfo, isPersonalChecked, myDropzone, requiredFields, isAcceptedConditions, isNoGuestRequest,
    } = this.state;
    let { uploadedFile } = this.state;
    const {
      readOnlyMode, handleDownloadPDF, setMode, isDownload, toPageNavigate, isClickedSubmitRfp, userPrimaryColor,
    } = this.props;
    const {
      guestInfo, contactDetails: contact, data, uploadedDocuments,
    } = hotelInfo;
    const { primary_color, privacy_policy, terms_and_condtions } = data;
    const primaryColor = {
      color: userPrimaryColor,
    };
    const borderColor = {
      borderColor: userPrimaryColor,
    };
    const readMode = readOnlyMode ? 'no-pointers' : '';
    // if (documentLength) {
    //   uploadedFile = documentLength >= 1;
    // }
    const path = window.location.href;
    let deviceName;
    if (/iphone|ipad|iPod/i.test(navigator.userAgent)) {
      deviceName = 'ios';
    }
    return (
      <div
        className={`submit-container prelative ${toPageNavigate === 'room_setup' ? 'tpadding20' : ''}`}
      >
        <div className="overlay" />
        <div className={`room-info-sections ${readMode}`}>
          <Row className={`${toPageNavigate === 'room_setup' ? '' : 'rfp-label'}`}>
            <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
              <div className="img-class">
                <img src="dist/img/guest-room.png" alt="Guest Room" />
              </div>
              <div className="meeting-info-title">
                Guests Rooms Info
              </div>
              <div className="hyphen-class">
                <hr />
              </div>
            </Col>
          </Row>
          <div>
            {
              guestInfo.length >= 1 && (
                <div className="box-rfp">
                  {
                    guestInfo.map(guest => ([
                      <Row className="guest-room-section border-right" key={guest.id}>
                        <Col xs={12} sm={12} md={3} lg={3} className="no-padding prelative add-arrival">
                          <span className="meeting-info-label vtop5">Arrival</span>
                          <DateRangePicker
                            singleDatePicker
                            containerClass="arrival-input"
                            minDate={moment()}
                            onApply={(e, picker) => this.handleDate(guest.id, 'start', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                          >
                            {
                              guest.hasOwnProperty('start') && guest.start ? (
                                <span className="meeting-info-value pointer">
                                  {moment(guest.start).format('ddd, MMM DD YYYY')}
                                </span>
                              ) : (
                                <input 
                                className="form-control i-block arrival-departure-input" 
                                type="button" 
                                style={((guest.start === '') && isClickedSubmitRfp) ? {border: '2px solid red'}: {border: '1px solid #00000040'}} 
                                />
                              )
                            }
                          </DateRangePicker>
                          <Button
                            className="delete-btn fright visible-xs"
                            onClick={() => this.handleDelete(guest.id)}
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                          </Button>
                        </Col>
                        <Col xs={12} sm={12} md={3} lg={3} className="no-padding add-departure">
                          <span className="meeting-info-label vtop5">Departure</span>
                          <DateRangePicker
                            singleDatePicker
                            containerClass="departure-input"
                            minDate={moment()}
                            onApply={(e, picker) => this.handleDate(guest.id, 'end', e, moment(picker.startDate).format('MMMM DD, YYYY'))}
                          >
                            {
                              guest.hasOwnProperty('end') && guest.end ? (
                                <span className="meeting-info-value pointer">
                                  {moment(guest.end).format('ddd, MMM DD YYYY')}
                                </span>
                              ) : (
                                <input 
                                className="form-control i-block arrival-departure-input" 
                                type="button"
                                style={((guest.end === '') && isClickedSubmitRfp) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}  
                                />
                              )
                            }
                          </DateRangePicker>
                        </Col>
                        <Col xs={12} sm={12} md={3} lg={3} className="no-padding add-single">
                          <span className="meeting-info-label">Single rooms</span>
                          {
                            // (guest.edit && guest.edit.indexOf('single_rooms') > -1) || guest.single_rooms === ''
                            //   ? (
                            <input
                              type="text"
                              className="form-control i-block single-room-input"
                              style={((guest.single_rooms === '' || Number.isNaN(guest.single_rooms)) && isClickedSubmitRfp) ? {border: '2px solid red'}: {border: '1px solid #00000040'}}  
                              value={guest.single_rooms >= 0? guest.single_rooms : ''}
                              id={guest.id}
                              min="1"
                              onChange={this.handleChange}
                              onFocus={this.handleChange}
                              onBlur={() => this.handleSaveData(guest.id, 'single_rooms')}
                              name="single_rooms"
                            />
                            // ) : (
                            //   <span
                            //     className="meeting-info-value"
                            //     onClick={() => this.handleEdit(guest.id, 'single_rooms')}
                            //     onKeyUp={this.handleKeyup}
                            //   >
                            //     {guest.single_rooms}
                            //   </span>
                            // )
                          }
                        </Col>
                        <Col xs={12} sm={12} md={3} lg={3} className="no-padding add-double">
                          <span className="meeting-info-label">Double rooms</span>
                          <Button
                            className="delete-btn fright hidden-xs"
                            onClick={() => this.handleDelete(guest.id)}
                          >
                            <ReactSVG wrapper="span" className="svg-wrapper" src="dist/img/meeting_space/icons/delete-red@3x.svg" alt="delete" />
                          </Button>
                          {
                            // (guest.edit && guest.edit.indexOf('double_rooms') > -1) || guest.double_rooms === ''
                            //   ? (
                            <input
                              type="text"
                              className="form-control i-block double-rooms-input"
                              style={((guest.double_rooms === '' || Number.isNaN(guest.double_rooms)) && isClickedSubmitRfp) ? {border: '2px solid red'} : {border: '1px solid #00000040'}}  
                              min="1"
                              value={guest.double_rooms >= 0? guest.double_rooms : ''}
                              id={guest.id}
                              onChange={this.handleChange}
                              onFocus={this.handleChange}
                              onBlur={() => this.handleSaveData(guest.id, 'double_rooms')}
                              name="double_rooms"
                            />
                            // ) : (
                            //   <span
                            //     className="meeting-info-value"
                            //     onClick={() => this.handleEdit(guest.id, 'double_rooms')}
                            //     onKeyUp={this.handleKeyup}
                            //   >
                            //     {guest.double_rooms}
                            //   </span>
                            // )
                          }
                        </Col>
                      </Row>,
                      <hr className="info-separator" key="new-line" />,
                    ]))
                  }
                </div>
              )
            }
            {
              ((path.includes('/view') && guestInfo.length <= 0) || isNoGuestRequest) && (
                <div className="text-center tmargin20 bpadding20">No Guest rooms requested</div>
              )
            }
            <Row>
              <Col xs={12} sm={12} md={12} lg={12} className="lpadding0">
                <div className="add-items-wrapper">
                  <div className="add-items lpadding0">
                    <div
                      className="i-block new-meeting-room"
                      onClick={this.handleNewPeriod}
                      onKeyUp={this.handleKeyup}
                      style={(readOnlyMode || isDownload) ? { display: 'none' } : { display: 'inline-block' }}
                    >
                      <img
                        className="add-image"
                        style={{ backgroundColor: userPrimaryColor }}
                        src="dist/img/meeting_space/icons/plus.png"
                        alt="plus"
                      />
                      <span>
                        Add New Period
                      </span>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div className={`room-info-sections tmargin20 ${readMode}`} id="contact-info-section">
          <Row className={`${toPageNavigate === 'room_setup' ? '' : 'rfp-label'}`}>
            <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
              <div className="img-class">
                <img src="dist/img/contact-icon.png" alt="Contact Info" />
              </div>
              <div className={`meeting-info-title ${toPageNavigate === 'room_setup' ? '' : 'rfp-label'}`}>
                Contact Info
              </div>
              <div className="hyphen-class">
                <hr />
              </div>
            </Col>
          </Row>
          <div className="box-rfp">
            <Row className="contact-info-row">
              <Col md={6} lg={6} className="bmargin20">
                <span className="contact-sub-title block">Contact details</span>
                <span className="meeting-info-label">* Name</span>
                {
                  // (contact.edit && contact.edit.indexOf('name') > -1) || contact.name === ''
                  //   ? (
                  <input
                    type="text"
                    className={`form-control i-block contact-input ${requiredFields.includes('name') && isClickedSubmitRfp ? 'required' : ''}`}
                    onChange={this.handleContactChange}
                    onFocus={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('name')}
                    name="name"
                    value={contact.name}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('name')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.name}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">* Email</span>
                {
                  // (contact.edit && contact.edit.indexOf('email') > -1) || contact.email === ''
                  //   ? (
                  <input
                    type="text"
                    className={`form-control i-block contact-input ${requiredFields.includes('email') && isClickedSubmitRfp ? 'required' : ''}`}
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('email')}
                    onFocus={this.handleContactChange}
                    name="email"
                    value={contact.email}
                  />
                    // ) : (
                    //   <span
                    //     className="meeting-info-value"
                    //     onClick={() => this.handleContactEdit('email')}
                    //     onKeyUp={this.handleKeyup}
                    //   >
                    //     {contact.email}
                    //   </span>
                    // )
                }
                <br />
                <span className="meeting-info-label">* Phone</span>
                {
                  // (contact.edit && contact.edit.indexOf('phone') > -1) || contact.phone === null
                  //   ? (
                  <input
                    type="text"
                    className={`form-control i-block contact-input ${requiredFields.includes('phone') && isClickedSubmitRfp? 'required' : ''}`}
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('phone')}
                    onFocus={this.handleContactChange}
                    name="phone"
                    value={contact.phone}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('phone')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.phone}
                  //   </span>
                  // )
                }
                <input
                  type="radio"
                  className="rmargin20"
                  id="business"
                  onChange={() => this.handleCheck('business')}
                  checked={contact.contact_type === 'business'}
                />
                <label htmlFor="business" className="i-block width50 tmargin20">Business</label>
                <input
                  type="radio"
                  id="personal"
                  className="rmargin20"
                  onChange={() => this.handleCheck('personal')}
                  checked={contact.contact_type === 'personal'}
                />
                <label htmlFor="personal" className="i-block  tmargin20">Personal</label>
                <br />
                <span className="meeting-info-label">Company <span className="i-block mob-block">Name</span></span>
                {
                  // (contact.edit && contact.edit.indexOf('company_name') > -1) || contact.company_name === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('company_name')}
                    onFocus={this.handleContactChange}
                    name="company_name"
                    value={contact.company_name}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('company_name')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.company_name}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">* Total # of attendees</span>
                {
                  <input
                    type="text"
                    className={`form-control i-block contact-input ${requiredFields.includes('attendees') && isClickedSubmitRfp? 'required' : ''}`}
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('attendees')}
                    onFocus={this.handleContactChange}
                    name="attendees"
                    value={contact.attendees}
                  />
                }
                <br />
                <span className="meeting-info-label">* Name of your event</span>
                {
                  <input
                    type="text"
                    className={`form-control i-block contact-input ${requiredFields.includes('name_of_event') && isClickedSubmitRfp? 'required' : ''}`}
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('name_of_event')}
                    onFocus={this.handleContactChange}
                    name="name_of_event"
                    value={contact.name_of_event}
                  />
                }
                <input
                  className="styled-checkbox"
                  id="styled-checkbox-1"
                  type="checkbox"
                  checked={(readOnlyMode || setMode === 'viewOnly') || isAcceptedConditions}
                  onChange={this.handlePrivacyPolicy}
                />
                <label htmlFor="styled-checkbox-1" className="terms-privacy">
                  {'I have read and accept the '}
                  <a
                    className="terms"
                    href={terms_and_condtions}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={primaryColor}
                  >
                    {'terms and conditions'}
                  </a>
                  {' and the '}
                  <a
                    className="privacy"
                    href={privacy_policy}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={primaryColor}
                  >
                    privacy policy
                  </a>
                  {
                    deviceName === 'ios' && isDownload && (
                      <span className="check-mark-condition">&#10003;</span>
                    )
                  }
                </label>
              </Col>
              <Col md={6} lg={6} className="fix-top-rfp-contact-box">
                <span className="contact-sub-title block tmargin20">Address</span>
                <br />
                <span className="meeting-info-label">Country / Region</span>
                <CountryDropdown className="form-control i-block contact-input"
                  defaultOptionLabel = "Not selected"
                  onChange={(e)=>this.handleContactChange(e, 'country')}
                  value={contact.country}
                  name="country"
                />

                <br />
                <span className="meeting-info-label">Address</span>
                {
                  // (contact.edit && contact.edit.indexOf('address') > -1) || contact.address === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('address')}
                    onFocus={this.handleContactChange}
                    name="address"
                    value={contact.address}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('address')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.address}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">Address line 2</span>
                {
                  // (contact.edit && contact.edit.indexOf('address_line_2') > -1) || contact.address_line_2 === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onBlur={() => this.handleSaveContact('address_line_2')}
                    onChange={this.handleContactChange}
                    onFocus={this.handleContactChange}
                    name="address_line_2"
                    value={contact.address_line_2}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('address_line_2')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.address_line_2}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">City</span>
                {
                  // (contact.edit && contact.edit.indexOf('city') > -1) || contact.city === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('city')}
                    onFocus={this.handleContactChange}
                    name="city"
                    value={contact.city}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('city')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.city}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">State / Province</span>
                {
                  // (contact.edit && contact.edit.indexOf('state') > -1) || contact.state === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('state')}
                    onFocus={this.handleContactChange}
                    name="state"
                    value={contact.state}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('state')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.state}
                  //   </span>
                  // )
                }
                <br />
                <span className="meeting-info-label">Zip / Postal code</span>
                {
                  // (contact.edit && contact.edit.indexOf('postal_code') > -1) || contact.postal_code === null
                  //   ? (
                  <input
                    type="text"
                    className="form-control i-block contact-input"
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('postal_code')}
                    onFocus={this.handleContactChange}
                    name="postal_code"
                    value={contact.postal_code}
                  />
                  // ) : (
                  //   <span
                  //     className="meeting-info-value"
                  //     onClick={() => this.handleContactEdit('postal_code')}
                  //     onKeyUp={this.handleKeyup}
                  //   >
                  //     {contact.postal_code}
                  //   </span>
                  // )
                }
              </Col>
            </Row>
            <Row className="fix-center-rfp-additional-info">
              <Col
                md={12}
                lg={12}
                className={` ${toPageNavigate === 'room_setup' ? '' : ''}`}
              >
                <div className="additional-info-div tmargin20">
                  <span className="event-summ-label block">
                    Additional Information
                  </span>
                  <textarea
                    rows="10"
                    maxLength="500"
                    value={contact.additional_information}
                    onChange={this.handleContactChange}
                    onBlur={() => this.handleSaveContact('additional_information')}
                    onFocus={this.handleContactChange}
                    placeholder="Include any additional request here. (500 character limit)"
                    name="additional_information"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div
          className={`room-info-sections tmargin20 ${readMode}
            ${setMode === 'viewOnly' ? 'visible-pointers bpadding20' : ''}`}
          id="add-document-section"
        >
          <Row className="rfp-label">
            <Col xs={12} sm={12} md={12} lg={12} className="meeting-room-wrapper">
              <div className="img-class">
                <img src="dist/img/add-document-icon.png" alt="Document Info" />
              </div>
              <div className="meeting-info-title">
                { readOnlyMode ? 'Uploaded documents' : 'Add documents' }
              </div>
              <div className="hyphen-class">
                <hr />
              </div>
            </Col>
          </Row>
          <div className="box-rfp">
            <Row className="doc-margin-class">
              {
                !uploadedFile && readOnlyMode ? (
                  <div
                    className="text-center bpadding20"
                    style={uploadedDocuments.length >= 1 ? { display: 'none' } : { display: 'block' }}
                  >
                    No additional files uploaded
                  </div>
                ) : (
                  <Col md={12} lg={12} id="upload-doc-section" className="text-center" style={uploadedDocuments.length >= 1 ? {display: 'none'} : {display: 'block'}}>
                    <div id="file-upload-zone" />
                    <div id="file-upload-template">
                      <div className="dz-preview dz-file-preview">
                        <div className="dz-details">
                          <img src="" data-dz-thumbnail />
                          <span data-dz-name className="file-dz-name" />
                          (
                          <span data-dz-size />
                          )
                          <span data-dz-remove className="dz-remove pointer">✘</span>
                          <span className="dz-error-message" data-dz-errormessage />
                        </div>
                        <div className="dz-progress">
                          <span className="dz-upload" data-dz-uploadprogress />
                        </div>
                      </div>
                    </div>
                    <div className={`drag-upload-file ${isDownload ? 'hide' : 'block'}`}>
                      {/*<img className="text-center" src="dist/img/upload-icon.png" alt="Upload Icon" />*/}
                      <ReactSVG wrapper="div" className="svg-wrapper text-center" src="dist/img/upload-icon.svg" alt="Upload Icon" />
                      <span className="block tmargin20 font16 text-center">
                        Drag and drop files here
                        <br />
                        or click upload
                      </span>
                    </div>
                  </Col>
                )
              }
              {
                // readOnlyMode && setMode === 'viewOnly' && uploadedDocuments.length >= 1 ? (
                //   <Col md={12} lg={12} id="uploaded-doc" className="text-center tpadding20 visible-pointers"
                //   >
                //     {
                //       uploadedDocuments.map((doc, i) => (
                //         <div
                //           className="text-center"
                //         >
                //           <a
                //             href={doc.file}
                //             target="_blank"
                //             noopener
                //             noreferrer
                //             style={{ color: hotelInfo.data.primaryColor }}
                //           >
                //             {`Uploaded file${i + 1}`}
                //           </a>
                //         </div>
                //       ))
                //     }
                //   </Col>
                // ) : (
                //   <div
                //     className="text-center tmargin20 bpadding20"
                //     style={setMode === 'viewOnly' ? { display: 'block' } : { display: 'none' }}
                //   >
                //     No additional files uploaded
                //   </div>
                // )
              }
              {
                uploadedDocuments.length >= 1 && (
                  <Col md={12} lg={12} id="uploaded-doc" className="text-center tpadding20 visible-pointers"
                  >
                    {
                      uploadedDocuments.map((doc, i) => (
                        <div
                          className="text-center"
                        >
                          <a
                            href={doc.file}
                            target="_blank"
                            noopener
                            noreferrer
                            style={{ color: hotelInfo.data.primaryColor }}
                          >
                            {doc.file.split('/')[doc.file.split('/').length-1]}
                          </a>
                        </div>
                      ))
                    }
                  </Col>
                // ) : (
                //   <div
                //     className="text-center tmargin20 bpadding20"
                //     style={setMode === 'viewOnly' ? { display: 'block' } : { display: 'none' }}
                //   >
                //     No additional files uploaded
                //   </div>
                )
              }
              <Col md={12} lg={12} className={`${readMode ? 'hide' : ''}`}>
                <hr style={borderColor} />
                <div>* required fields</div>
                <Button
                  id="submit-rfp"
                  className="fright tmargin20"
                  style={primaryColor}
                  onClick={this.handleSubmit}
                >
                  Submit RFP
                </Button>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

SubmitRFP.propTypes = {
  hotelInfo: PropTypes.object.isRequired,
  setReadWriteMode: PropTypes.func.isRequired,
  handleDownloadPDF: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
  setSuccessMessage: PropTypes.func.isRequired,
  setExpander: PropTypes.func.isRequired,
  isSubmitRfp: PropTypes.func.isRequired,
  readOnlyMode: PropTypes.bool.isRequired,
  isDownload: PropTypes.bool.isRequired,
  isClickedSubmitRfp: PropTypes.bool.isRequired,
  setMode: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
};

SubmitRFP.contextTypes = {
  router: PropTypes.object,
};

export default SubmitRFP;
